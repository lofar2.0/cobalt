# $Id$

lofar_add_library(lofar_pytools
        PycExcp.cc
        PycBasicData.cc
)

# Install Python modules
include(PythonInstall)
python_install(__init__.py DESTINATION lofar)


