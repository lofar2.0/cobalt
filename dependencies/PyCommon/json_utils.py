# Copyright (C) 2012-2015  ASTRON (Netherlands Institute for Radio Astronomy)
# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.

import json
import logging
import time
import typing

import jsonschema
from copy import deepcopy
import requests
from datetime import datetime, timedelta
from .util import single_line_with_single_spaces

class JSONError(Exception):
    pass

MAX_SCHEMA_CACHE_AGE = timedelta(minutes=5)
__SCHEMA_CACHE = {}

def remove_from_schema_cache(url: str):
    '''remove the given url (without trailing '/' and/or '#') from the schema cache'''
    url = url.rstrip('/').rstrip('#').rstrip('/')
    if url in __SCHEMA_CACHE:
        del __SCHEMA_CACHE[url]

def clear_schema_cache():
    '''clear the schema cache'''
    __SCHEMA_CACHE.clear()

def _extend_with_default(validator_class):
    """
    Extend the properties validation so that it adds missing properties with their default values (where one is defined
    in the schema).
    traverse down and add enclosed properties.
    see: <https://python-jsonschema.readthedocs.io/en/stable/faq/#why-doesn-t-my-schema-s-default-property-set-the-default-on-my-instance>
    """
    validate_properties = validator_class.VALIDATORS["properties"]

    def set_defaults(validator, properties, instance, schema):
        for property, subschema in properties.items():
            if "default" in subschema:
                instance.setdefault(property, subschema["default"])
            elif "type" not in subschema:
                # could be anything, probably a $ref.
                pass
            elif subschema["type"] == "object":
                # giving objects the {} default causes that default to be populated by the properties of the object
                instance.setdefault(property, {})
            elif subschema["type"] == "array":
                # giving arrays the [] default causes that default to be populated by the items of the array
                instance.setdefault(property, [])

        for error in validate_properties(
            validator, properties, instance, schema,
        ):
            yield error

    return jsonschema.validators.extend(
        validator_class, {"properties" : set_defaults},
    )


def _extend_with_required(validator_class):
    """
    Extend the required properties validation so that it adds missing required properties with their default values,
    (where one is defined in the schema).
    (Note: the check for required properties happens before property validation, so this is required even though the
           override in _extend_with_default would as well add the property.)
    see: <https://python-jsonschema.readthedocs.io/en/stable/faq/#why-doesn-t-my-schema-s-default-property-set-the-default-on-my-instance>
    """
    validate_required = validator_class.VALIDATORS["required"]

    def set_required_properties(validator, properties, instance, schema):
        for property in properties:
            subschema = schema['properties'].get(property, {})
            if "default" in subschema:
                instance.setdefault(property,  subschema["default"])
        for error in validate_required(
            validator, properties, instance, schema,
        ):
            yield error

    return jsonschema.validators.extend(
        validator_class, {"required" : set_required_properties},
    )

# define a custom validator that fills in properties before validation
_DefaultValidatingDraft6Validator = _extend_with_default(jsonschema.Draft6Validator)
_DefaultValidatingDraft6Validator = _extend_with_required(_DefaultValidatingDraft6Validator)


def get_validator_for_schema(schema: dict, add_defaults: bool=False):
    '''get a json validator for the given schema.
    If the schema is already known in the cache by its $id, then the validator from the cached is return.
    This saves many many lookups and ref resolving.
    the 'add_defaults' parameter indicates if we want the validator to add defaults while validating or not.'''
    if isinstance(schema, str):
        schema = json.loads(schema)

    validator = _DefaultValidatingDraft6Validator(schema) if add_defaults else jsonschema.Draft6Validator(schema=schema)
    return validator

def get_default_json_object_for_schema(schema: str) -> dict:
    '''return a valid json object for the given schema with all properties with their default values'''
    return add_defaults_to_json_object_for_schema({}, schema)

def add_defaults_to_json_object_for_schema(json_object: dict, schema: str) -> dict:
    '''return a copy of the json object with defaults filled in according to the schema for all the missing properties'''
    copy_of_json_object = deepcopy(json_object)

    # resolve $refs to fill in defaults for those, too
    schema = resolved_remote_refs(schema)

    # add a $schema to the json doc if needed
    if '$schema' not in copy_of_json_object and '$id' in schema:
        copy_of_json_object['$schema'] = schema['$id']

    # run validator, which populates the properties with defaults.
    get_validator_for_schema(schema, add_defaults=True).validate(copy_of_json_object)
    return deepcopy(copy_of_json_object)

def replace_host_in_urls(schema, new_base_url: str, keys=['$id', '$ref', '$schema']):
    '''return the given schema with all fields in the given keys which start with the given old_base_url updated so they point to the given new_base_url'''
    if isinstance(schema, dict):
        updated_schema = {}
        for key, value in schema.items():
            if key in keys:
                if isinstance(value,str) and (value.startswith('http://') or value.startswith('https://')) and 'json-schema.org' not in value:
                    try:
                        # deconstruct path from old url
                        head, anchor, tail = value.partition('#')
                        host, slash, path = head.lstrip('http://').lstrip('https://').partition('/')

                        # and reconstruct the proper new url
                        updated_schema[key] = (new_base_url.rstrip('/') + '/' + path + anchor + tail.rstrip('/')).replace(' ', '%20')
                    except:
                        # just accept the original value and assume that the user uploaded a proper schema
                        updated_schema[key] = value
                else:
                    updated_schema[key] = value
            else:
                updated_schema[key] = replace_host_in_urls(value, new_base_url, keys)
        return updated_schema

    if isinstance(schema, list):
        return [replace_host_in_urls(item, new_base_url, keys) for item in schema]

    return schema


def get_sub_schema(schema: dict, reference: str, default=None):
    '''resolve a JSON reference (f.e. /definitions/foo) in the schema and return the corresponding subschema.'''
    parts = reference.lstrip('#').strip('/').split('/')

    if parts == ['']:
        # reference to root
        return schema

    try:
        subschema = schema
        for part in parts:
            subschema = subschema[part]
        return subschema
    except KeyError as e:
        return default


def write_at_path(schema: dict, path: str, value):
    '''write the given value (list/dict/plain) at the given (nested) path (f.e. #/definitions/foo/bar) in the schema.'''
    path_parts = path.lstrip('#').strip('/').split('/')
    nested_schema = schema
    for i, path_part in enumerate(path_parts):
        if path_part not in nested_schema:
            if i < len(path_parts) - 1:
                nested_schema[path_part] = {}
            else:
                nested_schema[path_part] = value
        nested_schema = nested_schema[path_part]


def replace_local_refs(schema: dict, path: str):
    ''''''
    if isinstance(schema, list):
        # recurse over each item in the list
        for item in schema:
            replace_local_refs(item, path)

    if isinstance(schema, dict):
        for key, value in list(schema.items()):
            if key == '$ref' and isinstance(value, str) and not value.startswith('http'):
                current_ref_parts = value.lstrip('#').strip('/').split('/')
                path_parts = path.lstrip('#').strip('/').split('/')
                new_ref_parts = []
                for i, (current_part, path_part) in enumerate(zip(current_ref_parts, path_parts)):
                    if current_part==path_part:
                        new_ref_parts.append(path_part)
                    else:
                        new_ref_parts.extend(path_parts[i:])
                        new_ref_parts.extend(current_ref_parts[i:])
                        break

                new_ref_value = '#/'+'/'.join(new_ref_parts)
                schema[key] = new_ref_value

            # recurse over each value in the dict
            replace_local_refs(value, path)


def _fetch_url(url: str) -> str:
    '''try to obtain the provided URL.'''

    # try to fetch the url a few times (jsonschema.org is down quite often, but only for a brief moment)
    for attempt_nr in range(5):
        try:
            response = requests.get(url)
            if response.status_code == 200:
                return response.text
        except requests.exceptions.RequestException as e:
            time.sleep(2) # retry after a little sleep

    raise JSONError("Could not get: %s" % (url,))


def _get_referenced_definition(ref_url):
    '''fetch the schema given by the remote ref_url, and return a tuple of the now-local-ref, and the definition sub-schema'''
    referenced_schema = _get_referenced_schema(ref_url)

    # deduct referred schema name and version from ref-value
    head, anchor, tail = ref_url.partition('#')

    # extract the definition sub-schema
    definition = get_sub_schema(referenced_schema, tail)

    return tail, definition


def _get_referenced_schema(ref_url):
    '''fetch the schema given by the ref_url, and return it'''
    # deduct referred schema name and version from ref-value
    head, anchor, tail = ref_url.partition('#')

    def _fetch_url_and_update_cache_entry_if_needed():
        referenced_schema = json.loads(_fetch_url(ref_url))
        __SCHEMA_CACHE[head] = referenced_schema, datetime.utcnow()

        return referenced_schema

    if head in __SCHEMA_CACHE:
        # use cached value
        referenced_schema, last_update_timestamp = __SCHEMA_CACHE[head]

        # refresh cache if outdated
        if datetime.utcnow() - last_update_timestamp > MAX_SCHEMA_CACHE_AGE:
            referenced_schema = _fetch_url_and_update_cache_entry_if_needed()
        else:
            # return a copy so the schema instance in the cache is not altered from outside
            referenced_schema = deepcopy(referenced_schema)
    else:
        # fetch url, and store in cache
        referenced_schema = _fetch_url_and_update_cache_entry_if_needed()

    return referenced_schema


def ref_resolved_url(url: str) -> str:
    '''get a ref_resolved version of the given url'''
    return url.rstrip('#').rstrip('/').rstrip('/ref_resolved') + '/ref_resolved'


def resolved_remote_refs(schema):
    '''return the given schema with all remote $ref fields (to http://my.server.com/my/schema/#/definitions/...) replaced by the local $ref pointers to #/definitions/...'''
    # make a copy, which can be updated with resolved refs, and then be returned
    copy_of_schema = deepcopy(schema)

    if '$id' in copy_of_schema:
        # alter the original id, and append 'ref_resolved' to it, so it is distinguishable from the orginal non-ref-resolved schema
        copy_of_schema['$id'] = ref_resolved_url(copy_of_schema['$id'])
        clean_id_url = copy_of_schema['$id'].rstrip('/').rstrip('#').rstrip('/')

        if clean_id_url in __SCHEMA_CACHE:
            # return cached ref_resolved schema if not too old
            referenced_schema, last_update_timestamp = __SCHEMA_CACHE[clean_id_url]
            if datetime.utcnow() - last_update_timestamp < MAX_SCHEMA_CACHE_AGE:
                return referenced_schema

    # make sure we have a 'definitions' section
    if 'definitions' not in copy_of_schema:
        copy_of_schema['definitions'] = {}

    # helper function to resolve the refs of the given sub_schema.
    # adds the resoled refs to the root_schema_definitions
    def _recursive_resolved_remote_refs(sub_schema):
        if isinstance(sub_schema, list):
            # recurse over each item in the list
            return [_recursive_resolved_remote_refs(item) for item in sub_schema]

        if isinstance(sub_schema, dict):
            for key in list(sub_schema.keys()):
                # if the key is a remote ref,
                # then fetch the definition
                # and change it into a local definition and ref
                # and store it in the root_schema_definitions
                if key=="$ref" and isinstance(sub_schema['$ref'], str) and sub_schema['$ref'].startswith('http'):
                    # this is a truly remote reference to another schema
                    # resolve remote reference
                    ref_full_url = sub_schema['$ref']

                    # deduct and construct a replacement local_ref for the ref_url
                    schema_url, anchor, local_ref = ref_full_url.partition('#')
                    schema_url = schema_url.rstrip('/')
                    local_ref = '#/'+local_ref.lstrip('/')

                    # replace remote ref by new local_ref
                    sub_schema['$ref'] = local_ref

                    current_definition = get_sub_schema(copy_of_schema, local_ref, None)

                    #  fetch the remote schema...
                    referenced_schema = _get_referenced_schema(schema_url)

                    # recurse, thus resolving the remote refs in the referenced schema
                    referenced_schema = resolved_remote_refs(referenced_schema)

                    resolved_definition = get_sub_schema(referenced_schema, local_ref, None)

                    if current_definition is not None and current_definition != resolved_definition:
                        msg = "ambiguity while resolving remote references in schema $id='%s' $ref='%s' definition1='%s' definition2='%s'" % (schema.get('$id', '<no_id>'), local_ref, single_line_with_single_spaces(current_definition), single_line_with_single_spaces(resolved_definition))
                        raise JSONError(msg)

                    write_at_path(copy_of_schema, local_ref, resolved_definition)

                    for ref in get_refs(referenced_schema):
                        resolved_definition = get_sub_schema(referenced_schema, ref, None)
                        write_at_path(copy_of_schema, ref, resolved_definition)
                else:
                    # key is not a (remote) $ref,
                    # just copy a recursively resolved key/value into the sub_schema
                    sub_schema[key] = _recursive_resolved_remote_refs(sub_schema[key])

        # sub_schema is not a list or dict, so no need to resolve anything, just return it.
        return sub_schema

    # use the recursive helper method to replace the remote refs
    _recursive_resolved_remote_refs(copy_of_schema)

    if '$id' in copy_of_schema:
        # store ref_resolved schema in the cache
        clean_id_url = copy_of_schema['$id'].rstrip('/').rstrip('#').rstrip('/')
        __SCHEMA_CACHE[clean_id_url] = copy_of_schema, datetime.utcnow()

    return copy_of_schema


def resolved_local_refs(schema):
    '''return the given schema with all local $ref fields (to #/definitions/...) replaced by the referred definition that they point to.'''
    # define a helper recursive function
    def _recursive_resolved_local_refs(sub_schema):
        if isinstance(sub_schema, dict):
            resolved_sub_schema = {}
            keys = list(sub_schema.keys())

            # first resolve $kef if present
            if "$ref" in keys and isinstance(sub_schema['$ref'], str):
                ref = sub_schema['$ref']

                if ref.startswith('#/'):
                    # resolve local reference, a-la "#/definitions/foo"
                    definition = get_sub_schema(schema, ref[1:])
                    # resolve the potential refs in the definition as wells
                    resolved_sub_schema = _recursive_resolved_local_refs(definition)

                keys.remove("$ref")

            # then add each other key/value where each value is resolved is well
            for key in keys:
                resolved_sub_schema[key] = _recursive_resolved_local_refs(sub_schema[key])

            return resolved_sub_schema

        if isinstance(sub_schema, list):
            return [_recursive_resolved_local_refs(item) for item in sub_schema]

        return sub_schema

    # recursively resolve the schema
    resolved_schema = _recursive_resolved_local_refs(schema)

    return resolved_schema


def resolved_refs(schema):
    '''return the given schema with all remote AND local $ref fields replaced by their definitions. This also removes the 'definitions' section as it is not needed anymore for local lookups'''
    return resolved_local_refs(resolved_remote_refs(schema))


def get_refs(schema) -> set:
    '''return a set of all $refs in the schema'''
    refs = set()
    if isinstance(schema, dict):
        for key, value in schema.items():
            if key == "$ref" and isinstance(value, str):
                refs.add(value)
            else:
                refs.update(get_refs(value))

    if isinstance(schema, list):
        for item in schema:
            refs.update(get_refs(item))

    return refs


def validate_json_against_its_schema(json_object: dict, add_defaults: bool=False):
    '''validate the give json object against its own schema (the URI/URL that its propery $schema points to)'''
    schema_url = json_object['$schema']
    referenced_schema = _get_referenced_schema(schema_url)
    return validate_json_against_schema(json_object, referenced_schema, add_defaults=add_defaults)


def validate_json_against_schema(json_doc: typing.Union[str,dict], schema: typing.Union[str,dict], add_defaults: bool=False) -> dict:
    '''validate the given json_string against the given schema.
       If no exception if thrown, then the given json_string validates against the given schema.
       :raises SchemaValidationException if the json_string does not validate against the schema
     '''
    try:
        if isinstance(json_doc, str):
            json_doc = json.loads(json_doc)
    except json.decoder.JSONDecodeError as e:
        raise jsonschema.exceptions.ValidationError("Invalid JSON: %s\n%s" % (str(e), json_doc))

    try:
        if isinstance(schema, str):
            schema = json.loads(schema)
    except json.decoder.JSONDecodeError as e:
        raise jsonschema.exceptions.ValidationError("Invalid JSON: %s\n%s" % (str(e), schema))

    # TODO: uncomment, and make sure all production templates are ok.
    # if '$schema' not in json_doc:
    #     raise jsonschema.exceptions.ValidationError("The json document does not contain a $schema property\n%s" % (schema,))
    #
    # if json_doc['$schema'].rstrip('/ref_resolved').rstrip('#').rstrip('/') != schema.get('$id','').rstrip('/ref_resolved').rstrip('#').rstrip('/'):
    #     raise jsonschema.exceptions.ValidationError("The json document with $schema='%s' cannot be validated by schema with $id='%s'" % (json_doc['$schema'], schema.get('$id')))

    # resolve $refs to fill in defaults for those, too
    schema = resolved_remote_refs(schema)

    # now do the actual validation
    try:
        return validate_json_object_with_schema(json_doc, schema, add_defaults=add_defaults)
    except jsonschema.ValidationError as e:
        raise jsonschema.exceptions.ValidationError(str(e))


def get_default_json_object_for_schema(schema: str) -> dict:
    """
    TMSS wrapper for TMSS 'add_defaults_to_json_object_for_schema'
    :param schema:
    :return: json_object with default values of the schema
    """
    data = add_defaults_to_json_object_for_schema({}, schema)
    if '$id' in schema:
        data['$schema'] = schema['$id']
    return data

def raise_on_self_refs(schema: dict):
    '''raise if the given schema contains any (remote/http) reference to itself'''
    id = schema.get('$id','')
    if id.startswith('http'):
        # remove any trailing slashes or hashes
        id = id.rstrip('/').rstrip('#').rstrip('/')
        for ref in get_refs(schema):
            if ref.startswith(id):
                raise JSONError("schema $id='%s' contains a $ref to itself: '%s'" %(id, ref))


def validate_json_object_with_schema(json_object, schema, add_defaults: bool=False) -> dict:
    """
    Validate the given json_object with schema
    """
    if '$schema' in  json_object and '$id' in schema:
        object_schema_id = json_object['$schema'].partition('#')[0].rstrip('/').rstrip('ref_resolved').rstrip('/')
        schema_id = schema['$id'].partition('#')[0].rstrip('/').rstrip('ref_resolved').rstrip('/')
        if object_schema_id != schema_id:
            raise JSONError("json_object refers to different $schema='%s' and cannot be compared to the schema with $id='%s'" % (object_schema_id, schema_id))

    get_validator_for_schema(schema, add_defaults=add_defaults).validate(json_object)
    return json_object


def without_schema_property(json_doc: dict) -> dict:
    '''returns a copy of the json_doc without the $schema property if present'''
    json_doc_copy = deepcopy(json_doc)
    try:
        del json_doc_copy['$schema']
    except KeyError:
        pass
    return json_doc_copy
