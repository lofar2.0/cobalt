#!/usr/bin/env python3

# Copyright (C) 2012-2015  ASTRON (Netherlands Institute for Radio Astronomy)
# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.

# $Id$

'''
common abstract database connection class
'''

import logging
from datetime import  datetime, timedelta
import collections
import time
import re
from lofar.common.util import single_line_with_single_spaces
from lofar.common.dbcredentials import DBCredentials

logger = logging.getLogger(__name__)

FETCH_NONE=0
FETCH_ONE=1
FETCH_ALL=2

class DatabaseError(Exception):
    pass

class DatabaseConnectionError(DatabaseError):
    pass

class DatabaseExecutionError(DatabaseError):
    pass

class AbstractDatabaseConnection:
    '''Abstract DatabaseConnection class defining a uniform class API for lofar database connections.'''
    def __init__(self,
                 dbcreds: DBCredentials,
                 auto_commit_selects: bool=False,
                 num_connect_retries: int=5,
                 connect_retry_interval: float=1.0,
                 query_timeout: float=3600):
        self._dbcreds = dbcreds
        self._connection = None
        self._cursor = None
        self.__auto_commit_selects = auto_commit_selects
        self.__num_connect_retries = num_connect_retries
        self.__connect_retry_interval = connect_retry_interval
        self.__query_timeout = query_timeout

    def connect_if_needed(self):
        if not self.is_connected:
            self.connect()

    def connect(self):
        if self.is_connected:
            logger.debug("already connected to database: %s", self)
            return

        for retry_cntr in range(self.__num_connect_retries+1):
            try:
                logger.debug("connecting to database: %s", self)

                # let the subclass create the connection and cursor.
                # handle connection errors here.
                self._connection, self._cursor = self._do_create_connection_and_cursor()
                logger.info("connected to database: %s", self)
                # we have a proper connection, so return
                return
            except Exception as error:
                error_string = 'could not connect to %s. error: %s' % (self, single_line_with_single_spaces(error))
                logger.error(error_string)

                if self._is_recoverable_connection_error(error):
                    # try to reconnect on connection-like-errors
                    if retry_cntr == self.__num_connect_retries:
                        raise DatabaseConnectionError(error_string)

                    logger.info('retrying to connect to %s in %s seconds', self.database, self.__connect_retry_interval)
                    time.sleep(self.__connect_retry_interval)
                else:
                    # non-connection-error, raise generic DatabaseError
                    raise DatabaseError(error_string)

    def disconnect(self):
        if self._connection is not None or self._cursor is not None:
            logger.debug("disconnecting from database: %s", self)

            if self._cursor is not None:
                self._cursor.close()
                self._cursor = None

            if self._connection is not None:
                self._connection.close()
                self._connection = None

            logger.info("disconnected from database: %s", self)

    def _is_recoverable_connection_error(self, error: Exception) -> bool:
        return False

    def __str__(self) -> str:
        '''returns the class name and connection string with hidden password.'''
        return "%s %s" % (self.__class__.__name__, self._dbcreds.stringWithHiddenPassword())

    @property
    def database(self) -> str:
        '''returns the database name'''
        return self._dbcreds.database

    @property
    def dbcreds(self) -> DBCredentials:
        '''returns the database credentials'''
        return self._dbcreds

    @property
    def is_connected(self) -> bool:
        return self._connection is not None

    def reconnect(self):
        logger.info("reconnecting %s", self)
        self.disconnect()
        self.connect()

    def __enter__(self):
        '''connects to the database'''
        try:
            self.connect()
        except:
            self.disconnect()
            raise
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        '''disconnects from the database'''
        self.disconnect()

    @staticmethod
    def _queryAsSingleLine(query, qargs=None):
        line = ' '.join(single_line_with_single_spaces(query).split())
        if qargs:
            line = line % tuple(['\'%s\'' % a if isinstance(a, str) else a for a in qargs])
        return line

    def executeQuery(self, query, qargs=None, fetch=FETCH_NONE):
        start = datetime.utcnow()
        while True:
            try:
                return self._do_execute_query(query, qargs, fetch)
            except DatabaseConnectionError as e:
                logger.warning(e)
                if datetime.utcnow() - start < timedelta(seconds=self.__query_timeout):
                    try:
                        # reconnect, log retrying..., and do the retry in the next loop iteration
                        self.reconnect()
                        logger.info("retrying %s", self._queryAsSingleLine(query, qargs))
                    except DatabaseConnectionError as ce:
                        logger.warning(ce)
                else:
                    raise
            except Exception as error:
                self._log_error_rollback_and_raise(error, self._queryAsSingleLine(query, qargs))

    def _do_execute_query(self, query, qargs=None, fetch=FETCH_NONE):
        raise NotImplementedError()

    def _log_error_rollback_and_raise(self, e: Exception, query_log_line: str):
        error_string = single_line_with_single_spaces(e)
        logger.error("Rolling back query=\'%s\' due to error: \'%s\'" % (query_log_line, error_string))
        self.rollback()

        # wrap original error in DatabaseExecutionError and raise
        raise DatabaseExecutionError("Could not execute query '%s' error=%s" % (query_log_line, error_string))

    def _commit_selects_if_needed(self, query):
        if self.__auto_commit_selects and re.search('select', query, re.IGNORECASE):
            # prevent dangling in idle transaction on server
            self.commit()

    def commit(self):
        if self.is_connected:
            logger.debug('commit')
            self._connection.commit()

    def rollback(self):
        if self.is_connected:
            logger.debug('rollback')
            self._connection.rollback()

