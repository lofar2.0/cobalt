#!/usr/bin/env python3

from lofar.common.test_utils import exit_with_skipped_code_if_skip_unit_tests
exit_with_skipped_code_if_skip_unit_tests()

import os
import unittest
import tempfile
from uuid import uuid4
from lofar.common.dbcredentials import *
from configparser import NoSectionError
from lofar.common.test_utils import unit_test

def setUpModule():
  pass

def tearDownModule():
  pass

class TestCredentials(unittest.TestCase):

  @unit_test
  def test_default_values(self):
    c = Credentials()

    self.assertEqual(c.type, "postgres")
    self.assertEqual(c.host, "localhost")
    self.assertEqual(c.port, 0)
    #self.assertEqual(c.user, "")
    self.assertEqual(c.password, "")
    self.assertEqual(c.database, "")

  @unit_test
  def test_pg_connect_options(self):
    c = Credentials()

    self.assertEqual(
      c.pg_connect_options(),
      { "host": "localhost",
        "port": -1,
        "user": c.user,
        "passwd": "",
        "dbname": "",
      })


class TestDBCredentials(unittest.TestCase):

  @unit_test
  def test_set_get(self):
    dbc = DBCredentials(filepatterns=[])

    c_in = Credentials()
    c_in.host = "example.com"
    c_in.port = 1234
    c_in.user = "root"
    c_in.password = "secret"
    c_in.database = "mydb"

    dbc.set("DATABASE", c_in)
    c_out = dbc.get("DATABASE")

    self.assertEqual(str(c_out), str(c_in))

  @unit_test
  def test_get_non_existing(self):
    dbc = DBCredentials(filepatterns=[])

    non_existing_db_name = "UNKNOWN-%s" % (uuid4(),)
    with self.assertRaises(NoSectionError):
      dbc.get(non_existing_db_name)

  @unit_test
  def test_creation_for_non_existing(self):
    dbc = DBCredentials(filepatterns=["{HOME}/.lofar/dbcredentials/*.test_extension"])

    non_existing_db_name = "UNKNOWN-%s" % (uuid4(),)
    creds = dbc.get(non_existing_db_name)
    self.assertTrue(creds)
    self.assertEqual(non_existing_db_name, creds.database)

    expected_path = os.path.expanduser("~/.lofar/dbcredentials/%s.test_extension" % (non_existing_db_name,))
    self.assertTrue(os.path.exists(expected_path))
    os.remove(expected_path)

  @unit_test
  def test_list(self):
    dbc = DBCredentials(filepatterns=[])

    c = Credentials()
    c.host = "foo"
    dbc.set("FOO", c)

    c = Credentials()
    c.host = "bar"
    dbc.set("BAR", c)

    self.assertEqual(sorted(dbc.list()), ["BAR", "FOO"])

  @unit_test
  def test_config(self):
    f = tempfile.NamedTemporaryFile()
    f.write(b"""
[database:DATABASE]
type = postgres
host = example.com
port = 1234
user = root
password = secret
database = mydb
""")
    f.flush() # don't close since that will delete the TemporaryFile

    # test if DATABASE is there
    dbc = DBCredentials(filepatterns=[f.name])
    self.assertEqual(dbc.list(), ["DATABASE"])

    # test if credentials match with what we've written
    c_in = Credentials()
    c_in.host = "example.com"
    c_in.port = 1234
    c_in.user = "root"
    c_in.password = "secret"
    c_in.database = "mydb"

    c_out = dbc.get("DATABASE")

    self.assertEqual(str(c_out), str(c_in))

  @unit_test
  def test_freeform_config_option(self):
    f = tempfile.NamedTemporaryFile()
    f.write(b"""
[database:DATABASE]
foo = bar
test = word word
""")
    f.flush() # don't close since that will delete the TemporaryFile

    # extract our config
    dbc = DBCredentials(filepatterns=[f.name])
    c_out = dbc.get("DATABASE")

    # test if the free-form config options got through
    self.assertEqual(c_out.config["foo"], "bar")
    self.assertEqual(c_out.config["test"], "word word")

def main(argv):
  unittest.main()

if __name__ == "__main__":
  # run all tests
  import sys
  main(sys.argv[1:])
