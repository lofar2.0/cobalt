#!/usr/bin/env python3

from lofar.common.test_utils import exit_with_skipped_code_if_skip_unit_tests
exit_with_skipped_code_if_skip_unit_tests()

import unittest
import tempfile
from lofar.common.util import *
from lofar.common.test_utils import unit_test

def setUpModule():
    pass

def tearDownModule():
    pass


@unit_test
class TestUtils(unittest.TestCase):

    def test_is_iterable(self):
        #list
        self.assertTrue(is_iterable([]))
        self.assertTrue(is_iterable([1, 2, 3]))

        #dict
        self.assertTrue(is_iterable({}))
        self.assertTrue(is_iterable({1:2, 3:4}))

        #tuple
        self.assertTrue(is_iterable((1,2,4)))

        #string
        self.assertTrue(is_iterable("abc"))

        # non-iterale types
        self.assertFalse(is_iterable(1))
        self.assertFalse(is_iterable(None))

    def test_merge_nested_dicts(self):
        dict_a = {'a': 1, 
                  'b': {
                      'c': 3, 
                      'd': [4, 5, 6]}, 
                  'e': [{'foo': 1}, 
                        {'bar': 2}]}

        dict_b = {'a': 2}
        merged = dict_with_overrides(dict_a, dict_b)
        self.assertEqual({'a': 2, 'b': {'c': 3, 'd': [4, 5, 6]}, 'e': [{'foo': 1}, {'bar': 2}]}, merged)

        dict_b = {'a': 1, 'b': 2}
        merged = dict_with_overrides(dict_a, dict_b)
        self.assertEqual({'a': 1, 'b': 2, 'e': [{'foo': 1}, {'bar': 2}]}, merged)

        dict_b = {'b': {'c': 4}}
        merged = dict_with_overrides(dict_a, dict_b)
        self.assertEqual({'a': 1, 'b': {'c': 4, 'd': [4, 5, 6]}, 'e': [{'foo': 1}, {'bar': 2}]}, merged)

        dict_b = {'e': [{'foo': 2}, {'bar': 3}]}
        merged = dict_with_overrides(dict_a, dict_b)
        self.assertEqual({'a': 1, 'b': {'c': 3, 'd': [4, 5, 6]}, 'e': [{'foo': 2}, {'bar': 3}]}, merged)

def main(argv):
    unittest.main()

if __name__ == "__main__":
    # run all tests
    import sys
    main(sys.argv[1:])
