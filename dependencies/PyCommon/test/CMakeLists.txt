# $Id$

IF(BUILD_TESTING)
    lofar_find_package(Python 3.4 REQUIRED)

    include(PythonInstall)

    set(_py_files
      __init__.py
      postgres.py
      dbcredentials.py)

    python_install(${_py_files} DESTINATION lofar/common/testing)

    include(FindPythonModule)
    find_python_module(testing.postgresql)

    include(LofarCTest)

    configure_file(
      "${CMAKE_CURRENT_SOURCE_DIR}/python-coverage.sh.in"
      "${CMAKE_BINARY_DIR}/bin/python-coverage.sh"  # internal, no need to install
    )

    lofar_add_test(t_cache)
    lofar_add_test(t_cobaltblocksize)
    lofar_add_test(t_dbcredentials)
    lofar_add_test(t_defaultmailaddresses)
    lofar_add_test(t_methodtrigger)
    lofar_add_test(t_util)
    lofar_add_test(t_test_utils)
    lofar_add_test(t_cep4_utils)
    lofar_add_test(t_toposort)
    lofar_add_test(t_typing)

    IF(PYTHON_JSONSCHEMA)
        lofar_add_test(t_json_utils)
    ENDIF()
ENDIF()
