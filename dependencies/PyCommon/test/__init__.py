from lofar.common import isProductionEnvironment

if isProductionEnvironment():
    print("The test modules in %s should not be imported and used in a lofar production environment" % __package__)
    exit(1)