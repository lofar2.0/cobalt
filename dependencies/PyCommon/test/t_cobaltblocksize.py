import unittest
from lofar.common.cobaltblocksize import StokesSettings, CorrelatorSettings, BlockConstraints, BlockSize
import logging
from lofar.common.test_utils import unit_test

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

class TestBlockConstraints(unittest.TestCase):

    @unit_test
    def test_blockSizes(self):
        """ Test whether minBlockSize <= idealBlockSize <= maxBlockSize. """

        for clockMHz in [160, 200]:
            c = BlockConstraints(clockMHz=clockMHz)

            self.assertLessEqual(c.minBlockSize(), c.idealBlockSize())
            self.assertLessEqual(c.idealBlockSize(), c.maxBlockSize())

    @unit_test
    def testCorrelator(self):
        """ Test basic constraints for the Correlator. """

        corr = CorrelatorSettings()
        corr.nrChannelsPerSubband = 64
        corr.integrationTime = 1.0

        c = BlockConstraints(correlatorSettings=corr)

        self.assertEqual(c.nrSubblocks(), 1)
        self.assertGreaterEqual(c.factor(), 1)

    @unit_test
    def testCorrelatorSubblocks(self):
        """ Test basic constraints for the Correlator if there are subblocks. """

        corr = CorrelatorSettings()
        corr.nrChannelsPerSubband = 64
        corr.integrationTime = 0.1

        c = BlockConstraints(correlatorSettings=corr)

        self.assertGreater(c.nrSubblocks(), 1)
        self.assertGreaterEqual(c.factor(), 1)

        # Set of subblocks needs to be valid
        self.assertGreater(c.nrSubblocks() * c._time2samples(corr.integrationTime), c.minBlockSize())
        self.assertLess(c.nrSubblocks() * c._time2samples(corr.integrationTime), c.maxBlockSize())

    @unit_test
    def testCoherentStokes(self):
        """ Test basic constraints for the Coherent beamformer. """

        coh = StokesSettings()
        coh.nrChannelsPerSubband = 16
        coh.timeIntegrationFactor = 4

        c = BlockConstraints(coherentStokesSettings=[coh])

        self.assertEqual(c.nrSubblocks(), 1)
        self.assertGreaterEqual(c.factor(), 1)

    @unit_test
    def testIncoherentStokes(self):
        """ Test basic constraints for the Incoherent beamformer. """

        incoh = StokesSettings()
        incoh.nrChannelsPerSubband = 16
        incoh.timeIntegrationFactor = 4

        c = BlockConstraints(incoherentStokesSettings=[incoh])

        self.assertEqual(c.nrSubblocks(), 1)
        self.assertGreaterEqual(c.factor(), 1)

class TestBlockSize(unittest.TestCase):

    @unit_test
    def testCorrelatorIntegrationTime(self):
        """ Test whether specified integration times 0.05 - 30.0 result in integration times with an error <5%. """

        def integrationTimes():
            i = 0.05
            while i < 10.0:
                yield i
                i += 0.05
            while i < 30.0:
                yield i
                i += 0.5

        for integrationTime in integrationTimes():
            correlator = CorrelatorSettings()
            correlator.nrChannelsPerSubband = 64
            correlator.integrationTime = integrationTime

            c = BlockConstraints(correlator)
            bs = BlockSize(c)

            self.assertAlmostEqual(c._samples2time(bs.integrationSamples), integrationTime, delta = integrationTime * 0.05)

    @unit_test
    def testCoherentStokesBlocksize(self):
        """ Test the coherent stokes block size against reference output, based on cases that used to fail in production.
            If the output of these calculations change, make sure the described configurations do actually work in COBALT! """

        coh = StokesSettings()
        coh.nrChannelsPerSubband = 16
        coh.timeIntegrationFactor = 1

        c = BlockConstraints(coherentStokesSettings=[coh])
        bs = BlockSize(c)

        self.assertEqual(bs.blockSize, 196608)

    @unit_test
    def testFlysEyeHighIntegration(self):
        """ Test whether we can run Fly's Eye with high integration settings. See TMSS-1045. """

        coh = StokesSettings()
        coh.nrChannelsPerSubband = 16
        coh.timeIntegrationFactor = 128

        c = BlockConstraints(coherentStokesSettings=[coh])
        bs = BlockSize(c)

        # we're happy if a valid block size was found
        self.assertNotEqual(bs.blockSize, None)


if __name__ == "__main__":
    unittest.main(verbosity=2)
