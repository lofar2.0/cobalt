#!/usr/bin/env python3

# Copyright (C) 2012-2015  ASTRON (Netherlands Institute for Radio Astronomy)
# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.

import logging
logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s %(process)s %(threadName)s %(levelname)s %(message)s', level=logging.DEBUG)

from lofar.common.typing import check_type_hints

import unittest

class TestCheckTypeHints(unittest.TestCase):
    def test_no_argument(self):
        """ Elementary test for the type hint of the return type. """

        @check_type_hints
        def myfunc() -> str:
            return "ok"

        self.assertEqual("ok", myfunc())

    def test_one_argument(self):
        """ Elementary test for one argument with a type hint. """

        @check_type_hints
        def myfunc(i: int) -> str:
            return str(i)

        self.assertEqual("1", myfunc(1))

        with self.assertRaises(TypeError):
            myfunc("1")

        with self.assertRaises(TypeError):
            myfunc(i="1")

    def test_argument_default(self):
        """ Check whether argument defaults still function correctly. """

        @check_type_hints
        def myfunc(i: int = 1) -> str:
            return str(i)

        self.assertEqual("1", myfunc())

    def test_multiple_arguments(self):
        """ Check whether multiple arguments are handled correctly with various calling conventions. """

        @check_type_hints
        def myfunc(i: int, j:int) -> str:
            return "%d %d" % (i,j)

        self.assertEqual("1 2", myfunc(1,2))
        self.assertEqual("1 2", myfunc(1,j=2))
        self.assertEqual("1 2", myfunc(i=1,j=2))

        with self.assertRaises(TypeError):
            myfunc("1",2)

        with self.assertRaises(TypeError):
            myfunc(1,"2")

        with self.assertRaises(TypeError):
            myfunc(1, j="2")

        with self.assertRaises(TypeError):
            myfunc(i="1", j=2)

    def test_wrong_return_value(self):
        """ Check whether return values are validated. """

        @check_type_hints
        def myfunc(i: int) -> str:
            return i

        with self.assertRaises(TypeError):
            myfunc(1)

    def test_inheritance(self):
        """ Provided values can also be subclasses of the types provided in the hints. """

        @check_type_hints
        def myfunc(i: int) -> int:
            return i

        class DerivedInt(int):
            pass

        myfunc(DerivedInt(1))

    def test_no_hints(self):
        """ Functions without any hints should always work. """

        @check_type_hints
        def myfunc(i):
            return str(i)

        self.assertEqual("1", myfunc(1))
        self.assertEqual("1", myfunc("1"))

    def test_some_hints(self):
        """ Not all parameters are necessarily annotated. """

        @check_type_hints
        def myfunc(i, j: int):
            return str(i)

        self.assertEqual("1", myfunc(1, 2))
        self.assertEqual("1", myfunc("1", 2))

        with self.assertRaises(TypeError):
            self.assertEqual("1", myfunc("1", "2"))

    def test_union_hint(self):
        """ Python allows supplying multiple types as a list, any of which is valid. """

        @check_type_hints
        def myfunc(i: [int, str]):
            return str(i)

        self.assertEqual("1", myfunc(1))
        self.assertEqual("1", myfunc("1"))

        with self.assertRaises(TypeError):
            self.assertEqual("1", myfunc(1.0))

    def test_args_kwargs(self):
        """ Check whether args & kwargs don't break. """

        @check_type_hints
        def myfunc(*args, **kwargs):
            return str(kwargs["i"])

        self.assertEqual("1", myfunc(i=1))
        self.assertEqual("1", myfunc(i="1"))


    def test_asterics(self):
        """ Check whether forced named arguments don't break. """

        @check_type_hints
        def myfunc(*, i: int):
            return str(i)

        self.assertEqual("1", myfunc(i=1))

        with self.assertRaises(TypeError):
            self.assertEqual("1", myfunc(i="1"))

    def test_none(self):
        """ Check whether None as an argument functions correctly. """

        @check_type_hints
        def myfunc(i: int) -> str:
            return str(i)

        with self.assertRaises(TypeError):
            myfunc(None)

if __name__ == "__main__":
    unittest.main()
