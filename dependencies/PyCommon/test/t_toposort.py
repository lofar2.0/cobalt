import unittest
from lofar.common.toposort import Graph, toposorted
from lofar.common.test_utils import unit_test


class TestGraph(unittest.TestCase):
    @unit_test
    def test_edgeless(self):
        g = Graph([0,1,2])

        self.assertListEqual([0,1,2], sorted(g.vertices))

    @unit_test
    def test_addvertex(self):
        g = Graph([0,2])
        g.add_vertex(1)

        self.assertListEqual([0,1,2], sorted(g.vertices))

    @unit_test
    def test_addedge(self):
        g = Graph([0,1,2,3])
        g.add_edge(0,2)
        g.add_edge(1,3)

        self.assertListEqual([0,1,2,3], sorted(g.vertices))
        self.assertListEqual([2], g.edges[0])
        self.assertListEqual([3], g.edges[1])
        self.assertListEqual([], g.edges[2])
        self.assertListEqual([], g.edges[3])

    @unit_test
    def test_addsuccessors(self):
        g = Graph([0,1,2,3])
        g.add_successors(0,[2,3])

        self.assertListEqual([0,1,2,3], sorted(g.vertices))
        self.assertListEqual([2,3], g.edges[0])
        self.assertListEqual([], g.edges[1])
        self.assertListEqual([], g.edges[2])
        self.assertListEqual([], g.edges[3])

    @unit_test
    def test_addprecessors(self):
        g = Graph([0,1,2,3])
        g.add_predecessors([2,3], 0)

        self.assertListEqual([0,1,2,3], sorted(g.vertices))
        self.assertListEqual([], g.edges[0])
        self.assertListEqual([], g.edges[1])
        self.assertListEqual([0], g.edges[2])
        self.assertListEqual([0], g.edges[3])

    @unit_test
    def test_successorkey(self):
        g = Graph([0,1,2,3])
        successors = { 0: [2, 3], 1: [], 2: [], 3: [] }
        g.successor_key(lambda v: successors[v])

        self.assertListEqual([0,1,2,3], sorted(g.vertices))
        self.assertListEqual([2,3], g.edges[0])
        self.assertListEqual([], g.edges[1])
        self.assertListEqual([], g.edges[2])
        self.assertListEqual([], g.edges[3])

    @unit_test
    def test_successorkey_accepts_supergraph(self):
        """ Test whether the successor_key function accept edges to nodes not in the graph. """

        g = Graph([0,1,2,3])
        successors = { 0: [2, 3, 4], 1: [5], 2: [], 3: [] }
        g.successor_key(lambda v: successors[v])

        self.assertListEqual([0,1,2,3], sorted(g.vertices))
        self.assertListEqual([2,3], g.edges[0])
        self.assertListEqual([], g.edges[1])
        self.assertListEqual([], g.edges[2])
        self.assertListEqual([], g.edges[3])

    @unit_test
    def test_predecessorkey(self):
        g = Graph([0,1,2,3])
        predecessors = { 0: [2, 3], 1: [], 2: [], 3: [] }
        g.predecessor_key(lambda v: predecessors[v])

        self.assertListEqual([0,1,2,3], sorted(g.vertices))
        self.assertListEqual([], g.edges[0])
        self.assertListEqual([], g.edges[1])
        self.assertListEqual([0], g.edges[2])
        self.assertListEqual([0], g.edges[3])

    @unit_test
    def test_predecessorkey_accepts_supergraph(self):
        """ Test whether the predecessor_key function accept edges to nodes not in the graph. """

        g = Graph([0,1,2,3])
        predecessors = { 0: [2, 3, 4], 1: [5], 2: [], 3: [] }
        g.predecessor_key(lambda v: predecessors[v])

        self.assertListEqual([0,1,2,3], sorted(g.vertices))
        self.assertListEqual([], g.edges[0])
        self.assertListEqual([], g.edges[1])
        self.assertListEqual([0], g.edges[2])
        self.assertListEqual([0], g.edges[3])

class TestToposort(unittest.TestCase):
    def assertTopoSorted(self, g: Graph, vlist: list) -> bool:
        """ Asserts whether the list of vertices represents a sorted set of vertices of g. """

        # check whether both inputs describe the same vertex list using total ordering
        self.assertListEqual(sorted(g.vertices), sorted(vlist))

        # check whether all edges are respected
        for src, dstlist in g.edges.items():
            for dst in dstlist:
                # src must appear in the list before dst
                self.assertLessEqual(vlist.index(src), vlist.index(dst))

    @unit_test
    def test_empty_set(self):
        g = Graph([])
        result = toposorted(g)

        self.assertListEqual([], result)

    @unit_test
    def test_one_vertex(self):
        g = Graph([0])
        result = toposorted(g)

        self.assertListEqual([0], result)

    @unit_test
    def test_two_linked_vertices(self):
        g = Graph([0,1])
        g.add_edge(0, 1)
        result = toposorted(g)

        self.assertListEqual([0, 1], result)

    @unit_test
    def test_reverse_order(self):
        # 5 -> 4 -> 3 -> 2 -> 1 -> 0
        g = Graph([0,1,2,3,4,5])
        g.add_edge(5, 4)
        g.add_edge(4, 3)
        g.add_edge(3, 2)
        g.add_edge(2, 1)
        g.add_edge(1, 0)

        result = toposorted(g)
        self.assertTopoSorted(g, result)

    @unit_test
    def test_disconnected_graph(self):
        # 5 -> 4 -> 3 
        # 2 -> 1 -> 0
        g = Graph([0,1,2,3,4,5])
        g.add_edge(5, 4)
        g.add_edge(4, 3)
        g.add_edge(2, 1)
        g.add_edge(1, 0)

        result = toposorted(g)
        self.assertTopoSorted(g, result)

    @unit_test
    def test_graph(self):
        # 5 -> 0
        # 5 -> 2 -> 3 -> 1
        # 4 -> 0
        g = Graph([0,1,2,3,4,5])
        g.add_edge(5, 0)
        g.add_edge(5, 2)
        g.add_edge(4, 0)
        g.add_edge(2, 3)
        g.add_edge(3, 1)

        result = toposorted(g)
        self.assertTopoSorted(g, result)

    @unit_test
    def test_hba_strategy(self):
        g = Graph(["cal_obs1", "target_obs", "cal_obs2", "cal_preproc1", "target_preproc", "cal_preproc2", "ingest", "cleanup"])
        # process each observation with its own pipeline
        g.add_edge("cal_obs1", "cal_preproc1")
        g.add_edge("target_obs", "target_preproc")
        g.add_edge("cal_obs2", "cal_preproc2")

        # ingest the pipeline output
        g.add_edge("cal_preproc1", "ingest")
        g.add_edge("target_preproc", "ingest")
        g.add_edge("cal_preproc2", "ingest")

        # cleanup everything
        g.add_edge("cal_obs1", "cleanup")
        g.add_edge("target_obs", "cleanup")
        g.add_edge("cal_obs2", "cleanup")
        g.add_edge("cal_preproc1", "cleanup")
        g.add_edge("target_preproc", "cleanup")
        g.add_edge("cal_preproc2", "cleanup")
        g.add_edge("ingest", "cleanup")

        result = toposorted(g)
        self.assertTopoSorted(g, result)

def main(argv):
    unittest.main()

if __name__ == "__main__":
    # run all tests
    import sys
    main(sys.argv[1:])

