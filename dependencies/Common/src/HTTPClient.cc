//  HTTPClient.cpp: A very simple http client
//
//  Copyright (C) 2020
//  ASTRON (Netherlands Foundation for Research in Astronomy)
//  P.O.Box 2, 7990 AA Dwingeloo, The Netherlands, softwaresupport@astron.nl
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//  $Id$

#include <lofar_config.h>
#include <Common/LofarLogger.h>

#include <Common/HTTPClient.h>

#include <boost/algorithm/string.hpp>
#include <cstdlib>
#include <iostream>
#include <string>
#include <curl/curl.h>


namespace LOFAR {

using namespace std;

//
// HTTPClient Constructor
//
HTTPClient::HTTPClient(const std::string &hostname, int port, const std::string &username, const std::string &password):
	itsUser(username),
	itsPassword(password),
	itsHost(hostname),
	itsPort(port)
{
}

//
// HTTPClient Destructor
//
HTTPClient::~HTTPClient()
{

}

std::size_t callback(const char* in,
                     std::size_t size,
                     std::size_t num,
                     std::string* out)
{
    const std::size_t totalBytes(size * num);
    out->append(in, totalBytes);
    return totalBytes;
}


//
// Performs an HTTP query and return the response body
// Need to check response status code of http (200)
// Inspired by https://gist.github.com/connormanning/41efa6075515019e499c
// Example:
//    httpQuery("/api/subtask/?scheduled_start_time__lt=2020-03-04T12:03:00")
//    results in a json string output
//
bool HTTPClient::httpQuery(const string& target, string &result, const string& query_method, const string& data, bool content_type_json)
{
    const std::string scheme = itsPort==443 ? std::string("https://") : std::string("http://");
    const std::string port_postfix = itsPort==80 || itsPort==443 ? std::string("") : std::string(":") + std::to_string(itsPort);
    const std::string url(scheme + itsHost + port_postfix + target);

    CURL* curl = curl_easy_init();

    // setup authentication
    curl_easy_setopt(curl, CURLOPT_USERNAME, itsUser.c_str());
    curl_easy_setopt(curl, CURLOPT_PASSWORD, itsPassword.c_str());
    curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

    // Set remote URL.
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_USERAGENT, "HTTPClient using libcurl");

    LOG_DEBUG_STR("[" << query_method << "] url=" << url << " data=" << data);

    // Set HTTP method
    if (query_method == "GET")
    {
        curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);
    }
    else if (query_method == "POST" || query_method == "PUT" || query_method == "PATCH" )
    {
        if (query_method == "PUT" || query_method == "PATCH" ) {
            curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, query_method.c_str());
        }

        curl_easy_setopt(curl, CURLOPT_POST, 1L);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data.c_str());
    }

    if (content_type_json) {
        struct curl_slist *headers = NULL;
        headers = curl_slist_append(headers, "Expect:");
        headers = curl_slist_append(headers, "Content-Type: application/json");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    }

    // Don't bother trying IPv6, which would increase DNS resolution time.
    curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);

    // Don't wait forever, time out after 10 seconds.
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);

    // Follow HTTP redirects if necessary.
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);


    // Response information.
    long httpCode(0);
    std::unique_ptr<std::string> httpData(new std::string());

    // Hook up data handling function.
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callback);

    // Hook up data container (will be passed as the last parameter to the
    // callback handling function).  Can be any pointer type, since it will
    // internally be passed as a void pointer.
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, httpData.get());

    // Run our HTTP GET command, capture the HTTP response code, and clean up.
    curl_easy_perform(curl);
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
    result = string(*httpData.get());

    // cleanup
    curl_easy_cleanup(curl);
    curl_global_cleanup();

    LOG_INFO_STR(string("[") << query_method << "] code=" << httpCode << " " << url);

    if (httpCode >= 200L && httpCode < 300L) {
        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Status#successful_responses
        return true;
    }

    LOG_ERROR_STR(string("Could not ") << query_method << " " << url << " code=" << to_string(httpCode) << " result: " << result);
    return false;
}

};//LOFAR
