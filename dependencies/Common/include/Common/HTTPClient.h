//  HTTPClient.h: A very simple http client
//
//  Copyright (C) 2020
//  ASTRON (Netherlands Foundation for Research in Astronomy)
//  P.O.Box 2, 7990 AA Dwingeloo, The Netherlands, softwaresupport@astron.nl
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
//  $Id$

#ifndef HTTPCLIENT_H
#define HTTPCLIENT_H

#include <Common/LofarTypes.h>

namespace LOFAR {

class HTTPClient
{
public:
    HTTPClient (const std::string &hostname, int port, const std::string &username, const std::string &password);
    ~HTTPClient ();

    bool httpQuery(const std::string& target, std::string &result, const std::string& query_method="GET", const std::string& data="", bool content_type_json=false);

private:
    // Copying is not allowed
    HTTPClient(const HTTPClient&);
    HTTPClient& operator=(const HTTPClient&);

    std::string    itsUser;
    std::string    itsPassword;
    std::string    itsHost;
    int            itsPort;
};

};//LOFAR
#endif
