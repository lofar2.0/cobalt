#ifndef LOFAR_COMMON_COMPLEX_H
#define LOFAR_COMMON_COMPLEX_H

#include <complex>
#include <cstdint>
#include <Common/i4complex.h>

namespace LOFAR {
  typedef std::complex<float> fcomplex;
  typedef std::complex<double> dcomplex;

  typedef std::complex<int8_t>  i8complex;
  typedef std::complex<int16_t> i16complex;

  using LOFAR::TYPES::i4complex;
}

#endif
