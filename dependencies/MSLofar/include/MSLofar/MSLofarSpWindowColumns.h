//# MSLofarSpWindowColumns.h: provides easy access to LOFAR's Spectral Window columns
//# Copyright (C) 2021
//# ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$
//#
//# @author Jan David Mol

#ifndef MSLOFAR_MSLOFARSPECTRALWINDOWCOLUMNS_H
#define MSLOFAR_MSLOFARSPECTRALWINDOWCOLUMNS_H

#include <casacore/ms/MeasurementSets/MSSpWindowColumns.h>

namespace LOFAR {

  //# Forward Declaration
  class MSLofarSpectralWindow;

  // This class provides read-only access to the columns in the MSLofarSpectralWindow
  // Table. It does the declaration of all the Scalar and ArrayColumns with the
  // correct types, so the application programmer doesn't have to worry about
  // getting those right. There is an access function for every predefined
  // column. Access to non-predefined columns will still have to be done with
  // explicit declarations.

  class ROMSLofarSpWindowColumns: public casacore::ROMSSpWindowColumns
  {
  public:

    // Create a columns object that accesses the data in the specified Table.
    ROMSLofarSpWindowColumns(const MSLofarSpectralWindow& msLofarSpectralWindow);

    // Access to columns.
    // <group>
    const casacore::ROScalarColumn<casacore::Bool>& topocentricFrequencyCorrectionApplied() const
      { return roTopocentricFrequencyCorrectionApplied_p; }
    // </group>

  protected:
    //# Default constructor creates a object that is not usable. Use the attach
    //# function correct this.
    ROMSLofarSpWindowColumns();

    //# Attach this object to the supplied table.
    void attach (const MSLofarSpectralWindow& msLofarSpectralWindow);

  private:
    //# Make the assignment operator and the copy constructor private to prevent
    //# any compiler generated one from being used.
    ROMSLofarSpWindowColumns(const ROMSLofarSpWindowColumns&);
    ROMSLofarSpWindowColumns& operator=(const ROMSLofarSpWindowColumns&);

    //# required columns
    casacore::ROScalarColumn<casacore::Bool> roTopocentricFrequencyCorrectionApplied_p;
  };


  // This class provides read/write access to the columns in the MSLofarSpectralWindow
  // Table. It does the declaration of all the Scalar and ArrayColumns with the
  // correct types, so the application programmer doesn't have to
  // worry about getting those right. There is an access function
  // for every predefined column. Access to non-predefined columns will still
  // have to be done with explicit declarations.

  class MSLofarSpWindowColumns: public casacore::MSSpWindowColumns
  {
  public:

    // Create a columns object that accesses the data in the specified Table.
    MSLofarSpWindowColumns(MSLofarSpectralWindow& msLofarSpectralWindow);

    // The destructor does nothing special.
    ~MSLofarSpWindowColumns();

    // Readonly access to columns.
    // <group>
    const casacore::ROScalarColumn<casacore::Bool>& topocentricFrequencyCorrectionApplied() const
      { return roTopocentricFrequencyCorrectionApplied_p; }
    // </group>

    // Read/write access to columns.
    // <group>
    casacore::ScalarColumn<casacore::Bool>& topocentricFrequencyCorrectionApplied()
      { return rwTopocentricFrequencyCorrectionApplied_p; }
    // </group>

  protected:
    //# Default constructor creates a object that is not usable. Use the attach
    //# function correct this.
    MSLofarSpWindowColumns();

    //# Attach this object to the supplied table.
    void attach(MSLofarSpectralWindow& msLofarSpectralWindow);

  private:
    //# Make the assignment operator and the copy constructor private to prevent
    //# any compiler generated one from being used.
    MSLofarSpWindowColumns(const MSLofarSpWindowColumns&);
    MSLofarSpWindowColumns& operator=(const MSLofarSpWindowColumns&);

    //# required columns
    casacore::ROScalarColumn<casacore::Bool> roTopocentricFrequencyCorrectionApplied_p;
    //# required columns
    casacore::ScalarColumn<casacore::Bool> rwTopocentricFrequencyCorrectionApplied_p;
  };

} //# end namespace

#endif
