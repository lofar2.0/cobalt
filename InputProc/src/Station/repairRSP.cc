
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#include <lofar_config.h>

#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <unistd.h>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <Common/LofarLogger.h>
#include <ApplCommon/PosixTime.h>
#include <Stream/StreamFactory.h>
#include <CoInterface/RspStationPacket.h>

using namespace LOFAR;
using namespace Cobalt;

time_t parseTime(const char *str)
{
  return LOFAR::to_time_t(boost::posix_time::time_from_string(str));
}

void usage()
{
  puts("Usage: repairRSP [options] < input.udp > output.udp");
  puts("");
  puts("-f from       Discard packets before `from' (format: '2012-01-01 11:12:00')");
  puts("-t to         Discard packets at or after `to' (format: '2012-01-01 11:12:00')");
  puts("-3            Upgrade packets to v3 (BDI 6.0)");
  puts("-s nrbeamlets Override the number of beamlets per packet");
}

int main(int argc, char **argv)
{
  INIT_LOGGER("repairRSP");

  // Force printing times in UTC
  setenv("TZ", "UTC", 1);

  int opt;

  time_t from = 0;
  time_t to = 0;
  bool quit_after_to = false;
  unsigned nrbeamlets = 0;
  bool to_v3 = false;

  // parse all command-line options
  while ((opt = getopt(argc, argv, "f:t:qs:3")) != -1) {
    switch (opt) {
    case 'f':
      from = parseTime(optarg);
      break;

    case 't':
      to = parseTime(optarg);
      break;

    case 'q':
      quit_after_to = true;
      break;

    case 's':
      nrbeamlets = atoi(optarg);
      break;

    case '3':
      to_v3 = true;
      break;

    default: /* '?' */
      usage();
      exit(1);
    }
  }

  // we expect no further arguments
  if (optind != argc) {
    usage();
    exit(1);
  }

  // create in- and output streams
  std::unique_ptr<Stream> inputStream(createStream("file:/dev/stdin", true));
  std::unique_ptr<Stream> outputStream(createStream("file:/dev/stdout", false));

  try {
    for(;;) {
        std::vector<uint8_t> dataFrame(RspStationPacket::PACKET_SIZE);
      RspStationPacket packet(dataFrame.data());

      // Read header
      inputStream->read(&packet.header(), packet.headerSize());

      // **** Apply NRBEAMLETS ****
      if (nrbeamlets > 0) {
        packet.beamletCount(nrbeamlets);
      }

      // **** Convert to VERSION 3 ****
      if (to_v3 && packet.header().version < 3) {
        packet.header().version = 3;
        packet.bitMode(16);
      }

      // Read payload after header repair
      inputStream->read(packet.payload().data, packet.packetSize() - sizeof packet.headerSize());

      // **** Apply FROM filter ****
      if (from > 0 && packet.seqId() < from)
        continue;

      // **** Apply TO filter ****
      if (to > 0 && packet.seqId() >= to) {
        if (quit_after_to)
          break;

        continue;
      }

      // Write packet
      outputStream->write(packet.data, packet.packetSize());
    }
  } catch(EndOfStreamException&) {
  }
}

