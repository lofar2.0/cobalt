
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#ifndef LOFAR_INPUT_PROC_STREAMPACKETFACTORY_H
#define LOFAR_INPUT_PROC_STREAMPACKETFACTORY_H

#include "PacketFactory.h"
#include <istream>

namespace LOFAR {
    namespace Cobalt {
        // Specialization of the generic PacketFactory class. This class make it
        // possible, e.g., to manually override the number of beamlets that will be
        // used.
        class StreamPacketFactory : public PacketFactory {
        public:
            // Construct a factory that will read its input from the input stream \a
            // inStream.
            StreamPacketFactory(std::istream &inStream,
                                const BoardMode &mode,
                                unsigned nrSubbands);

            virtual ~StreamPacketFactory();

        private:
            // Fill RSP packet header.
            virtual bool makeHeader(std::shared_ptr<StationPacket> &packet, const uint64_t packetBegin,
                                    const size_t boardNr);

            // Fill RSP packet payload.
            virtual bool makePayload(std::shared_ptr<StationPacket> &packet);

            // Create a RSP payload of 16-bit samples
            bool make16bitPayload(std::shared_ptr<StationPacket> &packet);

            // Create a RSP payload of 8-bit samples
            bool make8bitPayload(std::shared_ptr<StationPacket> &packet);

            // Create a RSP payload of 4-bit samples
            bool make4bitPayload(std::shared_ptr<StationPacket> &packet);

            // Number of subbands (or beamlets) to produce.
            unsigned itsNrSubbands;

            // Input stream
            std::istream &itsInputStream;
        };

    } // namespace Cobalt
} // namespace LOFAR

#endif

