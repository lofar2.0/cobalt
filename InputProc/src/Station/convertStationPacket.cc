// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#include <lofar_config.h>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <unistd.h>

#include "PacketReader.h"
#include <ApplCommon/PosixTime.h>
#include <CoInterface/Queue.h>
#include <CoInterface/RspStationPacket.h>
#include <CoInterface/SdpStationPacket.h>
#include <Common/LofarLogger.h>
#include <Common/Thread/Thread.h>
#include <Stream/StreamFactory.h>

using namespace LOFAR;
using namespace Cobalt;
using namespace std;

time_t parseTime(const char *str) {
    return LOFAR::to_time_t(boost::posix_time::time_from_string(str));
}

void usage() {
    puts("Usage: convertStationPacket [options] < input.udp > output.udp");
    puts("");
    puts("-i streamdesc Stream descriptor for input (default = file:/dev/stdin)");
    puts("-o streamdesc Stream descriptor for output (default = file+sdp:/dev/stdout)");
    puts("");
    puts("Note: invalid packets are always discarded.");
}

int main(int argc, char **argv) {
    INIT_LOGGER("convertStationPacket");

    // Force printing times in UTC
    setenv("TZ", "UTC", 1);

    int opt;

    string inputStreamDesc = "file:/dev/stdin";
    string outputStreamDesc = "file:/dev/stdout";

    // parse all command-line options
    while ((opt = getopt(argc, argv, "i:o:f")) != -1) {
        switch (opt) {
            case 'i':
                inputStreamDesc = optarg;
                break;

            case 'o':
                outputStreamDesc = optarg;
                break;

            default: /* '?' */
                usage();
                exit(1);
        }
    }

    // we expect no further arguments
    if (optind != argc) {
        usage();
        exit(1);
    }
    // create in- and output streams
    std::unique_ptr<Stream> inputStream(createStream(inputStreamDesc, true));
    std::unique_ptr<Stream> outputStream(createStream(outputStreamDesc, false));
    PacketReader reader("", *inputStream);

    auto readCreateFunc = inputStreamDesc.find("+sdp") != string::npos ? StationPackets::create<SdpStationPacket>
                                                                      : StationPackets::create<RspStationPacket>;
    auto writeCreateFunc = outputStreamDesc.find("+sdp") != string::npos ? StationPackets::create<SdpStationPacket>
                                                                      : StationPackets::create<RspStationPacket>;

    auto p = readCreateFunc(1);
    auto outPacket = writeCreateFunc(1);

    try {
        while (true) {
            // Read packets and queue them
            reader.readPackets(p);

            StationPackets::convert(outPacket, p);
            for (auto &packet : outPacket->packets) {
                // Write packet
                outputStream->write(packet->data, packet->packetSize());
            }
        }
    } catch (EndOfStreamException &) {
    }
}
