
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#ifndef LOFAR_INPUT_PROC_PACKETFACTORY_H
#define LOFAR_INPUT_PROC_PACKETFACTORY_H

#include <CoInterface/RSPTimeStamp.h>
#include <CoInterface/StationPacket.h>
#include <InputProc/Buffer/BoardMode.h>

namespace LOFAR {
    namespace Cobalt {
        // Generic factory for creating standard RSP packets.
        class PacketFactory {
        public:
            PacketFactory(const BoardMode &mode);

            virtual ~PacketFactory();

            // Fill an RSP packet for a certain RSP board and time stamp.
            // \return \c true if successful, \c false otherwise.
            bool makePacket(std::shared_ptr<StationPacket> &packet, const uint64_t packetBegin, const size_t boardNr);

        protected:
            const BoardMode boardMode;

            // Fill RSP packet header.
            // \return \c true if successful, \c false otherwise.
            virtual bool
            makeHeader(std::shared_ptr<StationPacket> &packet, const uint64_t packetBegin, const size_t boardNr);

            // Fill RSP packet payload.
            // \return \c true if successful, \c false otherwise.
            // \attention This method creates dummy data. Please override it in a
            // derived class if you want to have useful payload data.
            virtual bool makePayload(std::shared_ptr<StationPacket> &packet);
        };

    } // namespace Cobalt
} // namespace LOFAR

#endif

