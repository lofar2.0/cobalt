
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#include <lofar_config.h>

#include "PacketFactory.h"

#include <cstring>

namespace LOFAR {
    namespace Cobalt {

        PacketFactory::PacketFactory(const struct BoardMode &mode)
                :
                boardMode(mode) {
        }

        PacketFactory::~PacketFactory() {
        }

        bool PacketFactory::makePacket(std::shared_ptr<StationPacket> &packet, const uint64_t packetBegin,
                                       const size_t boardNr) {
            memset(packet->data, 0, packet->packetSize());

            return
                    makeHeader(packet, packetBegin, boardNr) &&
                    makePayload(packet);
        }

        bool PacketFactory::makeHeader(std::shared_ptr<StationPacket> &packet, const uint64_t packetBegin,
                                       const size_t boardNr) {
            // configure the packet header
            packet->initHeader(); // we emulate BDI 6.0

            packet->source(boardNr);
            packet->clockMHz(boardMode.clockMHz);

            packet->bitMode(boardMode.bitMode);

            packet->beamletCount(boardMode.nrBeamletsPerBoard);
            packet->blockCount(boardMode.nrBeamletsPerBoard == 488 ? 4 : 16);

            packet->packetBegin(packetBegin);

            // verify whether the packet really reflects what we intended
            ASSERT(packet->source() == boardNr);
            ASSERT(packet->payloadError() == false);
            ASSERT(packet->bitMode() == boardMode.bitMode);
            ASSERT(packet->clockMHz() == boardMode.clockMHz);

            // verify that the packet has a valid size
            //ASSERT(packet->packetSize()   <= sizeof packet);

            return true;
        }

        bool PacketFactory::makePayload(std::shared_ptr<StationPacket> &packet) {
            // insert data that is different for each packet
            int64 data = packet->packetBegin();
            memset(packet->payload().data, data & 0xFF, sizeof(Payload));
            return true;
        }

    }
}

