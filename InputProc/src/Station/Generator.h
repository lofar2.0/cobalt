
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#ifndef LOFAR_INPUT_PROC_GENERATOR_H
#define LOFAR_INPUT_PROC_GENERATOR_H

#include <string>
#include <vector>

#include <CoInterface/RSPTimeStamp.h>
#include <Stream/Stream.h>

#include <InputProc/Buffer/StationID.h>

#include <boost/format.hpp>

#include <Common/LofarLogger.h>
#include <Stream/Stream.h>

#include <CoInterface/OMPThread.h>
#include <CoInterface/RspStationPacket.h>
#include <CoInterface/SdpStationPacket.h>

#include "PacketFactory.h"
#include "../WallClockTime.h"

namespace LOFAR
{
    namespace Cobalt
    {

        /* Generate station input data */

        template <class T>
        class Generator
        {
        public:
            Generator( const StationID &stationID, const std::vector<std::shared_ptr<Stream>> &outputStreams, PacketFactory &packetFactory, const TimeStamp &from, const TimeStamp &to );

            void process();

            void stop();

        protected:
            const std::string logPrefix;
            const size_t nrBoards;

            WallClockTime waiter;

            const StationID stationID;
            std::vector<std::shared_ptr<Stream>> outputStreams;
            PacketFactory &packetFactory;

            std::vector<size_t> nrSent;

            const TimeStamp from, to;

            void processBoard( size_t nr );
            void logStatistics();
        };


        template <class T>
        Generator<T>::Generator( const StationID &stationID, const std::vector<std::shared_ptr<Stream>> &outputStreams_, PacketFactory &packetFactory, const TimeStamp &from, const TimeStamp &to )
                :
                logPrefix(str(boost::format("[station %s] [Generator] ") % stationID.name())),
                nrBoards(outputStreams_.size()),
                stationID(stationID),
                outputStreams(outputStreams_.size()),
                packetFactory(packetFactory),
                nrSent(nrBoards, 0),
                from(from),
                to(to)
        {
            for (size_t i = 0; i < outputStreams.size(); ++i) {
                outputStreams[i] = outputStreams_[i];
            }

            LOG_INFO_STR( logPrefix << "Initialised" );
        }


        template <class T>
        void Generator<T>::processBoard( size_t nr )
        {
            const std::string logPrefix(str(boost::format("[station %s board %u] [Generator] ") % stationID.name() % nr));

            try {
                Stream &s = *outputStreams[nr];

                LOG_INFO_STR( logPrefix << "Start" );

                std::vector<uint8_t> dataFrame(T::PACKET_SIZE);
                std::shared_ptr<StationPacket> packet = std::make_shared<T>(dataFrame.data());
                for(TimeStamp current = from; !to || current < to; /* increment in loop */ ) {

                    // generate packet
                    packetFactory.makePacket( packet, current, nr );

                    // wait until it is due
                    if (!waiter.waitUntil(current)) {
                        break;
                    }

                    // send packet
                    try {
                        s.write(packet->data, packet->packetSize());
                    } catch (SystemCallException &ex) {
                        // UDP can return ECONNREFUSED or EINVAL if server does not have its port open
                        if (ex.error != ECONNREFUSED && ex.error != EINVAL) {
                            throw;
                        }
                    }

                    nrSent[nr]++;

                    current += packet->blockCount();
                }
            } catch (EndOfStreamException &ex) {
                LOG_INFO_STR( logPrefix << "End of stream");
            } catch (SystemCallException &ex) {
                if (ex.error == EINTR) {
                    LOG_INFO_STR(logPrefix << "Stopped: " << ex.what());
                } else {
                    LOG_ERROR_STR(logPrefix << "Caught Exception: " << ex);
                }
            } catch (Exception &ex) {
                LOG_ERROR_STR( logPrefix << "Caught Exception: " << ex);
            }

            LOG_INFO_STR( logPrefix << "End");
        }

        template <class T>
        void Generator<T>::process()
        {
            // References to all threads that will need aborting
            OMPThreadSet threads("RSPBoards");

            ASSERT(nrBoards > 0);

            LOG_DEBUG_STR( logPrefix << "Start" );

# pragma omp parallel sections num_threads(3)
            {
                // Board threads
#   pragma omp section
                {
                    // start all boards
                    LOG_DEBUG_STR( logPrefix << "Starting all boards" );
#     pragma omp parallel for num_threads(nrBoards)
                    for (size_t i = 0; i < nrBoards; ++i) {
                        try {
                            OMPThreadSet::ScopedRun sr(threads);

                            processBoard(i);
                        } catch(Exception &ex) {
                            LOG_ERROR_STR("Caught exception: " << ex);
                        }
                    }

                    // we're done
                    stop();
                }

                // Log threads
#   pragma omp section
                {
                    // start log statistics
                    LOG_DEBUG_STR( logPrefix << "Starting log statistics" );

                    try {
                        OMPThreadSet::ScopedRun sr(threads);

                        for(;;) {
                            if (usleep(999999) == -1 && errno == EINTR)
                                // got killed
                                break;

                            logStatistics();
                        }
                    } catch(Exception &ex) {
                        LOG_ERROR_STR("Caught exception: " << ex);
                    }
                }

                // Watcher thread
#   pragma omp section
                {
                    // wait until we have to stop
                    LOG_DEBUG_STR( logPrefix << "Waiting for stop signal" );
                    waiter.waitForever();

                    // kill all boards
                    LOG_DEBUG_STR( logPrefix << "Stopping all boards" );
                    threads.killAll();
                }
            }

            LOG_DEBUG_STR( logPrefix << "End" );
        }


        template <class T>
        void Generator<T>::stop()
        {
            waiter.cancelWait();
        }

        template <class T>
        void Generator<T>::logStatistics()
        {
            //TODO: packets per second
            for( size_t nr = 0; nr < nrBoards; nr++ ) {
                const Stream &s = *outputStreams[nr];
                const std::string logPrefix(str(boost::format("[station %s board %u] [Generator] ") % stationID.name() % nr));

                LOG_INFO_STR( logPrefix << nrSent[nr] << " packets sent via " << s.description());

                nrSent[nr] = 0;
            }
        }
    }
}

#endif

