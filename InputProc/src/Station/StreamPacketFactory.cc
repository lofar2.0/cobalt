
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#include <lofar_config.h>

#include "StreamPacketFactory.h"
#include <cstring>

using namespace std;

namespace LOFAR {
    namespace Cobalt {

        StreamPacketFactory::StreamPacketFactory(istream &inStream,
                                                 const BoardMode &mode,
                                                 unsigned nrSubbands) :
                PacketFactory(mode),
                itsNrSubbands(nrSubbands),
                itsInputStream(inStream) {
            if (nrSubbands == 0 || nrSubbands > mode.nrBeamletsPerBoard) {
                THROW(Exception, "Invalid number of subbands: " << nrSubbands);
            }
        }

        StreamPacketFactory::~StreamPacketFactory() {
        }

        bool StreamPacketFactory::makeHeader(std::shared_ptr<StationPacket> &packet, const uint64_t packetBegin,
                                             const size_t boardNr) {
            if (PacketFactory::makeHeader(packet, packetBegin, boardNr)) {
                packet->beamletCount(itsNrSubbands);
                return true;
            }
            return false;
        }

        bool StreamPacketFactory::makePayload(std::shared_ptr<StationPacket> &packet) {
            // Clear payload first, to make sure a packet is always zero padded.
            memset(packet->payload().data, 0, sizeof(packet->payload().data));
            switch (boardMode.bitMode) {
                case 16:
                    return make16bitPayload(packet);
                case 8 :
                    return make8bitPayload(packet);
                case 4 :
                    return make4bitPayload(packet);
                default:
                    THROW(Exception, "Invalid bit mode: " << boardMode.bitMode);
            }
        }

        bool StreamPacketFactory::make16bitPayload(std::shared_ptr<StationPacket> &packet) {
            int xr, xi, yr, yi;
            for (size_t b = 0; b < packet->blockCount(); b++) {
                for (size_t sb = 0; sb < itsNrSubbands; sb++) {
                    if (!(itsInputStream >> xr >> xi >> yr >> yi)) {
                        return false;
                    }
                    auto &s = packet->payload().samples16bit[sb * packet->blockCount() + b];
                    s.Xr = xr;
                    s.Xi = xi;
                    s.Yr = yr;
                    s.Yi = yi;
                }
            }
            return true;
        }

        bool StreamPacketFactory::make8bitPayload(std::shared_ptr<StationPacket> &packet) {
            int xr, xi, yr, yi;
            for (size_t b = 0; b < packet->blockCount(); b++) {
                for (size_t sb = 0; sb < itsNrSubbands; sb++) {
                    if (!(itsInputStream >> xr >> xi >> yr >> yi)) {
                        return false;
                    }
                    auto &s = packet->payload().samples8bit[sb * packet->blockCount() + b];
                    s.Xr = xr;
                    s.Xi = xi;
                    s.Yr = yr;
                    s.Yi = yi;
                }
            }
            return true;
        }

        bool StreamPacketFactory::make4bitPayload(std::shared_ptr<StationPacket> &packet) {
            int xr, xi, yr, yi;
            for (size_t b = 0; b < packet->blockCount(); b++) {
                for (size_t sb = 0; sb < itsNrSubbands; sb++) {
                    if (!(itsInputStream >> xr >> xi >> yr >> yi)) {
                        return false;
                    }
                    auto &s = packet->payload().samples4bit[sb * packet->blockCount() + b];
                    s.X = (xr & 0xF) | ((xi & 0xF) << 4);
                    s.Y = (yr & 0xF) | ((yi & 0xF) << 4);
                }
            }
            return true;
        }
    } // namespace Cobalt
} // namespace LOFAR

