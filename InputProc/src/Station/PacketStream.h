
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#ifndef LOFAR_INPUT_PROC_PACKETSTREAM_H
#define LOFAR_INPUT_PROC_PACKETSTREAM_H

#include "PacketFactory.h"
#include <CoInterface/RSPTimeStamp.h>
#include <Common/Thread/Cancellation.h>
#include <Stream/Stream.h>
#include <cstring>

namespace LOFAR {
    namespace Cobalt {
        /* Generate a Stream of RSP packets. */

        template<class T>
        class PacketStream : public Stream {
        public:
            // 'factory' will be copied.
            PacketStream(const PacketFactory &factory, const TimeStamp &from, const TimeStamp &to, size_t boardNr = 0)
                    :
                    factory(factory),
                    from(from),
                    to(to),
                    current(from),
                    boardNr(boardNr),
                    dataFrame(T::PACKET_SIZE),
                    packet(std::make_shared<T>(dataFrame.data())),
                    offset(0) {
            }

            virtual size_t tryRead(void *ptr, size_t size) {
                Cancellation::point();

                if (size == 0) {
                    return 0;
                }

                if (current >= to) {
                    THROW(EndOfStreamException, "No data beyond " << to);
                }

                if (offset == 0) {
                    // generate new packet
                    factory.makePacket(packet, current, boardNr);
                    current += packet->blockCount();
                }

                size_t pktSize = packet->packetSize();
                size_t numBytes = std::min(pktSize - offset, size);
                memcpy(ptr, reinterpret_cast<char *>(packet->data) + offset, numBytes);

                offset += numBytes;
                if (offset == pktSize) {
                    // written full packet, so we'll need a new one on next read
                    offset = 0;
                }

                return numBytes;
            }

            virtual size_t tryWrite(const void * /*ptr*/, size_t /*size*/) {
                THROW(NotImplemented, "Writing to PacketStream is not supported");
            }

            virtual size_t tryReadv(const struct iovec *iov, int iovcnt) {
                Cancellation::point();

                size_t nread = 0;
                for (int i = 0; i < iovcnt; i++) {
                    if (iov[i].iov_len == 0) {
                        continue;
                    }

                    if (current >= to) {
                        if (nread == 0) {
                            THROW(EndOfStreamException, "No data beyond " << to);
                        } else {
                            break;
                        }
                    }

                    if (offset == 0) {
                        // generate new packet
                        factory.makePacket(packet, current, boardNr);
                        current += packet->blockCount();
                    }

                    size_t pktSize = packet->packetSize();
                    size_t numBytes = std::min(pktSize - offset, iov[i].iov_len);
                    memcpy(iov[i].iov_base, packet->data + offset, numBytes);

                    offset += numBytes;
                    if (offset == pktSize) {
                        // written full packet, so we'll need a new one on next read
                        offset = 0;
                    }

                    nread += numBytes;

                    // Mimic tryRead() impl above: max 1 (partial) packet per buffer.
                    // Then we can only use the next iov if we could exactly fill the previous, else our retval is ambiguous.
                    if (numBytes < pktSize) {
                        break;
                    }
                }

                return nread;
            }

            virtual size_t tryWritev(const struct iovec * /*iov*/, int /*iovcnt*/) {
                THROW(NotImplemented, "Writing to PacketStream is not supported");
            }

            virtual std::string description() const {
                //TODO: fill in all the packetstream details
                return "PacketStream ";
            }

        private:
            PacketFactory factory;

            const TimeStamp from;
            const TimeStamp to;
            TimeStamp current;
            const size_t boardNr;

            std::vector<uint8_t> dataFrame;
            std::shared_ptr<StationPacket> packet;

            // Write offset within packet. If 0, a new
            // packet is required.
            size_t offset;
        };
    } // namespace Cobalt
} // namespace LOFAR

#endif

