
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#include <lofar_config.h>

#include "PacketReader.h"

#include <cmath>
#include <sys/time.h>
#include <typeinfo>

#include <Common/LofarLogger.h>


namespace LOFAR {
    namespace Cobalt {
        // Create an 'invalid' mode to make it unique and not match any actually used mode.
        const BoardMode PacketReader::MODE_ANY(0, 0);

        PacketReader::PacketReader(const std::string &logPrefix, Stream &inputStream,
                                   const BoardMode &mode)
                : mode(mode),
                  inputStream(inputStream),
                  logPrefix(logPrefix),

                  hadSizeError(false),
                  nrReceived(0),
                  nrBadMode(0),
                  nrBadTime(0),
                  nrBadData(0),
                  nrBadOther(0),
                  totalNrReceived(0),
                  totalNrBadMode(0),
                  totalNrBadTime(0),
                  totalNrBadData(0),
                  totalNrBadOther(0),
                  lastLogTime(0.0) {
            // Partial reads are not supported on UDP streams, because each read()
            // will consume a full packet.
            try {
                SocketStream &asSocket = dynamic_cast<SocketStream &>(inputStream);
                const bool isUDP = asSocket.protocol == SocketStream::UDP;

                inputIsUDP = isUDP;
            } catch (std::bad_cast &) {
                // inputStream is not a SocketStream
                inputIsUDP = false;
            }
        }

        PacketReader::~PacketReader() {
            const size_t totalNrDiscarded = totalNrBadMode + totalNrBadTime + totalNrBadData + totalNrBadOther;
            const float lostPerc = totalNrReceived == 0.0 ? 0.0 : 100.0 * totalNrDiscarded / totalNrReceived;

            if (lostPerc > 0) {
                LOG_WARN_STR(
                        logPrefix << "Total discarded packets is " << lostPerc << "% on " << inputStream.description());

                if (totalNrBadMode > 0) {
                    LOG_WARN_STR(logPrefix
                                         << "Total discarded packets due to bad clock/bitmode is "
                                         << (100.0 * totalNrBadMode / totalNrReceived) << "% on "
                                         << inputStream.description());
                }
                if (totalNrBadTime > 0) {
                    LOG_WARN_STR(logPrefix
                                         << "Total discarded packets due to bad timestamps is "
                                         << (100.0 * totalNrBadTime / totalNrReceived) << "% on "
                                         << inputStream.description());
                }
                if (totalNrBadData > 0) {
                    LOG_WARN_STR(logPrefix
                                         << "Total discarded packets due to payload errors is "
                                         << (100.0 * totalNrBadData / totalNrReceived) << "% on "
                                         << inputStream.description());
                }
                if (totalNrBadOther > 0) {
                    LOG_WARN_STR(logPrefix
                                         << "Total discarded packets due to other reasons is "
                                         << (100.0 * totalNrBadOther / totalNrReceived) << "% on "
                                         << inputStream.description());
                }
            }
        }


        void PacketReader::readPackets(std::shared_ptr<StationPackets> &packets) {
            size_t numRead;

            if (inputIsUDP) {
                SocketStream &sstream = dynamic_cast<SocketStream &>(inputStream);

                if (recvdSizes.size() != packets->count) {
                    recvdSizes.resize(packets->count);
                }
                numRead =
                        sstream.recvmmsg(packets->memory.data(), packets->size, recvdSizes);

                nrReceived += numRead;

                // validate received packets
                for (size_t i = 0; i < numRead; ++i) {
                    packets->packets[i]->payloadError(
                            !validatePacket(packets->packets[i].get(), recvdSizes[i]));
                }
            } else {
                packets->packets[0]->payloadError(!readPacket(packets));
                numRead = 1;
            }

            // mark unused packet buffers as invalid
            for (size_t i = numRead; i < packets->packets.size(); ++i) {
                packets->packets[i]->payloadError(true);
                packets->packets[i]->timeStampError(); // easier for RSP raw code to filter
            }
        }


        bool PacketReader::readPacket(std::shared_ptr<StationPackets> &packets) {
            size_t numbytes;

            if (inputIsUDP) {
                numbytes = inputStream.tryRead(packets->memory.data(), packets->size);
            } else {
                auto &packet = packets->packets[0];
                // read header first to determine actual packet size
                inputStream.read(packet->data, packet->headerSize());
                size_t pktSize = packet->packetSize();

                // read rest of packet
                inputStream.read(&packet->payload().data[0], pktSize - packet->headerSize());
                numbytes = pktSize;
            }

            ++nrReceived;

            return validatePacket(packets->packets[0].get(), numbytes);
        }

        bool PacketReader::validatePacket(const StationPacket *packet, size_t numbytes) {
            auto result = packet->validate(numbytes);

            // illegal size means illegal packet; don't touch
            if (result == SP_PACKET_ERROR_BAD_SIZE) {
                if (!hadSizeError) {
                    LOG_ERROR_STR(logPrefix << "Packet is " << numbytes <<
                                            " bytes, but should be " << packet->packetSize() << " bytes on "
                                            << inputStream.description());
                    hadSizeError = true;
                }

                ++nrBadOther;
                return false;
            }

            // illegal version means illegal packet
            if (result == SP_PACKET_ERROR_BAD_VERSION) {
                // This mainly catches packets that are all zero (f.e. /dev/zero or
                // null: streams).
                ++nrBadOther;
                return false;
            }

            // illegal timestamp means illegal packet
            if (result == SP_PACKET_ERROR_BAD_TIME) {
                ++nrBadTime;
                return false;
            }

            // discard packets with errors
            if (result == SP_PACKET_ERROR_BAD_DATA) {
                ++nrBadData;
                return false;
            }

            if (packet->bitMode() != mode.bitMode || packet->clockMHz() != mode.clockMHz) {
                if (mode != MODE_ANY) {
                    ++nrBadMode;
                    return false;
                }
            }

            // everything is ok
            return true;
        }


        void PacketReader::logStatistics(unsigned boardNr) {
            // Determine time since last log
            struct timeval tv;
            gettimeofday(&tv, NULL);
            const double now = (double) tv.tv_sec + (double) tv.tv_usec / 1000000.0;

            const double interval = now - lastLogTime;

            // Emit log line, but only once we are producing increments
            if (lastLogTime > 0.0) {
                LOG_INFO_STR(logPrefix << (nrReceived / interval) << " pps: received " <<
                                       nrReceived << " packets: " << nrBadTime << " bad timestamps, " <<
                                       nrBadMode << " bad clock/bitmode, " << nrBadData << " payload errors, " <<
                                       nrBadOther << " otherwise bad packets via " << inputStream.description());
            }

            // Update totals
            totalNrReceived += nrReceived;
            totalNrBadTime += nrBadTime;
            totalNrBadMode += nrBadMode;
            totalNrBadData += nrBadData;
            totalNrBadOther += nrBadOther;

            // Reset counters
            nrReceived = 0;
            nrBadTime = 0;
            nrBadMode = 0;
            nrBadData = 0;
            nrBadOther = 0;

            hadSizeError = false;

            lastLogTime = now;
        }


    } // namespace Cobalt
} // namespace LOFAR

