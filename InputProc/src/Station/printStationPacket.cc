
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

//# Always #include <lofar_config.h> first!
#include <lofar_config.h>

#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>

#include <CoInterface/RSPTimeStamp.h>
#include <CoInterface/RspStationPacket.h>
#include <CoInterface/SdpStationPacket.h>
#include <CoInterface/StationPacket.h>
#include <Common/LofarLogger.h>
#include <Stream/FileStream.h>

using namespace LOFAR;
using namespace LOFAR::Cobalt;
using namespace std;

void report(const string &filename) {

    std::vector<uint8_t> dataFrame;
    std::shared_ptr<StationPacket> packet;

    std::fstream file(filename, ios::in | ios::binary);
    char marker;
    file.read(&marker, 1);
    file.close();
    if (marker == 'b') {
        dataFrame.reserve(SdpStationPacket::PACKET_SIZE);
        packet = std::make_shared<SdpStationPacket>(dataFrame.data());
    } else {
        dataFrame.reserve(RspStationPacket::PACKET_SIZE);
        packet = std::make_shared<RspStationPacket>(dataFrame.data());
    }
    FileStream f(filename);

    // NOTE: this program is used in output verification in some test case(s). Expect a few tests to break when changing output formatting.
    try {
        for (;;) {
            // read header
            f.read(packet->data, packet->headerSize());

            TimeStamp timeStamp = packet->timeStamp();
            time_t seconds = packet->seqId();

            char buf[26];
            ctime_r(&seconds, buf);
            buf[strlen(buf) - 1] = 0; // remove trailing \n

            cout << "Time stamp:   " << buf << " sample " << timeStamp.getBlockId() << endl;
            cout << "RSP version:  " << packet->version() << endl;
            cout << "RSP board nr: " << packet->source() << endl;
            cout << "Payload OK:   " << (packet->payloadError() ? "NO" : "YES") << endl;
            cout << "Clock:        " << packet->clockMHz() << " MHz" << endl;
            cout << "Bit mode:     " << packet->bitMode() << " bit" << endl;
            cout << "Blocks:       " << (int) packet->blockCount() << endl;
            cout << "Beamlets:     " << (int) packet->beamletCount() << endl;

            // read payload
            f.read(packet->payload().data, packet->packetSize() - packet->headerSize());

            cout << "Payload: " << endl;
            for (unsigned beamlet = 0; beamlet < packet->beamletCount(); beamlet++) {
                cout << "Beamlet " << setw(2) << beamlet << ": ";
                for (size_t block = 0; block < packet->blockCount(); block++) {
                    cout << "X=" << setw(7) << packet->sample(beamlet, block, 'X') << " ";
                    cout << "Y=" << setw(7) << packet->sample(beamlet, block, 'Y') << ", ";
                }
                cout << endl;
            }
        }
    } catch (EndOfStreamException &) {
    }

}

int main() {
    // Force printing times in UTC
    setenv("TZ", "UTC", 1);

    INIT_LOGGER("printStationPacket");

    report("/dev/stdin");

    return 0;
}

