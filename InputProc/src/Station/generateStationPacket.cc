
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#include <lofar_config.h>

#include <climits>
#include <cstdlib>
#include <ctime>

#include <fstream>
#include <iostream>
#include <vector>

#include <ApplCommon/PosixTime.h>
#include <CoInterface/RSPTimeStamp.h>
#include <CoInterface/RspStationPacket.h>
#include <CoInterface/SdpStationPacket.h>
#include <Common/LofarLogger.h>
#include <InputProc/Buffer/BoardMode.h>
#include <InputProc/Station/StreamPacketFactory.h>
#include <Stream/StreamFactory.h>

using namespace std;
using namespace boost;
using namespace LOFAR;
using namespace LOFAR::Cobalt;

struct OptionsT {
    time_t from;
    time_t to;
    unsigned bitmode;
    unsigned clockmode;
    unsigned subbands;
    string streamdesc;
};

OptionsT defaultOptions = {0, INT_MAX, 16, 200, 61, "file:/dev/stdout"};

void usage() {
    char fromString[256], toString[256];
    const char *format = "%Y-%m-%d %H:%M:%S";
    strftime(fromString, sizeof fromString, format, gmtime(&defaultOptions.from));
    strftime(toString, sizeof toString, format, gmtime(&defaultOptions.to));
    cerr << "\nUsage: generateStationPacket [options] < input.asc > output.rsp\n\n"
         << "-b bitmode    Bitmode `bitmode' (16, 8, or 4)"
         << " (default: " << defaultOptions.bitmode << ")\n"
         << "-c clockmode  Clock frequency (MHz) `clock' (200 or 160)"
         << " (default: " << defaultOptions.clockmode << ")\n"
         << "-f from       Start time `from' (format: '2012-01-01 11:12:00')"
         << " (default: '" << fromString << "')\n"
         << "-h help       Print this help message\n"
         << "-o streamdesc Stream descriptor for output"
         << " (default: '" << defaultOptions.streamdesc << "')\n"
         << "-s subbands   Number of `subbands` (or beamlets)"
         << " (default: " << defaultOptions.subbands << ")\n"
         << "-t to         End time `to' (format: '2012-01-01 11:12:00')"
         << " (default: '" << toString << "')\n"
         << endl;
}

time_t parseTime(const char *str) {
    try {
        return LOFAR::to_time_t(posix_time::time_from_string(str));
    } catch (std::exception &err) {
        THROW (Exception, "Invalid date/time: " << err.what());
    }
}

int main(int argc, char **argv) {
    INIT_LOGGER("generateStationPacket");

    int opt;

    time_t from = defaultOptions.from;
    time_t to = defaultOptions.to;
    unsigned bitmode = defaultOptions.bitmode;
    unsigned clockmode = defaultOptions.clockmode;
    unsigned subbands = defaultOptions.subbands;
    string streamdesc = defaultOptions.streamdesc;

    try {
        // parse all command-line options
        while ((opt = getopt(argc, argv, "b:c:f:ho:s:t:")) != -1) {
            switch (opt) {
                default:
                    usage();
                    return 1;
                case 'b':
                    bitmode = atoi(optarg);
                    break;
                case 'c':
                    clockmode = atoi(optarg);
                    break;
                case 'f':
                    from = parseTime(optarg);
                    break;
                case 'h':
                    usage();
                    return 0;
                case 'o':
                    streamdesc = optarg;
                    break;
                case 's':
                    subbands = atoi(optarg);
                    break;
                case 't':
                    to = parseTime(optarg);
                    break;
            }
        }

        // validate command-line options
        ASSERTSTR(from < to, from << " < " << to);
        ASSERTSTR(bitmode == 16 || bitmode == 8 || bitmode == 4,
                  "bitmode = " << bitmode);
        ASSERTSTR(clockmode == 160 || clockmode == 200, "clockmode = " << clockmode);
        ASSERTSTR(subbands > 0, "subbands = " << subbands);

        // we expect no further arguments
        if (optind != argc) {
            usage();
            return 1;
        }

        ifstream inStream("/dev/stdin");
        std::unique_ptr<Stream> outStream(createStream(streamdesc, false));

        BoardMode boardMode(bitmode, clockmode);
        unsigned boardNr(0);

        StreamPacketFactory packetFactory(inStream, boardMode, subbands);
        std::vector<uint8_t> dataFrame;
        std::shared_ptr<StationPacket> packet;

        if (streamdesc.find("+sdp") == string::npos) {
            dataFrame.resize(RspStationPacket::PACKET_SIZE);
            packet = std::make_shared<RspStationPacket>(dataFrame.data());
        } else {
            dataFrame.resize(SdpStationPacket::PACKET_SIZE);
            packet = std::make_shared<SdpStationPacket>(dataFrame.data());

        }


        TimeStamp current(TimeStamp::convert(from, boardMode.clockHz()));
        TimeStamp end(TimeStamp::convert(to, boardMode.clockHz()));

        while (current < end && packetFactory.makePacket(packet, current, boardNr)) {
            // Write packet
            outStream->write(packet->data, packet->packetSize());
            // Increment time stamp
            current += packet->blockCount();
        }

    } catch (Exception &ex) {
        cerr << ex << endl;
        return 1;
    }

}

