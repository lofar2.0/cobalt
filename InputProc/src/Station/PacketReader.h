
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#ifndef LOFAR_INPUT_PROC_PACKETREADER_H
#define LOFAR_INPUT_PROC_PACKETREADER_H

#include <string>
#include <vector>

#include "../Buffer/BoardMode.h"
#include <CoInterface/StationPacket.h>
#include <Common/Exception.h>
#include <Stream/SocketStream.h>

namespace LOFAR
{
  namespace Cobalt
  {

    /*
     * Reads RSP packets from a Stream, and collects statistics.
     *
     * Thread-safefy: none.
     */
    class PacketReader
    {
    public:
      static const BoardMode MODE_ANY;

      PacketReader( const std::string &logPrefix, Stream &inputStream,
                    const BoardMode &mode = MODE_ANY );

      ~PacketReader();

      // Reads a set of packets from the input stream. Sets the payloadError
      // flag for all invalid packets.
      void readPackets(std::shared_ptr<StationPackets> &packets );

      // Reads a packet from the input stream. Returns true if a packet was
      // succesfully read.
      bool readPacket(std::shared_ptr<StationPackets> &packet);

      // Logs (and resets) statistics about the packets read.
      void logStatistics(unsigned boardNr);

    private:
      // The mode against which to validate (ignored if mode == MODE_ANY)
      const BoardMode mode;

      // The stream from which packets are read.
      Stream &inputStream;

      // For SocketStream recvmmsg() to indicate max nr packets to receive and to return bytes sent.
      std::vector<unsigned> recvdSizes;

      const std::string logPrefix;

      // Whether inputStream is an UDP stream
      // UDP streams do not allow partial reads and can use recvmmsg(2) (Linux).
      bool inputIsUDP;

      // Statistics covering the packets read so far
      bool hadSizeError; // already reported about wrongly sized packets since last logStatistics()
      size_t nrReceived; // nr. of packets received
      size_t nrBadMode; // nr. of packets with wrong mode (clock, bit mode)
      size_t nrBadTime; // nr. of packets with an illegal time stamp
      size_t nrBadData; // nr. of packets with payload errors
      size_t nrBadOther; // nr. of packets that are bad in another fashion (illegal header, packet size, etc)

      size_t totalNrReceived;
      size_t totalNrBadMode;
      size_t totalNrBadTime;
      size_t totalNrBadData;
      size_t totalNrBadOther;

      double lastLogTime; // time since last log print, to monitor data rates


      // numbytes is the actually received size, as indicated by the kernel
      bool validatePacket(const StationPacket *packet, size_t numbytes);
    };


  }
}

#endif

