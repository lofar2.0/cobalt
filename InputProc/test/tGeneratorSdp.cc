
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#include <lofar_config.h>

#include <omp.h>
#include <string>
#include <unistd.h>
#include <vector>

#include <CoInterface/OMPThread.h>
#include <CoInterface/SdpStationPacket.h>
#include <Common/LofarLogger.h>
#include <Stream/StreamFactory.h>

#include <InputProc/Station/Generator.h>
#include <InputProc/Station/PacketFactory.h>
#include <InputProc/Station/PacketReader.h>

using namespace LOFAR;
using namespace Cobalt;
using namespace std;

// The number of packets to transmit (note: there are 16 time samples/packet)
#define NUMPACKETS (200000000 / 1024 / 16)

int main( int, char **argv )
{
  INIT_LOGGER( argv[0] );

  // Don't run forever if communication fails for some reason
  alarm(10);

  omp_set_nested(true);
  omp_set_num_threads(16);

  OMPThread::init();

  const string desc = "tcp:localhost:54321";

  vector<std::shared_ptr<Stream> > inputStreams(1);
  vector<std::shared_ptr<Stream> > outputStreams(1);

  #pragma omp parallel sections num_threads(2)
  {
    #pragma omp section
    inputStreams[0].reset(createStream(desc, true));

    #pragma omp section
    outputStreams[0].reset(createStream(desc, false));
  }

  struct StationID stationID("RS106", "LBA");
  struct BoardMode mode(16, 200);

  const TimeStamp from(time(0), 0, mode.clockHz());
  const TimeStamp to = from + NUMPACKETS * 16; /* 16 timeslots/packet */
  PacketFactory factory(mode);
  Generator<SdpStationPacket> g(stationID, outputStreams, factory, from, to);

  bool error = false;

  #pragma omp parallel sections num_threads(2)
  {
    #pragma omp section
    {
      // Generate packets

      try {
        g.process();
      } catch(Exception &ex) {
        LOG_ERROR_STR("Caught exception: " << ex);
        error = true;
      }
    }

    #pragma omp section
    {
      // Read and verify the generated packets

      try {
        PacketReader reader("", *inputStreams[0]);

        for(size_t nr = 0; nr < NUMPACKETS; ++nr) {
          auto packet = StationPackets::create<SdpStationPacket>(1);

          if (!reader.readPacket(packet)) {
            const unsigned boardNr = 0;
            reader.logStatistics(boardNr);

            ASSERT(false);
          }
        }
      } catch(Exception &ex) {
        LOG_ERROR_STR("Caught exception: " << ex);
        error = true;
      }

      // We received NUMPACKETS packets, kill the generator
      g.stop();
    }
  }

  return error ? 1 : 0;
}

