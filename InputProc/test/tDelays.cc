/* tDelays.cc
 * Copyright (C) 2013  ASTRON (Netherlands Institute for Radio Astronomy)
 * P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * This file is part of the LOFAR software suite.
 * The LOFAR software suite is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The LOFAR software suite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */

#include <lofar_config.h>

#include <ctime>
#include <UnitTest++.h>

#include <Common/LofarLogger.h>
#include <Stream/FixedBufferStream.h>
#include <CoInterface/TimeFuncs.h>

#include <InputProc/Delays/Delays.h>

using namespace LOFAR;
using namespace Cobalt;
using namespace std;

const size_t dayOfSamples = 24UL * 3600 * 192315;

TEST(Tracking) {
  Parset ps;

  LOG_INFO("Test Tracking");

  ps.add( "Observation.referencePhaseCenter", "[0, 0, 0]" ); // center of earth
  ps.add( "PIC.Core.CS001LBA.phaseCenter", "[0, 0, 299792458]" ); // 1 lightsecond away from earth center
  ps.add( "Observation.VirtualInstrument.stationList", "[CS001]" );
  ps.add( "Observation.antennaSet", "LBA_INNER" );
  ps.add( "Observation.Dataslots.CS001LBA.RSPBoardList", "[0]" );
  ps.add( "Observation.Dataslots.CS001LBA.DataslotList", "[0]" );

  ps.add( "Observation.nrBeams", "1" );
  ps.add( "Observation.Beam[0].subbandList", "[0]" );
  ps.add( "Observation.Beam[0].directionType", "J2000" );
  ps.add( "Observation.Beam[0].angle1", "0" );
  ps.add( "Observation.Beam[0].angle2", "0" );
  ps.add( "Observation.Beam[0].nrTiedArrayBeams", "0" );
  ps.updateSettings();

  // blockSize is ~1s
  Delays delays(ps, 0, TimeStamp(time(0), 0, 200000000), dayOfSamples);

  Delays::AllDelays delaySet(ps), prevDelaySet(ps);

  std::chrono::steady_clock::time_point tStartTime = std::chrono::steady_clock::now();
  for (size_t block = 0; block < 1024; ++block) {
    delays.getNextDelays(prevDelaySet, delaySet);

    // There must be exactly one SAP
    CHECK_EQUAL(1U, delaySet.SAPs.size());
    CHECK_EQUAL(0U, delaySet.SAPs[0].TABs.size());

#ifdef HAVE_CASACORE
    // Delays must change over time
    if (block > 0) {
      CHECK(delaySet.SAPs[0].SAP.delay != prevDelaySet.SAPs[0].SAP.delay);
    }
#endif
  }
  std::chrono::steady_clock::time_point tEndTime = std::chrono::steady_clock::now();
  std::chrono::steady_clock::duration tDuration = tEndTime - tStartTime;
  double tDurationSec = double(tDuration.count()) * std::chrono::steady_clock::period::num / std::chrono::steady_clock::period::den;
  LOG_INFO_STR("Test Duration (sec): " << tDurationSec);
}

TEST(TiedArrayBeam) {
  Parset ps;

  LOG_INFO("Test Tied Array Beam");

  ps.add( "Observation.DataProducts.Output_CoherentStokes.enabled", "true" );
  ps.add( "Observation.DataProducts.Output_CoherentStokes.filenames", "[2*beam0.raw]" );
  ps.add( "Observation.DataProducts.Output_CoherentStokes.locations", "[2*localhost:.]" );

  ps.add( "Observation.referencePhaseCenter", "[0, 0, 0]" ); // center of earth
  ps.add( "PIC.Core.CS001LBA.phaseCenter", "[0, 0, 299792458]" ); // 1 lightsecond away from earth center
  ps.add( "Observation.VirtualInstrument.stationList", "[CS001]" );
  ps.add( "Observation.antennaSet", "LBA_INNER" );
  ps.add( "Observation.Dataslots.CS001LBA.RSPBoardList", "[0, 0]" );
  ps.add( "Observation.Dataslots.CS001LBA.DataslotList", "[5, 6]" );

  // Delays for SAP 0 and TAB 0 of SAP 1 should be equal
  ps.add( "Observation.nrBeams", "2" );
  ps.add( "Observation.Beam[0].subbandList", "[5]" );
  ps.add( "Observation.Beam[0].directionType", "J2000" );
  ps.add( "Observation.Beam[0].angle1", "1" );
  ps.add( "Observation.Beam[0].angle2", "1" );
  ps.add( "Observation.Beam[0].nrTiedArrayBeams", "0" );
  ps.add( "Observation.Beam[1].subbandList", "[6]" );
  ps.add( "Observation.Beam[1].directionType", "J2000" );
  ps.add( "Observation.Beam[1].angle1", "1" );
  ps.add( "Observation.Beam[1].angle2", "0" );
  ps.add( "Observation.Beam[1].nrTiedArrayBeams", "1" );
  ps.add( "Observation.Beam[1].TiedArrayBeam[0].directionType", "J2000" );
  ps.add( "Observation.Beam[1].TiedArrayBeam[0].angle1", "1" ); // Observation.Beam[1].angle1 + 0
  ps.add( "Observation.Beam[1].TiedArrayBeam[0].angle2", "1" ); // Observation.Beam[1].angle2 + 1
  ps.add( "Observation.Beam[1].TiedArrayBeam[0].coherent", "true" );
  ps.updateSettings();

  // blockSize is ~1s
  Delays delays(ps, 0, TimeStamp(time(0), 0, 200000000), dayOfSamples);

  Delays::AllDelays delaySet(ps), delaySetDummy(ps);

  std::chrono::steady_clock::time_point tStartTime = std::chrono::steady_clock::now();
  for (size_t block = 0; block < 10; ++block) {
    delays.getNextDelays(delaySet, delaySetDummy);

    // check dimensions of result
    CHECK_EQUAL(2U, delaySet.SAPs.size());
    CHECK_EQUAL(0U, delaySet.SAPs[0].TABs.size());
    CHECK_EQUAL(1U, delaySet.SAPs[1].TABs.size());

    // check values
    CHECK_CLOSE(delaySet.SAPs[0].SAP.delay, delaySet.SAPs[1].TABs[0].delay, 0.00001);
/*
    LOG_INFO_STR("block: " << block
      << " Delay: " << delaySet.SAPs[1].TABs[0].delay 
      << " Dir 0: " << delaySet.SAPs[1].TABs[0].direction[0]
      << " Dir 1: " << delaySet.SAPs[1].TABs[0].direction[1]
      << " Dir 2: " << delaySet.SAPs[1].TABs[0].direction[2]);
*/
  }
  std::chrono::steady_clock::time_point tEndTime = std::chrono::steady_clock::now();
  std::chrono::steady_clock::duration tDuration = tEndTime - tStartTime;
  double tDurationSec = double(tDuration.count()) * std::chrono::steady_clock::period::num / std::chrono::steady_clock::period::den;
  LOG_INFO_STR("Test Duration (sec): " << tDurationSec);
}

TEST(DelaysRePointingMultiple) {
  LOG_INFO("Test lmm rePointing");
  Parset ps;

  // Required keys to pass basic parset checks.
  ps.add("Observation.ObsID", "12345");
  // Use a valid station name (CS001) that is not in any tParset test,
  // so we don't have to remove board and slot list keys in some tests.
  ps.add("Observation.VirtualInstrument.stationList", "[CS001]");
  ps.add("Observation.antennaSet", "LBA_INNER");
  ps.add("Observation.bandFilter", "LBA_30_70");
  ps.add("Observation.nrBeams", "1");
  ps.add("Observation.Beam[0].subbandList", "[21..23]");
  ps.add("Observation.Dataslots.CS001LBA.RSPBoardList", "[3*0]");
  ps.add("Observation.Dataslots.CS001LBA.DataslotList", "[0..2]");

  ps.add("Observation.Beam[0].nrTiedArrayBeams", "1");
  ps.add("Observation.Beam[0].TiedArrayBeam[0].coherent", "true");

  // basic correlation output keys
  ps.add("Observation.DataProducts.Output_Correlated.enabled", "true");
  ps.add("Observation.DataProducts.Output_Correlated.filenames",
         "[L12345_SAP000_SB000_uv.MS, L12345_SAP000_SB001_uv.MS, L12345_SAP000_SB002_uv.MS]");
  ps.add("Observation.DataProducts.Output_Correlated.locations",
         "[3*localhost:tParset-data/]");

  // basic beamforming output keys
  ps.add("Observation.DataProducts.Output_CoherentStokes.enabled", "true");
  ps.add("Cobalt.BeamFormer.CoherentStokes.which", "I");

  // Delay compensation for this station
  ps.add("PIC.Core.CS001LBA.phaseCenter", "[3826923.546, 460915.441, 5064643.489]");
  // Phase compensation for this station (relative to CS002)
  ps.add("Observation.referencePhaseCenter", "[3826577.066, 461022.948, 5064892.786]"); 

  // Specify start and stop times
  ps.replace("Observation.startTime", "2021-04-08 14:58"); // 1617893880.0 in unix utc time
  double startTimeSpec = 1617893880.0;
  ps.replace("Observation.stopTime", "2021-04-08 15:58"); // 1617897480.0 in unix utc time
  double stopTimeSpec = 1617897480.0;
  // difference is 3600.0 in unix utc time

  // Specify base beamFormer
  ps.replace("Cobalt.BeamFormer.CoherentStokes.nrChannelsPerSubband", "16");
  ps.replace("Cobalt.BeamFormer.CoherentStokes.timeIntegrationFactor", "1");
  ps.replace("Cobalt.BeamFormer.CoherentStokes.which", "IQUV");

  // Disable correlator
  ps.replace("Observation.DataProducts.Output_Correlated.enabled", "false");

  // Set the number of beamFormer pipelines to 1
  ps.add("Cobalt.BeamFormer.nrPipelines", "1");

  // Specify 1 SAP, 3 TABs for reference and 1 TAB with 3 pointings for this beamFormer pipeline
  ps.add("Cobalt.BeamFormer.Pipeline[0].nrBeams", "1");
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].nrTiedArrayBeams", "4");

  // TAB 1
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[0].angle1", "10.0");
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[0].angle2", "10.0");
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[0].directionType", "J2000");

  // TAB 2
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[1].angle1", "20.0");
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[1].angle2", "20.0");
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[1].directionType", "AZEL");

  // TAB 3
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[2].angle1", "30.0");
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[2].angle2", "30.0");
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[2].directionType", "SUN");

  // TAB 4
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[3].nrPointings", "3");

  // Pointing 1
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[3].Pointing[0].duration", "1800"); //duration in seconds //1800
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[3].Pointing[0].angle1", "10.0");
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[3].Pointing[0].angle2", "10.0");
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[3].Pointing[0].directionType", "J2000");

  // Pointing 2
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[3].Pointing[1].duration", "800"); //duration in seconds // 800
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[3].Pointing[1].angle1", "20.0");
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[3].Pointing[1].angle2", "20.0");
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[3].Pointing[1].directionType", "AZEL");

  // Pointing 3
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[3].Pointing[2].duration", "800"); //duration in seconds //800
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[3].Pointing[2].angle1", "30.0");
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[3].Pointing[2].angle2", "30.0");
  ps.add("Cobalt.BeamFormer.Pipeline[0].Beam[0].TiedArrayBeam[3].Pointing[2].directionType", "SUN");

  // Observation has ~200 seconds to spare, for which there are no repointings.

  // Specify output files
  ps.add("Observation.DataProducts.Output_CoherentStokes.filenames",
    "[24*foo.h5]");
  ps.add("Observation.DataProducts.Output_CoherentStokes.locations",
       "[24*localhost:tParset-data/]");

  ps.updateSettings();

  /* blockSize is ~1s
  *  Dealys: parset, stationIdx, timestamp, time increment
  *  TimeStamp: start time, offset within second (blockID), clock (200 MHz)
  *  Time increment: nr samples in a block */
  Delays delays(ps, 0, TimeStamp::convert(ps.settings.startTime, ps.settings.clockHz()), ps.settings.blockSize); 

  // delays.logPointingIndices();

  Delays::AllDelays delaySet1(ps), delaySet2(ps);
  Delays::AllDelays *delaysAtBegin  = &delaySet1;
  Delays::AllDelays *delaysAfterEnd = &delaySet2;

  std::chrono::steady_clock::time_point tStartTime = std::chrono::steady_clock::now();

  // This test is similar to StationMetaData::computeMetaData()
  // Generate all the blocks. Start at block -1
  int pointing = 0;
  for (ssize_t block = -1; block < (ssize_t)ps.settings.nrBlocks(); ++block) {
    delays.getNextDelays(*delaysAtBegin, *delaysAfterEnd, block);
    
    // equivalence testing, relate pointings from 4th TAB to reference in TAB 0, 1, 2 depending on blockID
    // skip first block of each pointing (FIR history sample)
    if(block>1788) pointing = 1;
    if(block>2582) pointing = 2;
    if(block>3377) {
      // beyond last pointing
      pointing = -1;

      CHECK_EQUAL(0.0, delaySet1.SAPs[0].TABs[3].delay);
    } else if((block != 1789)&&(block != 2583)&&(block>=0))
    {
      LOG_DEBUG_STR("Block: " << block << " Re-pointing: " << pointing);
      CHECK(delaySet1.SAPs[0].TABs[pointing].delay != 0.0);
      CHECK_CLOSE(delaySet1.SAPs[0].TABs[3].delay, delaySet1.SAPs[0].TABs[pointing].delay, 0.00001);
      CHECK_CLOSE(delaySet1.SAPs[0].TABs[3].direction[0], delaySet1.SAPs[0].TABs[pointing].direction[0], 0.00001);
      CHECK_CLOSE(delaySet1.SAPs[0].TABs[3].direction[1], delaySet1.SAPs[0].TABs[pointing].direction[1], 0.00001);
      CHECK_CLOSE(delaySet1.SAPs[0].TABs[3].direction[2], delaySet1.SAPs[0].TABs[pointing].direction[2], 0.00001);
    }
    if(((int)block%100)==0)
    {
      LOG_DEBUG_STR("block: " << block << " Re-pointing: " << pointing 
      << " Delay: " << delaySet1.SAPs[0].TABs[3].delay 
      << " Dir 0: " << delaySet1.SAPs[0].TABs[3].direction[0]
      << " Dir 1: " << delaySet1.SAPs[0].TABs[3].direction[1]
      << " Dir 2: " << delaySet1.SAPs[0].TABs[3].direction[2]);

      if (pointing >= 0) {
        LOG_DEBUG_STR("block: " << block << " Reference: " << pointing 
        << " Delay: " << delaySet1.SAPs[0].TABs[pointing].delay  
        << " Dir 0: " << delaySet1.SAPs[0].TABs[pointing].direction[0]
        << " Dir 1: " << delaySet1.SAPs[0].TABs[pointing].direction[1]
        << " Dir 2: " << delaySet1.SAPs[0].TABs[pointing].direction[2]);
      }
    }
  }
  
  std::chrono::steady_clock::time_point tEndTime = std::chrono::steady_clock::now();
  std::chrono::steady_clock::duration tDuration = tEndTime - tStartTime;
  double tDurationSec = double(tDuration.count()) * std::chrono::steady_clock::period::num / std::chrono::steady_clock::period::den;
  LOG_INFO_STR("Test Duration (sec): " << tDurationSec);

  // check dimensions of result
  CHECK_EQUAL(1U, delaySet1.SAPs.size());
  CHECK_EQUAL(4U, delaySet1.SAPs[0].TABs.size());
   
  // Check start and stop times, time is unix time in UTC
  CHECK_CLOSE(startTimeSpec, ps.settings.startTime, 0.1);
  CHECK_CLOSE(stopTimeSpec, ps.settings.stopTime, 0.1);
  ssize_t nrBlocksExpectedTotal = floor(((stopTimeSpec - startTimeSpec) * (ps.settings.clockHz() / 1024)) / ps.settings.blockSize);
  CHECK_CLOSE( nrBlocksExpectedTotal, ps.settings.nrBlocks(), 1);
  CHECK_CLOSE(stopTimeSpec, ps.settings.getRealStopTime(), 1); //Observation Real Stop time
  
  // Check all pointings

  // TAB 3
  CHECK_EQUAL(3, ps.settings.beamFormer.pipelines[0].SAPs[0].TABs[3].pointings.size());
  CHECK_EQUAL(true, ps.settings.beamFormer.pipelines[0].SAPs[0].TABs[3].coherent);
}


int main()
{
  INIT_LOGGER( "tDelays" );

  // IERS table information
  struct Delays::IERS_tablestats stats = Delays::get_IERS_tablestats();

  LOG_INFO_STR("Using IERS table " << stats.realpath
            << ", last entry is " << TimeDouble::toString(stats.last_entry_timestamp, false)
            << ", table written on " << TimeDouble::toString(stats.last_fs_modification, false));

  return UnitTest::RunAllTests() > 0;
}

