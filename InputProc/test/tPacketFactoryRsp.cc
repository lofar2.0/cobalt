
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#include <lofar_config.h>

#include <CoInterface/RspStationPacket.h>
#include <InputProc/Station/PacketFactory.h>
#include <time.h>

using namespace LOFAR;
using namespace Cobalt;

void test() {
    struct BoardMode mode(16, 200);
    PacketFactory factory(mode);

    // Just generate packets.
    time_t now = time(0);
    TimeStamp start(now, 0, mode.clockHz());
    TimeStamp end(now + 1, 0, mode.clockHz());

    // The number of time slots per packet, which will
    // be read from the generated packets.
    size_t timesPerPacket = 16;

    for (TimeStamp i = start; i < end; i += timesPerPacket) {
        std::vector<uint8_t> dataFrame(RspStationPacket::PACKET_SIZE);
        std::shared_ptr<StationPacket> packet = std::make_shared<RspStationPacket>(dataFrame.data());

        factory.makePacket(packet, i, 0);
        timesPerPacket = packet->blockCount();

        // Basic sanity checks
        auto tmp = packet->timeStamp();
        ASSERT(tmp == i);

        // Prevent infinite loops
        ASSERT(timesPerPacket > 0);
    }
}

int main(int, char **) {
    INIT_LOGGER("tPacketFactory");

    // Don't run forever if communication fails for some reason
    alarm(10);

    test();
}

