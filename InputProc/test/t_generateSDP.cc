
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#include <lofar_config.h>

#include <cstdlib>    // for rand(), srand(), system()
#include <fstream>
#include <iostream>

#include <boost/format.hpp>

#include <CoInterface/SdpStationPacket.h>
#include <Common/LofarLogger.h>
#include <InputProc/Station/PacketReader.h>
#include <Stream/FileStream.h>

static const unsigned COMPLEX = 2;
static const unsigned NR_POLS = 2;
static const unsigned NR_BLOCKS = 16;

using namespace std;
using namespace boost;
using namespace LOFAR;
using namespace LOFAR::Cobalt;

void generateInput(ostream &os, unsigned bitMode,
                   unsigned nrPackets, unsigned nrSubbands) {
    srand(0);
    const unsigned mask = (1 << bitMode) - 1;
    const unsigned offset = mask >> 1;
    for (unsigned packet = 0; packet < nrPackets; packet++) {
        for (unsigned block = 0; block < NR_BLOCKS; block++) {
            for (unsigned subband = 0; subband < nrSubbands; subband++) {
                for (unsigned pol = 0; pol < NR_POLS; pol++) {
                    for (unsigned ri = 0; ri < COMPLEX; ri++) {
                        int sample = min((rand() & mask) - offset, offset);
                        os << sample << " ";
                    }
                }
            }
            os << endl;
        }
        os << endl;
    }
}

void readSdp(Stream &is, ostream &os, unsigned bitMode, unsigned nrSubbands) {
    PacketReader reader("", is);
    auto packet = StationPackets::create<SdpStationPacket>(1);
    complex<int> sample;
    try {
        while (true) {
            if (!reader.readPacket(packet)) {
                continue;
            }
            ASSERT(packet->packets[0]->bitMode() == bitMode);
            ASSERT(packet->packets[0]->beamletCount() == nrSubbands);
            ASSERT(packet->packets[0]->blockCount() == NR_BLOCKS);
            for (unsigned block = 0; block < NR_BLOCKS; block++) {
                for (unsigned subband = 0; subband < nrSubbands; subband++) {
                    for (char pol = 'X'; pol <= 'Y'; pol++) {
                        sample = packet->packets[0]->sample(subband, block, pol);
                        os << sample.real() << " " << sample.imag() << " ";
                    }
                }
                os << endl;
            }
            os << endl;
        }
    } catch (EndOfStreamException &) {}
}

int main() {
    INIT_LOGGER("t_generateSDP");

    unsigned bitMode[] = {16, 8, 4};
    unsigned nrPackets[] = {1, 2, 3, 5};
    unsigned nrSubbands[] = {1, 37, 61, 103, 122, 156, 244, 250};
    string ascFile, rspFile, outFile, command;

    for (unsigned b = 0; b < sizeof(bitMode) / sizeof(unsigned); b++) {
        for (unsigned p = 0; p < sizeof(nrPackets) / sizeof(unsigned); p++) {
            for (unsigned s = 0; s < sizeof(nrSubbands) / sizeof(unsigned); s++) {

                cout << "bitmode: " << bitMode[b] << ", "
                     << "nrPackets: " << nrPackets[p] << ", "
                     << "nrSubbands: " << nrSubbands[s] << endl;

                // Skip if number of subbands exceeds capacity of one RSP board
                if (nrSubbands[s] > 122 * 8 / bitMode[b]) {
                    break;
                }

                ascFile = str(format("t_generateSDP_tmp_b%d_p%d_s%d.asc") %
                              bitMode[b] % nrPackets[p] % nrSubbands[s]);
                rspFile = str(format("t_generateSDP_tmp_b%d_p%d_s%d.rsp") %
                              bitMode[b] % nrPackets[p] % nrSubbands[s]);
                outFile = str(format("t_generateSDP_tmp_b%d_p%d_s%d.out") %
                              bitMode[b] % nrPackets[p] % nrSubbands[s]);

                ofstream ascStream(ascFile.c_str());
                generateInput(ascStream, bitMode[b], nrPackets[p], nrSubbands[s]);

                command = str(
                        format("generateStationPacket -b%d -s%d < %s -o file+sdp:%s") %
                        bitMode[b] % nrSubbands[s] % ascFile % rspFile);
                cout << "Executing command: " << command << endl;
                ASSERT(system(command.c_str()) == 0);

                FileStream rspStream(rspFile);
                ofstream outStream(outFile.c_str());
                readSdp(rspStream, outStream, bitMode[b], nrSubbands[s]);

                command = str(format("/usr/bin/diff -q %s %s") % ascFile % outFile);
                cout << "Executing command: " << command << endl;
                ASSERT(system(command.c_str()) == 0);
            }
        }
    }
}

