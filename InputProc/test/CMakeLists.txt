# $Id$

include(LofarCTest)

if(UNITTEST++_FOUND)
  lofar_add_test(tRanges tRanges.cc)

  lofar_add_test(tDelays tDelays.cc)
endif(UNITTEST++_FOUND)

lofar_add_test(tPacketReaderRsp tPacketReaderRsp.cc)
lofar_add_test(tPacketReaderSdp tPacketReaderSdp.cc)
lofar_add_test(tPacketFactoryRsp tPacketFactoryRsp.cc)
lofar_add_test(tPacketFactorySdp tPacketFactorySdp.cc)
lofar_add_test(tGeneratorRsp tGeneratorRsp.cc)
lofar_add_test(tGeneratorSdp tGeneratorSdp.cc)
lofar_add_test(t_generateRSP t_generateRSP.cc)
lofar_add_test(t_generateSDP t_generateSDP.cc)

if(UNITTEST++_FOUND)
  lofar_add_test(tMPIUtil2 tMPIUtil2.cc)
endif(UNITTEST++_FOUND)

if(MPI_FOUND)
  lofar_add_test(tMPI tMPI.cc)
endif(MPI_FOUND)

