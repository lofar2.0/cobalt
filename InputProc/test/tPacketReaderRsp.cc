
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#include <lofar_config.h>

#include <string>

#include <Common/LofarLogger.h>
#include <Stream/FileStream.h>
#include <Stream/SocketStream.h>

#include <CoInterface/OMPThread.h>
#include <CoInterface/RspStationPacket.h>
#include <InputProc/Station/PacketReader.h>

#include <UnitTest++.h>

using namespace LOFAR;
using namespace Cobalt;

void test(const std::string &filename, unsigned bitmode, unsigned nrPackets) {
    FileStream fs(filename);

    PacketReader reader("", fs);

    auto packet = StationPackets::create<RspStationPacket>(1);

    // We should be able to read these packets
    for (size_t i = 0; i < nrPackets; ++i) {
        ASSERT(reader.readPacket(packet));
        ASSERT(packet->packets[0]->bitMode() == bitmode);
        ASSERT(packet->packets[0]->clockMHz() == 200);
    }

    // The file contains no more packets; test if readPacket survives
    // a few calls on the rest of the stream.
    for (size_t i = 0; i < 3; ++i) {
        try {
            ASSERT(!reader.readPacket(packet));
        } catch (EndOfStreamException &ex) {
            // expected
        }
    }
}

TEST (16bit) {
    test("tPacketReaderRsp.in_16bit", 16, 2);
}

TEST (8bit) {
    test("tPacketReaderRsp.in_8bit", 8, 2);
}

TEST (recvmmsg_killable) {
    SocketStream ss("localhost", 0, SocketStream::Protocol::UDP, SocketStream::Mode::Server, 0);
    PacketReader reader("", ss);
    bool success = false;

    OMPThreadSet packetReaderThread("recvmmsg_killable");

    OMPThread::init();

    #pragma omp parallel sections num_threads(2)
    {
        #pragma omp section
        {
            try {
                OMPThreadSet::ScopedRun sr(packetReaderThread);

                auto packets = StationPackets::create<RspStationPacket>(1024);

                // read packets -- will block as there is no sender
                reader.readPackets(packets);
            } catch (OMPThreadSet::CannotStartException &ex) {
                LOG_ERROR_STR("Killed reading thread too early. Fix test.");
            } catch (SystemCallException &ex) {
                LOG_INFO_STR("Caught exception: " << ex.what());

                if (ex.error == EINTR) {
                    success = true;
                }
            }
        }

        #pragma omp section
        {
            sleep(1);

            packetReaderThread.killAll();
        }
    }

            CHECK(success);
}

int main() {
    INIT_LOGGER("tPacketReaderRsp");

    return UnitTest::RunAllTests() > 0;
}

