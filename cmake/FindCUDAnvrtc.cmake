# - Try to find the CUDA nvToolsExt library
# Variables used by this module:
#  CUDA_TOOLKIT_ROOT_DIR     - CUDA toolkit root directory
# Variables defined by this module:
#  CUDA_nvToolsExt_FOUND        - system has CUDA_nvToolsExt_LIBRARY
#  CUDA_nvToolsExt_LIBRARY      - the CUDA_nvToolsExt_LIBRARY library

# Copyright (C) 2020
# ASTRON (Netherlands Institute for Radio Astronomy)
# Oude Hoogeveensedijk 4, 7991 PD Dwingeloo, The Netherlands
#
# This file is part of the LOFAR software suite.
# The LOFAR software suite is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# The LOFAR software suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.

if(NOT CUDA_nvrtc_LIBRARY_FOUND)

find_library(
  CUDA_nvrtc_LIBRARY
  NAMES nvrtc
  HINTS ${CUDA_TOOLKIT_ROOT_DIR}
  PATH_SUFFIXES lib64)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(CUDA_nvrtc_LIBRARY DEFAULT_MSG CUDA_nvrtc_LIBRARY)


endif(NOT CUDA_nvrtc_LIBRARY_FOUND)
