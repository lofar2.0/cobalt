# This module tries to find the Power Measurement Toolkit library on your system
#
# Once done this will define
#  PMT_FOUND       - system has Power Measurement Toolkit
#  PMT_INCLUDE_DIR - the Power Measurement Toolkit include directory
#  PMT_LIBRARY     - link these to use Power Measurement Toolkit

find_package(PackageHandleStandardArgs)

find_path(
  PMT_ROOT_DIR
  NAMES include/pmt.h
  PATHS ENV PMT_ROOT)

find_path(
  PMT_INCLUDE_DIR
  NAMES pmt.h
  HINTS ${PMT_ROOT_DIR}
  PATH_SUFFIXES include)

find_library(
  PMT_LIBRARY
  NAMES pmt
  HINTS ${PMT_ROOT_DIR}
  PATH_SUFFIXES lib)

mark_as_advanced(PMT_ROOT_DIR)

find_package_handle_standard_args(PMT DEFAULT_MSG PMT_LIBRARY PMT_INCLUDE_DIR)
