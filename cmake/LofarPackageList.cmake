# - Create for each LOFAR package a variable containing the absolute path to
# its source directory. 
#
# Generated by gen_LofarPackageList_cmake.sh at ma  5 dec 2022 15:15:08 CET
#
#                      ---- DO NOT EDIT ----
#
# ATTENTION: This file must be included BEFORE calling the lofar_package()
# macro; either directly, or indirectly.
#
# NOTE: This file must be kept up-to-date when LOFAR packages are added,
# moved, or deleted. You can use gen_LofarPackageList_cmake.sh to regenerate
# this file.
#
if(NOT DEFINED LOFAR_PACKAGE_LIST_INCLUDED)
  set(LOFAR_PACKAGE_LIST_INCLUDED TRUE)
  set(Cobalt_SOURCE_DIR ${CMAKE_SOURCE_DIR})
  set(InputProc_SOURCE_DIR ${CMAKE_SOURCE_DIR}/InputProc)
  set(OutputProc_SOURCE_DIR ${CMAKE_SOURCE_DIR}/OutputProc)
  set(GPUProc_SOURCE_DIR ${CMAKE_SOURCE_DIR}/GPUProc)
  set(CoInterface_SOURCE_DIR ${CMAKE_SOURCE_DIR}/CoInterface)
  set(CobaltTest_SOURCE_DIR ${CMAKE_SOURCE_DIR}/CobaltTest)
  set(BrokenAntennaInfo_SOURCE_DIR ${CMAKE_SOURCE_DIR}/BrokenAntennaInfo)
  set(TBBWriter_SOURCE_DIR ${CMAKE_SOURCE_DIR}/TBBWriter)

  set(Online_Cobalt_SOURCE_DIR ${CMAKE_SOURCE_DIR}/Online_Cobalt)
  set(Online_OutputProc_SOURCE_DIR ${CMAKE_SOURCE_DIR}/Online_OutputProc)

  set(Docker_SOURCE_DIR ${CMAKE_SOURCE_DIR}/Docker)

  set(dependencies_SOURCE_DIR ${CMAKE_SOURCE_DIR}/dependencies)

  set(Deployment_SOURCE_DIR ${CMAKE_SOURCE_DIR}/dependencies/Deployment)
  set(StaticMetaData_SOURCE_DIR ${CMAKE_SOURCE_DIR}/dependencies/Deployment/data/StaticMetaData)

  set(PyCommon_SOURCE_DIR ${CMAKE_SOURCE_DIR}/dependencies/PyCommon)
  set(MSLofar_SOURCE_DIR ${CMAKE_SOURCE_DIR}/dependencies/MSLofar)
  set(pyparameterset_SOURCE_DIR ${CMAKE_SOURCE_DIR}/dependencies/pyparameterset)
  set(pytools_SOURCE_DIR ${CMAKE_SOURCE_DIR}/dependencies/pytools)
  set(LofarStMan_SOURCE_DIR ${CMAKE_SOURCE_DIR}/dependencies/LofarStMan)
  set(Common_SOURCE_DIR ${CMAKE_SOURCE_DIR}/dependencies/Common)
  set(ApplCommon_SOURCE_DIR ${CMAKE_SOURCE_DIR}/dependencies/ApplCommon)
  set(Stream_SOURCE_DIR ${CMAKE_SOURCE_DIR}/dependencies/Stream)
  set(OTDB_SOURCE_DIR ${CMAKE_SOURCE_DIR}/dependencies/OTDB)
endif(NOT DEFINED LOFAR_PACKAGE_LIST_INCLUDED)
