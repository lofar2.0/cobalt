# This module tries to find the Binutils library on your system
#
# Once done this will define
#  BINUTILS_FOUND       - system has Binutils
#  BINUTILS_INCLUDE_DIR - the Binutils include directory
#  BINUTILS_LIBRARY     - link these to use Binutils

find_package(PackageHandleStandardArgs)

find_path(
  BINUTILS_INCLUDE_DIR
  NAMES bfd.h
  PATHS /usr
  HINTS ${BINUTILS_ROOT_DIR}
  PATH_SUFFIXES include)

get_filename_component(
  BINUTILS_ROOT_DIR
  ${BINUTILS_INCLUDE_DIR} DIRECTORY)

find_library(
  BINUTILS_LIBRARY
  NAMES bfd
  HINTS ${BINUTILS_ROOT_DIR}
  PATH_SUFFIXES lib lib64)

mark_as_advanced(BINUTILS_ROOT_DIR)

find_package_handle_standard_args(BINUTILS DEFAULT_MSG BINUTILS_LIBRARY BINUTILS_INCLUDE_DIR)
