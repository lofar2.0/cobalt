//# MSWriterDAL.cc: an implementation of MSWriter using the DAL to write HDF5
//# Copyright (C) 2011-2015  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

#include <lofar_config.h>

#ifndef HAVE_DAL
#  warning The outputProc writer may be built without DAL, but will not write beamformed HDF5 output
#else

#include "MSWriterDAL.h"

#include <cmath>
#include <numeric>
#include <boost/format.hpp>

#include <Common/LofarLogger.h>
#ifdef basename // some glibc have this as a macro
#undef basename
#endif
#include <Common/SystemUtil.h>
#include <Common/StreamUtil.h>
#include <Common/Thread/Mutex.h>
#include <CoInterface/StreamableData.h>
#include <CoInterface/LTAFeedback.h>
#include <CoInterface/CommonLofarAttributes.h>

#include <dal/lofar/BF_File.h>
#include <dal/dal_version.h>


using namespace std;
using namespace dal;
using boost::format;

static string stripextension( const string pathname )
{
  size_t endPosDot   = pathname.rfind('.');
  size_t endPosSlash = pathname.rfind('/');
  size_t endPos = string::npos;

  // only strip if there is a '.' in the last component (filename)
  if (endPosDot != string::npos &&
      (endPosSlash == string::npos || endPosSlash < endPosDot))
    endPos = endPosDot;
  return pathname.substr(0, endPos);
}

static string forceextension( const string pathname, const string extension )
{
  return stripextension(pathname) + extension;
}

namespace LOFAR
{

  namespace Cobalt
  {
    // Prevent concurrent access to HDF5, which may not be compiled thread-safe. The Thread-safe version
    // uses global locks too anyway.
    static Mutex HDF5Mutex;

    template <typename T,unsigned DIM, unsigned FLAGS_DIM>
    MSWriterDAL<T,DIM,FLAGS_DIM>::MSWriterDAL (const string &filename,
     const Parset &parset,
     unsigned fileno)
      :
      MSWriterFile(forceextension(string(filename), ".raw")),
      itsFilename(filename),
      itsParset(parset),
      itsNextSeqNr(0),
      itsFileNr(fileno)
    {
      // add re-pointing here

      const struct ObservationSettings::BeamFormer::File &f = itsParset.settings.beamFormer.files[itsFileNr];


      auto& tab = itsParset.settings.beamFormer.SAPs[f.sapNr].TABs[f.tabNr];
      auto& pipeline = itsParset.settings.beamFormer.pipelines[tab.pipelineNr];

      const struct ObservationSettings::BeamFormer::StokesSettings &stokesSet =
        f.coherent ? pipeline.coherentSettings
                   : pipeline.incoherentSettings;

      // update expected number of blocks, for re-pointings
      itsNrExpectedBlocks=f.nrBlocks();
      
      const ObservationSettings::BeamFormer::StokesSettings::QuantizerSettings &quantizer =
               stokesSet.quantizerSettings;

      itsQuantizerEnabled=quantizer.enabled;

      // All subbands in the SAP that we store in this file.
      // We could have multiple SAPs and/or have split up the subbands over multiple files (parts).
      unsigned nrSubbands = f.lastSubbandIdx - f.firstSubbandIdx;

      itsNrChannels = stokesSet.nrChannels * nrSubbands; 
      itsNrSamples = itsParset.settings.blockSize /
                     stokesSet.nrChannels / stokesSet.timeIntegrationFactor;

      itsBlockSize = itsNrSamples * itsNrChannels;

      // Add file-specific processing feedback
      LTAFeedback fb(itsParset.settings);
      itsConfiguration.adoptCollection(fb.beamFormedFeedback(itsFileNr));
      itsConfigurationPrefix = fb.beamFormedPrefix(itsFileNr);
      if (itsQuantizerEnabled) {
        string rawSfilename = forceextension(itsFilename, ".scale");
        string rawOfilename = forceextension(itsFilename, ".offset");
        int file_flags=O_RDWR | O_CREAT | O_TRUNC;
        int file_mode=S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
#ifdef USE_O_DIRECT
        itsSFile=std::unique_ptr<FastFileStream>(new FastFileStream(rawSfilename, file_flags, file_mode));
        itsOFile=std::unique_ptr<FastFileStream>(new FastFileStream(rawOfilename, file_flags, file_mode));
#else
        itsSFile=std::unique_ptr<FileStream>(new FileStream(rawSfilename, file_flags, file_mode));
        itsOFile=std::unique_ptr<FileStream>(new FileStream(rawOfilename, file_flags, file_mode));
#endif
      }
    }

    template <typename T,unsigned DIM, unsigned FLAGS_DIM>
    void MSWriterDAL<T,DIM,FLAGS_DIM>::init()
    {
      string h5filename = forceextension(itsFilename, ".h5");
      string rawfilename = forceextension(itsFilename, ".raw");

      ScopedLock sl(HDF5Mutex);

#if 0
      // install our own error handler
      H5Eset_auto_stack(H5E_DEFAULT, my_hdf5_error_handler, NULL);
#endif

      const struct ObservationSettings::BeamFormer::File &f = itsParset.settings.beamFormer.files[itsFileNr];

      const unsigned sapNr = f.sapNr;
      const unsigned beamNr = f.tabNr;
      const unsigned stokesNr = f.stokesNr;
      auto& tab = itsParset.settings.beamFormer.SAPs.at(f.sapNr).TABs.at(f.tabNr);
      // Flag to denote this is a repointing
      bool rePointing=(f.pointingNr>=0);

      const struct ObservationSettings::BeamFormer::StokesSettings &stokesSet =
        f.coherent ? itsParset.settings.beamFormer.pipelines[tab.pipelineNr].coherentSettings
                   : itsParset.settings.beamFormer.pipelines[tab.pipelineNr].incoherentSettings;

      //*******************************

      // All subbands in the SAP that we store in this file.
      // We could have multiple SAPs and/or have split up the subbands over multiple files (parts).
      unsigned firstSubbandIdx = f.firstSubbandIdx;
      unsigned nrSubbands = f.lastSubbandIdx - f.firstSubbandIdx;

      //*******************************

      vector<string> stokesVars;
      vector<string> stokesVars_LTA;


      switch (stokesSet.type) {
      case STOKES_I:
        stokesVars.push_back("I");
        stokesVars_LTA = stokesVars;
        break;

      case STOKES_IQUV:
        stokesVars.push_back("I");
        stokesVars.push_back("Q");
        stokesVars.push_back("U");
        stokesVars.push_back("V");
        stokesVars_LTA = stokesVars;
        break;

      case STOKES_XXYY:
        stokesVars.push_back("Xr");
        stokesVars.push_back("Xi");
        stokesVars.push_back("Yr");
        stokesVars.push_back("Yi");
        stokesVars_LTA.push_back("Xre");
        stokesVars_LTA.push_back("Xim");
        stokesVars_LTA.push_back("Yre");
        stokesVars_LTA.push_back("Yim");
        break;

      case INVALID_STOKES:
        LOG_ERROR("MSWriterDAL asked to write INVALID_STOKES");
        return;
      }

      LOG_DEBUG_STR("MSWriterDAL: opening " << h5filename);

      // create the top structure
      BF_File file(h5filename, BF_File::CREATE);

      // Common Attributes
      writeCommonLofarAttributes(file, itsParset);

      // BF_File specific root group parameters
      file.createOfflineOnline().value = itsParset.settings.realTime ? "Online" : "Offline";
      file.BFFormat().value = "TAB";
      file.BFVersion().value = str(format("Cobalt2/OutputProc using DAL %s and HDF5 %s") % dal::version().to_string() % dal::version_hdf5().to_string());

      file.totalIntegrationTime().value = itsNrExpectedBlocks * itsParset.settings.blockDuration();
      file.totalIntegrationTimeUnit().value = "s";

      //file.subArrayPointingDiameter().value = 0.0;
      //file.subArrayPointingDiameterUnit().value = "arcmin";
      file.bandwidth().value = itsParset.settings.subbands.size() * itsParset.settings.subbandWidth() / 1e6;
      file.bandwidthUnit().value = "MHz";
      //file.beamDiameter()            .value = 0.0;
      //file.beamDiameterUnit()          .value = "arcmin";

      file.observationNofSubArrayPointings().value = itsParset.settings.SAPs.size();
      file.nofSubArrayPointings().value = 1;

      // SysLog group -- empty for now
      file.sysLog().create();

      // information about the station beam (SAP)
      BF_SubArrayPointing sap = file.subArrayPointing(sapNr);
      sap.create();
      sap.groupType().value = "SubArrayPointing";

      // ToDo: increase accuracy from double to long double for toUTC and toMJD methods
      sap.expTimeStartUTC().value = file.observationStartUTC().value;
      sap.expTimeStartMJD().value = file.observationStartMJD().value;
      sap.expTimeEndUTC().value = file.observationEndUTC().value;
      sap.expTimeEndMJD().value = file.observationEndMJD().value;

      LOG_DEBUG_STR("File streamNr: " << f.streamNr << " sapNr: " << f.sapNr << " tabNr: " << f.tabNr << " pointingNr: " << f.pointingNr);
      LOG_DEBUG_STR("File nrBlocks: " << f.nrBlocks() << " blockStart: " << f.blockStart << " blockEnd: " << f.blockEnd);

      // TODO: fix the system to use the itsParset.beamDuration(sapNr), but OLAP
      // does not work that way yet (beamDuration is currently unsupported).
      sap.totalIntegrationTime().value = itsNrExpectedBlocks * itsParset.settings.blockDuration();
      sap.totalIntegrationTimeUnit().value = "s";

      const struct ObservationSettings::Direction &beamDir = itsParset.settings.SAPs[sapNr].direction;
      sap.pointDirectionType().value = beamDir.type;
      sap.pointRA().value = beamDir.angle1 * 180.0 / M_PI;
      sap.pointRAUnit().value = "deg";
      sap.pointDEC().value = beamDir.angle2 * 180.0 / M_PI;
      sap.pointDECUnit().value = "deg";

      unsigned nofBeams = 0;
      for (auto& pipeline : itsParset.settings.beamFormer.pipelines)
      {
        for (auto& sap : pipeline.SAPs)
        {
          nofBeams += sap.TABs.size();
        }
      }
      sap.observationNofBeams().value = nofBeams;
      sap.nofBeams().value = 1;

      BF_ProcessingHistory sapHistory = sap.processHistory();
      sapHistory.create();
      sapHistory.groupType().value = "ProcessingHistory";

      Attribute<string> sapObservationParset(sapHistory, "OBSERVATION_PARSET");
      string itsParsetAsString;
      itsParset.writeBuffer(itsParsetAsString);

      sapObservationParset.value = itsParsetAsString;

      // information about the pencil beam

      BF_BeamGroup beam = sap.beam(beamNr);
      beam.create();
      beam.groupType().value = "Beam";

      auto& pipeline = itsParset.settings.beamFormer.pipelines[tab.pipelineNr];
      auto& antennaFieldNames = pipeline.antennaFieldNames;

      if (pipeline.doFlysEye) {
        beam.nofStations().value = 1;
        auto& antennaFieldName = itsParset.settings.beamFormer.pipelines.at(tab.pipelineNr).antennaFieldNames.at(tab.localTabNr);
        beam.stationsList().value = vector<string>(1, antennaFieldName.fullName());
      } else {
        beam.nofStations().value = antennaFieldNames.size();
        beam.stationsList().value = ObservationSettings::AntennaFieldName::names(antennaFieldNames);
      }

      // ToDo: increase accuracy from double to long double for toUTC and toMJD methods
      double beamStartTime=itsParset.settings.getBlockStartTime(f.blockStart);
      beam.beamStartUTC().value = toUTC(beamStartTime);
      beam.beamStartMJD().value = toMJD(beamStartTime);

      double realBeamStopTime=itsParset.settings.getBlockStopTime(f.blockEnd);
      beam.beamEndUTC().value = toUTC(realBeamStopTime);
      beam.beamEndMJD().value = toMJD(realBeamStopTime);

      // use SAP target name until TAB target name can be specified in TMSS
      //const string &tab_target = rePointing ? tab.pointings[f.pointingNr].target : tab.target;
      //const vector<string> beamtargets(1, tab_target != "" ? tab_target : itsParset.settings.SAPs[sapNr].target);
      const vector<string> beamtargets(1, itsParset.settings.SAPs[sapNr].target);

      beam.targets().value = beamtargets;
      beam.tracking().value = itsParset.settings.SAPs[sapNr].direction.type;


      // add pointing information here tab.pointings[].direction
      const struct ObservationSettings::Direction &tabDir = tab.direction;
      double angle1=(rePointing?tab.pointings[f.pointingNr].direction.angle1:tabDir.angle1);
      double angle2=(rePointing?tab.pointings[f.pointingNr].direction.angle2:tabDir.angle2);
      beam.pointDirectionType().value = tabDir.type;
      beam.pointRA().value = angle1 * 180.0 / M_PI;
      beam.pointRAUnit().value = "deg";
      beam.pointDEC().value = angle2 * 180.0 / M_PI;
      beam.pointDECUnit().value = "deg";
      beam.pointOffsetRA().value = (angle1 - beamDir.angle1) * 180.0 / M_PI; // TODO: missing projection bug (also below for angle2)
      beam.pointOffsetRAUnit().value = "deg";
      beam.pointOffsetDEC().value = (angle2 - beamDir.angle2) * 180.0 / M_PI;
      beam.pointOffsetDECUnit().value = "deg";


      beam.subbandWidth().value = itsParset.settings.subbandWidth();
      beam.subbandWidthUnit().value = "Hz";

      beam.beamDiameterRA().value = 0;
      beam.beamDiameterRAUnit().value = "arcmin";
      beam.beamDiameterDEC().value = 0;
      beam.beamDiameterDECUnit().value = "arcmin";

      beam.nofSamples().value = itsNrSamples * itsNrExpectedBlocks;
      beam.samplingRate().value = itsParset.settings.subbandWidth() / stokesSet.nrChannels / stokesSet.timeIntegrationFactor;
      beam.samplingRateUnit().value = "Hz";
      beam.samplingTime().value = itsParset.settings.sampleDuration() * stokesSet.nrChannels * stokesSet.timeIntegrationFactor;
      beam.samplingTimeUnit().value = "s";

      beam.channelsPerSubband().value = stokesSet.nrChannels;
      const double channelBandwidth = itsParset.settings.subbandWidth() / stokesSet.nrChannels;
      beam.channelWidth().value = channelBandwidth;
      beam.channelWidthUnit().value = "Hz";

      // First, init tmp vector for the whole obs, regardless which SAP and subbands (parts) this file contains.
      vector<double> subbandCenterFrequencies(itsParset.settings.subbands.size());
      for (unsigned sb = 0; sb < subbandCenterFrequencies.size(); ++sb) {
        subbandCenterFrequencies[sb] = itsParset.settings.subbands[sb].centralFrequency;
      }

      vector<double> beamCenterFrequencies(nrSubbands, 0.0);
      for (unsigned sb = 0; sb < nrSubbands; sb++) {
        beamCenterFrequencies[sb] = subbandCenterFrequencies[firstSubbandIdx + sb];
      }

      const double beamCenterFrequencySum = accumulate(beamCenterFrequencies.begin(), beamCenterFrequencies.end(), 0.0);
      const double frequencyOffsetPPF = stokesSet.nrChannels > 1 ? // See getFrequencyOffsetPPF() for why/how.
                                        getFrequencyOffsetPPF(itsParset.settings.subbandWidth(), stokesSet.nrChannels) :
                                        0.0;
      beam.beamFrequencyCenter().value = (beamCenterFrequencySum / nrSubbands - frequencyOffsetPPF) / 1e6;
      beam.beamFrequencyCenterUnit().value = "MHz";

      const double DM = itsParset.settings.corrections.dedisperse ? tab.dispersionMeasure : 0.0;

      beam.foldedData().value = false;
      beam.foldPeriod().value = 0.0;
      beam.foldPeriodUnit().value = "s";

      beam.dedispersion().value = DM == 0.0 ? "NONE" : "COHERENT";
      beam.dispersionMeasure().value = DM;
      beam.dispersionMeasureUnit().value = "pc/cm^3";

      beam.barycentered().value = false;

      beam.observationNofStokes().value = stokesSet.nrStokes;
      beam.nofStokes().value = 1;

      const vector<string> stokesComponents(1, stokesVars[stokesNr]);

      beam.stokesComponents().value = stokesComponents;
      beam.complexVoltage().value = stokesSet.type == STOKES_XXYY;
      beam.signalSum().value = stokesSet.coherent ? "COHERENT" : "INCOHERENT";

      BF_ProcessingHistory beamHistory = beam.processHistory();
      beamHistory.create();

      Attribute<string> beamObservationParset(beamHistory, "OBSERVATION_PARSET");

      beamObservationParset.value = itsParsetAsString;

      CoordinatesGroup coordinates = beam.coordinates();
      coordinates.create();
      coordinates.groupType().value = "Coordinates";

      coordinates.refLocationValue().value = itsParset.settings.delayCompensation.referencePhaseCenter;
      coordinates.refLocationUnit().value = vector<string>(3, "m");
      coordinates.refLocationFrame().value = "ITRF";

      coordinates.refTimeValue().value = toMJD(itsParset.settings.startTime);
      coordinates.refTimeUnit().value = "d";
      coordinates.refTimeFrame().value = "MJD";

      coordinates.nofCoordinates().value = 2;
      coordinates.nofAxes().value = 2;

      vector<string> coordinateTypes(2);
      coordinateTypes[0] = "Time"; // or TimeCoord ?
      coordinateTypes[1] = "Spectral"; // or SpectralCoord ?
      coordinates.coordinateTypes().value = coordinateTypes;

      const vector<double> unitvector(1, 1);

      std::unique_ptr<TimeCoordinate> timeCoordinate(dynamic_cast<TimeCoordinate*>(coordinates.coordinate(0)));
      timeCoordinate.get()->create();
      timeCoordinate.get()->groupType().value = "TimeCoord";

      timeCoordinate.get()->coordinateType().value = "Time";
      timeCoordinate.get()->storageType().value = vector<string>(1,"Linear");
      timeCoordinate.get()->nofAxes().value = 1;
      timeCoordinate.get()->axisNames().value = vector<string>(1,"Time");
      timeCoordinate.get()->axisUnits().value = vector<string>(1,"us");

      // linear coordinates:
      //   referenceValue = offset from starting time, in axisUnits
      //   referencePixel = offset from first sample
      //   increment      = time increment for each sample
      //   pc             = scaling factor (?)

      timeCoordinate.get()->referenceValue().value = 0;
      timeCoordinate.get()->referencePixel().value = 0;
      timeCoordinate.get()->increment().value = itsParset.sampleDuration() * stokesSet.nrChannels * stokesSet.timeIntegrationFactor;
      timeCoordinate.get()->pc().value = unitvector;

      timeCoordinate.get()->axisValuesPixel().value = vector<unsigned>(1, 0); // not used
      timeCoordinate.get()->axisValuesWorld().value = vector<double>(1, 0.0); // not used

      std::unique_ptr<SpectralCoordinate> spectralCoordinate(dynamic_cast<SpectralCoordinate*>(coordinates.coordinate(1)));
      spectralCoordinate.get()->create();
      spectralCoordinate.get()->groupType().value = "SpectralCoord";

      spectralCoordinate.get()->coordinateType().value = "Spectral";
      spectralCoordinate.get()->storageType().value = vector<string>(1,"Tabular");
      spectralCoordinate.get()->nofAxes().value = 1;
      spectralCoordinate.get()->axisNames().value = vector<string>(1,"Frequency");
      spectralCoordinate.get()->axisUnits().value = vector<string>(1,"Hz");

      spectralCoordinate.get()->referenceValue().value = 0; // not used
      spectralCoordinate.get()->referencePixel().value = 0; // not used
      spectralCoordinate.get()->increment().value = 0;      // not used
      spectralCoordinate.get()->pc().value = unitvector;             // not used

      // tabular coordinates:
      //   axisValuePixel = data indices
      //   axisValueWorld = corresponding (central) frequencies

      vector<unsigned> spectralPixels;
      vector<double> spectralWorld;

      for (unsigned sb = 0; sb < nrSubbands; sb++) {
        const double subbandBeginFreq = itsParset.channel0Frequency( firstSubbandIdx + sb, stokesSet.nrChannels );

        // NOTE: channel 0 will be wrongly annotated if nrChannels > 1, because it is a combination of the
        // highest and the lowest frequencies (half a channel each).
        for (unsigned ch = 0; ch < stokesSet.nrChannels; ch++) {
          spectralPixels.push_back(spectralPixels.size());
          spectralWorld.push_back(subbandBeginFreq + ch * channelBandwidth);
        }
      }

      spectralCoordinate.get()->axisValuesPixel().value = spectralPixels;
      spectralCoordinate.get()->axisValuesWorld().value = spectralWorld;


      vector<ssize_t> dims(2), maxdims(2);

      dims[0] = itsNrSamples * itsNrExpectedBlocks;
      dims[1] = itsNrChannels;

      maxdims[0] = -1;
      maxdims[1] = itsNrChannels;

      const vector<int> stokesQuantized(1, itsQuantizerEnabled);
      beam.quantized().value=stokesQuantized;
      if (!itsQuantizerEnabled) {
        BF_StokesDataset stokesDS = beam.stokes(stokesNr);
        stokesDS.create(dims, maxdims, LOFAR::basename(rawfilename), BF_StokesDataset::LITTLE);
        stokesDS.groupType().value = "bfData";
        stokesDS.dataType().value = "float";
        stokesDS.stokesComponent().value = stokesVars[stokesNr];
        stokesDS.nofChannels().value = vector<unsigned>(nrSubbands, stokesSet.nrChannels);
        stokesDS.nofSubbands().value = nrSubbands;
        stokesDS.nofSamples().value = dims[0];
      } else {
       string rawSfilename = forceextension(itsFilename, ".scale");
       string rawOfilename = forceextension(itsFilename, ".offset");
       vector<ssize_t> sodims(2);
       sodims[0]=itsNrExpectedBlocks; // one value for each NrSamples
       sodims[1]=itsNrChannels;
      
       bool unsignedData= stokesVars[stokesNr]=="I";
       BF_QStokesDataset qstokes(beam.qstokes(stokesNr));
       qstokes.create();
       qstokes.groupType().value = "bfData";
       qstokes.dataType().value = unsignedData?"unsigned char":"signed char"; //from dal/hdf5/types/h5typemap.h
       qstokes.scaleOffsetType().value = "float";
       qstokes.stokesComponent().value = stokesVars[stokesNr];
       qstokes.nofChannels().value = vector<unsigned>(nrSubbands, stokesSet.nrChannels);
       qstokes.nofSubbands().value = nrSubbands;
       qstokes.nofSamples().value = dims[0];
       qstokes.nofSamplesPerBlock().value=itsNrSamples;
       if (unsignedData) {
         dal::Dataset<uint8_t> qdata=qstokes.data<uint8_t>();
         qdata.create(dims, maxdims, LOFAR::basename(rawfilename), Dataset<uint8_t>::LITTLE);
       } else {
         dal::Dataset<int8_t> qdata=qstokes.data<int8_t>();
         qdata.create(dims, maxdims, LOFAR::basename(rawfilename), Dataset<int8_t>::LITTLE);
       }
       dal::Dataset<float> qscale=qstokes.scale();
       qscale.create(sodims, maxdims, LOFAR::basename(rawSfilename), Dataset<float>::LITTLE);
       dal::Dataset<float> qoffset=qstokes.offset();
       qoffset.create(sodims, maxdims, LOFAR::basename(rawOfilename), Dataset<float>::LITTLE);
      }

    }

    template <typename T,unsigned DIM, unsigned FLAGS_DIM>
    MSWriterDAL<T,DIM,FLAGS_DIM>::~MSWriterDAL()
    {
    }

    template <typename T,unsigned DIM, unsigned FLAGS_DIM>
    void MSWriterDAL<T,DIM,FLAGS_DIM>::write(StreamableData *data)
    {
      SampleData<T,DIM,FLAGS_DIM> *sdata = dynamic_cast<SampleData<T,DIM,FLAGS_DIM> *>(data);

      ASSERT( data );
      ASSERT( sdata );
      
      if (!itsQuantizerEnabled) {
        ASSERTSTR( sdata->samples.num_elements() >= itsBlockSize,
             "A block is at least " << itsBlockSize <<
             " elements, but provided sdata only has " << 
             sdata->samples.num_elements() << " elements" );
      } else {
        ASSERTSTR( sdata->qsamples.num_elements() >= itsBlockSize,
             "A block is at least " << itsBlockSize <<
             " elements, but provided sdata only has " << 
             sdata->qsamples.num_elements() << " elements" );
        ASSERTSTR( sdata->qscales.num_elements() ==   sdata->qoffsets.num_elements() && sdata->qoffsets.num_elements() >= 1 && sdata->qoffsets.num_elements() < itsBlockSize,
             "Offset/scale should be valid for " << itsBlockSize <<
             " elements, but provided offset/scale has " << 
             sdata->qoffsets.num_elements() <<","<< sdata->qscales.num_elements() << " elements" );
      }

      // add pointing information here ? re-calculate block sequence nr's
      unsigned seqNr = data->sequenceNumber();
      unsigned bytesPerBlock = itsBlockSize * (!itsQuantizerEnabled?sizeof(T):sizeof(int8_t));  

      // per each block, there is only one scale/offset
      unsigned bytesPerBlockSO = sizeof(float);  
      if (itsQuantizerEnabled) {
       bytesPerBlockSO *=sdata->qoffsets.num_elements();
      }
      // fill in zeroes for lost blocks
      if (itsNextSeqNr < seqNr) {
        itsFile.skip((seqNr - itsNextSeqNr) * bytesPerBlock);
        if (itsQuantizerEnabled) {
         itsSFile->skip((seqNr - itsNextSeqNr) * bytesPerBlockSO);
         itsOFile->skip((seqNr - itsNextSeqNr) * bytesPerBlockSO);
        }

        // the write below can throw, so record that we've
        // skipped this far to prevent skipping twice.
        itsNextSeqNr = seqNr;
      }

      // make sure we skip |2 in the highest dimension
      if (!itsQuantizerEnabled) {
       itsFile.write(sdata->samples.origin(), bytesPerBlock);
      } else {
       itsSFile->write(sdata->qscales.origin(), bytesPerBlockSO);
       itsOFile->write(sdata->qoffsets.origin(), bytesPerBlockSO);
       itsFile.write(sdata->qsamples.origin(), bytesPerBlock);
      }

      itsNextSeqNr = seqNr + 1;
      itsNrBlocksWritten++;

      itsConfiguration.replace(itsConfigurationPrefix + "size",              str(format("%u") % getDataSize()));
      itsConfiguration.replace(itsConfigurationPrefix + "duration",          str(format("%f") % ((data->sequenceNumber() + 1) * itsParset.settings.blockDuration())));
      itsConfiguration.replace(itsConfigurationPrefix + "percentageWritten", str(format("%u") % percentageWritten()));
    }

    template <typename T,unsigned DIM, unsigned FLAGS_DIM>
    void MSWriterDAL<T,DIM,FLAGS_DIM>::fini(const FinalMetaData &finalMetaData)
    {
      const struct ObservationSettings::BeamFormer::File &f = itsParset.settings.beamFormer.files[itsFileNr];
      const unsigned sapNr = f.sapNr;

      string h5filename = forceextension(itsFilename, ".h5");
      ScopedLock sl(HDF5Mutex);

      // update the metadata
      BF_File file(h5filename, BF_File::READWRITE);
      BF_SubArrayPointing sap = file.subArrayPointing(sapNr);

      // last block written in the observation
      ssize_t lastBlock = static_cast<ssize_t>(finalMetaData.nrBlocksInObservation) - 1;

      // convert to last block written for this file
      if (f.blockStart > lastBlock) {
          // nothing was written for this TAB
          lastBlock = f.blockStart - 1;
      } else if (f.blockEnd <= lastBlock) {
          // file was fully written
          lastBlock = f.blockEnd;
      } else {
          // file was cut short
      }

      sap.expTimeEndUTC().value = toUTC(itsParset.settings.getBlockStopTime(lastBlock));
      sap.expTimeEndMJD().value = toMJD(itsParset.settings.getBlockStopTime(lastBlock));
      sap.totalIntegrationTime().value = finalMetaData.nrBlocksInObservation * itsParset.settings.blockDuration();
    }

    // specialisations for FinalBeamFormedData
    template class MSWriterDAL<float,3>;
  } // namespace Cobalt
} // namespace LOFAR

#endif // HAVE_DAL

