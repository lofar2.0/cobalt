//# tMultiPartTABMultiplePointingOutput.cc: test TAB sb range output into multiple files (parts)
//# with re-pointings specified in TABs
//# Copyright (C) 2014  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

#include <lofar_config.h>

#include <unistd.h>
#include <string>
#include <vector>
#include <iostream>
#include <omp.h>
#include <boost/format.hpp>

#include <Common/LofarLogger.h>
#include <CoInterface/Parset.h>
#include <GPUProc/Pipelines/Pipeline.h>
#include <GPUProc/SubbandProcs/SubbandProcOutputData.h>
#include <GPUProc/Station/StationInput.h>
#include <GPUProc/Storage/StorageProcesses.h>

using namespace std;
using namespace LOFAR;
using namespace LOFAR::Cobalt;
using boost::format;
using boost::str;

// Fill sb with all values equal to its station sb nr (see .parset).
std::shared_ptr<SubbandProcOutputData> getTestSbCohData(const Parset& ps, gpu::Context& ctx,
                                                   ssize_t blockIdx, unsigned sbIdx)
{
  SubbandProcOutputData *bfData = new SubbandProcOutputData(ps, ctx);

  bfData->blockID.block = blockIdx;
  bfData->blockID.globalSubbandIdx = sbIdx;
  bfData->blockID.localSubbandIdx = sbIdx;
  bfData->blockID.subbandProcSubbandIdx = sbIdx;
  auto& coherentData = bfData->coherentData();
  // Give data its block Id as value, so re-pointing output can be checked
  if (!coherentData.is_quantized()) {
    for (size_t v = 0; v < coherentData.data_num_elements(); v++) {
      coherentData.get_data_origin()[v] = (float)blockIdx;
    }
  } else {
    for (size_t v = 0; v < coherentData.qdata_num_elements(); v++) {
      coherentData.get_qdata_origin()[v] = (int8_t)blockIdx;
    }
    for (size_t v = 0; v < coherentData.qoffsets_num_elements(); v++) {
      coherentData.get_qoffsets_origin()[v] = (float)1.0;
    }
    for (size_t v = 0; v < coherentData.qscales_num_elements(); v++) {
      coherentData.get_qscales_origin()[v] = (float)1.0;
    }
  }

  std::shared_ptr<SubbandProcOutputData> data(bfData);
  return data;
}

// Test strategy:
// Skip all the station input and GPU processing.
// Create a parset and BF pipeline, prepare data and write it to outputProc.
// This covers all multiple output parts specific lines of code.
// 
// Assume the startup script of this test has started outputProc. Supply parset.
// It can also test whether the output files contain the expected values.
int main(int argc, char *argv[])
{
  // Incoh St I: 1 TAB :  2 sb in 2 parts per TAB: 1 * 1 * 2 =  2 files

  INIT_LOGGER("tMultiPartTABMultiplePointingOutput");

  // Get command line option "-q" to enable quantization 
  int opt;
  bool quantize=false;
  while ((opt=getopt(argc,argv,"q"))!=-1) 
  {
    switch(opt) 
    {
     case 'q':
       quantize=true;
       break;
     default:
       LOG_ERROR_STR(formatString("Usage: %s [-q]", argv[0]));
       break;
    }
  }

  Parset ps("tMultiPartTABMultiplePointingOutput.parset");

  if (quantize) {
    LOG_INFO_STR("Enable quantization");
    ps.replace("Cobalt.BeamFormer.CoherentStokes.quantize","true");
    ps.replace("Cobalt.BeamFormer.IncoherentStokes.quantize","true");
  }

  ps.updateSettings();

  // expected: 1 Pipeline x 1 SAP x 2 TABs x 3 Pointings x 4 stokes x 2 parts = 48
  LOG_INFO_STR("Check output files");
  LOG_INFO_STR("There are :" << ps.settings.beamFormer.files.size() << " files");
  //We do not use unittest so just do a direct check
  assert(48==ps.settings.beamFormer.files.size());

  for(unsigned int i = 0; i< ps.settings.beamFormer.files.size(); i++)
  {
    LOG_INFO_STR("File :" << i 
        << " sapNr: " << ps.settings.beamFormer.files[i].sapNr
        << " tabNr: " << ps.settings.beamFormer.files[i].tabNr
        << " pointingNr: " << ps.settings.beamFormer.files[i].pointingNr
        << " stokesNr: " << ps.settings.beamFormer.files[i].stokesNr
        << " partNr: " << ps.settings.beamFormer.files[i].partNr
        << " blockStart: " << ps.settings.beamFormer.files[i].blockStart
        << " blockEnd: " << ps.settings.beamFormer.files[i].blockEnd);
  }

  // GPU devices for BF pipeline constr and context for BF host buffers.
  // The BF Sb Proc compiles kernels, but we don't use GPUs in this test.
  vector<gpu::Device> devices = gpu::Platform().devices();
  gpu::Context ctx(devices[0]);

  const unsigned nrSubbands = ps.settings.subbands.size();

  // Create BF Pipeline. We're the only rank: do all the subbands.
  // So for the rest of the test code globalSubbandIdx equals localSubbandIdx.
  vector<size_t> localSbIndices;
  for (unsigned i = 0; i < nrSubbands; i++) {
    localSbIndices.push_back(i);
  }
  omp_set_nested(true); // for around and within .multiSender.process()

  Pool<struct MPIRecvData> MPI_receive_pool("rtcp::MPI_receive_pool", true);
  Pipeline bfpl(ps, localSbIndices, devices, MPI_receive_pool);
  bfpl.allocateResources();

  // Set up control line to outputProc. This also supplies the parset.
  // This must happen after kernel compilation to avoid fork() during PortBroker getaddrinfo().
  string spLogPrefix = "StorageProcesses: ";
  StorageProcesses stPr(ps, spLogPrefix);

#pragma omp parallel sections num_threads(2)
  {
#  pragma omp section
    {
      // Set up data connections with outputProc.
      // Does not return until end of obs, so call from a thread.
      bfpl.multiSender.process();
    }

#  pragma omp section
    {
  // Insert blocks of test data (full duration) per sb.
  // this takes a lot of time, therefore we use a short observation
  std::vector<struct Pipeline::Output> writePool(nrSubbands); // [localSubbandIdx]
  for (unsigned i = 0; i < writePool.size(); i++) {
    std::shared_ptr<SubbandProcOutputData> data;

    writePool[i].queue.reset(new Queue<std::shared_ptr<SubbandProcOutputData>>(str(format("writePool [file %u]") % i)));
    for(unsigned blockIdx = 0; blockIdx < ps.settings.nrBlocks(); blockIdx++)
    {
      data = getTestSbCohData(ps, ctx, blockIdx, i);
      writePool[i].queue->append(data);
    }

    writePool[i].queue->append(NULL, false);
  }

  // Have it push a block of values per sb to outputProc.
  // writeOutput() takes a globalSubbandIdx.
  for (unsigned globalSubbandIdx = 0; globalSubbandIdx < nrSubbands;
       globalSubbandIdx++) {
    unsigned localSubbandIdx = globalSubbandIdx;
    bfpl.writeOutput(globalSubbandIdx, *writePool[localSubbandIdx].queue, bfpl.subbandProcs[localSubbandIdx % bfpl.subbandProcs.size()]->outputPool.free);
  }

  bfpl.multiSender.finish();

    } // omp section
  } // omp parallel sections ...

  // Looks good, but the startup script will check the output files.
  return 0;
}
