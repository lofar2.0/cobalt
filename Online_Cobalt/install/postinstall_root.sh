#!/bin/bash -eu

echo "Giving /opt to lofarbuild..."
chown lofarbuild.lofarbuild /opt

echo "Giving /localhome/lofar to lofarbuild..."
mkdir /localhome/lofar
chown lofarbuild.lofarbuild /localhome/lofar

echo "Giving capabilities to lofarbuild..."
addgroup --system capabilities
usermod -a -G capabilities lofarbuild
echo "%capabilities  ALL= NOPASSWD:/sbin/setcap" >> /etc/sudoers

#
# Casacore
#
echo "Removing system-supplied /opt/casacore..."
rm -rf /opt/casacore

#
# Python packages
#

# doxypypy is used to generate documentation on cbt009
pip install doxypypy
