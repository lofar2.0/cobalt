#!/bin/bash
#
# Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
# SPDX-License-Identifier: Apache-2.0
#

TESTNAME=`basename "${0%%.sh}"`
./runctest.sh $TESTNAME "${1}"
