#!/usr/bin/env python2

import dal
import numpy

def apply_quantization(stokes):
  # Check if data are unsigned
  datatype = stokes.dataType().value
  dataUnsigned = datatype=='unsigned char'
  if dataUnsigned:
    data = stokes.dataUInt8()
  else:
    data = stokes.dataInt8()

  # Load scale and offset
  scale = stokes.scale()
  offset = stokes.offset()

  # Data dimensions
  nSamples = data.dims()[0]
  nChannels = data.dims()[1]
  nBlocks = scale.dims()[0]
  nSamplesBlock = nSamples / nBlocks

  # Storage for data
  xint = numpy.zeros(data.dims(), dtype=data.dtype)
  xscale = numpy.zeros(scale.dims(), dtype=scale.dtype)
  xoffset = numpy.zeros(offset.dims(), dtype=offset.dtype)

  # Read the data
  data.get2D([0, 0], xint)
  scale.get2D([0, 0], xscale)
  offset.get2D([0, 0], xoffset)

  xfloat = numpy.zeros((nBlocks, nSamplesBlock, nChannels))

  # Reshape the data
  xint = xint.reshape((nBlocks, nSamplesBlock, nChannels))

  # Undo the quantization
  for ci in range(0, nBlocks):
    for cj in range(0, nChannels):
      xfloat[ci,:,cj] = xint[ci,:,cj] * xscale[ci,cj] + xoffset[ci,cj]

  return xfloat.reshape((nSamples, nChannels))


def parse(filename):
  f = dal.BF_File(filename)

  nSAPs = f.nofSubArrayPointings().value

  # SAPs are not indexed from 0, advance sapOffset until a valid SAP is found
  sapOffset = 0
  while (not f.subArrayPointing(sapOffset).exists()):
    sapOffset += 1

  # Iterate all Sub Array Points (SAPs)
  for sapIdx in range(sapOffset, nSAPs + sapOffset):
    sap = f.subArrayPointing(sapIdx)
    nBeams = sap.nofBeams().value

    # Iterate all Tight Array Beams (TABs)
    for beamIdx in range(nBeams):
      # Beams are not indexed from 0, advance beamIdx until a valid beam is found
      while (not sap.beam(beamIdx).exists()):
        beamIdx += 1

      beam = sap.beam(beamIdx)
      nStokes = beam.nofStokes().value

      for stokesIdx in range(nStokes):
        try:
          quantized = beam.quantized().value[stokesIdx]
        except TypeError:
          # Workaround for older LOFAR version
          quantized = False

        stokescomp = beam.stokesComponents().value[stokesIdx]

        print "SAP %d, stokes %s, quantized: %d" % (sapIdx, stokescomp, quantized),

        if quantized:
          # Stokes are not indexed from 0, advance stokesIdx until a valid beam is found
          while (not beam.qstokes(stokesIdx).exists()):
            stokesIdx += 1

          # Load the quantized stokes data
          stokes = beam.qstokes(stokesIdx)

          # Get the data in floating-point format
          data = apply_quantization(stokes)
        else:
          # Stokes are not indexed from 0, advance stokesIdx until a valid beam is found
          while (not beam.stokes(stokesIdx).exists()):
            stokesIdx += 1

          # Load the stokes data
          stokes = beam.stokes(stokesIdx)

          # Get the data as numpy array
          data = numpy.zeros(stokes.dims(), dtype=stokes.dtype)
          stokes.get2D([0,0], data)

        print "\t#subbands = %s" % (str(stokes.nofSubbands().value)),
        print "\t#channels = %s" % (str(stokes.nofChannels().value)),
        print "\t#samples  = %s" % (str(stokes.nofSamples().value)),
        print "\tmean = %f" % (numpy.mean(data)),
        print "\tstd  = %f" % (numpy.std(data)),

    return data


def get_argument_parser():
    import argparse
    parser = argparse.ArgumentParser(description='Examine an HDF5 file with beamformed data.')
    parser.add_argument('filename', help='the file to open, it should have 32-bit float data or 8-bit (quantized) data')
    parser.add_argument('-d', help='read all the files in a directory', action='store_true', dest='directory')
    return parser


def run_file(input_filename):
  # Check argument
  if not input_filename.endswith(".h5"):
    print "ERROR: the filename should end with .h5"
    exit(1)

  # Parse the data file
  data = parse(input_filename)

def run_directory(input_directory):
  import os
  input_filenames = os.listdir(input_directory)
  input_filenames = list(filter(lambda x : x.endswith(".h5"), input_filenames))
  input_filenames = numpy.sort(input_filenames)

  n = len(input_filenames)
  if (n < 1):
    print "ERROR: no .h5 files found"
    exit(1)

  for i, input_filename in enumerate(input_filenames):
    print input_filename + ": ",

    # Parse the data file
    data = parse(input_directory + "/" + input_filename)
    print ""

if __name__ == '__main__':
  # Parse command-line arguments
  parser = get_argument_parser()
  args = parser.parse_args()
  filename = args.filename
  directory = args.directory

  if directory:
    run_directory(filename)
  else:
    run_file(filename)
