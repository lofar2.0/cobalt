#!/bin/bash
#
# Quantized output: int8 and float32 scale,offset
#

if [ $# -ne 2 ]; then
  echo 'Syntax: '$0' <test-input-dir> <test-output-dir>' >&2
  exit 1
fi

INDIR="$1"
OUTDIR="$2"

# Input (reference) 
IN_FILENAME1="$INDIR/Beam_0.int8.raw"
IN_FILENAME2="$INDIR/Beam_0.int8.scale"
IN_FILENAME3="$INDIR/Beam_0.int8.offset"
# Output (test)
OUT_FILENAME1="$OUTDIR/Beam_0.int8.raw"
OUT_FILENAME2="$OUTDIR/Beam_0.int8.scale"
OUT_FILENAME3="$OUTDIR/Beam_0.int8.offset"

# for float32 comparison
numfp32eps=\(1.192092896/10000000\)
EPSILON=$(echo  1024.0 \* $numfp32eps | bc -l)

echo "diff 1: $IN_FILENAME1 (original) vs $OUT_FILENAME1"
echo "diff 2: $IN_FILENAME2 (original) vs $OUT_FILENAME2"
echo "diff 3: $IN_FILENAME3 (original) vs $OUT_FILENAME3"
diff -q $IN_FILENAME1 $OUT_FILENAME1 \
&& \
cmpfloat --type='float' --epsilon=${EPSILON} --verbose \
 $IN_FILENAME2 $OUT_FILENAME2\
&& \
cmpfloat --type='float' --epsilon=${EPSILON} --verbose \
 $IN_FILENAME3 $OUT_FILENAME3\
