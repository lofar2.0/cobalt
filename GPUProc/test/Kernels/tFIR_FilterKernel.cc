//# tFIR_FilterKernel.cc: test FIR_FilterKernel class
//#
//# Copyright (C) 2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

#include <lofar_config.h>

#include <GPUProc/Kernels/FIR_FilterKernel.h>
#include <GPUProc/MultiDimArrayHostBuffer.h>
#include <CoInterface/Parset.h>
#include <Common/complex.h>

#include <UnitTest++.h>
#include <memory>
#include <GPUProc/PerformanceCounter.h>
#include <CoInterface/BlockID.h>

using namespace LOFAR::Cobalt;
using namespace LOFAR;
using namespace std;

TEST(FIR_FilterKernel)
{

      // How we test:
      // 1 : run the kernel in the normal way, without doppler correction. save the output
      // 2 : re-run the kernel with doppler correction, but the delays =0, so effectively no correction, save the output
      // 3 : re-run the kernel with non-zero delays, so the output should be affected by a phase ramp
      // compare outputs for 1 and 2 above, and output ratios for 1 and 3 above,
      // ratio of outputs for 1 and 3 above should give us back the applied correction

      // some constants (not in the parset)
      const size_t NR_POLARIZATIONS=2;
      const size_t COMPLEX=2;
      const double subbandFreq=50e6;
      const double CLOCK_MHZ=200.0;

      Parset ps;
      ps.add("Observation.nrBitsPerSample", "8");
      ps.add("Observation.VirtualInstrument.stationList", "[RS000]");
      ps.add("Observation.antennaSet", "LBA_INNER");
      ps.add("Observation.Dataslots.RS000LBA.RSPBoardList", "[0]");
      ps.add("Observation.Dataslots.RS000LBA.DataslotList", "[0]");
      ps.add("Observation.nrBeams", "1");
      ps.add("Observation.Beam[0].subbandList", "[0]");
      ps.add("OLAP.CNProc.integrationSteps", "128");
      ps.add("Cobalt.Correlator.nrChannelsPerSubband", "64");
      ps.add("Observation.DataProducts.Output_Correlated.enabled", "true");
      ps.add("Observation.DataProducts.Output_Correlated.filenames", "[L12345_SAP000_SB000_uv.MS]");
      ps.add("Observation.DataProducts.Output_Correlated.locations", "[localhost:.]");
      ps.updateSettings();

      //************************ Test 1 (without Doppler correction)
      FIR_FilterKernel::Parameters params(
        ps.settings.antennaFields.size(),
        ps.settings.nrBitsPerSample,
        true,
        ps.settings.correlator.dopplerCorrection,
        1,
        ps.settings.correlator.nrChannels,
        ps.settings.blockSize / ps.settings.correlator.nrChannels,
        ps.settings.clockMHz,
        1.0f
      );

      KernelFactory<FIR_FilterKernel> factory(params);

      gpu::Device device(gpu::Platform().devices()[0]);
      gpu::Context context(device);
      gpu::Stream stream(context);

      gpu::DeviceMemory
        dInput(context, factory.bufferSize(FIR_FilterKernel::INPUT_DATA)),
        dOutput(context, factory.bufferSize(FIR_FilterKernel::OUTPUT_DATA)),
        dCoeff(context, factory.bufferSize(FIR_FilterKernel::FILTER_WEIGHTS)),
        dHistory(context, factory.bufferSize(FIR_FilterKernel::HISTORY_DATA));

      const size_t NR_SAMPLES_PER_CHANNEL=ps.settings.blockSize/ps.settings.correlator.nrChannels;
      const size_t NR_CHANNELS=ps.settings.correlator.nrChannels;
      MultiDimArrayHostBuffer<signed char, 5> hInput(boost::extents
                         [ps.settings.antennaFields.size()] //NR_STATIONS
                         [NR_SAMPLES_PER_CHANNEL] 
                         [NR_CHANNELS] 
                         [NR_POLARIZATIONS] 
                         [COMPLEX], 
                         context);
      MultiDimArrayHostBuffer<float, 5> hOutput(boost::extents
                         [ps.settings.antennaFields.size()] //NR_STATIONS
                         [NR_POLARIZATIONS] 
                         [NR_SAMPLES_PER_CHANNEL] 
                         [NR_CHANNELS] 
                         [COMPLEX], 
                         context);


      // Create a recognizable input pattern
#pragma omp parallel for
      for(size_t i = 0; i < NR_SAMPLES_PER_CHANNEL; ++i) {
          hInput[0][i][32][0][0]=1;
          hInput[0][i][32][0][1]=-1;
          hInput[0][i][32][1][0]=2;
          hInput[0][i][32][1][1]=-12;
      }
     //Doppler phase expected (with channel):
     //phi = 2.0 * M_PI * (subbandFrequency / (CLOCK_MHZ*1e6/1024.0) )*( delayAfterEnd - delayAtBegin )/NR_SAMPLES_PER_CHANNEL * double(channel)/NR_CHANNELS;
     //in order to test this, also add a pattern across channels
#pragma omp parallel for
      for(size_t i = 0; i < NR_CHANNELS; ++i) {
          // fill with stride so convolution becoms a scaling
          hInput[0][NR_SAMPLES_PER_CHANNEL/2][i][0][0]=1;
          hInput[0][NR_SAMPLES_PER_CHANNEL/2][i][0][1]=-1;
          hInput[0][NR_SAMPLES_PER_CHANNEL/2][i][1][0]=2;
          hInput[0][NR_SAMPLES_PER_CHANNEL/2][i][1][1]=-12;
      }

      stream.writeBuffer(dInput, hInput);

      // initialize history data : not really needed here
      // because it is done in the construcor
      dHistory.set(0);

      unique_ptr<FIR_FilterKernel> kernel(factory.create(stream, dInput, dOutput));
      BlockID blockId;
      kernel->enqueue(blockId, 0);
      stream.readBuffer(hOutput, dOutput,true);

      //************************ Test 2: with Doppler correction
      //
      ps.replace("Cobalt.Correlator.dopplerCorrection","true");
      ps.updateSettings();

      FIR_FilterKernel::Parameters params_dop(
        ps.settings.antennaFields.size(),
        ps.settings.nrBitsPerSample,
        true,
        ps.settings.correlator.dopplerCorrection,
        1,
        ps.settings.correlator.nrChannels,
        ps.settings.blockSize / ps.settings.correlator.nrChannels,
        ps.settings.clockMHz,
        1.0f
      );

      KernelFactory<FIR_FilterKernel> factory_dop(params_dop);

      MultiDimArrayHostBuffer<double, 2> delaysAtBegin(boost::extents
                         [ps.settings.antennaFields.size()] //NR_DELAYS
                         [NR_POLARIZATIONS], 
                         context);
      MultiDimArrayHostBuffer<double, 2> delaysAfterEnd(boost::extents
                         [ps.settings.antennaFields.size()] //NR_DELAYS
                         [NR_POLARIZATIONS], 
                         context);
      MultiDimArrayHostBuffer<float, 5> hOutput1(boost::extents
                         [ps.settings.antennaFields.size()] //NR_STATIONS
                         [NR_POLARIZATIONS] 
                         [NR_SAMPLES_PER_CHANNEL]
                         [NR_CHANNELS] 
                         [COMPLEX], 
                         context);


      CHECK_EQUAL(delaysAtBegin.size(),factory_dop.bufferSize(FIR_FilterKernel::DELAYS));
      CHECK_EQUAL(hInput.size(),factory_dop.bufferSize(FIR_FilterKernel::INPUT_DATA));
      CHECK_EQUAL(hOutput1.size(),factory_dop.bufferSize(FIR_FilterKernel::OUTPUT_DATA));
      const double delayBegin=1.3e-2;
      const double delayEnd=34.0e-2;
      for (size_t i = 0; i < delaysAtBegin.num_elements(); i++) {
        delaysAtBegin.origin()[i] = delayBegin;
      }
      for (size_t i = 0; i < delaysAfterEnd.num_elements(); i++) {
        delaysAfterEnd.origin()[i] = delayEnd;
      }
      unique_ptr<FIR_FilterKernel> kernel_dop_zero(factory_dop.create(stream, dInput, dOutput));
      // Note: delays are zero
      kernel_dop_zero->enqueue(blockId, 0, subbandFreq);
      stream.readBuffer(hOutput1, dOutput,true);
      // compare results (for a fraction of the output, to save time)
      for(size_t i = NR_SAMPLES_PER_CHANNEL/2; i < (NR_SAMPLES_PER_CHANNEL*3)/4; ++i) {
          CHECK_CLOSE(hOutput[0][0][i][32][0],hOutput1[0][0][i][32][0],0.00000001);
          CHECK_CLOSE(hOutput[0][0][i][32][1],hOutput1[0][0][i][32][1],0.00000001);
          CHECK_CLOSE(hOutput[0][1][i][32][0],hOutput1[0][1][i][32][0],0.00000001);
          CHECK_CLOSE(hOutput[0][1][i][32][1],hOutput1[0][1][i][32][1],0.00000001);
      }


      // recreate a kernel because we have to start with fresh History data
      unique_ptr<FIR_FilterKernel> kernel_dop(factory_dop.create(stream, dInput, dOutput));
      // Copy non-zero delays
      stream.writeBuffer(kernel_dop->delaysAtBegin, delaysAtBegin, true);
      stream.writeBuffer(kernel_dop->delaysAfterEnd, delaysAfterEnd, true);
      kernel_dop->enqueue(blockId, 0, subbandFreq);
      stream.readBuffer(hOutput1, dOutput,true);
      // compare the result for a subset of channels, to save time
      for(size_t i = NR_CHANNELS/4; i < NR_CHANNELS/2; ++i) {
          double phiX=  atan2(hOutput1[0][0][NR_SAMPLES_PER_CHANNEL/2][i][1], 
                            hOutput1[0][0][NR_SAMPLES_PER_CHANNEL/2][i][0]) 
                      - atan2(hOutput[0][0][NR_SAMPLES_PER_CHANNEL/2][i][1], 
                              hOutput[0][0][NR_SAMPLES_PER_CHANNEL/2][i][0]);
          double phiY=  atan2(hOutput1[0][1][NR_SAMPLES_PER_CHANNEL/2][i][1], 
                            hOutput1[0][1][NR_SAMPLES_PER_CHANNEL/2][i][0]) 
                      - atan2(hOutput[0][1][NR_SAMPLES_PER_CHANNEL/2][i][1], 
                              hOutput[0][1][NR_SAMPLES_PER_CHANNEL/2][i][0]);

          double phiRef = 2.0 * M_PI * (subbandFreq/ (CLOCK_MHZ*1e6/1024.0) )*( delayEnd - delayBegin )/NR_SAMPLES_PER_CHANNEL * double(i)/NR_CHANNELS;

         CHECK_CLOSE(phiX,phiRef,0.0001);
         CHECK_CLOSE(phiY,phiRef,0.0001);
      }

}

TEST(HistoryFlags)
{
  /*
   * Set up a Kernel
   */

  Parset ps;
  ps.add("Observation.nrBitsPerSample", "8");
  ps.add("Observation.VirtualInstrument.stationList", "[RS000]");
  ps.add("Observation.antennaSet", "LBA_INNER");
  ps.add("Observation.Dataslots.RS000LBA.RSPBoardList", "[0]");
  ps.add("Observation.Dataslots.RS000LBA.DataslotList", "[0]");
  ps.add("Observation.nrBeams", "1");
  ps.add("Observation.Beam[0].subbandList", "[0]");
  ps.add("OLAP.CNProc.integrationSteps", "128");
  ps.add("Cobalt.Correlator.nrChannelsPerSubband", "64");
  ps.add("Observation.DataProducts.Output_Correlated.enabled", "true");
  ps.add("Observation.DataProducts.Output_Correlated.filenames", "[L12345_SAP000_SB000_uv.MS]");
  ps.add("Observation.DataProducts.Output_Correlated.locations", "[localhost:.]");
  ps.updateSettings();

  FIR_FilterKernel::Parameters params(
    ps.settings.antennaFields.size(),
    ps.settings.nrBitsPerSample,
    true,
    false, // doDopplerCorrection
    1,
    ps.settings.correlator.nrChannels,
    ps.settings.blockSize / ps.settings.correlator.nrChannels,
    200, // clockMHz
    1.0f
    );

  KernelFactory<FIR_FilterKernel> factory(params);

  gpu::Device device(gpu::Platform().devices()[0]);
  gpu::Context context(device);
  gpu::Stream stream(context);

  gpu::DeviceMemory
    dInput(context, factory.bufferSize(FIR_FilterKernel::INPUT_DATA)),
    dOutput(context, factory.bufferSize(FIR_FilterKernel::OUTPUT_DATA));

  unique_ptr<FIR_FilterKernel> kernel(factory.create(stream, dInput, dOutput));

  /*
   * Test propagation of history flags. Each block tests for the flags of
   * the history samples of the previous block, so the order of these tests
   * matters.
   */

  MultiDimArray<SparseSet<unsigned>, 1> inputFlags(boost::extents[1]);

  /*
   * Block 0: only last sample is flagged
   */

  // Flag only the last sample
  inputFlags[0].reset();
  inputFlags[0].include(ps.settings.blockSize - 1);

  // insert and update history flags
  kernel->prefixHistoryFlags(inputFlags, 0);

  // the first set of history flags are all flagged, and so is our last sample
  CHECK_EQUAL(params.nrHistorySamples() + 1, inputFlags[0].count());

  /*
   * Block 1: no samples are flagged
   */

  // next block
  inputFlags[0].reset();
  kernel->prefixHistoryFlags(inputFlags, 0);

  // the second set of history flags should have one sample flagged (the last
  // sample of the previous block)
  CHECK_EQUAL(1U, inputFlags[0].count());

  /*
   * Block 2: all samples are flagged
   */

  // next block
  inputFlags[0].reset();
  inputFlags[0].include(0, ps.settings.blockSize);
  kernel->prefixHistoryFlags(inputFlags, 0);

  // the number of flagged samples should have remained unchanged (the last
  // block had no flags)
  CHECK_EQUAL(ps.settings.blockSize, inputFlags[0].count());

  /*
   * Block 3: no samples are flagged
   */

  // next block
  inputFlags[0].reset();
  kernel->prefixHistoryFlags(inputFlags, 0);

  // only the history samples should be flagged
  CHECK_EQUAL(params.nrHistorySamples(), inputFlags[0].count());
  CHECK_EQUAL(params.nrHistorySamples(), inputFlags[0].subset(0, params.nrHistorySamples()).count());
}

int main()
{
  INIT_LOGGER("tFIR_FilterKernel");
  try {
    gpu::Platform pf;
    return UnitTest::RunAllTests() > 0;
  } catch (gpu::GPUException& e) {
    cerr << "No GPU device(s) found. Skipping tests." << endl;
    return 3;
  }
}
