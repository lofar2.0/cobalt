//# tQuantizeOutputKernel.cc: test QuantizeOutputKernel class
//#
//# Copyright (C) 2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

#include <lofar_config.h>

#include <GPUProc/Kernels/CoherentStokesKernel.h>
#include <GPUProc/Kernels/QuantizeOutputKernel.h>
#include <GPUProc/MultiDimArrayHostBuffer.h>
#include <GPUProc/gpu_wrapper.h>
#include <CoInterface/BlockID.h>
#include <CoInterface/Config.h>
#include <CoInterface/Parset.h>
#include <Common/LofarLogger.h>

#include <UnitTest++.h>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/scoped_ptr.hpp>
#include <iostream>
#include <iomanip>
#include <vector>
#include <random>


#include "KernelTestHelpers.h"

using namespace std;
using namespace boost;
using namespace LOFAR::Cobalt;
using namespace LOFAR::Cobalt::gpu;

typedef complex<float> fcomplex;

// Fixture for testing correct translation of parset values
struct ParsetSUT
{
  size_t
    timeIntegrationFactor,
    nrChannels,
    nrOutputSamples,
    nrStations,
    nrInputSamples,
    blockSize,
    nrDelayCompensationChannels,
    nrTabs,
    nrStokes,
    nrQuantizeBits;

  float
    quantizeScaleMax,
    quantizeScaleMin;

  Parset parset;

  ParsetSUT(
    size_t inrChannels = 16,
    size_t inrOutputSamples = 1024,
    size_t inrStations = 43,
    size_t inrTabs = 21,
    size_t itimeIntegrationFactor = 1,
    size_t inrStokes = 4,
    string stokes = "IQUV",
    size_t nrQuantizeBits = 8,
    float quantizeScaleMax = 5,
    float quantizeScaleMin = -5)
  :
    timeIntegrationFactor(itimeIntegrationFactor),
    nrChannels(inrChannels),
    nrOutputSamples(inrOutputSamples),
    nrStations(inrStations),
    nrInputSamples(nrOutputSamples * timeIntegrationFactor),
    blockSize(nrChannels * nrInputSamples),
    nrDelayCompensationChannels(64),
    nrTabs(inrTabs),
    nrStokes(inrStokes),
    nrQuantizeBits(nrQuantizeBits),
    quantizeScaleMax(quantizeScaleMax),
    quantizeScaleMin(quantizeScaleMin)
  {
    size_t nr_files = inrTabs * 4; // 4 for number of stokes
    parset.add("Observation.DataProducts.Output_CoherentStokes.enabled", "true");
    parset.add("Cobalt.BeamFormer.CoherentStokes.timeIntegrationFactor",
               lexical_cast<string>(timeIntegrationFactor));
    parset.add("Cobalt.BeamFormer.CoherentStokes.nrChannelsPerSubband",
      lexical_cast<string>(nrChannels));
    parset.add("Cobalt.BeamFormer.CoherentStokes.which", stokes);
    parset.add("Observation.VirtualInstrument.stationList",
      str(format("[%d*RS000]") % nrStations));
    parset.add("Cobalt.blockSize",
      lexical_cast<string>(blockSize));
    parset.add("Observation.Beam[0].subbandList", "[0]");
    parset.add("Observation.Beam[0].nrTiedArrayBeams",lexical_cast<string>(inrTabs));
    parset.add("Observation.DataProducts.Output_CoherentStokes.filenames",
      str(format("[%d*dummy.raw]") % nr_files));
    parset.add("Observation.DataProducts.Output_CoherentStokes.locations", str(format("[%d*:.]") % nr_files));
    parset.add("Cobalt.BeamFormer.nrDelayCompensationChannels",
               lexical_cast<string>(nrDelayCompensationChannels));
    parset.add("Cobalt.BeamFormer.CoherentStokes.quantize", lexical_cast<string>(true));
    parset.add("Cobalt.BeamFormer.CoherentStokes.quantizeBits", lexical_cast<string>(nrQuantizeBits));
    parset.add("Cobalt.BeamFormer.CoherentStokes.quantizeScaleMax", lexical_cast<string>(quantizeScaleMax));
    parset.add("Cobalt.BeamFormer.CoherentStokes.quantizeScaleMin", lexical_cast<string>(quantizeScaleMin));
    parset.add("Cobalt.BeamFormer.CoherentStokes.quantizeIpositive", lexical_cast<string>(false));
    parset.updateSettings();
  }
};


// Test if we can succesfully create a KernelFactory
TEST(KernelFactory)
{
  LOG_INFO("Test KernelFactory");
  ParsetSUT sut;

  CoherentStokesKernel::Parameters csParameters(
    sut.nrChannels,
    sut.nrInputSamples,
    sut.nrTabs,
    sut.nrStokes,
    false, // outputComplexVoltages
    sut.timeIntegrationFactor,
    false // quantizeOutput
  );

  QuantizeOutputKernel::Parameters qoParameters(
    sut.nrChannels,
    sut.nrInputSamples,
    sut.nrTabs,
    sut.nrStokes,
    false, // outputComplexVoltages
    sut.nrQuantizeBits,
    sut.quantizeScaleMax,
    sut.quantizeScaleMin,
    false // quantizeIpositive
  );

  KernelFactory<CoherentStokesKernel> kfcoherentstokes(csParameters);
  KernelFactory<QuantizeOutputKernel> kfquantization(qoParameters);
 }

struct SUTWrapper:  ParsetSUT
{
  gpu::Device device;
  gpu::Context context;
  gpu::Stream stream;
  size_t nrStokes;
  size_t nrTabs;
  CoherentStokesKernel::Parameters csparameters;
  QuantizeOutputKernel::Parameters qoparameters;
  KernelFactory<CoherentStokesKernel> csfactory;
  KernelFactory<QuantizeOutputKernel> qofactory;
  MultiDimArrayHostBuffer<fcomplex, 4> hCoherentStokesInput;
  MultiDimArrayHostBuffer<float, 4> hCoherentStokesOutput;
  MultiDimArrayHostBuffer<float, 4> hCoherentStokesOutputTransposed;
  MultiDimArrayHostBuffer<float, 4> hQuantizedOutput;
  gpu::DeviceMemory inputBuffer;
  gpu::DeviceMemory outputBuffer;
  scoped_ptr<QuantizeOutputKernel> qokernel;

  SUTWrapper(size_t inrChannels = 16,
             size_t inrOutputSamples = 1024,
             size_t inrStations = 43,
             size_t inrTabs = 21,
             size_t itimeIntegrationFactor = 1,
             size_t quantizeBits = 8,
             float quantizeScaleMax = 5,
             float quantizeScaleMin = -5) :
      ParsetSUT(inrChannels, inrOutputSamples, inrStations ,
                inrTabs,itimeIntegrationFactor, 4, "IQUV",
                quantizeBits, quantizeScaleMax, quantizeScaleMin),
    device(gpu::Platform().devices()[0]),
    context(device),
    stream(context),
    nrStokes(parset.settings.beamFormer.coherentSettings.nrStokes),
    nrTabs(parset.settings.beamFormer.maxNrTABsPerSAP()),
    csfactory(CoherentStokesKernel::Parameters(
        nrChannels,
        nrInputSamples,
        nrTabs,
        nrStokes,
        false, // outputComplexVoltages
        timeIntegrationFactor,
        false // quantizeOutput
    )),
    qofactory(QuantizeOutputKernel::Parameters(
      nrChannels,
      nrInputSamples,
      nrTabs,
      nrStokes,
      false, // outputComplexVoltages
      nrQuantizeBits,
      quantizeScaleMax,
      quantizeScaleMin,
      false // quantizeIpositive
    )),
    hCoherentStokesInput(
      boost::extents[nrTabs][NR_POLARIZATIONS][nrInputSamples][nrChannels],
      context),
    hCoherentStokesOutput(
      boost::extents[nrTabs][nrStokes][nrOutputSamples][nrChannels],
      context),
    hCoherentStokesOutputTransposed(
      boost::extents[nrTabs][nrStokes][nrChannels][nrOutputSamples],
      context),
    hQuantizedOutput(
      boost::extents[nrTabs][nrStokes][nrOutputSamples][nrChannels],
      context),
    inputBuffer(context, csfactory.bufferSize(CoherentStokesKernel::INPUT_DATA)),
    outputBuffer(context, csfactory.bufferSize(CoherentStokesKernel::OUTPUT_DATA)),
    qokernel(qofactory.create(stream, outputBuffer, inputBuffer))
  {
    initializeHostBuffers();
  }

  // Initialize all the elements of the input host buffer to zero, and all
  // elements of the output host buffer to NaN.
  void initializeHostBuffers()
  {
    cout << "\nInitializing host buffers..."
         << "\n  buffers.input.size()  = " << setw(7) << inputBuffer.size()
         << "\n  buffers.output.size() = " << setw(7) << outputBuffer.size()
         << endl;
    CHECK_EQUAL(inputBuffer.size(), hCoherentStokesInput.size());
    CHECK_EQUAL(outputBuffer.size(), hQuantizedOutput.size());
    fill(hCoherentStokesInput.data(), hCoherentStokesInput.data() + hCoherentStokesInput.num_elements(), 0.0f);
    fill(hCoherentStokesOutput.data(), hCoherentStokesOutput.data() + hCoherentStokesOutput.num_elements(), 0.0f);
    fill(hCoherentStokesOutputTransposed.data(), hCoherentStokesOutputTransposed.data() + hCoherentStokesOutputTransposed.num_elements(), 0.0f);
    fill(hQuantizedOutput.data(), hQuantizedOutput.data() + hQuantizedOutput.num_elements(), 0.0f);
  }

  void runKernel()
  {
    // Dummy BlockID
    BlockID blockId;
    // Copy reference output data from host- to device buffer synchronously
    stream.writeBuffer(outputBuffer, hCoherentStokesOutputTransposed, true);
    // Launch the quantize output kernel
    std::cout << "launch quantize output kernel" << std::endl;
    qokernel->enqueue(blockId);
    // Copy output data from device- to host buffer synchronously
    stream.readBuffer(hCoherentStokesInput, inputBuffer, true);
  }

};

void initialize(
  MultiDimArray<float, 4>& ref_float,
  bool outputComplexVoltages = false)
{
  // Get parameters
  unsigned nrTABs     = ref_float.shape()[0];
  unsigned nrStokes   = ref_float.shape()[1];
  unsigned nrSamples  = ref_float.shape()[2];
  unsigned nrChannels = ref_float.shape()[3];

  // Initialize random number generators
  std::mt19937 mt(0);
  std::normal_distribution<double> dist_signed(-1, 1);
  std::normal_distribution<double> dist_unsigned(0, 1);

  for (size_t tab = 0; tab < nrTABs; tab++)
  {
    for (size_t stokes = 0; stokes < nrStokes; stokes++)
    {
      // Determine the type of quantization to apply
      bool stokes_quv = (stokes > 0);
      bool zero_mean = (outputComplexVoltages || stokes_quv);

      for (size_t sample = 0; sample < nrSamples; sample++)
      {
        for (size_t channel = 0; channel < nrChannels; channel++)
        {
          float ref = zero_mean ? dist_signed(mt) : dist_unsigned(mt);
          ref_float[tab][stokes][sample][channel] = ref;
        }
      }
    }
  }
}

void transpose4D(
  MultiDimArray<float, 4>& dst,
  MultiDimArray<float, 4>& src)
{
  // Sanity check
  assert(dst.num_elements() == src.num_elements());

  // Get parameters
  unsigned x_dim = src.shape()[0];
  unsigned y_dim = src.shape()[1];
  unsigned z_dim = src.shape()[2];
  unsigned w_dim = src.shape()[3];

  for (size_t x = 0; x < x_dim; x++)
  {
    for (size_t y = 0; y < y_dim; y++)
    {
      for (size_t z = 0; z < z_dim; z++)
      {
        for (size_t w = 0; w < w_dim; w++)
        {
          dst[x][y][w][z] = src[x][y][z][w];
        }
      }
    }
  }
}

template<typename INT, typename UINT>
void zeroClipped(
  MultiDimArray<float, 4>& ref_float,
  MultiDimArray<float, 4>& x_float,
  MultiDimArray<INT, 4>& x_int,
  bool outputComplexVoltages = false)
{
  // Sanity check
  assert(ref_float.num_elements() == x_float.num_elements());
  assert(ref_float.num_elements() == x_int.num_elements());

  // Get parameters
  unsigned nrTABs     = ref_float.shape()[0];
  unsigned nrStokes   = ref_float.shape()[1];
  unsigned nrSamples  = ref_float.shape()[2];
  unsigned nrChannels = ref_float.shape()[3];
  unsigned nrQuantizeBits = sizeof(INT) * 8;

  unsigned int nrClipped = 0;

  for (size_t tab = 0; tab < nrTABs; tab++)
  {
    for (size_t stokes = 0; stokes < nrStokes; stokes++)
    {
      // Determine the type of quantization to apply
      bool stokes_quv = (stokes > 0);
      bool zero_mean = (outputComplexVoltages || stokes_quv);

      // Determine min and max values
      int min_unsigned = 0;
      int max_unsigned =      (1 <<  nrQuantizeBits) - 1;
      int min_signed   = -1 * (1 << (nrQuantizeBits - 1));
      int max_signed   =      (1 << (nrQuantizeBits - 1)) - 1;
      int max_value = zero_mean ? max_signed : max_unsigned;
      int min_value = zero_mean ? min_signed : min_unsigned;

      for (size_t sample = 0; sample < nrSamples; sample++)
      {
        for (size_t channel = 0; channel < nrChannels; channel++)
        {
            auto x_ptr = &(x_int[tab][stokes][sample][channel]);
            auto x     = zero_mean ? *((INT *) x_ptr) : *((UINT *) x_ptr);

            if (x == min_value || x == max_value)
            {
              ref_float[tab][stokes][sample][channel] = 0.0f;
              x_float[tab][stokes][sample][channel] = 0.0f;
              nrClipped++;
            }
        }
      }
    }
  }

  // Report
  float fractionClipped = ((float) nrClipped) / ref_float.num_elements();
  float percentageClipped = ((int) (fractionClipped * 1000)) / 10.;
  std::cout << "Number of clipped values set to zero: " << nrClipped;
  std::cout << " (" << percentageClipped << "%)" << std::endl;
}

// CHECK_ARRAY_CLOSE makes it very hard to inspect differences
// between the expected and actual output. This method
// prints only the incorrect values.
bool checkArrayClose(
  const float* ref_float,
  const float* x_float,
  size_t n,
  float tolerance)
{
  size_t nnz = 0;
  double error = 0;

  for (size_t idx = 0; idx < n; ++idx)
  {
    float ref = ref_float[idx];
    float x = x_float[idx];
    float diff = ref - x;
    if (abs(diff) > tolerance)
    {
      std::cerr << "[" << idx << "] ";
      std::cerr << "ref: " << ref << ", ";
      std::cerr << "x: " << x << ", ";
      std::cerr << "diff: " << diff << std::endl;
    }
    nnz   += ref != 0;
    error += diff * diff;
  }

  nnz = nnz == 0 ? 1 : nnz;
  error = sqrt(error) / nnz;
  std::clog << "Error: " << error << std::endl;

  return error < tolerance;
}

TEST(QuantizeOutputRunTest)
{
  SUTWrapper sut(8,  // channels
                 32, // inrOutputSamples
                 1,  // inrStations
                 1,  // inrTabs
                 1); // itimeIntegrationFactor

  // Run the kernels
  sut.runKernel();
}

template<typename INT, typename UINT>
void testQuantizeOutput(
  unsigned int nrChannels,
  unsigned int nrSamples,
  float tolerance)
{
  size_t NR_TABS = 64;
  size_t INTEGRATION_SIZE = 1;
  size_t QUANTIZE_BITS = sizeof(INT) * 8;
  float  QUANTIZE_SCALE = 2;

  SUTWrapper sut(nrChannels,
                 nrSamples,
                 1,
                 NR_TABS,
                 INTEGRATION_SIZE,
                 QUANTIZE_BITS,
                 QUANTIZE_SCALE,
                 -QUANTIZE_SCALE);

  // Set the reference output
  initialize(sut.hCoherentStokesOutput);
  transpose4D(sut.hCoherentStokesOutputTransposed, sut.hCoherentStokesOutput);

  // Run the kernels
  sut.runKernel();

  // Reconstruct hOutput from hInput
  sut.qokernel->convertI2F(sut.hQuantizedOutput, sut.hCoherentStokesInput);

  // Get reference to quantized data
  auto qdata = sut.qokernel->getQuantizedData<INT>(sut.hCoherentStokesInput);

  // Set clipped values to zero
  zeroClipped<INT, UINT>(
    sut.hCoherentStokesOutput,
    sut.hQuantizedOutput,
    qdata);

  checkArrayClose(
    sut.hCoherentStokesOutput.data(),
    sut.hQuantizedOutput.data(),
    sut.hQuantizedOutput.num_elements(),
    tolerance);

  CHECK_ARRAY_CLOSE(sut.hCoherentStokesOutput.data(),
                    sut.hQuantizedOutput.data(),
                    sut.hQuantizedOutput.num_elements(),
                    tolerance);
}

TEST(QuantizeOutput8Bit12288Samples)
{
  LOG_INFO("Test QuantizeOutput 8-bit 12288 samples");
  testQuantizeOutput<int8_t, uint8_t>(16, 12288, 1e-1);
}

TEST(QuantizeOutput8Bit196608Samples)
{
  LOG_INFO("Test QuantizeOutput 8-bit 196608 samples");
  testQuantizeOutput<int8_t, uint8_t>(1, 196608, 1e-1);
}

TEST(QuantizeOutput8Bit49152Samples)
{
  LOG_INFO("Test QuantizeOutput 8-bit 49152 samples");
  testQuantizeOutput<int8_t, uint8_t>(4, 49152, 1e-1);
}

TEST(QuantizeOutput8Bit384Samples)
{
  LOG_INFO("Test QuantizeOutput 8-bit 384 samples");
  testQuantizeOutput<int8_t, uint8_t>(512, 384, 1e-1);
}

TEST(QuantizeOutput16Bit12288Samples)
{
  LOG_INFO("Test QuantizeOutput 16-bit 12288 samples");
  testQuantizeOutput<int16_t, uint16_t>(16, 12288, 1e-3);
}

TEST(QuantizeOutput16Bit49152Samples)
{
  LOG_INFO("Test QuantizeOutput 16-bit 49152 samples");
  testQuantizeOutput<int16_t, uint16_t>(4, 49152, 1e-1);
}

TEST(QuantizeOutput16Bit196608Samples)
{
  LOG_INFO("Test QuantizeOutput 16-bit 196608 samples");
  testQuantizeOutput<int16_t, uint16_t>(1, 196608, 1e-1);
}

TEST(QuantizeOutput16Bit384Samples)
{
  LOG_INFO("Test QuantizeOutput 16-bit 384 samples");
  testQuantizeOutput<int16_t, uint16_t>(512, 384, 1e-1);
}

int main(int argc, char *argv[])
{
  const char * testName = "tCoherentStokesKernel";
  INIT_LOGGER(testName);

  Parset ps;
  KernelTestParameters params;
  parseCommandlineParameters(argc, argv, ps, params, testName);
  //  If no arguments were parsed
  try
  {
    gpu::Platform pf;
  }
  catch (gpu::GPUException&)
  {
    cerr << "No GPU device(s) found. Skipping tests." << endl;
    return 3;
  }

  if (!params.parameterParsed)
  {
    cout << "Running unittests" << endl;
    return UnitTest::RunAllTests() == 0 ? 0 : 1;
  }

  gpu::Device device(0);
  vector<gpu::Device> devices(1, device);
  gpu::Context ctx(device);
  gpu::Stream stream(ctx);

  SUTWrapper sut;

  // Setup CoherentStokesKernel
  CoherentStokesKernel::Parameters& csparams = sut.csparameters;
  KernelFactory<CoherentStokesKernel> csfactory(csparams);

  DeviceMemory coherentStokesInputMem(ctx, csfactory.bufferSize(CoherentStokesKernel::INPUT_DATA));
  DeviceMemory coherentStokesOutputMem(ctx, csfactory.bufferSize(CoherentStokesKernel::OUTPUT_DATA));

  unique_ptr<CoherentStokesKernel> coherentStokesKernel(csfactory.create(stream, coherentStokesInputMem, coherentStokesOutputMem));

  // Setup QuantizeOutputKernel
  QuantizeOutputKernel::Parameters& qoparams = sut.qoparameters;
  KernelFactory<QuantizeOutputKernel> qofactory(qoparams);

  unique_ptr<QuantizeOutputKernel> quantizeOutputKernel(qofactory.create(stream, coherentStokesOutputMem, coherentStokesInputMem));

  BlockID blockId;

  // run
  coherentStokesKernel->enqueue(blockId);
  quantizeOutputKernel->enqueue(blockId);
  stream.synchronize();
}
