//# tBeamFormerStationSubsetSubbandProcProcessSb: test of Beamformer subband processor.
//#
//# Copyright (C) 2020  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

#include <lofar_config.h>

#include <complex>
#include <cmath>
#include <iomanip>

#include <Common/LofarLogger.h>
#include <Common/complex.h>
#include <CoInterface/Parset.h>
#include <CoInterface/fpequals.h>
#include <GPUProc/gpu_utils.h>
#include <GPUProc/SubbandProcs/SubbandProc.h>

using namespace std;
using namespace LOFAR::Cobalt;
using namespace LOFAR;

void initialize_input(
  Parset& ps,
  SubbandProcInputData& in)
{
  const size_t nrStations = ps.settings.antennaFieldNames.size();
  const size_t nrSamplesPerSubband = ps.settings.blockSize;
  const size_t nrPolarisations = ps.settings.nrPolarisations;
  const size_t nrBitsPerSample = ps.settings.nrBitsPerSample;

  // Initialize synthetic input to input signal
  for (size_t st = 0; st < nrStations; st++) {
    for (size_t i = 0; i < nrSamplesPerSubband; i++) {
      size_t pol = i % nrPolarisations;
      if (st == 0)
      {
        switch(nrBitsPerSample) {
        case 8:
          reinterpret_cast<i8complex&>(in.inputSamples[st][i][pol][0]) = (st + 1);
          break;
        case 16:
          reinterpret_cast<i16complex&>(in.inputSamples[st][i][pol][0]) = (st + 1);
          break;
        default:
          break;
        }
      }
    }
  }

  // Initialize subbands partitioning administration (struct BlockID). We only
  // do the 1st block of whatever.

  // Block number: 0 .. inf
  in.blockID.block = 0;

  // Subband index in the observation: [0, ps.nrSubbands())
  in.blockID.globalSubbandIdx = 0;

  // Subband index for this pipeline/workqueue: [0, subbandIndices.size())
  in.blockID.localSubbandIdx = 0;

  // Subband index for this SubbandProc
  in.blockID.subbandProcSubbandIdx = 0;

  // Initialize delays. We skip delay compensation, but init anyway,
  // so we won't copy uninitialized data to the device.
  for (size_t i = 0; i < in.delaysAtBegin.num_elements(); i++)
    in.delaysAtBegin.get<float>()[i] = 0.0f;
  for (size_t i = 0; i < in.delaysAfterEnd.num_elements(); i++)
    in.delaysAfterEnd.get<float>()[i] = 0.0f;
  for (size_t i = 0; i < in.phase0s.num_elements(); i++)
    in.phase0s.get<float>()[i] = 0.0f;
  for (auto tabDelays : in.tabDelays) {
   for (size_t i = 0; i < tabDelays->num_elements(); i++)
     tabDelays->get<float>()[i] = 0.0f;
  }
}

void initialize_output(
  SubbandProcOutputData& out)
{
  for (size_t i = 0; i < out.coherentData().data_num_elements(); i++)
    out.coherentData().get_data_origin()[i] = 42.0f;
  for (size_t i = 0; i < out.incoherentData().data_num_elements(); i++)
    out.incoherentData().get_data_origin()[i] = 42.0f;
}


int main() {
  INIT_LOGGER("tBeamFormerStationSubsetSubbandProcProcessSb");

  try {
    gpu::Platform pf;
    cout << "Detected " << pf.size() << " CUDA devices" << endl;
  } catch (gpu::CUDAException& e) {
    cerr << e.what() << endl;
    return 3;
  }

  gpu::Device device(0);
  vector<gpu::Device> devices(1, device);
  gpu::Context ctx(device);

  Parset ps("tBeamFormerStationSubsetSubbandProcProcessSb.parset");

  // Set the stations to process in the preprocessor
  ps.replace("Cobalt.BeamFormer.stationList", "[CS002,CS003,CS004,CS005,CS006,CS007]");
  ps.replace("Cobalt.BeamFormer.Pipeline[0].stationList", "[CS002,CS003,CS004,CS005,CS006,CS007]");
  ps.updateSettings();

  // Input array sizes
  const size_t nrBeams = ps.settings.SAPs.size();
  const size_t nrStations = ps.settings.antennaFieldNames.size();
  const size_t nrBFStations = ps.settings.beamFormer.pipelines[0].antennaFieldNames.size();
  const size_t nrPolarisations = ps.settings.nrPolarisations;
  const size_t nrSamplesPerSubband = ps.settings.blockSize;
  const size_t nrBitsPerSample = ps.settings.nrBitsPerSample;
  const size_t nrBytesPerComplexSample = ps.nrBytesPerComplexSample();

  const unsigned fftSize = ps.settings.beamFormer.nrDelayCompensationChannels;

  // We only support 8-bit or 16-bit input samples
  ASSERT(nrBitsPerSample == 8 || nrBitsPerSample == 16);

  LOG_INFO_STR(
    "Input info:" <<
    "\n  nrBeams = " << nrBeams <<
    "\n  nrStations = " << nrStations <<
    "\n  nrBFStations = " << nrBFStations <<
    "\n  nrPolarisations = " << nrPolarisations <<
    "\n  nrSamplesPerSubband = " << nrSamplesPerSubband <<
    "\n  nrBitsPerSample = " << nrBitsPerSample <<
    "\n  nrBytesPerComplexSample = " << nrBytesPerComplexSample <<
    "\n  fftSize = " << fftSize);

  // Create very simple kernel programs, with predictable output. Skip as much
  // as possible. Nr of channels/sb from the parset is 1, so the PPF will not
  // even run. Parset also has turned of delay compensation and bandpass
  // correction (but that kernel will run to convert int to float and to
  // transform the data order).

  // Initialize reference parset (copy of the base parset)
  auto ps_ref = ps;

  // Initialize parset with station subset, the preprocessor step processes
  // one station more than the coherent and incoherent steps
  auto ps_sub = ps;
  ps_sub.replace("Cobalt.BeamFormer.stationList", "[CS001,CS002,CS003,CS004,CS005,CS006,CS007]");
  ps_sub.updateSettings();

  // Initialize input data
  SubbandProcInputData in_ref(ps_ref, ctx);
  initialize_input(ps_ref, in_ref);
  SubbandProcInputData in_sub(ps_sub, ctx);
  initialize_input(ps_sub, in_sub);

  // Sanity check: the input data objects should be the same size
  ASSERTSTR(in_ref.inputSamples.num_elements() == in_sub.inputSamples.num_elements(), "length(in_ref) != length(in_sub)");

  // Initialize output data
  SubbandProcOutputData out_ref(ps_ref, ctx);
  initialize_output(out_ref);
  SubbandProcOutputData out_sub(ps_sub, ctx);
  initialize_output(out_sub);

  // Sanity check: the output data objects should be the same size
  ASSERTSTR(out_ref.coherentData().data_num_elements() == out_sub.coherentData().data_num_elements(), "length(out_ref) != length(out_sub)");

  // Initialize SubbandProcs
  SubbandProc bwq_ref(ps_ref, ctx);
  SubbandProc bwq_sub(ps_sub, ctx);

  // Don't bother initializing out.blockID; processSubband() doesn't need it.

  cout << "processSubband()" << endl;
  bwq_ref.processSubband(in_ref, out_ref);
  bwq_ref.synchronize();
  bwq_sub.processSubband(in_sub, out_sub);
  bwq_sub.synchronize();
  cout << "processSubband() done" << endl;

  cout << "Output: " << endl;

  // Output verification

  // *** COHERENT STOKES ***

  for (size_t t = 0; t < ps.settings.beamFormer.coherentSettings.nrSamples; t++)
  {
    for (size_t c = 0; c < ps.settings.beamFormer.coherentSettings.nrChannels; c++)
    {
      auto result_ref = out_ref.coherentData().get_data(0,0,t,c);
      auto result_sub = out_sub.coherentData().get_data(0,0,t,c);
      ASSERTSTR(fpEquals(result_ref, result_sub, 1e-4f),
                "out.coherentData.data[0][0][" << t << "][" << c << "] = " <<
                setprecision(12) << result_sub <<
                "; reference = " << result_ref);
    }
  }

  // *** INCOHERENT STOKES ***

  for (size_t t = 0; t < ps.settings.beamFormer.incoherentSettings.nrSamples; t++)
  {
    for (size_t c = 0; c < ps.settings.beamFormer.incoherentSettings.nrChannels; c++)
    {
      auto result_ref = out_ref.incoherentData().get_data(0,0,t,c);
      auto result_sub = out_sub.incoherentData().get_data(0,0,t,c);
      ASSERTSTR(fpEquals(result_ref, result_sub, 1e-4f),
                "out.incoherentData.data[0][0][" << t << "][" << c << "] = " <<
                setprecision(12) << result_sub <<
                "; reference = " << result_ref);
    }
  }

  return 0;
}

