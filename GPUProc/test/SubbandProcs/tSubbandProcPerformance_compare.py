#!/usr/bin/env python3

import sys
import argparse
import csv
import os

def create_arg_parser():
    # Creates and returns the ArgumentParser object
    parser = argparse.ArgumentParser(description='Compare mean runtime from different runs from gpu_load performance benchmarks.')
    parser.add_argument('filename_reference',
                    help='The full path to the reference file')
    parser.add_argument('filename_candidate',
                    help='The full path to the current file')
    parser.add_argument('--tolerance', type=float, default=5,
                    help='Maximum tolerable performance degradation (in percent)')
    return parser

argParser = create_arg_parser()
parsedArgs = argParser.parse_args(sys.argv[1:])

# Reference input
filename_reference = parsedArgs.filename_reference
if not os.path.exists(filename_reference):
    print("Could not open: {}".format(filename_reference))
    exit(1)

# Candidate input
filename_candidate = parsedArgs.filename_candidate
if not os.path.exists(filename_candidate):
    print("Could not open: {}".format(filename_candidate))
    exit(1)

# Tolerance
tolerance = parsedArgs.tolerance

# Print arguments
print(">>> Parameters")
print("Reference: {}".format(filename_reference))
print("Candidate: {}".format(filename_candidate))
print("Tolerance: {}%".format(tolerance))

# Read measurements
def read_measurements(filename):
    # Open results file, skip the first two lines:
    # Line 0: date
    # Line 1: info
    # Parse the remainder as dictionary
    # Line 3: header
    # Line 4 and further: data
    data = open(filename).readlines()[2:]
    reader = csv.DictReader(data, delimiter=";")
    measurements = dict()
    for row in reader:
        name = row[' kernelName'].lstrip()
        mean = float(row[' mean'])
        count = int(row[' count'])
        measurements[name] = mean

    return measurements

measurements_reference = read_measurements(filename_reference)
measurements_candidate = read_measurements(filename_candidate)
runtime_total = measurements_reference["total"]
runtime_threshold = runtime_total * 0.05 # 5 percent

# Organize measurements in categories passed, failed and skipped
class Measurement():
    name = ""
    runtime_reference = 0.0
    runtime_candidate = 0.0
    result = 0.0

    def __str__(self):
        name = '\"{}\"'.format(self.name)
        return '{:32}: {:5.2f} -> {:5.2f} ({:5.1f} %)'.format(name, self.runtime_reference, self.runtime_candidate, self.result)

measurements = set()
missing = set()

for name, runtime_reference in measurements_reference.items():
    if (name in measurements_candidate):
        # Skip very short measurements
        if (runtime_reference < runtime_threshold):
            continue

        # Add measurement entry
        measurement = Measurement()
        measurement.name = name
        measurement.runtime_reference = runtime_reference
        measurement.runtime_candidate = measurements_candidate[name]
        measurement.result = measurement.runtime_candidate / runtime_reference * 100
        measurements.add(measurement)
    else:
        missing.append("\"{}\"".format(name))
        status = 1

passed = set(filter(lambda measurement : measurement.result < (100 + tolerance), measurements))
failed = list(measurements - passed)
passed = list(passed)

passed = sorted(passed, key=lambda m: m.runtime_reference, reverse=True)
failed = sorted(failed, key=lambda m: m.runtime_reference, reverse=True)

# Print summary
print(">>> Results")
print("PASSED:", end='')
if len(passed):
    print()
    print("\n".join([str(m) for m in passed]))
else:
    print(" none")

print("FAILED:", end='')
if len(failed):
    print()
    print("\n".join([str(m) for m in failed]))
else:
    print(" none")

print("MISSING:", end='')
if len(missing):
    print()
    print(", ".join(missing))
else:
    print(" none")

# Determine exit status and exit
if (len(failed) > 0 or len(missing) > 0):
    exit(1)
else:
    exit(0)