#include <cooperative_groups.h>

#include "reduction.cuh"

namespace cg = cooperative_groups;

extern "C" __global__ void reduction(float *out, float *in, int size) {
  cg::thread_block block = cg::this_thread_block();
  __shared__ float s_data[BATCH_SIZE];
  *out = reduce_sum(block, in, size, s_data, BATCH_SIZE);
}