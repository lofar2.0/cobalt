#include <lofar_config.h>

#include <iostream>
#include <vector>

#include <boost/lexical_cast.hpp>
#include <UnitTest++.h>

#include <Common/LofarLogger.h>

#include <GPUProc/MultiDimArrayHostBuffer.h>
#include <GPUProc/gpu_utils.h>
#include <GPUProc/gpu_wrapper.h>

using namespace LOFAR::Cobalt;

static gpu::Stream *stream;

template <typename T>
void runKernel(gpu::Function kfunc, MultiDimArrayHostBuffer<T, 1> &input,
               MultiDimArrayHostBuffer<T, 1> &output, int block_size) {
  gpu::Context ctx(stream->getContext());

  gpu::DeviceMemory devInput(ctx, input.size());
  gpu::DeviceMemory devOutput(ctx, output.size());
  int n = input.num_elements();

  kfunc.setArg(0, devOutput);
  kfunc.setArg(1, devInput);
  kfunc.setArg(2, n);

  gpu::Grid globalWorkSize(1, 1, 1);
  gpu::Block localWorkSize(block_size, 1, 1);

  stream->writeBuffer(devOutput, output);
  stream->writeBuffer(devInput, input);
  stream->launchKernel(kfunc, globalWorkSize, localWorkSize);
  stream->readBuffer(output, devOutput);
  stream->synchronize();
}

gpu::Function initKernel(gpu::Context ctx, const CompileDefinitions &defs) {
  std::string kernelPath("tReduction.in_.cu");
  CompileFlags flags(defaultCompileFlags());
  std::vector<gpu::Device> devices(1, gpu::Device(0));
  std::string ptx(createPTX(kernelPath, defs, flags, devices));
  gpu::Module module(createModule(ctx, kernelPath, ptx));
  gpu::Function kfunc(module, "reduction");

  return kfunc;
}

template <typename T> T computeValue(int i) {
  return static_cast<T>((i + 1) % 100 + (i % 16) / 100);
}

template <typename T> void initArray(MultiDimArrayHostBuffer<T, 1> &buf) {
  for (size_t i = 0; i < buf.num_elements(); i++) {
    buf[i] = computeValue<T>(i);
  }
}

float computeResult(int n) {
  float result = 0;
  for (int i = 0; i < n; i++) {
    result += computeValue<float>(i);
  }
  return result;
}

template <typename T> T runTest(int n, int batch_size, int block_size) {
  gpu::Context ctx(stream->getContext());

  MultiDimArrayHostBuffer<T, 1> input(boost::extents[n], ctx);
  MultiDimArrayHostBuffer<T, 1> output(boost::extents[1], ctx);

  initArray(input);
  memset(output.origin(), 0, output.size());

  CompileDefinitions compileDefs;
  compileDefs["BATCH_SIZE"] = boost::lexical_cast<string>(batch_size);
  gpu::Function kfunc(initKernel(ctx, compileDefs));

  runKernel(kfunc, input, output, block_size);

  return output.origin()[0];
}

TEST(Float) {
  std::vector<int> block_sizes;
  block_sizes.push_back(128);
  block_sizes.push_back(384);
  block_sizes.push_back(1024);

  for (int block_size : block_sizes) {
    std::vector<int> batch_sizes;
    batch_sizes.push_back(block_size / 2);
    batch_sizes.push_back(block_size);
    batch_sizes.push_back(2 * block_size);
    batch_sizes.push_back(3 * block_size);

    for (int batch_size : batch_sizes) {
      std::vector<int> input_lengths;
      input_lengths.push_back(32);
      input_lengths.push_back(100);
      input_lengths.push_back(block_size);
      input_lengths.push_back(block_size - 1);
      input_lengths.push_back(block_size + 1);
      if (block_size != batch_size) {
        input_lengths.push_back(batch_size);
      }
      input_lengths.push_back(batch_size * 0.8);
      input_lengths.push_back(batch_size * 1.2);

      for (int n : input_lengths) {
        const float result = runTest<float>(n, batch_size, block_size);
        CHECK_CLOSE(computeResult(n), result, 1e-6);
      }
    }
  }
}

gpu::Stream initDevice() {
  try {
    gpu::Platform pf;
    std::cout << "Detected " << pf.size() << " GPU devices" << std::endl;
  } catch (gpu::CUDAException &e) {
    std::cerr << e.what() << std::endl;
    exit(3); // test skipped
  }
  gpu::Device device(0);
  std::vector<gpu::Device> devices(1, device);
  gpu::Context ctx(device);
  gpu::Stream cuStream(ctx);

  return cuStream;
}

int main() {
  INIT_LOGGER("tReduction");

  gpu::Stream strm(initDevice());
  stream = &strm;

  int exitStatus = UnitTest::RunAllTests();
  return exitStatus > 0 ? 1 : 0;
}