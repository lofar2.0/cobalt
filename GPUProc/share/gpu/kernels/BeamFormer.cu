//# BeamFormer.cu
//# Copyright (C) 2012-2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

#include "gpu_math.cuh"

//# Some defines used to determine the correct way the process the data
#define MAX(A,B) ((A)>(B) ? (A) : (B))

#define NR_PASSES MAX((NR_OUTPUT_STATIONS + 6) / 16, 1) // gives best results on GTX 680

#ifndef NR_STATIONS_PER_PASS  // Allow overriding for testing optimalizations 
#define NR_STATIONS_PER_PASS ((NR_OUTPUT_STATIONS + NR_PASSES - 1) / NR_PASSES)
#endif
#if NR_STATIONS_PER_PASS > 32
#error "need more passes to beamform this number of stations"
#endif

#ifndef NR_TIME_PER_PASS
#define NR_TIME_PER_PASS 16
#endif

#ifdef FLYS_EYE
# if !(NR_INPUT_STATIONS == NR_TABS)
# error Precondition violated: NR_INPUT_STATIONS == NR_TABS
# endif
#endif

#define STATION_INDEX(s) (stationIndices[s])
#define DELAY_INDEX(s) (delayIndices[s])

//# Typedefs used to map input data on arrays
typedef  double (*DelaysType)[NR_DELAYS][NR_TABS];
#ifdef FLYS_EYE
typedef  float2 (*BandPassCorrectedType)[NR_INPUT_STATIONS][NR_CHANNELS][NR_SAMPLES_PER_CHANNEL][NR_POLARIZATIONS];
#else
typedef  float4 (*BandPassCorrectedType)[NR_INPUT_STATIONS][NR_CHANNELS][NR_SAMPLES_PER_CHANNEL];
#endif
typedef  float2 (*ComplexVoltagesType)[NR_CHANNELS][NR_SAMPLES_PER_CHANNEL][NR_TABS][NR_POLARIZATIONS];

/*!
 * The beamformer kernel performs a complex weighted multiply-add of each sample
 * of the provided input data, except when fly's eye mode is enabled (FLYS_EYE
 * is defined). Then data is only transposed, nothing more.
 *
 * \param[out] complexVoltagesPtr      4D output array of beams. For each channel a number of Tied Array Beams time serires is created for two polarizations
 * \param[in]  samplesPtr              3D input array of samples. A time series for each station and channel pair. Each sample contains the 2 polarizations X, Y, each of complex float type.
 * \param[in]  delaysPtr               3D input array of complex valued delays to be applied to the correctData samples. There is a delay for each Sub-Array Pointing, station, and Tied Array Beam triplet.
 * \param[in]  delayIndices            1D input array of which stations to use out of delaysPtr, if a subset of the stations need to be beam formed
 * \param[in]  subbandFrequency        central frequency of the subband
 *
 * Pre-processor input symbols (some are tied to the execution configuration)
 * Symbol                  | Valid Values            | Description
 * ----------------------- | ----------------------- | -----------
 * NR_INPUT_STATIONS       | >= 1                    | number of antenna fields in the input
 * NR_OUTPUT_STATIONS    | >= 1                    | number of antenna fields selected for beamforming
 * NR_DELAYS               | >= 1                    | number of delays = number of antenna fields in the observation
 * NR_SAMPLES_PER_CHANNEL  | >= 1                    | number of input samples per channel
 * NR_CHANNELS             | >= 1                    | number of frequency channels per subband
 * NR_TABS                 | >= 1                    | number of Tied Array Beams (old name: pencil beams) to create
 * SUBBAND_BANDWIDTH       | double, multiple of NR_CHANNELS | Bandwidth of a subband in Hz
 * NR_STATIONS_PER_PASS    | 1 >= && <= 32           | Set to overide default: Parallelization parameter, controls the number stations to beamform in a single pass over the input data. 
 *
 * Note that this kernel assumes  NR_POLARIZATIONS == 2
 *
 * Execution configuration:
 * - LocalWorkSize = (NR_POLARIZATIONS, NR_TABS, NR_CHANNELS) Note that for full utilization NR_TABS * NR_CHANNELS % 16 = 0. Also note that NR_CHANNELS should be set to 1 per block (i.e. a 2D block). To process N channels, launch N blocks.
 */
extern "C" __global__ void beamFormer( void *complexVoltagesPtr,
                                       const void *samplesPtr,
                                       const unsigned *stationIndices, // lookup index for stations to use in samplesPtr
                                       const void *delaysPtr, 
                                       const unsigned *delayIndices, // lookup index for stations to use in delaysPtr
                                       double subbandFrequency)
{
  ComplexVoltagesType complexVoltages = (ComplexVoltagesType) complexVoltagesPtr;
  BandPassCorrectedType samples = (BandPassCorrectedType) samplesPtr;

  unsigned pol = threadIdx.x;
  unsigned tab = threadIdx.y;
  unsigned channel = blockDim.z * blockIdx.z + threadIdx.z; // The parallelization in the channel is controllable with extra blocks only, not extra threads per block

#ifdef FLYS_EYE
  if (tab < NR_TABS) {
    unsigned t = blockIdx.x;

    (*complexVoltages)[channel][t][tab][pol] = (*samples)[STATION_INDEX(tab)][channel][t][pol];
  }
#else

  DelaysType delays = (DelaysType) delaysPtr;

  // This union is in shared memory because it is used by all threads in the block
  __shared__ union { // Union: Maps two variables to the same adress space
    float2 samples[NR_STATIONS_PER_PASS][NR_TIME_PER_PASS][NR_POLARIZATIONS];
    float4 samples4[NR_STATIONS_PER_PASS][NR_TIME_PER_PASS];
  } _local;

#if NR_CHANNELS == 1
  double frequency = subbandFrequency;
#else
  double frequency = subbandFrequency - 0.5 * SUBBAND_BANDWIDTH + channel * (SUBBAND_BANDWIDTH / NR_CHANNELS);
#endif

#pragma unroll
  for (unsigned first_station = 0;  // Step over data with NR_STATIONS_PER_PASS stride
       first_station < NR_OUTPUT_STATIONS;
       first_station += NR_STATIONS_PER_PASS) 
  { // this for loop spans the whole file
    fcomplex weights[NR_STATIONS_PER_PASS];

    #pragma unroll
    for (unsigned i = 0; i < NR_STATIONS_PER_PASS; i++)
    {
      // Number of station might be larger then 32:
      // We then do multiple passes to span all stations
      if (first_station + i < NR_OUTPUT_STATIONS && tab < NR_TABS) {
        double delay = (*delays)[DELAY_INDEX(first_station + i)][tab];
        dcomplex weight = dphaseShift(frequency, delay);
        weights[i] = make_float2(weight.x, weight.y);
      }
    }

    // Loop over all the samples in time. Perform the addition for NR_TIME_PER_PASS time steps.
    for (unsigned time = 0; time < NR_SAMPLES_PER_CHANNEL; time += NR_TIME_PER_PASS)
    {
      // precalculate the upper limit and only do the loop for a valid range
      unsigned laststation = min(NR_STATIONS_PER_PASS, NR_OUTPUT_STATIONS - first_station);
      // Optimized memory transfer: Threads load from memory in parallel
      for (unsigned i = threadIdx.x + NR_POLARIZATIONS * threadIdx.y;
                    i < laststation * NR_TIME_PER_PASS;
                    i += blockDim.y * NR_POLARIZATIONS) // Use blockdim steps: memmory access done by more threads then we might have tabs
      {
        unsigned t = i % NR_TIME_PER_PASS;
        unsigned s = i / NR_TIME_PER_PASS;

        if (NR_SAMPLES_PER_CHANNEL % NR_TIME_PER_PASS == 0 || time + t < NR_SAMPLES_PER_CHANNEL)
            _local.samples4[0][i] = (*samples)[STATION_INDEX(first_station + s)][channel][time + t];
      }

      __syncthreads();

      // Some threads are scheduled to fill the wave, so check for actual work.
      // Note that we can't simply call 'continue' for idle threads, as they still need
      // to participate in the sync_threads below
      if (tab < NR_TABS) {
        for (unsigned t = 0; 
                      t < (NR_SAMPLES_PER_CHANNEL % NR_TIME_PER_PASS == 0 ? NR_TIME_PER_PASS : min(NR_TIME_PER_PASS, NR_SAMPLES_PER_CHANNEL - time));
                      t++) 
        {
          fcomplex sum = first_station == 0 ? // The first run the sum should be zero, otherwise we need to take the sum of the previous run
                      make_float2(0,0) :
                      (*complexVoltages)[channel][time + t][tab][pol];

          // Calculate the weighted complex sum of the samples
          #pragma unroll
          for (unsigned j = 0; j < NR_STATIONS_PER_PASS; j++)
          {
            if (first_station + (j+1) <= NR_OUTPUT_STATIONS) {  // Remember that the number of stations might not be a multiple of 32. Skip if station does not exist
              fcomplex sample = _local.samples[j][t][pol];
              sum = sum + weights[j] * sample;
            }
          }

          // Write data to global mem
          (*complexVoltages)[channel][time + t][tab][pol] = sum;
        }
      }

      __syncthreads();
    }
  }
#endif /* FLYS_EYE */
}

