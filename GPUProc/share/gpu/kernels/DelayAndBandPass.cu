//# DelayAndBandPass.cu
//# Copyright (C) 2012-2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

/** @file
* This file contains a CUDA implementation of the GPU kernel for the delay
* and bandpass correction. It can also transpose the data (pol to dim 0).
*
* Usually, this kernel will be run after the polyphase filter kernel FIR.cl. In
* that case, the input data for this kernel is already in floating point format
* (@c NR_CHANNELS > 1). However, if this kernel is the first in row, then the
* input data is still in integer format (@c NR_CHANNELS == 1), and this kernel
* needs to do the integer-to-float conversion. If we do BANDPASS_CORRECTION
* (implies NR_CHANNELS > 1), then we typically also want to transpose the pol
* dim to the stride 1 dim (@c DO_TRANSPOSE).
*
* @attention The following pre-processor variables must be supplied when
* compiling this program. Please take the pre-conditions for these variables
* into account:
* - @c NR_CHANNELS: 1 or a multiple of 16
* - if @c NR_CHANNELS == 1 (input data is in integer format):
*   - @c NR_BITS_PER_SAMPLE: 4, 8 or 16
*   - @c NR_SAMPLES_PER_SUBBAND: a multiple of 16
* - if @c NR_CHANNELS > 1 (input data is in floating point format):
*   - @c NR_SAMPLES_PER_CHANNEL: a multiple of 16
* - @c NR_POLARIZATIONS: 2
* - @c SUBBAND_BANDWIDTH: a multiple of @c NR_CHANNELS
*
* - @c DELAY_COMPENSATION: defined or not
* - @c BANDPASS_CORRECTION: defined or not
* - @c DO_TRANSPOSE: defined or not
* - @c DOPPLER_CORRECTION: if defined, DELAY_COMPENSATION and CLOCK_MHZ also must be defined
* - @c CLOCK_MHZ: clock frequency in MHz, normally 200 or 160 
*/

#include "gpu_math.cuh"

#include "IntToFloat.cuh"

#if NR_CHANNELS == 1
//# #chnl==1 && BANDPASS_CORRECTION is rejected on the CPU early, (TODO)
//# but once here, don't do difficult and adjust cleanly here.
#  undef BANDPASS_CORRECTION
#endif

#if defined DOPPLER_CORRECTION   
#ifndef CLOCK_MHZ
#error DOPPLER_CORRECTION=1 but CLOCK_MHZ not defined
#endif
#ifndef DELAY_COMPENSATION
#error DOPPLER_CORRECTION=1 but DELAY_COMPENSATION not enabled
#endif
#endif

#if defined DO_TRANSPOSE
typedef  fcomplex(*OutputDataType)[NR_STATIONS][NR_CHANNELS][NR_SAMPLES_PER_CHANNEL][NR_POLARIZATIONS];
#else
typedef  fcomplex(*OutputDataType)[NR_STATIONS][NR_POLARIZATIONS][NR_CHANNELS][NR_SAMPLES_PER_CHANNEL];
#endif

typedef  fcomplex(*InputDataType)[NR_STATIONS][NR_POLARIZATIONS][NR_SAMPLES_PER_CHANNEL][NR_CHANNELS];
typedef  const double(*DelaysType)[NR_DELAYS][NR_POLARIZATIONS]; // 2 Polarizations; in seconds
typedef  const double2(*Phase0sType)[NR_STATIONS]; // 2 Polarizations; in radians
typedef  const float(*BandPassFactorsType)[NR_CHANNELS];

#if defined DO_ZEROING
typedef char MaskType[NR_STATIONS][NR_SAMPLES_PER_CHANNEL];
#endif

inline __device__ fcomplex sincos_f2f(float phi)
{
  float2 r;

  sincosf(phi, &r.y, &r.x);
  return r;
}

inline __device__ fcomplex sincos_d2f(double phi)
{
  double s, c;

  sincos(phi, &s, &c);
  return make_float2(c, s);
}

/**
* This kernel performs (up to) three operations on the input data:
* - Apply a fine delay by doing a per channel phase correction.
* - Apply a bandpass correction to compensate for the errors introduced by the
*   polyphase filter that produced the subbands. This error is deterministic,
*   hence it can be fully compensated for.
* - Transpose the data so that the time slices for each channel are placed
*   consecutively in memory.
*
* @param[out] correctedDataPtr    pointer to output data of ::OutputDataType,
*                                 a 3D array [station][channel][sample][complex]
*                                 of ::complex (2 complex polarizations)
* @param[in]  filteredDataPtr     pointer to input data; this can either be a
*                                 4D array [station][polarization][sample][channel][complex]
*                                 of ::fcomplex
* @param[in]  subbandFrequency    center freqency of the subband
* @param[in]  delaysAtBeginPtr    pointer to delay data of ::DelaysType,
*                                 a 1D array [station] of float2 (real:
*                                 2 polarizations), containing delays in
*                                 seconds at begin of integration period
* @param[in]  delaysAfterEndPtr   pointer to delay data of ::DelaysType,
*                                 a 1D array [station] of float2 (real:
*                                 2 polarizations), containing delays in
*                                 seconds after end of integration period
* @param[in]  phase0sPt     r     pointer to phase offset data of
*                                 ::Phase0sType, a 1D array [station] of
*                                 float2 (real: 2 polarizations), containing
*                                 phase offsets in radians
* @param[in]  bandPassFactorsPtr  pointer to bandpass correction data of
*                                 ::BandPassFactorsType, a 1D array [channel] of
*                                 float, containing bandpass correction factors
*/

extern "C" {
  __global__ void applyDelaysAndCorrectBandPass(fcomplex * correctedDataPtr,
    const fcomplex * filteredDataPtr,
    const unsigned * delayIndices,
    double subbandFrequency,
    const double * delaysAtBeginPtr,
    const double * delaysAfterEndPtr,
    const double * phase0sPtr,
    const float * bandPassFactorsPtr
#if defined DO_ZEROING
    , MaskType mask
#endif
  ) {
    OutputDataType outputData = (OutputDataType)correctedDataPtr;
    InputDataType inputData = (InputDataType)filteredDataPtr;

    /* The z dimension is NR_STATIONS wide. */
    const unsigned station = blockIdx.z * blockDim.z + threadIdx.z;
#if defined DELAY_COMPENSATION
    const unsigned delayIdx = delayIndices[station];
#endif

    /*
     * channel: will cover all channels
     * timeStart & timeInc: will cover all samples
     */

#ifdef DO_TRANSPOSE
    /* X width: 256 wide, split into major and minor. */
    /* minor is used to index channels */
    /* major is used to index time */
    /* (minor,major) is used for a local transpose before writing output */
    const unsigned major = (blockIdx.x * blockDim.x + threadIdx.x) / 16;
    const unsigned minor = (blockIdx.x * blockDim.x + threadIdx.x) % 16;

    /* Y width: NR_CHANNELS/16 (or 1 if NR_CHANNELS == 1) */
    const unsigned channel = NR_CHANNELS == 1 ? 0 : (blockIdx.y * blockDim.y + threadIdx.y) * 16 + minor;

    /* NR_CHANNELS == 1: Start at 0..255,        jump by 256 */
    /* NR_CHANNELS  > 1: Start at 0..15 (major), jump by 16 */
    const unsigned timeStart = NR_CHANNELS == 1 ? threadIdx.x : major;
    const unsigned timeInc = NR_CHANNELS == 1 ? blockDim.x : 16;
#else
    /* Y width: nrChannels, with 16 per block (or 1 if NR_CHANNELS == 1) */
    const unsigned channel = blockIdx.y * blockDim.y + threadIdx.y;

    /* X width: nrSamplesPerChannel, with 16 per block */
    const unsigned timeStart = blockIdx.x * blockDim.x + threadIdx.x;

    /* Do one sample per thread */
    const unsigned timeInc = NR_SAMPLES_PER_CHANNEL;
#endif

#if defined BANDPASS_CORRECTION
#ifndef DOPPLER_CORRECTION
    BandPassFactorsType bandPassFactors = (BandPassFactorsType)bandPassFactorsPtr;

    float weight((*bandPassFactors)[channel]);
#endif
#endif

#if defined DELAY_COMPENSATION
    DelaysType delaysAtBegin = (DelaysType)delaysAtBeginPtr;
    DelaysType delaysAfterEnd = (DelaysType)delaysAfterEndPtr;
    Phase0sType phase0s = (Phase0sType)phase0sPtr;

    /*
    * Delay compensation means rotating the phase of each sample BACK.
    *
    * n     = channel number (f.e. 0 .. 255)
    * f_n   = channel frequency of channel n
    * f_ref = base frequency of subband (f.e. 200 MHz)
    * df    = delta frequency of 1 channel (f.e. 768 Hz)
    *
    * f_n := f_ref + n * df
    *
    * m      = sample number (f.e. 0 .. 3071)
    * tau_m  = delay at sample m
    * tau_0  = delayAtBegin (f.e. -2.56us .. +2.56us)
    * dtau   = delta delay for 1 sample (f.e. <= 1.6ns)
    *
    * tau_m := tau_0 + m * dtau
    *
    * Then, the required phase shift is:
    *
    *   phi_mn = -2 * pi * f_n * tau_m
    *          = -2 * pi * (f_ref + n * df) * (tau_0 + m * dtau)
    *          = -2 * pi * (f_ref * tau_0 + f_ref * m * dtau + tau_0 * n * df + m * n * df * dtau)
    *                       -------------   ----------------   --------------   -----------------
    *                           O(100)           O(0.1)            O(0.01)          O(0.001)
    *
    * Finally, we also want to correct for fixed phase offsets per station,
    * as given by the phase0 array.
    */

    const double frequency = NR_CHANNELS == 1
      ? subbandFrequency
      : subbandFrequency - 0.5 * SUBBAND_BANDWIDTH + channel * (SUBBAND_BANDWIDTH / NR_CHANNELS);

    const double2 delayAtBegin = make_double2((*delaysAtBegin)[delayIdx][0], (*delaysAtBegin)[delayIdx][1]);
    const double2 delayAfterEnd = make_double2((*delaysAfterEnd)[delayIdx][0], (*delaysAfterEnd)[delayIdx][1]);

    // Calculate the angles to rotate for for the first and (beyond the) last sample.
    //
    // We need to undo the delay, so we rotate BACK, resulting in a negative constant factor.
#if defined DOPPLER_CORRECTION   
    const double2 freqOffset=(( delayAfterEnd - delayAtBegin ))*((subbandFrequency / NR_CHANNELS)/(NR_SAMPLES_PER_CHANNEL)); //divide this with (CLOCK_MHZ*1e6/1024.0)/NR_CHANNELS to get channel offset 

    // Since Doppler correction has already been applied at subbandFrequency,
    // we shift frequencies
    const double2 phiAtBegin = make_double2(-2.0 * M_PI * (frequency+freqOffset.x) * delayAtBegin.x - ((*phase0s)[delayIdx]).x,
            -2.0 * M_PI * (frequency+freqOffset.y) * delayAtBegin.y - ((*phase0s)[delayIdx]).y);
    const double2 phiAfterEnd = make_double2(-2.0 * M_PI * (frequency+freqOffset.x) * delayAfterEnd.x - ((*phase0s)[delayIdx]).x,
            -2.0 * M_PI * (frequency+freqOffset.y) * delayAfterEnd.y - ((*phase0s)[delayIdx]).y);

#if defined BANDPASS_CORRECTION
    BandPassFactorsType bandPassFactors = (BandPassFactorsType)bandPassFactorsPtr;

    // positive offset means moving to right ->->
    const double2 chanOffset=freqOffset*(NR_CHANNELS/(CLOCK_MHZ*1e6/1024.0)); //mult this with (CLOCK_MHZ*1e6/1024.0)/NR_CHANNELS to get freq

    const float2 chanShifted={chanOffset.x+channel,chanOffset.y+channel};
    unsigned chanlow[2]={__float2uint_rd(chanShifted.x),__float2uint_rd(chanShifted.y)};
    // check and adjust to valid range
    chanlow[0]=(chanlow[0]>NR_CHANNELS-1?NR_CHANNELS-1:chanlow[0]);
    chanlow[1]=(chanlow[1]>NR_CHANNELS-1?NR_CHANNELS-1:chanlow[1]);
    const unsigned chanhigh[2]={(chanlow[0]<NR_CHANNELS-1?chanlow[0]+1:chanlow[0]),
                          (chanlow[1]<NR_CHANNELS-1?chanlow[1]+1:chanlow[1])};
    const float2 w1={chanShifted.x-chanlow[0],chanShifted.y-chanlow[1]};
    float2 weight=make_float2((*bandPassFactors)[chanlow[0]]*(1.0f-w1.x)+(*bandPassFactors)[chanhigh[0]]*w1.x,
               (*bandPassFactors)[chanlow[1]]*(1.0f-w1.y)+(*bandPassFactors)[chanhigh[1]]*w1.y);
#endif
#else
    const double2 phiAtBegin = -2.0 * M_PI * frequency * delayAtBegin - (*phase0s)[delayIdx];
    const double2 phiAfterEnd = -2.0 * M_PI * frequency * delayAfterEnd - (*phase0s)[delayIdx];
#endif // DOPPLER_CORRECTION
#endif // DELAY_COMPENSATION

    for (unsigned time = timeStart; time < NR_SAMPLES_PER_CHANNEL; time += timeInc)
    {
      fcomplex sampleX = make_float2(0.0f, 0.0f);
      fcomplex sampleY = make_float2(0.0f, 0.0f);

#if defined DO_ZEROING
      if (!mask[station][time]) {
        sampleX = (*inputData)[station][0][time][channel];
        sampleY = (*inputData)[station][1][time][channel];
      }
#else
      sampleX = (*inputData)[station][0][time][channel];
      sampleY = (*inputData)[station][1][time][channel];
#endif

#if defined DELAY_COMPENSATION    
      // Offset of this sample between begin and end.
      const double timeOffset = double(time) / NR_SAMPLES_PER_CHANNEL;

      // Interpolate the required phase rotation for this sample.
      //
      // Single precision angle + sincos is measured to be good enough (2013-11-20).
      // Note that we do the interpolation in double precision still.
      // We can afford to if we keep this kernel memory bound.
      const float2 phi = make_float2(phiAtBegin.x  * (1.0 - timeOffset)
        + phiAfterEnd.x *        timeOffset,
        phiAtBegin.y  * (1.0 - timeOffset)
        + phiAfterEnd.y *        timeOffset);

      sampleX = sampleX * sincos_f2f(phi.x);
      sampleY = sampleY * sincos_f2f(phi.y);
#endif

#if defined BANDPASS_CORRECTION
#if defined DOPPLER_CORRECTION   
      sampleX.x *= weight.x;
      sampleX.y *= weight.x;
      sampleY.x *= weight.y;
      sampleY.y *= weight.y;
#else
      sampleX.x *= weight;
      sampleX.y *= weight;
      sampleY.x *= weight;
      sampleY.y *= weight;
#endif
#endif

      // Support all variants of NR_CHANNELS and DO_TRANSPOSE for testing etc.
      // Transpose: data order is [station][channel][time][pol]
#if NR_CHANNELS > 1 && defined DO_TRANSPOSE
      __shared__ fcomplex tmp[16][17][2]; // one too wide to avoid bank-conflicts on read

      tmp[major][minor][0] = sampleX;
      tmp[major][minor][1] = sampleY;
      __syncthreads();
      (*outputData)[station][channel - minor + major][time - major + minor][0] = tmp[minor][major][0];
      (*outputData)[station][channel - minor + major][time - major + minor][1] = tmp[minor][major][1];
      __syncthreads();
#elif NR_CHANNELS == 1 && defined DO_TRANSPOSE
      (*outputData)[station][0][time][0] = sampleX;
      (*outputData)[station][0][time][1] = sampleY;

#else
      // No transpose: data order is [station][pol][channel][time]
      (*outputData)[station][0][channel][time] = sampleX;
      (*outputData)[station][1][channel][time] = sampleY;
#endif
    }
  }
}

