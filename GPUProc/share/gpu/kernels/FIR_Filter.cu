//# FIR_Filter.cu
//# Copyright (C) 2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

#include "IntToFloat.cuh"
#include "gpu_math.cuh"

#if !(NR_STABS >= 1)
#error Precondition violated: NR_STABS >= 1
#endif

#if !(NR_TAPS == 16)
#error Precondition violated: NR_TAPS == 16
#endif

#if !(NR_SAMPLES_PER_CHANNEL > 0 && NR_SAMPLES_PER_CHANNEL % NR_TAPS == 0)
#error Precondition violated: NR_SAMPLES_PER_CHANNEL > 0 && NR_SAMPLES_PER_CHANNEL % NR_TAPS == 0
#endif

#ifdef INPUT_IS_STATIONDATA
// Each SampleType is HALF a COMPLEX sample (so real OR imag)
#  if NR_BITS_PER_SAMPLE == 16
typedef signed short SampleType;
#  elif NR_BITS_PER_SAMPLE == 8
typedef signed char SampleType;
#  elif NR_BITS_PER_SAMPLE == 4
// Input samples are half a byte, requiring a special implementation.
// The HistorySamples will store each input as a full byte though,
// to avoid unnecessary marshalling and unmarshalling, and locking
// between different threads that write real and imag of the same (complex) sample.
typedef signed char SampleType;
#  else
#    error Precondition violated: NR_BITS_PER_SAMPLE == 4 || NR_BITS_PER_SAMPLE == 8 || NR_BITS_PER_SAMPLE == 16
#  endif
#else
typedef float SampleType;
#endif

#if !(NR_CHANNELS > 0 && NR_CHANNELS % 16 == 0)
#error Precondition violated: NR_CHANNELS > 0 && NR_CHANNELS % 16 == 0
#endif

#if !(NR_POLARIZATIONS == 2)
#error Precondition violated: NR_POLARIZATIONS == 2
#endif

#if !(COMPLEX == 2)
#error Precondition violated: COMPLEX == 2
#endif

//# NR_STABS means #stations (correlator) or #TABs (beamformer).

#ifdef INPUT_IS_STATIONDATA
// Data comes from station: icomplex[stab][sample][pol]
#  if NR_BITS_PER_SAMPLE == 4
typedef signed char (*SampledDataType)[NR_STABS][NR_SAMPLES_PER_CHANNEL][NR_CHANNELS][NR_POLARIZATIONS];
#define SAMPLE(time) extractRI((*sampledData)[station][time][channel][pol], ri)
#  else
#ifndef DOPPLER_CORRECTION
typedef SampleType (*SampledDataType)[NR_STABS][NR_SAMPLES_PER_CHANNEL][NR_CHANNELS][NR_POLARIZATIONS * COMPLEX];
#define SAMPLE(time) (*sampledData)[station][time][channel][pol_ri]
#else //DOPPLER_CORRECTION
typedef SampleType (*SampledDataType)[NR_STABS][NR_SAMPLES_PER_CHANNEL][NR_CHANNELS][NR_POLARIZATIONS][COMPLEX];
#define REAL(time) convertIntToFloat((*sampledData)[station][time][channel][pol][0])
#define IMAG(time) convertIntToFloat((*sampledData)[station][time][channel][pol][1])
#endif //DOPPLER_CORRECTION
#endif

#else

// Data comes from beam-former pipeline: fcomplex [stab][pol][sample]
typedef float (*SampledDataType)[NR_STABS][NR_POLARIZATIONS][NR_SAMPLES_PER_CHANNEL][NR_CHANNELS][COMPLEX];
#define SAMPLE(time) (*sampledData)[station][pol][time][channel][ri]

// No conversion needed, but define this function to keep the code simple
inline __device__ float convertIntToFloat(float x)
{
  return x;
}
#endif

// subbandIdx=1 is assumed below
#ifdef DOPPLER_CORRECTION
typedef float (*HistoryDataType)[1][NR_STABS][NR_TAPS - 1][NR_CHANNELS][NR_POLARIZATIONS * COMPLEX];
#else
typedef SampleType (*HistoryDataType)[1][NR_STABS][NR_TAPS - 1][NR_CHANNELS][NR_POLARIZATIONS * COMPLEX];
#endif
typedef float (*FilteredDataType)[NR_STABS][NR_POLARIZATIONS][NR_SAMPLES_PER_CHANNEL][NR_CHANNELS][COMPLEX];
typedef const float (*WeightsType)[NR_CHANNELS][NR_TAPS];

#ifdef DOPPLER_CORRECTION
// Check if CLOCK_MHZ is also defined
#ifndef CLOCK_MHZ
#error DOPPLER_CORRECTION=1 but CLOCK_MHZ not defined
#endif

// this is faster than doing "pol ? sin(phi) : cos(phi)"
// because that statement forces CUDA to still compute both
// as GPUs always compute both branches.
inline __device__ float2 sincos_d2f_select(double phi, int ri)
{
  double r[2];

  sincos(phi, &r[1], &r[0]);
  float c=__double2float_rz(r[0]);
  float s=__double2float_rz(r[1]);
  // return: ri==0 (cos,-sin) ri==1 (sin,cos)
  return make_float2((ri?s:c),(ri?c:-s));
}

typedef  const double(*DelaysType)[NR_STABS][NR_POLARIZATIONS]; // 2 Polarizations; in seconds
typedef  const double(*Phase0sType)[NR_STABS][NR_POLARIZATIONS]; // 2 Polarizations; in radians
#endif /* DOPPLER_CORRECTION */

/*!
 * Applies the Finite Input Response filter defined by the weightsPtr array
 * to the sampledDataPtr array. Output is written into the filteredDataPtr
 * array. The filter works on complex numbers. The weights are real values only.
 *
 * Input values are first converted to (complex) float.
 * The kernel also reorders the polarization dimension and expects the weights
 * per channel in reverse order. If an FFT is applied afterwards, the weights
 * of the odd channels are often supplied negated to get the resulting channels
 * in increasing order of frequency.
 *
 * \param[out] filteredDataPtr         4D output array of floats
 * \param[in]  sampledDataPtr          4D input array of signed chars or shorts
 * \param[in]  weightsPtr              2D per-channel FIR filter coefficient array of floats (considering float16 as a dim)
 * \param[in]  historyDataPtr          5D input array of history input samples needed to initialize the FIR filter
 * \param[in]  subbandIdx              index of the subband to process
 *
 * Pre-processor input symbols (some are tied to the execution configuration)
 * Symbol                  | Valid Values                | Description
 * ----------------------- | --------------------------- | -----------
 * NR_STABS                | >= 1                        | number of antenna fields (correlator), or number of tight array beams (tabs) (beamformer)
 * NR_TAPS                 | 16                          | number of FIR filtering coefficients
 * NR_SAMPLES_PER_CHANNEL  | multiple of NR_TAPS and > 0 | number of input samples per channel
 * NR_BITS_PER_SAMPLE      | 4, 8 or 16                  | number of bits of signed integral value type of sampledDataPtr
 * NR_CHANNELS             | multiple of 16 and > 0      | number of frequency channels per subband
 * NR_POLARIZATIONS        | 2                           | number of polarizations
 * COMPLEX                 | 2                           | size of complex in number of floats/doubles
 * INPUT_IS_STATIONDATA    | defined or not              | if true, input is intX[stabs][samples][pol]
 *                         |                             | if false, input is float[stabs][pol][samples]
 * DOPPLER_CORRECTION      | defined or not              | If true, apply subband-based delay compensation
 * CLOCK_MHZ               | >0 clock Freq in MHz        | Must be defined with DOPPLER_CORRECTION
 * REVERSE_TAPS            | defined or not              | if defined, reverse the order of taps
 *
 * Execution configuration: (TODO: enforce using __attribute__ reqd_work_group_size)
 * - Work dim == 2  (can be 1 iff NR_STABS == 1)
 *     + Inner dim: the channel, pol, real/imag the thread processes
 *     + Outer dim: the station the thread processes
 * - Work group size: must divide global size, no other kernel restrictions
 * - Global size: (NR_CHANNELS * NR_POLARIZATIONS * 2, NR_STABS)
 *
 * TODO: convert complex dim to fcomplex (=float2 in math.cl) in device code and to complex<float> in host code.
 */
extern "C" {
__global__ void FIR_filter( void *filteredDataPtr,
                            const void *sampledDataPtr,
                            const void *weightsPtr,
                            void *historyDataPtr,
                            const double * delaysAtBeginPtr,
                            const double * delaysAfterEndPtr,
                            unsigned subbandIdx,
                            double subbandFrequency)
{
  SampledDataType sampledData = (SampledDataType) sampledDataPtr;
  FilteredDataType filteredData = (FilteredDataType) filteredDataPtr;
  WeightsType weightsData = (WeightsType) weightsPtr;
  HistoryDataType historyData = (HistoryDataType) historyDataPtr;

  unsigned cpr = blockIdx.x*blockDim.x+threadIdx.x;
#if 0
  //# Straight index calc for NR_CHANNELS == 1
  uint pol_ri = cpr & 3;
  uint channel = cpr >> 2;
  uint ri = cpr & 1;
  uint pol = pol_ri >> 1;
#else
  unsigned ri = cpr & 1;                        // index (real/imag) in output data
  unsigned channel = (cpr >> 1) % NR_CHANNELS;  // index in input & output data
  unsigned pol = (cpr >> 1) / NR_CHANNELS;      // index (polarization) in output data
  unsigned pol_ri = (pol << 1) | ri;            // index (polarization & real/imag) in input data
#endif
  unsigned station = blockIdx.y;

#ifdef DOPPLER_CORRECTION
  DelaysType delaysAtBegin = (DelaysType)delaysAtBeginPtr;
  DelaysType delaysAfterEnd = (DelaysType)delaysAfterEndPtr;
  const double delayAtBegin = (*delaysAtBegin)[station][pol];
  const double delayAfterEnd = (*delaysAfterEnd)[station][pol];

  // Calculate the angles to rotate for for the first and (beyond the) last sample.
  //
  // We need to undo the delay, so we rotate BACK, resulting in a negative constant factor.
  // Note we use the sample frequency=CLOCK_MHZ/1024 (MHz) to normalize the frequency
  // Let tau_0: delay at begin, tau_1: delay after end 
  // t : time within a block, T: total time (duration) of block,
  // So, t in [0,T] and  t/T=blockoffset in [0,1]
  // then delay at t = tau_1 (t/T) + tau_0 (1 - t/T)
  // exponent at frqeuency f = -j 2 pi (f tau) 
  // simplifying, exponent = -j 2 pi f tau_0 - j 2 pi f (tau_1 -tau_0) (t/T)
  // We only need the term that changes with t, so discard the rest
  // and only keep : 2 pi f (tau_1 - tau_0) (t/T) for the rest of calculations
  // also replace f with f/f_s where f_s is sample frequency = clock/1024
  // phi = phiGradient x blockOffset
  // Offset of this sample between begin and end. = t/T fraction, within one FFT block of NR_CHANNELS
  const double phi = 2.0 * M_PI * (subbandFrequency / (CLOCK_MHZ*1e6/1024.0) )*( delayAfterEnd - delayAtBegin )/NR_SAMPLES_PER_CHANNEL * double(channel)/NR_CHANNELS;

  //
  // Use double precision here, when phi~=0, error in cos() is minimum
  // but error in sin() is highest, and will affect most baselines (whose Doppler effect ~=0)
  // Note: both X and Y polarizations will have same correction
  // so real=cos(phi), imag=sin(phi) correction factor
  // phi=phiGradient * blockOffset
  const float2 FACTOR=sincos_d2f_select(phi, ri);
#endif

  float weights[16];
  for (unsigned i = 0; i < 16; i++)
  {
    #ifndef REVERSE_TAPS
    weights[i] = (*weightsData)[channel][i];
    #else
    weights[i] = (*weightsData)[channel][15 - i];
    #endif
  }

  float delayLines[16];
  for (unsigned i = 0; i < 15; i++)
  {
#if defined DOPPLER_CORRECTION
    delayLines[i] = ((*historyData)[subbandIdx][station][i][channel][pol_ri]);
#else
    delayLines[i] = convertIntToFloat((*historyData)[subbandIdx][station][i][channel][pol_ri]);
#endif
  }

  float sums[16];

  for (unsigned time = 0; time < NR_SAMPLES_PER_CHANNEL; time += NR_TAPS)
  {
    int timeIdx = time;

#if defined DOPPLER_CORRECTION
    //(X,Y): sample real,imag parts, output a X + b Y
    // FACTOR (cos,sin) = (c,s)
    // ri=0 : a=cos, b=-sin ; ri=1: a=sin, b=cos
    delayLines[15] = REAL(timeIdx)*FACTOR.x+IMAG(timeIdx)*FACTOR.y;
#else
    delayLines[15] = convertIntToFloat(SAMPLE(timeIdx));
#endif

    #pragma unroll
    for (int delayIdx = 0; delayIdx < 16; delayIdx++)
    {
      sums[delayIdx] = weights[15] * delayLines[delayIdx];
      timeIdx = time + delayIdx + 1;
      if (delayIdx < 15) {
#if defined DOPPLER_CORRECTION
        delayLines[delayIdx] = REAL(timeIdx)*FACTOR.x+IMAG(timeIdx)*FACTOR.y;
#else
        delayLines[delayIdx] = convertIntToFloat(SAMPLE(timeIdx));
#endif
      }

      #pragma unroll
      for (int i = 0; i < 15; i++)
      {
        mac(sums[delayIdx], weights[14 - i], delayLines[(delayIdx + i + 1) % 16]);
      }
      (*filteredData)[station][pol][time + delayIdx][channel][ri] = sums[delayIdx];
    }
  } // end for time

  for (unsigned tap = 0; tap < NR_TAPS - 1; tap++)
  {
#if defined DOPPLER_CORRECTION
    const unsigned time = NR_SAMPLES_PER_CHANNEL - (NR_TAPS - 1) + tap;
    (*historyData)[subbandIdx][station][tap][channel][pol_ri] = // subbandIdx=1 assumed here
      REAL(time)*FACTOR.x+IMAG(time)*FACTOR.y;
#else
    (*historyData)[subbandIdx][station][tap][channel][pol_ri] = // subbandIdx=1 assumed here
      SAMPLE(NR_SAMPLES_PER_CHANNEL - (NR_TAPS - 1) + tap);
#endif
  } // end for tap
}
}
