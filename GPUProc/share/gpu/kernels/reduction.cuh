#include <cooperative_groups.h>

namespace cg = cooperative_groups;

__device__ inline unsigned int round_down_to_power_of_2(unsigned int value) {
  return value <= 0 ? 0 : 1 << (unsigned int)(log2f(value));
}

/*!
 * Compute and return the sum of data_length elements of data. This is
 * implemented in the form of an parallel reduction using nr_threads threeads.
 * The first nr_threads elements of the input are overwritten. The result (the
 * sum) is written to data[0] and returned.
 */
template <typename T>
__device__ T reduce_sum(cg::thread_group group, T *data, unsigned data_length,
                        unsigned nr_threads) {
  const unsigned lane = group.thread_rank();
  assert(nr_threads <= group.size());

  // Each thread computes a partial sum by iterating over the data with a stride
  // of nr_threads elements. At the end, the results are stored in data,
  // overwriting data[0:min(data_length, nr_threads)] with the partial sums.
  T sum;
  if (lane < data_length) {
    sum = data[lane];
    for (unsigned i = lane + nr_threads; i < data_length; i += nr_threads) {
      sum += data[i];
    }
    data[lane] = sum;
  }

  // Iteratively accumulate the partial sums untill the total sum is present in
  // data[0]. Each iteration halves the number of active threads.
  for (unsigned i = nr_threads / 2; i > 0; i /= 2) {
    group.sync();
    if (lane < i && (lane + i) < data_length) {
      data[lane] += data[lane + i];
    }
  }

  group.sync();

  return data[0];
}

/*!
 * Compute and return the sum of n elements of data.
 * This is implemented in the form of a parallel reduction that processes
 * the input in batches of length temp_size using the temp memory as
 * intermediate storage.
 * The result (the sum) is written to temp[0] and returned.
 */
template <typename T>
__device__ T reduce_sum(cg::thread_group group, T *data, size_t data_length,
                        T *temp, size_t temp_size) {
  const unsigned lane = group.thread_rank();
  const unsigned nr_threads = group.size();
  const unsigned nr_threads_reduction = round_down_to_power_of_2(nr_threads);

  T sum;

  for (unsigned i = 0; i < data_length; i += temp_size) {
    group.sync();
    // Load at most temp_size elements from data into temp.
    for (unsigned j = lane; j < temp_size; j += nr_threads_reduction) {
      if (lane < nr_threads_reduction && (i + j) < data_length) {
        temp[j] = data[i + j];
      }
    }
    group.sync();
    // Perform a sum of the data in temp and add the result to sum.
    const unsigned n = min(temp_size, data_length - i);
    const T result = reduce_sum(group, temp, n, nr_threads_reduction);
    if (i == 0) {
      sum = result;
    } else {
      sum += result;
    }
  }

  return sum;
}