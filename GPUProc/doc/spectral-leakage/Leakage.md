# Spectral leakage in beamformer
This is a brief description of the spectral leakage in the beamformer pipeline, between the first FFT and IFFT operations. 

<img src="leakage-scheme.png" width="900"/>

Ideally, we should use a polyphase analysis filter (instead of the FFT) and a polyphase synthesis filter (instead of the IFFT) to minimize the leakage.

We consider two solutions: 

1. Analysis PPF and Synthesis IFFT 
2. Analysis PPF and Synthesis PPF

In other words, the FFT/IFFT shift is replaced by FIR filters in both solutions. In (1), only the input side has an FIR, and in (2), both input and output have FIR filters.


The figure below shows a subset of the beamformed data (coherent Stokes I), comparing the original pipeline and the options 1 and 2 above. 
<img src="leakage-stokes-I.png" width="900"/>


For a more detailed comparison, the figure below shows the power spectra of the data.
<img src="leakage-spectra.png" width="900"/>

Use parset key *Cobalt.BeamFormer.inputPPF=true* to enable option (2) above.

