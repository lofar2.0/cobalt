# Goal
We run a set of representative benchmarks to test performance regression of the kernels for the different pipelines. As we only aim to test a representative parameter space for the kernels it seems best to run a set of pipeline tests with representative use cases for LOFAR and LOFAR Mega Mode. Tests are specified through the LOFAR parsets and are passed to the `gpu_load` test that sets up a relative simple pipeline with performance benchmarking enabled.

# The `tSubbandProcPerformance` test
Start the performance test using the `ctest` framework:
```
ctest -V -R tSubbandProcPerformance
```
This will run all the `tSubbandProcPerformance_*.parset` in `test/SubbandProc`, and compare the results with the reference output in `tSubbandProcPerformance_reference`. Note that these parsets do not yet contain the `Cobalt.Benchmark` keys, these are filled in during the test.

The format of the reference output is `<OBSID>_<GPUNAME>.csv`. The GPU name
should match the name reported by `nvidia-smi`, with spaces replaced with dashes. E.g. "Tesla V100-PCIE-16GB" becomes "Tesla-V100-PCIE-16GB". In case no reference output is found (for instance, when running on a system with different GPUs), the test is skipped.

One can add new or update existing reference files as follows:
1. Get a parset and make sure that the `OBSID` is either unique (for new tests), or matches the `OBSID` of an existing `tSubbandProcPerformance_*.parset` (when updating an existing reference output).
2. Add the `Cobalt.Benchmark` keys to enable dumping of the performance counter data to a CSV file.
3. Run `gpu_load` with the new parset.
4. Rename the resulting CSV file as described above and copy it to the `tSubbandProcPerformance_reference` directory.

The `tSubbandProcPerformance_compare.py` script compares two CSV files and has a configurable tolerance. E.g. if the tolerance is 10%, a measurement is considered as PASS as it is no more than 10% slower than the reference. The benchmark considers measurements running faster than the reference as PASS as well. This can for instance happen when a kernel is optimized.

It is adviced to add new reference files seperately from any code-changes (that may have caused performance differences). This way, one can not (accidentally) introduce a performance regression.

This test has two caveats:
1. Some timings are unreliable, as the kernels run to short to provide a meaningfull measurement. Therefore, any timing of less than 5% of the total runtime is ignored. This is currently a hard-coded value in the comparison script.
2. When using an LMM parset, performance counters with the same names are likely to co-exist. This case is not yet taken into account in the benchmark.