This document describes the usage of the *gpu_load* test for performance measurements. 

The *gpu_load* test source is in: `lofar/RTCP/Cobalt/GPUProc/src`

After build and installation the *gpu_load* binary is in: `<install directory>/lofar/bin`

Usage: call *gpu_load* with a parset as parameter, e.g. `~/opt/lofar/bin/gpu_load ~/parsets/rtcp-698421-COBALT2-ppf-stokes-coh-incoh.parset`

Optional: add an [int] nr of iterations for the tests, the default nr of iterations is set to 10. e.g. `~/opt/lofar/bin/gpu_load ~/parsets/rtcp-698421-COBALT2-ppf-stokes-coh-incoh.parset 1000`

The *gpu_load* test is intended for performance measurements of a GPU kernels. The test sets up a processing pipeline on a single node. It takes the configuration as specified in the parset. It does not use actual data and does not produce functional output.

By default the *gpu_load* test reports kernel and pipeline performance numbers to stdout. In order to store the performance numbers to file the parset should contain the following two keys:
* `Cobalt.Benchmark.enabled=true`
* `Cobalt.Benchmark.file=<benchmark file path>/<file name>.csv`

If logging to file is not required then these keys can be left out of the parset. By default `Cobalt.Benchmark.enabled` is set to *false*. If `Cobalt.Benchmark.enabled` is set to *true* then individual kernel statistics will be logged to file upon completion of the kernel (handled in the destructor).
If a path is specified and it is not accessible (directory does not exist or rights issue) then `Cobalt.Benchmark.enabled` is set to *false* again and no performance numbers will be stored. 
If the specified file does not exist yet it will be created, if it already exists then it will be overwritten.

The test reports extrapolated performance numbers on stdout. In order to calculate these numbers the following parset keys will need to be specified:
* `Observation.Beam[0].subbandList` How many subbands are in this SAP. Work load scales with total number of subbands across all SAPs. Typical: 400, 480, 488. e.g. *[0..487]*
* `Cobalt.Nodes` Determines how many GPUs will be available for load distribution. Typical: 22*localhost.

Performance numbers are controlled by `LOFAR::Cobalt::PerformanceCounter` (part of *GPUProc*) and collected and printed by `LOFAR::Cobalt::RunningStatistics` (part of *CoInterface*) which are both used through `LOFAR::Cobalt::Kernel`.

Alternativley the *gpu_load* test can be used with NVVP to visualize and inspect the processing pipline and individual kernel performance.


Example output to file:

```
Tue Jun  2 08:36:21 2020
info; Test gpu_load: /home/.../opt/lofar/bin/gpu_load with parset: /home/.../parsets/rtcp-698421-COBALT2-ppf-stokes-coh-incoh.parset
format; kernelName; count; mean; stDev; min; max; unit
PerformanceCounter; output (incoherent); 10; 0.28483; 0.01499; 0.24310; 0.29318; ms
PerformanceCounter; incoherentStokes; 10; 1.05907; 0.10314; 0.85702; 1.20778; ms
PerformanceCounter; FFT (incoherent, final); 10; 0.40620; 0.00172; 0.40406; 0.40963; ms
PerformanceCounter; FIR (incoherent, final); 10; 1.65774; 0.02571; 1.59002; 1.67645; ms
PerformanceCounter; FFT-shift (incoherent, inverse); 10; 0.36252; 0.00027; 0.36224; 0.36307; ms
PerformanceCounter; FFT (incoherent, inverse); 10; 0.40509; 0.00212; 0.40211; 0.40813; ms
PerformanceCounter; incoherentStokesTranspose; 10; 0.42096; 0.00722; 0.41098; 0.43024; ms
PerformanceCounter; quantizeOutput; 10; 0.04149; 0.00192; 0.04010; 0.04682; ms
PerformanceCounter; output (coherent); 10; 0.00794; 0.00076; 0.00698; 0.00954; ms
PerformanceCounter; coherentStokes; 10; 0.03905; 0.00132; 0.03814; 0.04266; ms
PerformanceCounter; FFT (coherent, final); 10; 0.01025; 0.00239; 0.00941; 0.01706; ms
PerformanceCounter; FIR (coherent, final); 10; 0.77494; 0.01318; 0.74938; 0.78496; ms
PerformanceCounter; FFT-shift (coherent, inverse); 10; 0.00765; 0.00114; 0.00723; 0.01088; ms
PerformanceCounter; FFT (coherent, inverse); 10; 0.00963; 0.00090; 0.00915; 0.01219; ms
PerformanceCounter; coherentStokesTranspose; 10; 0.02697; 0.00137; 0.02643; 0.03085; ms
PerformanceCounter; beamFormer; 10; 1.20297; 0.00429; 1.19654; 1.21264; ms
PerformanceCounter; bandPassCorrection; 10; 0.63590; 0.00828; 0.62384; 0.64678; ms
PerformanceCounter; delayAndBandPass; 10; 0.35521; 0.00149; 0.35408; 0.35910; ms
PerformanceCounter; Zeroing (beamformer); 10; 0.06737; 0.00092; 0.06682; 0.06989; ms
```