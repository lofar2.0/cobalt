# Doppler correction
This is a brief description of how Doppler correction is implemented in the FIR_Filter and subsequent DelayAndBandpass correction.


Let the intrinsic sky signal be *g(t)* with its Fourier transform *G(f)*. Due to the Doppler shift, the measured signal will have a frequency shift *f_0* and in order to get back the orignal signal, we shift the measured signal Fourier transform as *G(f-f_0)*, which is done in continuous time by multiplication of *g(t)* with *exp(-j 2 pi f_0 t)*.

<img src="phaseramp.png" width="900"/>

The signal streaming to FIR_Filter is a discrete time signal, about 190k samples per one block of about 1 second duration (see the figure). The sampling frequency is *f_s* = clock frequency/1024.
The delays at the start of the block of data and after the end of the block are *tau0* and *tau1*, respectively.

Let *t* denote time within the block and *T* be the duration of the block, so *t* in *[0,T]*.
The linear delay for the sample at time *t* is *tau=tau1 (t/T) + tau0(1-t/T)*. The corresponding discrete time exponential is *exp(-j 2 pi f tau)*. Discarding the constant terms (that do not vary with time), the exponential becomes  *exp(-j 2 pi f (tau1-tau0)/T t)*. The subband frequency is *f*.

As seen in the figure, each block is divided to many FFT blocks, so within each block, we use the same delay gradient *(tau0-tau0)/T* to calculate the phase ramp. In other words, the constant part of the delay ramp is not used as it will only increase decorelation, not affecting the needed shift in frequency.

To summarize, the exact term used at channel *chan* is *j 2 pi (f/f_s) (tau1-tau0)/NR_FFT_BLOCKS chan/NR_CHANNELS*. 

Due to this correction, the shift in channels is *(tau1-tau0)/NR_FFT_BLOCKS (f/f_s)*. The corresponding shift in frequency is *(tau1-tau0)/NR_FFT_BLOCKS (f/NR_CHANNELS)*.

The Doppler correction is not affected by the rotation of the dipoles, so both polarizations get the same correction (unless the delays for each polarization is different).
The bandpass correction is modified by linear interpolation of the bandpass correction weights with the appropriate channel shift.