------------------------
Beamformer Pipeline
------------------------

Design decisions:
-------------------

* The PPF is at the end of the pipeline, because CS and IS can request a different number of channels.
* The Delay Compensation has to run on a specific time/frequency resolution depending on the baseline and
  declination of the source.
* Coherent Dedispersion is under construction.

To investigate:

* Coherent Dedispersion
* FFT scaling (FFT + FFT-1 means scaling the amplitude), does it require compensation?
  If so, we can do so in the Delay Compensation kernel.

Pipeline
-------------------

For max size, we assume:
 - 48 antenna fields (beam former) or 80 antenna fields (correlator) 
 - 1 subband
 - 1 second blocks (195312.5 samples) rounded to next multiple of 4096 (= 196608 samples).
 - 10 integration periods/block (~0.1s integration time)

Note:
  MiB = 2^20 bytes (= 1048576 bytes).

Flow:           Data dimensions:                        Max size (fcomplex):                        Buffer:
===================================================================================================================
(input)         [station][sample][pol]                  [48][196608][2]     =  72 MiB               A
                                                        (i16complex)

Correlator [A -> E, trashes B]
-----------------------------------
NB: Numbers are for 80 antenna fields.

(input)         [station][sample][pol]                  [80][196608][2]     = 120 MiB               A
   |
   V
  FIR (if >1ch) {IntToFloat} {I/O: history samples}
   |            [station][pol][sample]                  [80][2][196608]     = 240 MiB               Nch: B
   V
  FFT (if >1ch) {out-of-place}
   |            [station][pol][sample][channel]         [80][2][3072][64]   = 240 MiB               Nch: E
   V
Zeroing + Delay compensation (channel) + Band pass + Transpose {I/O: delays}
   |            [station][channel][sample][pol]         [80][64][3072][2]   = 240 MiB               B
   V
Correlator
   |            [subblks][baseline][channel][pol][pol]  [10][3240][64][2][2]=  60 MiB               E
   V
(output)

If 1ch, IntToFloat (A->E) is used instead of FIR+FFT.

(BF) Preprocessing [A -> B, trashes A]
-----------------------------------
NB: The key Cobalt.BeamFormer.stationList can be used to select a subset of antenna fields in the observation

(input)         [station][sample][pol]                  [48][196608][2]     =  72 MiB               A
   |                                                    (i16complex)
   V
IntToFloat + Transpose + FFT-shift
{or FIR if inputPPF=true}
   |            [station][pol][sample]                  [48][2][196608]     = 144 MiB               B
   |
   V
FFT-64 {inplace}
   |            [station][pol][sample][channel]         [48][2][3072][64]   = 144 MiB               B
   V
Zeroing + Delay compensation + Transpose (implicit, DO_TRANSPOSE not defined) {I/O: delays}
   |            [station][pol][channel][sample]         [48][2][64][3072]   = 144 MiB               A
   V
BandPass + Transpose {I/O: weights}
   |            [station][chan1][chan2][sample][pol]    [48][64][64][48][2] = 144 MiB               B
   V          = [station][channel][sample][pol]
   X

Complex Voltages/Coherent Stokes: [B -> O, trashes D, quantization: trashes E]
-----------------------------------
   X            [station][channel][sample][pol]         [48][4096][48][2]   = 144 MiB               B
   |
   V
BeamFormer {I/O: weights}
   |            [channel][sample][tab][pol]             [4096][48][tab][2]  = 3 MiB/TAB             D
   V
Transpose  
   |            [tab][pol][sample][channel]             [tab][2][48][4096]  = 3 MiB/TAB             O
   |
   V
iFFT-64 {inplace if inputPPF=true O->O}
   |            [tab][pol][sample]                      [tab][2][196608]    = 3 MiB/TAB             D
   |
   V
FFT-shift {inplace}
{ or FIR if inputPPF=true O->D}
   |            [tab][pol][sample]                      [tab][2][196608]    = 3 MiB/TAB             D
   |
   V
FIR-16 (if >1ch) {I/O: history samples}
   |            [tab][pol][sample]                      [tab][2][196608]    = 3 MiB/TAB             Nch: O
   |
   V
FFT-16 {inplace} (if >1ch)
   |            [tab][pol][sample][channel]             [tab][2][12288][16] = 3 MiB/TAB             Nch: D
   |
   V
Coherent Stokes 
{if inputPPF=true I/O: bandpass weights, size: channels/delay_compensation_channels}
   |            [tab][stokes][sample][channel]          [tab][4][12288][16] = 0.75 MiB/TAB/Stokes   O
   |                                                    (float)                                     Quantization: E
   V
Rebitting (quantization)
   |            [tab][stokes][sample][channel]          [tab][4][12288][16] = 3/16 MiB/TAB/Stokes   O
   |                                                    (short)
   |            [tab][stokes][channel]                  [tab][4][16]        = 1/16 KiB/TAB/Stokes   O
   |                                                    (float)
   |            [tab][stokes][channel]                  [tab][4][16]        = 1/16 KiB/TAB/Stokes   O
   |                                                    (float)
   V
(output)

Incoherent Stokes: [B -> O, trashes D, with quantization: trashes E (also trashes B if inputPPF=True)]
-----------------------------------
   X            [station][channel][sample][pol]         [48][4096][48][2]   = 144 MiB               B
   |
   V
Transpose + Copy
   |            [station][pol][sample][channel]         [48][2][48][4096]   = 144 MiB               A
   V
iFFT-64 {inplace, or A->B if inputPPF=true}
   |            [station][pol][sample]                  [48][2][196608]     = 144 MiB               A
   V
FFT-shift {inplace}
{ or FIR if inputPPF=true, B->A}
   |            [station][pol][sample]                  [48][2][196608]     = 144 MiB               A
   V
FIR-16 (if >1ch) {I/O: history samples}
   |            [station][pol][sample]                  [48][2][196608]     = 144 MiB               Nch: O
   |
   V
FFT-16 {inplace} (if >1ch)
   |            [station][pol][sample][channel]         [48][2][12288][16]  = 144 MiB               Nch: A
   V
Incoherent Stokes
{if inputPPF=true I/O: bandpass weights, size: channels/delay_compensation_channels}
   |            [stokes][sample][channel]               [4][12288][16]      = 3 MiB                 O
   |                                                    (float)                                     Quantization: E
   V
Rebitting (quantization)
   |            [stokes][sample][channel]               [4][12288][16]      = 3/16 MiB/Stokes       O
   |                                                    (short)
   |            [stokes][channel]                       [4][16]             = 1/16 KiB/Stokes       O
   |                                                    (float)
   |            [stokes][channel]                       [4][16]             = 1/16 KiB/Stokes       O
   |                                                    (float)
   V
(output)

The buffers thus have the following sizes (in MiB) in the various observational modes:

buffer |             bf | corr |      commensal
-------+----------------+------+----------------
A      |      144       | 120  |            144
B      |      144       | 240  |            240
D      | 3 * #TAB       |   -  |       3 * #TAB
E      |        -       | 240  |            240
O      | 3 * #TAB * #BF |   -  | 3 * #TAB * #BF

#TAB = number of coherent TABs (for one subband)
#BF  = number of beam former pipelines, each pipeline has its own O buffer

The WQ has 1791 MiB GPU memory available, because we have 2 WQ's per GPU core,
and (according to nvidia-smi) we have 3583 MiB GPU memory per core.

BF only:
---------
Since we can form more than 48 TABs, the size of buffers D and O are determined by the number of TABs.
So, the maximum number of TABs that we can form is: (1791 - 288) MiB / (2 * 3 MiB/TAB) >= 250 TABs.

Corr+BF:
---------
Since we can form more than 48 TABs, the size of buffers D and O are determined by the number of TABs.
So, the maximum number of TABs that we can form is: (1791 - 624) MiB / (2 * 3 MiB/TAB) >= 194 TABs.

