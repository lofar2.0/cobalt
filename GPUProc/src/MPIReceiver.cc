
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#include <lofar_config.h>

#include "MPIReceiver.h"
#include <InputProc/Transpose/MPIProtocol.h>

namespace LOFAR {
    namespace Cobalt {

        template<typename SampleT>
        void MPIRecvData::allocate(
                size_t nrStations, size_t nrBeamlets, size_t nrSamples) {
            data.reset(static_cast<char *>(mpiAllocator.allocate(
                    nrStations * nrBeamlets * nrSamples * sizeof(SampleT))));
            metaData.reset(static_cast<char *>(mpiAllocator.allocate(
                    nrStations * nrBeamlets * sizeof(struct MPIProtocol::MetaData))));
        }

        template void MPIRecvData::allocate<SampleType<i16complex> >(
                size_t nrStations, size_t nrBeamlets, size_t nrSamples);

        template void MPIRecvData::allocate<SampleType<i8complex> >(
                size_t nrStations, size_t nrBeamlets, size_t nrSamples);

        template void MPIRecvData::allocate<SampleType<i4complex> >(
                size_t nrStations, size_t nrBeamlets, size_t nrSamples);

        MPIReceiver::MPIReceiver(Pool<struct MPIRecvData> &pool,
                                 const std::vector<size_t> &subbandIndices,
                                 const bool processingSubband0,
                                 size_t iNrSamplesPerSubband,
                                 size_t iNrStations,
                                 size_t iNrBitsPerSample)
                :
                mpiPool(pool),
                subbandIndices(subbandIndices),
                processingSubband0(processingSubband0),
                nrSamplesPerSubband(iNrSamplesPerSubband),
                nrStations(iNrStations),
                nrBitsPerSample(iNrBitsPerSample) {}

        template<typename SampleT>
        size_t MPIReceiver::receiveInput() {
            NSTimer receiveTimer("MPI: Receive station data", true, true);

            // RECEIVE: Set up to receive our subbands as indicated by subbandIndices
            MPIReceiveStations receiver(nrStations, subbandIndices,
                                        nrSamplesPerSubband);

            // Fill the pool with data items
            size_t nPoolItems = 4;
            for (size_t i = 0; i < nPoolItems; i++) {
                // Create a raw + meta datablock
                auto mpiData = std::make_shared<MPIRecvData>();

                // allocate the data (using mpi allocate)
                mpiData->allocate<SampleT>(nrStations, subbandIndices.size(),
                                           nrSamplesPerSubband);

                // add to the free pool
                mpiPool.free.append(mpiData, false);
            }

            bool allDone = false;
            ssize_t block;

            // Receive input from StationInput::sendInputToPipeline.
            //
            // Start processing from block -1, and don't process anything if the
            // observation is empty.
            for (block = -1; !allDone; block++) {
                // Receive the samples from all subbands from the ant fields for this block.
                LOG_INFO_STR("[block " << block << "] Collecting input buffers");

                // Get a free data item
                std::shared_ptr<struct MPIRecvData> mpiData = mpiPool.free.remove();

                mpiData->block = block;

                // Lay a multidim array on the raw data
                MultiDimArray<SampleT, 3> data(
                        boost::extents[nrStations][subbandIndices.size()][
                                nrSamplesPerSubband],
                        (SampleT *) mpiData->data.get(), false);

                // Idem for data: map multidim array
                MultiDimArray<struct MPIProtocol::MetaData, 2> metaData(
                        boost::extents[nrStations][subbandIndices.size()],
                        (struct MPIProtocol::MetaData *) mpiData->metaData.get(), false);

                LOG_INFO_STR("[block " << block << "] Receive input");

                if (block > 2) {// allow warmup before starting the timers
                    receiveTimer.start();
                }
                allDone = receiver.receiveBlock<SampleT>(data, metaData);
                if (block > 2) {
                    receiveTimer.stop();
                }

                if (processingSubband0) {
                    LOG_INFO_STR("[block " << block << "] Input received");
                } else {
                    LOG_INFO_STR("[block " << block << "] Input received");
                }

                if (allDone) {
                    mpiPool.free.append(mpiData);
                } else {
                    mpiPool.filled.append(mpiData);
                }
            }

            // Signal end of input
            mpiPool.filled.append(NULL, false);

            // "block" indicates the block number of the EOS marker
            const ssize_t lastBlock = block - 1;

            // Return the number of blocks we've processed, excluding any negative blocks
            return lastBlock < 0 ? 0 : lastBlock + 1;
        }

        size_t MPIReceiver::receiveInput() {
            switch (nrBitsPerSample) {
                default:
                case 16:
                    return receiveInput<SampleType<i16complex> >();
                case 8:
                    return receiveInput<SampleType<i8complex> >();
                case 4:
                    return receiveInput<SampleType<i4complex> >();
            }
        }
    } // namespace Cobalt
} // namespace LOFAR
