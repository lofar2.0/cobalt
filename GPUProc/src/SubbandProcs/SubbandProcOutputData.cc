//# SubbandProcOutputData.cc
//# Copyright (C) 2012-2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

#include <lofar_config.h>

#include "SubbandProcOutputData.h"

namespace LOFAR
{
  namespace Cobalt
  {
    SubbandProcOutputData::SubbandProcOutputData(
        const Parset &ps,
        gpu::Context &context) :

      correlatedData(ps.settings.correlator.enabled ? ps.settings.correlator.nrIntegrationsPerBlock    : 0,
                     ps.settings.correlator.enabled ? ps.settings.antennaFields.size()                 : 0,
                     ps.settings.correlator.enabled ? ps.settings.correlator.nrChannels                : 0,
                     ps.settings.correlator.enabled ? ps.settings.correlator.nrSamplesPerIntegration() : 0,
                     context),
      emit_correlatedData(false)
    {
      auto& bfPipelines = ps.settings.beamFormer.pipelines;

      coherentDatas.resize(bfPipelines.size());
      incoherentDatas.resize(bfPipelines.size());

      for (unsigned i = 0; i < bfPipelines.size(); i++)
      {
        auto& bfPipeline = bfPipelines[i];
        coherentDatas[i].reset(
          new TABOutputData(
            bfPipeline.coherentSettings.quantizerSettings.enabled,
            bfPipeline.maxNrCoherentTABsPerSAP(),
            bfPipeline.coherentSettings.nrStokes,
            bfPipeline.coherentSettings.nrSamples,
            bfPipeline.coherentSettings.nrChannels,
            context
        ));

        incoherentDatas[i].reset(
          new TABOutputData(
            bfPipeline.incoherentSettings.quantizerSettings.enabled,
            bfPipeline.maxNrIncoherentTABsPerSAP(),
            bfPipeline.incoherentSettings.nrStokes,
            bfPipeline.incoherentSettings.nrSamples,
            bfPipeline.incoherentSettings.nrChannels,
            context
        ));
      }
    }


    SubbandProcOutputData::CorrelatedData::CorrelatedData(
      unsigned nrIntegrations, 
      unsigned nrStations, unsigned nrChannels,
      unsigned maxNrValidSamples, gpu::Context &context)
      :
      data(
        boost::extents
        [nrIntegrations]
        [nrStations * (nrStations + 1) / 2]
        [nrChannels][NR_POLARIZATIONS]
        [NR_POLARIZATIONS], 
        context, 0),
      subblocks(nrIntegrations)
    {
      for (size_t i = 0; i < nrIntegrations; ++i) {
        const size_t num_elements = data.strides()[0];

        subblocks[i].reset(new LOFAR::Cobalt::CorrelatedData(
                       nrStations, nrChannels, maxNrValidSamples,
                       &data[i][0][0][0][0], num_elements,
                       heapAllocator, 1));
      }
    }



    SubbandProcOutputData::TABOutputData::TABOutputData(
      bool quantized,
      long unsigned nrTABs,
      unsigned nrStokes,
      long unsigned nrSamples,
      unsigned nrChannels,
      gpu::Context &context)
     :
     quantized(quantized),
     nrStokes(nrStokes),
     data((!quantized ? boost::extents[nrTABs*nrStokes][nrSamples][nrChannels]
                      : boost::extents[0][0][0]),
           context, 0),
     qdata((quantized ? boost::extents[nrTABs*nrStokes][nrSamples][nrChannels]
                      : boost::extents[0][0][0]),
           context, 0),
     qoffsets((quantized ? boost::extents[nrTABs*nrStokes][nrChannels]
                         : boost::extents[0][0]),
           context, 0),
     qscales((quantized ? boost::extents[nrTABs*nrStokes][nrChannels]
                        : boost::extents[0][0]),
           context, 0)
    {
    }

    float SubbandProcOutputData::TABOutputData::get_data(
      unsigned tab,
      unsigned stokes,
      unsigned sample,
      unsigned channel) const
    {
      return data[tab * nrStokes + stokes][sample][channel];
    }

    float* SubbandProcOutputData::TABOutputData::get_data_origin(
      unsigned tab,
      unsigned stokes)
    {
      return data[tab * nrStokes + stokes].origin();
    }

    int8_t* SubbandProcOutputData::TABOutputData::get_qdata_origin(
      unsigned tab,
      unsigned stokes)
    {
      return qdata[tab * nrStokes + stokes].origin();
    }

    float* SubbandProcOutputData::TABOutputData::get_qoffsets_origin(
      unsigned tab,
      unsigned stokes)
    {
      return qoffsets[tab * nrStokes + stokes].origin();
    }

    float* SubbandProcOutputData::TABOutputData::get_qscales_origin(
      unsigned tab,
      unsigned stokes)
    {
      return qscales[tab * nrStokes + stokes].origin();
    }

    void SubbandProcOutputData::TABOutputData::resize(
      long unsigned nrTABs,
      long unsigned nrStokes)
    {
      if (!quantized)
      {
        data.resizeOneDimensionInplace(0, nrTABs * nrStokes);
      } else {
        qdata.resizeOneDimensionInplace(0, nrTABs * nrStokes);
        qoffsets.resizeOneDimensionInplace(0, nrTABs * nrStokes);
        qscales.resizeOneDimensionInplace(0, nrTABs * nrStokes);
      }
    }

    size_t SubbandProcOutputData::TABOutputData::data_offset(
      unsigned long tabStokes)
    {
      unsigned nrSamples = data.shape()[1];
      unsigned nrChannels = data.shape()[2];
      return tabStokes * nrSamples * nrChannels * sizeof(float);
    }

    size_t SubbandProcOutputData::TABOutputData::qdata_offset(
      unsigned long tabStokes)
    {
      unsigned nrSamples = qdata.shape()[1];
      unsigned nrChannels = qdata.shape()[2];
      return tabStokes * nrSamples * nrChannels * sizeof(int8_t);
    }

    size_t SubbandProcOutputData::TABOutputData::qoffsets_offset(
      unsigned long tabStokes)
    {
      unsigned nrChannels = qoffsets.shape()[1];
      return tabStokes * nrChannels * sizeof(float);
    }

    size_t SubbandProcOutputData::TABOutputData::qscales_offset(
      unsigned long tabStokes)
    {
      unsigned nrChannels = qscales.shape()[1];
      return tabStokes * nrChannels * sizeof(float);
    }
  }
}

