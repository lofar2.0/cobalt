//# ProcessStep.h
//# Copyright (C) 2012-2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id: ProcessStep.h 29848 2014-07-31 09:32:20Z mol $

#ifndef LOFAR_GPUPROC_CUDA_PROCESS_STEP_H
#define LOFAR_GPUPROC_CUDA_PROCESS_STEP_H

#include <thread>
#include <memory>

#include <GPUProc/gpu_wrapper.h>
#include <GPUProc/KernelParameters.h>

#include "SubbandProcInputData.h"

namespace LOFAR
{
  namespace Cobalt
  {
    class ProcessStep
    {
      friend class SubbandProc;

    public:
      virtual void process(const SubbandProcInputData &input) = 0;

      virtual ~ProcessStep()
      {
        marker_thread_enabled = false;
        marker_mutex.unlock();
        if (marker_thread.joinable())
        {
          marker_thread.join();
        }
      }

    protected:
      ProcessStep(
        std::shared_ptr<gpu::Stream> i_htod_stream,
        std::shared_ptr<gpu::Stream> i_dtoh_stream,
        std::shared_ptr<gpu::Stream> i_execute_stream,
        gpu::Context &context)
        :
        htodStream(i_htod_stream),
        dtohStream(i_dtoh_stream),
        executeStream(i_execute_stream),
        inputFinished(context),
        executeFinished(context),
        outputFinished(context)
        {
          marker_mutex.lock();
          marker_thread = std::thread([&]{
            while (true)
            {
              marker_mutex.lock();
              if (!marker_thread_enabled)
              {
                break;
              }
              marker_gpu->start(inputFinished);
              marker_gpu->end(executeFinished);
            }
          });
        };

      std::shared_ptr<gpu::Stream> htodStream;
      std::shared_ptr<gpu::Stream> dtohStream;
      std::shared_ptr<gpu::Stream> executeStream;

      gpu::Event inputFinished;
      gpu::Event executeFinished;
      gpu::Event outputFinished;

      std::unique_ptr<gpu::Marker> marker_cpu;
      std::unique_ptr<gpu::Marker> marker_gpu;
      std::thread marker_thread;
      std::mutex marker_mutex;
      bool marker_thread_enabled = true;
    };
  }
}

#endif
