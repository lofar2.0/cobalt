//# CorrelatorStep.cc
//# Copyright (C) 2012-2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

#include <lofar_config.h>

#include "CorrelatorStep.h"
#include "SubbandProc.h"

#include <GPUProc/global_defines.h>
#include <GPUProc/gpu_wrapper.h>

#include <CoInterface/Parset.h>
#include <ApplCommon/PosixTime.h>
#include <Common/LofarLogger.h>

#include <iomanip>

namespace LOFAR
{
  namespace Cobalt
  {

    CorrelatorStep::Factories::Factories(
      const KernelParameters::Observation& obsParameters,
      const KernelParameters::Correlator& corParameters,
      const KernelParameters::Preprocessor& preParameters,
      const KernelParameters::Cobalt& cobParameters,
      size_t nrSubbandsPerSubbandProc) :
      fft(corParameters.nrChannels > 1
        ? new KernelFactory<FFT_Kernel>(FFT_Kernel::Parameters(
          corParameters.nrChannels,
          obsParameters.nrStations * NR_POLARIZATIONS * obsParameters.blockSize,
          true,
          "FFT (correlator)"))
        : NULL),
      firFilter(corParameters.nrChannels > 1
          ? new KernelFactory<FIR_FilterKernel>(FIR_FilterKernel::Parameters(
            obsParameters.nrStations,
            obsParameters.nrBitsPerSample,
            true, // inputIsStationData
            corParameters.dopplerCorrection, // DopplerCorrection
            nrSubbandsPerSubbandProc,
            corParameters.nrChannels,
            obsParameters.blockSize / corParameters.nrChannels,
            obsParameters.clockMHz,
            // Scale to always output visibilities or stokes with the same flux scale.
            // With the same bandwidth, twice the (narrower) channels _average_ (not
            // sum) to the same fluxes (and same noise). Twice the channels (twice the
            // total bandwidth) _average_ to the _same_ flux, but noise * 1/sqrt(2).
            // Note: FFTW/CUFFT do not normalize, correlation or stokes calculation
            // effectively squares, integr on fewer channels averages over more values.
            std::sqrt((double)corParameters.nrChannels),
            "FIR (correlator)"))
          : NULL),

      intToFloat(corParameters.nrChannels == 1
          ? new KernelFactory<IntToFloatKernel>(IntToFloatKernel::Parameters(
            obsParameters.nrStations,
            {}, // stationIndices
            obsParameters.nrBitsPerSample,
            obsParameters.blockSize,
            false, // fftShift
            false, // beamFormerStationSubset
            cobParameters.kernel.dumpIntToFloatKernel,
            str(boost::format("L%d_SB%%03d_BL%%03d_Cor_IntToFloatKernel.dat") % obsParameters.observationID)))
          : NULL),

      delayAndBandPass(DelayAndBandPassKernel::Parameters(
        obsParameters.nrStations, // nrStations
        {}, // delayIndices are set in the constructor
        obsParameters.nrStations, // nrDelays
        obsParameters.nrBitsPerSample,
        corParameters.nrChannels,
        obsParameters.blockSize / corParameters.nrChannels,
        obsParameters.clockMHz,
        obsParameters.subbandWidth,
        true, // correlator
        true, // zeroing
        preParameters.delayCompensationEnabled,
        preParameters.bandPassCorrectionEnabled,
        1.0, // bandPassScale
        true, // transpose
        corParameters.dopplerCorrection, // DopplerCorrection
        cobParameters.kernel.dumpDelayAndBandPassKernel,
        str(boost::format("L%d_SB%%03d_BL%%03d_Cor_DelayAndBandPassKernel_%c%c%c.dat") %
          obsParameters.observationID %
          (preParameters.bandPassCorrectionEnabled ? "B" : "b") %
          (preParameters.delayCompensationEnabled ? "D" : "d") %
          (true ? "T" : "t"))
      )),

      correlator(CorrelatorKernel::Parameters(
        obsParameters.nrStations,
        corParameters.nrChannels,
        corParameters.nrSamplesPerBlock / corParameters.nrIntegrationsPerBlock,
        corParameters.nrIntegrationsPerBlock,
        cobParameters.kernel.dumpCorrelatorKernel,
        str(boost::format("L%d_SB%%03d_BL%%03d_Cor_CorrelatorKernel.dat") % obsParameters.observationID)
      ))
    {
    }

    namespace {
      // Return the baseline number for a pair of stations
      unsigned baseline(unsigned major, unsigned minor)
      {
        ASSERT(major >= minor);

        return major * (major + 1) / 2 + minor;
      }
    }

    template<typename T> void CorrelatorStep::Flagger::calcNrValidSamples(
      unsigned nrStations,
      unsigned nrSamplesPerBlock,
      unsigned nrIntegrationsPerBlock,
      unsigned nrChannels,
      MultiDimArray<SparseSet<unsigned>, 1>const & flagsPerChannel,
      SubbandProcOutputData::CorrelatedData &output)
    {
      /*
       * NOTE: This routine is performance critical. This is called as part
       *       of processCPU(). The tCorrelatorStep test validates its performance.
       */

      // The number of samples per integration within this block.
      const unsigned nrSamples = nrSamplesPerBlock / nrIntegrationsPerBlock;

      // loop the stations
      for (unsigned stat1 = 0; stat1 < nrStations; stat1 ++) {
        for (unsigned stat2 = 0; stat2 <= stat1; stat2 ++) {
          const unsigned bl = baseline(stat1, stat2);

          // The number of invalid (flagged) samples is the union of the
          // flagged samples in the two stations
          const SparseSet<unsigned> flags =
            flagsPerChannel[stat1] | flagsPerChannel[stat2];

          for (size_t i = 0; i < nrIntegrationsPerBlock; ++i) {
            LOFAR::Cobalt::CorrelatedData &correlatedData = *output.subblocks[i];

            // Count the flags for this subblock
            const T nrValidSamples =
              nrSamples - flags.count(i * nrSamples, (i+1) * nrSamples);

            // Channel zero is invalid, unless we have only one channel
            if (nrChannels > 1) {
              correlatedData.nrValidSamples<T>(bl, 0) = 0;
            } else {
              correlatedData.nrValidSamples<T>(bl, 0) = nrValidSamples;
            }

            // Set the channels from 1 onward
            for(unsigned ch = 1; ch < nrChannels; ch ++) {
              correlatedData.nrValidSamples<T>(bl, ch) = nrValidSamples;
            }
          }
        }
      }
    }


    void CorrelatorStep::Flagger::calcNrValidSamples(
      unsigned nrStations,
      unsigned nrSamplesPerBlock,
      unsigned nrIntegrationsPerBlock,
      unsigned nrChannels,
      MultiDimArray<SparseSet<unsigned>, 1>const & flagsPerChannel,
      SubbandProcOutputData::CorrelatedData &output)
    {
      switch (output.subblocks[0]->itsNrBytesPerNrValidSamples) {
        case 4:
          calcNrValidSamples<uint32_t>(
            nrStations, nrSamplesPerBlock, nrIntegrationsPerBlock,
            nrChannels, flagsPerChannel, output);
          break;

        case 2:
          calcNrValidSamples<uint16_t>(
            nrStations, nrSamplesPerBlock, nrIntegrationsPerBlock,
            nrChannels, flagsPerChannel, output);
          break;

        case 1:
          calcNrValidSamples<uint8_t>(
            nrStations, nrSamplesPerBlock, nrIntegrationsPerBlock,
            nrChannels, flagsPerChannel, output);
          break;
      }
    }


    void CorrelatorStep::Flagger::applyWeight(unsigned baseline, 
      unsigned nrChannels, float weight, LOFAR::Cobalt::CorrelatedData &output)
    {
      /*
       * All channels and polarisations are stored consecutively, so
       * we can just grab a pointer to the first sample and walk over
       * all samples in the baseline.
       */
      fcomplex *s = &output.visibilities[baseline][0][0][0];

      unsigned i = 0;

      if (nrChannels > 1) {
        // Channel 0 has weight 0.0 (unless it's the only channel)
        for(; i < NR_POLARIZATIONS * NR_POLARIZATIONS; ++i)
          *(s++) = 0.0;
      }

      // Remaining channels are adjusted by the provided weight
      for(; i < nrChannels * NR_POLARIZATIONS * NR_POLARIZATIONS; ++i)
        *(s++) *= weight;
    }


    template<typename T> void
    CorrelatorStep::Flagger::applyNrValidSamples(
      unsigned nrChannels,
      LOFAR::Cobalt::CorrelatedData &output)
    {
      const bool singleChannel = nrChannels == 1;

      for (unsigned bl = 0; bl < output.itsNrBaselines; ++bl)
      {
        // Calculate the weights for the channels
        //
        // NOTE: We assume all channels to have the same nrValidSamples (except possibly channel 0).
        const T nrValidSamples = output.nrValidSamples<T>(bl, singleChannel ? 0 : 1);

        // If all samples flagged, weights is zero.
        const float weight = nrValidSamples ? 1.0f / nrValidSamples : 0;

        // Apply the weight to this sample, turning the visibilities into the
        // average visibility over the non-flagged samples.
        //
        // This step thus normalises the visibilities for any integration time.
        applyWeight(bl, nrChannels, weight, output);
      }
    }


    void CorrelatorStep::Flagger::applyNrValidSamples(
      unsigned nrChannels,
      LOFAR::Cobalt::CorrelatedData &output)
    {
      switch (output.itsNrBytesPerNrValidSamples) {
        case 4:
          applyNrValidSamples<uint32_t>(nrChannels, output);
          break;

        case 2:
          applyNrValidSamples<uint16_t>(nrChannels, output);
          break;

        case 1:
          applyNrValidSamples<uint8_t>(nrChannels, output);
          break;
      }
    }

    CorrelatorStep::CorrelatorStep(
      const KernelParameters::Observation& obsParameters,
      const KernelParameters::Preprocessor& preParameters,
      const KernelParameters::Correlator& corParameters,
      std::shared_ptr<gpu::Stream> i_htod_stream,
      std::shared_ptr<gpu::Stream> i_dtoh_stream,
      std::shared_ptr<gpu::Stream> i_execute_stream,
      gpu::Context &context,
      Factories &factories,
      std::shared_ptr<gpu::DeviceMemory> i_devA,
      std::shared_ptr<gpu::DeviceMemory> i_devB,
      std::shared_ptr<gpu::DeviceMemory> i_devE,
      size_t nrSubbandsPerSubbandProc)
      :
      ProcessStep(i_htod_stream, i_dtoh_stream, i_execute_stream, context),
      obsParameters(obsParameters),
      preParameters(preParameters),
      corParameters(corParameters),
      correlatorPPF(corParameters.nrChannels > 1),
      flagsWithHistorySamples(boost::extents[obsParameters.nrStations]),
      flagsPerChannel(boost::extents[obsParameters.nrStations]),
      outputCounter(context, "output (correlator)"),
      integratedData(nrSubbandsPerSubbandProc)
    {
      devA=i_devA;
      devB=i_devB;
      devE=i_devE;

      marker_gpu.reset(new gpu::Marker("correlatorStep (gpu)", gpu::Marker::red));
      marker_cpu.reset(new gpu::Marker("correlatorStep (cpu)", gpu::Marker::black));

      if (correlatorPPF) {
        // FIR filter: A -> B
        firFilterKernel.reset(factories.firFilter->create(*executeStream, *devA, *devB));

        // FFT: B -> E
        fftKernel.reset(factories.fft->create(*executeStream, *devB, *devE));
      } else {
        // intToFloat: A -> E
        intToFloatKernel.reset(factories.intToFloat->create(*executeStream, *devA, *devE));
      }

      // Zeroing + Delay and Bandpass: E -> B
      delayAndBandPassKernel.reset(factories.delayAndBandPass.create(*executeStream, *devE, *devB));

      // Correlator: B -> E
      correlatorKernel.reset(factories.correlator.create(*executeStream, *devB, *devE));

      // Initialize the output buffers for the long-time integration
      for (size_t i = 0; i < integratedData.size(); i++) {
        integratedData[i] = 
          // Note that we always integrate complete blocks
          make_pair(0, std::unique_ptr<LOFAR::Cobalt::CorrelatedData>(
            new LOFAR::Cobalt::CorrelatedData(
              obsParameters.nrStations,
              corParameters.nrChannels,
              corParameters.nrSamplesPerBlock)));
      }
    }

    void CorrelatorStep::writeInput(const SubbandProcInputData &input)
    {
      htodStream->waitEvent(executeFinished);

      if (correlatorPPF && corParameters.dopplerCorrection) { // check nrChannels>1 for Doppler corr. 
        htodStream->writeBuffer(firFilterKernel->delaysAtBegin,
          input.delaysAtBegin, false);
        htodStream->writeBuffer(firFilterKernel->delaysAfterEnd,
          input.delaysAfterEnd, false);
      }

      if (preParameters.delayCompensationEnabled) {
        htodStream->writeBuffer(delayAndBandPassKernel->delaysAtBegin,
          input.delaysAtBegin, false);
        htodStream->writeBuffer(delayAndBandPassKernel->delaysAfterEnd,
          input.delaysAfterEnd, false);
        htodStream->writeBuffer(delayAndBandPassKernel->phase0s,
          input.phase0s, false);
      }

      if (correlatorPPF) {
        // Process flags enough to determine which data to zero
        flagsWithHistorySamples = input.inputFlags;

        firFilterKernel->prefixHistoryFlags(
          flagsWithHistorySamples, input.blockID.subbandProcSubbandIdx);

        Cobalt::Flagger::convertFlagsToChannelFlags(
          flagsWithHistorySamples,
          flagsPerChannel,
          obsParameters.blockSize,
          corParameters.nrChannels,
          NR_TAPS - 1);

        delayAndBandPassKernel->writeChannelFlags(flagsPerChannel);
      } else {
        delayAndBandPassKernel->writeChannelFlags(input.inputFlags);
      }

      htodStream->recordEvent(inputFinished);
    }

    void CorrelatorStep::process(const SubbandProcInputData &input)
    {
      executeStream->waitEvent(inputFinished);
      executeStream->waitEvent(outputFinished);

      if (correlatorPPF) {
        // The subbandIdx immediate kernel arg must outlive kernel runs.
        firFilterKernel->enqueue(input.blockID, 
                                 input.blockID.subbandProcSubbandIdx,
                                 obsParameters.subbands[input.blockID.globalSubbandIdx].centralFrequency);
        fftKernel->enqueue(input.blockID);

      } else {
        intToFloatKernel->enqueue(input.blockID);
      }

      // Even if we skip delay compensation and bandpass correction (rare), run
      // that kernel, as it also reorders the data for the correlator kernel.
      //
      // The centralFrequency and SAP immediate kernel args must outlive kernel runs.
      delayAndBandPassKernel->enqueue(
        input.blockID,
        obsParameters.subbands[input.blockID.globalSubbandIdx].centralFrequency);

      correlatorKernel->enqueue(input.blockID);

      executeStream->recordEvent(executeFinished);

      marker_mutex.unlock();
    }


    void CorrelatorStep::readOutput(SubbandProcOutputData &output)
    {
      // Read data back from the kernel
      dtohStream->waitEvent(executeFinished);
      dtohStream->readBuffer(output.correlatedData.data, *devE, outputCounter, false);
      dtohStream->synchronize();
      dtohStream->recordEvent(outputFinished);
    }


    void CorrelatorStep::processCPU(const SubbandProcInputData &input, SubbandProcOutputData &output)
    {
      outputFinished.wait();
      marker_cpu->start(outputFinished);

      // Propagate the flags.
      if (correlatorPPF) {
        flagsWithHistorySamples = input.inputFlags;

        // Put the history flags in front of the sample flags,
        firFilterKernel->prefixHistoryFlags(
          flagsWithHistorySamples, input.blockID.subbandProcSubbandIdx);

        // Transform the flags to channel flags: taking in account
        // reduced resolution in time and the size of the filter
        Cobalt::Flagger::convertFlagsToChannelFlags(
          flagsWithHistorySamples,
          flagsPerChannel,
          obsParameters.blockSize,
          corParameters.nrChannels,
          NR_TAPS - 1);
      }

      // Calculate the number of flags per baseline and assign to
      // output object.
      Flagger::calcNrValidSamples(
        corParameters.stations.size(),
        corParameters.nrSamplesPerBlock,
        corParameters.nrIntegrationsPerBlock,
        corParameters.nrChannels,
        correlatorPPF ? flagsPerChannel : input.inputFlags,
        output.correlatedData);

        marker_cpu->end();
    }


    bool CorrelatorStep::integrate(SubbandProcOutputData &output)
    {
      const size_t idx = output.blockID.subbandProcSubbandIdx;
      const size_t nblock    = corParameters.nrBlocksPerIntegration;
      const size_t nsubblock = corParameters.nrIntegrationsPerBlock;

      // We don't want to copy the data if we don't need to integrate.
      if (nblock == 1) {
        for (size_t i = 0; i < nsubblock; ++i) {
          output.correlatedData.subblocks[i]->setSequenceNumber(output.blockID.block * nsubblock + i);
        }
        return true;
      }

      // We don't have subblocks if we integrate multiple blocks.
      ASSERT( nsubblock == 1 );

      integratedData[idx].first++;

      if (integratedData[idx].first < nblock) {
        if (integratedData[idx].first == 1) {
          // first block: reset
          integratedData[idx].second->reset();
        }

        // non-last block
        *integratedData[idx].second += *output.correlatedData.subblocks[0];
        return false;
      }
      else {
        // last block
        *output.correlatedData.subblocks[0] += *integratedData[idx].second;
        output.correlatedData.subblocks[0]->setSequenceNumber(output.blockID.block / nblock);
        integratedData[idx].first = 0;
        return true;
      }
    }


    bool CorrelatorStep::postprocessSubband(SubbandProcOutputData &output)
    {
      if (!integrate(output)) {
        // Not yet done constructing output block 
        return false;
      }

      // The flags are already copied to the correct location
      // now the flagged amount should be applied to the visibilities
      for (size_t i = 0; i < corParameters.nrIntegrationsPerBlock; ++i) {
        Flagger::applyNrValidSamples(corParameters.nrChannels, *output.correlatedData.subblocks[i]);
      }

      return true;
    }
  }
}
