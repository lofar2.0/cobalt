//# BeamFormerPreprocessingStep.cc
//# Copyright (C) 2012-2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

#include <lofar_config.h>

#include "BeamFormerPreprocessingStep.h"

#include <GPUProc/global_defines.h>
#include <GPUProc/gpu_wrapper.h>
#include <GPUProc/Flagger.h>

#include <ApplCommon/PosixTime.h>
#include <Common/LofarLogger.h>

#include <iomanip>

namespace LOFAR
{
  namespace Cobalt
  {
    BeamFormerPreprocessingStep::Factories::Factories(
      const KernelParameters::Observation& obsParameters,
      const KernelParameters::Preprocessor& preParameters,
      const KernelParameters::Cobalt& cobParameters,
      size_t nrSubbandsPerSubbandProc) :

        nrSTABs(preParameters.obsStationIndices.size()),
        intToFloat(
          preParameters.inputPPF ? NULL :
          new KernelFactory<IntToFloatKernel>(IntToFloatKernel::Parameters(
          obsParameters.nrStations,
          preParameters.obsStationIndices,
          obsParameters.nrBitsPerSample,
          obsParameters.blockSize,
          (preParameters.nrDelayCompensationChannels > 1), // !preParameters.inputPPF is already true
          true, // beamFormerStationSubset
          cobParameters.kernel.dumpIntToFloatKernel,
          str(boost::format("L%d_SB%%03d_BL%%03d_BFPre_IntToFloatKernel.dat") % obsParameters.observationID)
          ))),

        inputFirFilter(
          preParameters.inputPPF ?
          new KernelFactory<FIR_FilterKernel>(FIR_FilterKernel::Parameters(
            nrSTABs, //obsParameters.nrStations,
            obsParameters.nrBitsPerSample,
            true, // inputIsStationData
            false, // Doppler correction
            nrSubbandsPerSubbandProc, //nrSubbands
            preParameters.nrDelayCompensationChannels,
            obsParameters.blockSize / preParameters.nrDelayCompensationChannels, //nrSamplesPerChannel
            obsParameters.clockMHz, // not used
            1.0f, //scalefactor
            "FIR (BF input)",
            cobParameters.kernel.dumpFIR_FilterKernel,
            str(boost::format("L%d_SB%%03d_BL%%03d_BFPre_FIR_FilterKernel.dat") % obsParameters.observationID)
            ))
        : NULL),


        firstFFT(FFT_Kernel::Parameters(
          preParameters.nrDelayCompensationChannels,
          nrSTABs * NR_POLARIZATIONS * obsParameters.blockSize,
          true,
          "FFT (beamformer, 1st)")),

        delayCompensation(DelayAndBandPassKernel::Parameters(
          nrSTABs, //nrStations
          preParameters.obsStationIndices,
          obsParameters.nrStations, //nrDelays
          obsParameters.nrBitsPerSample,
          preParameters.nrDelayCompensationChannels,
          obsParameters.blockSize / preParameters.nrDelayCompensationChannels,
          obsParameters.clockMHz, //not needed in beamformer pipeline
          obsParameters.subbandWidth,
          false, // correlator
          true, // zeroing
          preParameters.delayCompensationEnabled,
          preParameters.bandPassCorrectionEnabled,
          1.0 / preParameters.nrDelayCompensationChannels, // bandpassScale
          false, // transpose
          false, // dopplerCorrection
          cobParameters.kernel.dumpDelayAndBandPassKernel,
          str(boost::format("L%d_SB%%03d_BL%%03d_BFPre_DelayAndBandPassKernel_%c%c%c.dat") %
            obsParameters.observationID %
            (false ? "B" : "b") % //correctBandpass
            (preParameters.delayCompensationEnabled ? "D" : "d") %
            (false ? "T" : "t")) //transpose
        )),

        bandPassCorrection(BandPassCorrectionKernel::Parameters(
          nrSTABs,
          preParameters.nrDelayCompensationChannels,
          obsParameters.blockSize / preParameters.nrDelayCompensationChannels,
          false, // bandPassCorrection
          cobParameters.kernel.dumpBandPassCorrectionKernel,
          str(boost::format("L%d_SB%%03d_BL%%03d_BFPre_BandPassCorrectionKernel.dat") % obsParameters.observationID)))
    {
    }

    BeamFormerPreprocessingStep::BeamFormerPreprocessingStep(
      const KernelParameters::Observation& obsParameters,
      const KernelParameters::Preprocessor& preParameters,
      std::shared_ptr<gpu::Stream> i_htod_stream,
      std::shared_ptr<gpu::Stream> i_dtoh_stream,
      std::shared_ptr<gpu::Stream> i_execute_stream,
      gpu::Context &context,
      Factories &factories,
      std::shared_ptr<gpu::DeviceMemory> i_devA,
      std::shared_ptr<gpu::DeviceMemory> i_devB)
      :
      ProcessStep(i_htod_stream, i_dtoh_stream, i_execute_stream, context),
      obsParameters(obsParameters),
      preParameters(preParameters),
      flagsPerChannel(boost::extents[preParameters.obsStationIndices.size()])
    {
      devA=i_devA;
      devB=i_devB;
      (void)context;

      marker_gpu.reset(new gpu::Marker("preprocessingStep", gpu::Marker::yellow));

      //make sure one signal path is available
      assert(factories.intToFloat || factories.inputFirFilter);
      // intToFloat + FFTShift: A -> B
      if (factories.intToFloat) {
      intToFloatKernel = std::unique_ptr<IntToFloatKernel>(
        factories.intToFloat->create(*executeStream, *devA, *devB));
        inputFirFilterKernel=NULL;
      } else {
      inputFirFilterKernel=std::unique_ptr<FIR_FilterKernel>(
        factories.inputFirFilter->create(*executeStream, *devA, *devB));
        intToFloatKernel = NULL; 
      }

      // FFT: B -> B
      firstFFT = std::unique_ptr<FFT_Kernel>(
        factories.firstFFT.create(*executeStream, *devB, *devB));

      // zeroing + delayComp: B -> A
      delayCompensationKernel = std::unique_ptr<DelayAndBandPassKernel>(
        factories.delayCompensation.create(*executeStream, *devB, *devA));

      // bandPass: A -> B
      bandPassCorrectionKernel = std::unique_ptr<BandPassCorrectionKernel>(
        factories.bandPassCorrection.create(*executeStream, *devA, *devB));
    }

    void BeamFormerPreprocessingStep::writeInput(const SubbandProcInputData &input)
    {
      htodStream->waitEvent(executeFinished);

      if (preParameters.delayCompensationEnabled)
      {
        htodStream->writeBuffer(delayCompensationKernel->delaysAtBegin,
          input.delaysAtBegin, false);
        htodStream->writeBuffer(delayCompensationKernel->delaysAfterEnd,
          input.delaysAfterEnd, false);
        htodStream->writeBuffer(delayCompensationKernel->phase0s,
          input.phase0s, false);
      }

      // Convert input flags to channel flags
      Flagger::convertFlagsToChannelFlags(
        preParameters.obsStationIndices,
        input.inputFlags,
        flagsPerChannel,
        obsParameters.blockSize,
        preParameters.nrDelayCompensationChannels,
        0);

      delayCompensationKernel->writeChannelFlags(flagsPerChannel);

      htodStream->recordEvent(inputFinished);
    }

    void BeamFormerPreprocessingStep::process(const SubbandProcInputData &input)
    {
      executeStream->waitEvent(inputFinished);

      //****************************************
      // Enqueue the kernels
      // Note: make sure to call the right enqueue() for each kernel.
      // Otherwise, a kernel arg may not be set...
      if (intToFloatKernel) {
       intToFloatKernel->enqueue(input.blockID);
      } else {
       inputFirFilterKernel->enqueue(input.blockID,input.blockID.subbandProcSubbandIdx);
      }

      firstFFT->enqueue(input.blockID);

      // The centralFrequency and SAP immediate kernel args must outlive kernel runs.
      delayCompensationKernel->enqueue(
        input.blockID,
        obsParameters.subbands[input.blockID.globalSubbandIdx].centralFrequency);

      bandPassCorrectionKernel->enqueue(
        input.blockID);

      executeStream->recordEvent(executeFinished);

      marker_mutex.unlock();
    }
  }
}
