//# BeamFormerCoherentStep.h
//# Copyright (C) 2012-2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$
#include <lofar_config.h>

#include <complex>

#include <Common/LofarLogger.h>

#include <boost/shared_ptr.hpp>

#include <GPUProc/gpu_wrapper.h>
#include <GPUProc/global_defines.h>
#include <GPUProc/MultiDimArrayHostBuffer.h>
#include <CoInterface/BlockID.h>

#include "SubbandProc.h"

#include "BeamFormerIncoherentStep.h"

#include <iomanip>

namespace LOFAR
{
  namespace Cobalt
  {
    BeamFormerIncoherentStep::Factories::Factories(
      const KernelParameters::Observation& obsParameters,
      const KernelParameters::Preprocessor& preParameters,
      const KernelParameters::BeamFormer& bfParameters,
      const KernelParameters::Cobalt& cobParameters,
      size_t nrSubbandsPerSubbandProc) :
      incoherentStokesTranspose(IncoherentStokesTransposeKernel::Parameters(
        preParameters.obsStationIndices.size(), // nrInputStations
        bfParameters.preStationIndices, // stationIndices
        preParameters.nrDelayCompensationChannels,
        obsParameters.blockSize / preParameters.nrDelayCompensationChannels,
        true, // beamFormerStationSubset
        cobParameters.kernel.dumpIncoherentStokesTransposeKernel,
        str(boost::format("L%d_SB%%03d_BL%%03d_BFIcoh_IncoherentStokesTransposeKernel.dat") % obsParameters.observationID)
      )),

      incoherentInverseFFT(FFT_Kernel::Parameters(
        preParameters.nrDelayCompensationChannels,
        bfParameters.preStationIndices.size() * NR_POLARIZATIONS * obsParameters.blockSize, false,
        "FFT (incoherent, inverse)")),
      incoherentInverseFFTShift(
        !preParameters.inputPPF ?
        new KernelFactory<FFTShiftKernel>(FFTShiftKernel::Parameters(
        bfParameters.preStationIndices.size(),
        preParameters.nrDelayCompensationChannels,
        obsParameters.blockSize / preParameters.nrDelayCompensationChannels,
        "FFT-shift (incoherent, inverse)",
        cobParameters.kernel.dumpFFTShiftKernel,
        str(boost::format("L%d_SB%%03d_BL%%03d_BFIcoh_FFTShiftKernel.dat") % obsParameters.observationID)
      )) : NULL),

     incoherentIFirFilter(
        preParameters.inputPPF 
        ? new KernelFactory<FIR_FilterKernel>(FIR_FilterKernel::Parameters(
            bfParameters.preStationIndices.size(),
            obsParameters.nrBitsPerSample,
            false, // inputIsStationData
            false, // dopplerCorrection
            nrSubbandsPerSubbandProc,
            preParameters.nrDelayCompensationChannels,
            obsParameters.blockSize / preParameters.nrDelayCompensationChannels,
            obsParameters.clockMHz,
            1.0f,
            "FIR (incoherent, synthesis)"))
        : NULL),

      incoherentFirFilter(
        bfParameters.incoherentSettings.nrChannels > 1
        ? new KernelFactory<FIR_FilterKernel>(FIR_FilterKernel::Parameters(
            bfParameters.preStationIndices.size(),
            obsParameters.nrBitsPerSample,
            false, // inputIsStationData
            false, // dopplerCorrection
            nrSubbandsPerSubbandProc,
            bfParameters.incoherentSettings.nrChannels,
            obsParameters.blockSize / bfParameters.incoherentSettings.nrChannels,
            obsParameters.clockMHz,
            static_cast<float>(bfParameters.incoherentSettings.nrChannels),
            "FIR (incoherent, final)"))
        : NULL),
      incoherentFinalFFT(
        bfParameters.incoherentSettings.nrChannels > 1
        ? new KernelFactory<FFT_Kernel>(FFT_Kernel::Parameters(
            bfParameters.incoherentSettings.nrChannels,
            bfParameters.preStationIndices.size() * NR_POLARIZATIONS * obsParameters.blockSize, true,
            "FFT (incoherent, final)"))
        : NULL),

      incoherentStokes(IncoherentStokesKernel::Parameters(
        bfParameters.preStationIndices.size(),
        bfParameters.incoherentSettings.nrChannels,
        obsParameters.blockSize / bfParameters.incoherentSettings.nrChannels,
        bfParameters.incoherentSettings.nrStokes,
        bfParameters.incoherentSettings.timeIntegrationFactor,
        bfParameters.incoherentSettings.quantizerSettings.enabled,
        // bandpass correction for input PPF if output channels 
        // are greater than delay comp. channels
        (preParameters.inputPPF && (bfParameters.coherentSettings.nrChannels > preParameters.nrDelayCompensationChannels) ? preParameters.nrDelayCompensationChannels:0),
        cobParameters.kernel.dumpIncoherentStokesKernel,
        str(boost::format("L%d_SB%%03d_BL%%03d_BFIcoh_IncoherentStokesKernel.dat") % obsParameters.observationID)
      )),

      quantizeOutput(bfParameters.incoherentSettings.quantizerSettings.enabled
        ? new KernelFactory<QuantizeOutputKernel>(QuantizeOutputKernel::Parameters(
        bfParameters.incoherentSettings.nrChannels,
        obsParameters.blockSize / (bfParameters.incoherentSettings.nrChannels
          * bfParameters.incoherentSettings.timeIntegrationFactor),
        1, // nrTABs
        bfParameters.incoherentSettings.nrStokes,
        1, // nrStokes
        bfParameters.incoherentSettings.quantizerSettings.nrBits,
        bfParameters.incoherentSettings.quantizerSettings.scaleMax,
        bfParameters.incoherentSettings.quantizerSettings.scaleMin,
        bfParameters.coherentSettings.quantizerSettings.sIpositive,
        cobParameters.kernel.dumpQuantizeOutputKernel,
        str(boost::format("L%d_SB%%03d_BL%%03d_BFIcoh_QuantizeOutputKernel.dat") % obsParameters.observationID)
        ))
        : NULL)
    {
    }

    BeamFormerIncoherentStep::BeamFormerIncoherentStep(
      std::shared_ptr<gpu::Stream> i_htod_stream,
      std::shared_ptr<gpu::Stream> i_dtoh_stream,
      std::shared_ptr<gpu::Stream> i_execute_stream,
      gpu::Context &context,
      Factories &factories,
      std::shared_ptr<gpu::DeviceMemory> i_devA,
      std::shared_ptr<gpu::DeviceMemory> i_devB,
      std::shared_ptr<gpu::DeviceMemory> i_devE)
      :
      ProcessStep(i_htod_stream, i_dtoh_stream, i_execute_stream, context),
      incoherentStokesPPF(factories.incoherentFirFilter != NULL),
      quantizeOutput(factories.quantizeOutput != NULL),
      devO(context, incoherentStokesPPF
        ? factories.incoherentFirFilter->bufferSize(FIR_FilterKernel::OUTPUT_DATA)
        : factories.incoherentStokes.bufferSize(IncoherentStokesKernel::OUTPUT_DATA)),
      outputCounter(context, "output (incoherent)")
    {
      devA = i_devA;
      devB = i_devB;
      devE = i_devE;

      marker_gpu.reset(new gpu::Marker("incoherentStep", gpu::Marker::blue));

      // Transpose: B -> A
      incoherentTranspose = std::unique_ptr<IncoherentStokesTransposeKernel>(
        factories.incoherentStokesTranspose.create(*executeStream, *devB, *devA));

      // inverse FFT: A -> A (in place)
      incoherentInverseFFT = std::unique_ptr<FFT_Kernel>(
        factories.incoherentInverseFFT.create(*executeStream, *devA, *devA));

      //make sure one signal path is available
      assert(factories.incoherentInverseFFTShift || factories.incoherentIFirFilter);

      if (factories.incoherentInverseFFTShift) {
        // inverse FFT: A -> A (in place)
        incoherentInverseFFT = std::unique_ptr<FFT_Kernel>(
          factories.incoherentInverseFFT.create(*executeStream, *devA, *devA));
        // inverse FFTShift: A -> A (in place)
        incoherentInverseFFTShiftKernel = std::unique_ptr<FFTShiftKernel>(
         factories.incoherentInverseFFTShift->create(*executeStream, *devA, *devA));
        incoherentIFirFilterKernel=NULL;
      } else {
        // inverse FFT: A -> B (out of place)
        incoherentInverseFFT = std::unique_ptr<FFT_Kernel>(
          factories.incoherentInverseFFT.create(*executeStream, *devA, *devB));
        // synthesis FIR B -> A 
        incoherentIFirFilterKernel=std::unique_ptr<FIR_FilterKernel>(
        factories.incoherentIFirFilter->create(*executeStream, *devB, *devA));
        incoherentInverseFFTShiftKernel=NULL;
      }

      if (incoherentStokesPPF) {
        // final FIR: A -> O
        incoherentFirFilterKernel = std::unique_ptr<FIR_FilterKernel>(
          factories.incoherentFirFilter->create(*executeStream, *devA, devO));

        // final FFT: O -> A
        incoherentFinalFFT = std::unique_ptr<FFT_Kernel>(
          factories.incoherentFinalFFT->create(*executeStream, devO, *devA));
      }

      sizeof_data = factories.incoherentStokes.bufferSize(IncoherentStokesKernel::OUTPUT_DATA);

      if (quantizeOutput) {
        // incoherent Stokes: A -> E
        incoherentStokesKernel = std::unique_ptr<IncoherentStokesKernel>(
          factories.incoherentStokes.create(*executeStream, *devA, *devE));

        // quantizeOutput: A -> O
        quantizeOutputKernel = std::unique_ptr<QuantizeOutputKernel>(
         factories.quantizeOutput->create(*executeStream, *devE, devO));

        sizeof_qdata = factories.quantizeOutput->bufferSize(QuantizeOutputKernel::OUTPUT_VALUES);
        sizeof_qmeta = factories.quantizeOutput->bufferSize(QuantizeOutputKernel::OUTPUT_METADATA);
      } else {
        // incoherent Stokes: A -> O
        incoherentStokesKernel = std::unique_ptr<IncoherentStokesKernel>(
          factories.incoherentStokes.create(*executeStream, *devA, devO));
      }
    }

    gpu::DeviceMemory BeamFormerIncoherentStep::outputBuffer() {
      return devO;
    }


    void BeamFormerIncoherentStep::process(const SubbandProcInputData &input)
    {
      executeStream->waitEvent(inputFinished);
      executeStream->waitEvent(outputFinished);

      // ********************************************************************
      // incoherent stokes kernels
      incoherentTranspose->enqueue(input.blockID);

      incoherentInverseFFT->enqueue(input.blockID);

      if (incoherentInverseFFTShiftKernel) {
        incoherentInverseFFTShiftKernel->enqueue(input.blockID);
      } else {
        incoherentIFirFilterKernel->enqueue(input.blockID,
          input.blockID.subbandProcSubbandIdx);
      }

      if (incoherentStokesPPF)
      {
        // The subbandIdx immediate kernel arg must outlive kernel runs.
        incoherentFirFilterKernel->enqueue(input.blockID,
          input.blockID.subbandProcSubbandIdx);

        incoherentFinalFFT->enqueue(input.blockID);
      }

      incoherentStokesKernel->enqueue(input.blockID);

      if (quantizeOutput) {
        quantizeOutputKernel->enqueue(input.blockID);
      }

      executeStream->recordEvent(executeFinished);

      marker_mutex.unlock();
    }


    void BeamFormerIncoherentStep::readOutput(
      SubbandProcOutputData::TABOutputData &output)
    {
      dtohStream->waitEvent(executeFinished);

      if (!quantizeOutput) {
        dtohStream->readBuffer(
          output.get_data(), outputBuffer(),
          0, 0,
          sizeof_data,
          outputCounter, false);
      } else {
        struct QuantizeOutputKernel::GPUBufferOffsets v = quantizeOutputKernel->bufferOffsets();
        dtohStream->readBuffer(
          output.get_qdata(), outputBuffer(),
          0, v.data,
          sizeof_qdata,
          outputCounter, false);
        dtohStream->readBuffer(
          output.get_qoffsets(), outputBuffer(),
          0, v.offset,
          sizeof_qmeta,
          outputCounter, false);
        dtohStream->readBuffer(
          output.get_qscales(), outputBuffer(),
          0, v.scale,
          sizeof_qmeta,
          outputCounter, false);
      }

      dtohStream->recordEvent(outputFinished);
    }
  }
}
