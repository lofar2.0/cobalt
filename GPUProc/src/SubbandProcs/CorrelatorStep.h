//# CorrelatorStep.h
//# Copyright (C) 2012-2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id: CorrelatorStep.h 42125 2019-03-08 13:17:27Z mol $

#ifndef LOFAR_GPUPROC_CUDA_CORRELATOR_STEP_H
#define LOFAR_GPUPROC_CUDA_CORRELATOR_STEP_H

#include <complex>
#include <vector>
#include <utility> // for std::pair

#include <GPUProc/gpu_wrapper.h>

#include <GPUProc/MultiDimArrayHostBuffer.h>
#include <CoInterface/BlockID.h>
#include <CoInterface/CorrelatedData.h>

#include "SubbandProcInputData.h"
#include "SubbandProcOutputData.h"
#include "ProcessStep.h"

#include <GPUProc/PerformanceCounter.h>
#include <GPUProc/Flagger.h>
#include <GPUProc/KernelFactory.h>
#include <GPUProc/Kernels/DelayAndBandPassKernel.h>
#include <GPUProc/Kernels/FIR_FilterKernel.h>
#include <GPUProc/Kernels/FFT_Kernel.h>
#include <GPUProc/Kernels/IntToFloatKernel.h>
#include <GPUProc/Kernels/CorrelatorKernel.h>


namespace LOFAR
{
  namespace Cobalt
  {
    class CorrelatorStep: public ProcessStep
    {
      friend class SubbandProc;

    public:
      struct Factories {
        Factories(
          const KernelParameters::Observation& obsParameters,
          const KernelParameters::Correlator& corParameters,
          const KernelParameters::Preprocessor& preParameters,
          const KernelParameters::Cobalt& cobParameters,
          size_t nrSubbandsPerSubbandProc);

        std::unique_ptr<KernelFactory<FFT_Kernel>> fft;
        std::unique_ptr<KernelFactory<FIR_FilterKernel>> firFilter;
        std::unique_ptr<KernelFactory<IntToFloatKernel>> intToFloat;

        KernelFactory<DelayAndBandPassKernel> delayAndBandPass;

        KernelFactory<CorrelatorKernel> correlator;
      };

      CorrelatorStep(
        const KernelParameters::Observation &obsParameters,
        const KernelParameters::Preprocessor &preParameters,
        const KernelParameters::Correlator &corParameters,
        std::shared_ptr<gpu::Stream> i_htod_stream,
        std::shared_ptr<gpu::Stream> i_dtoh_stream,
        std::shared_ptr<gpu::Stream> i_execute_stream,
        gpu::Context &context,
        Factories &factories,
        std::shared_ptr<gpu::DeviceMemory> i_devA,
        std::shared_ptr<gpu::DeviceMemory> i_devB,
        std::shared_ptr<gpu::DeviceMemory> i_devE,
        size_t nrSubbandsPerSubbandProc);

      void writeInput(const SubbandProcInputData &input);

      void process(const SubbandProcInputData &input);
      void processCPU(const SubbandProcInputData &input, SubbandProcOutputData &output);

      void readOutput(SubbandProcOutputData &output);

      bool postprocessSubband(SubbandProcOutputData &output);

      // Collection of functions to tranfer the input flags to the output.
      // \c propagateFlags can be called parallel to the kernels.
      // After the data is copied from the the shared buffer
      // \c applyNrValidSamples can be used to weight the visibilities
      class Flagger: public Cobalt::Flagger
      {
      public:
        // 1.1 Convert the flags per station to channel flags, change time scale
        // if nchannel > 1
        // (Uses convertFlagsToChannelFlags)

        // 2. Calculate the weight based on the number of flags and apply this
        // weighting to all output values
        static void applyNrValidSamples(
          unsigned nrChannels,
          LOFAR::Cobalt::CorrelatedData &output);

        // 1.2 Calculate the number of flagged samples and set this on the
        // output dataproduct This function is aware of the used filter width a
        // corrects for this.
        static void
        calcNrValidSamples(
          unsigned nrStations,
          unsigned nrSamplesPerBlock,
          unsigned nrIntegrationsPerBlock,
          unsigned nrChannels,
          MultiDimArray<SparseSet<unsigned>, 1> const &flagsPerChannel,
          SubbandProcOutputData::CorrelatedData &output);

        // 2.1 Apply the supplied weight to the complex values in the channels
        // for the given baseline. If nrChannels > 1, visibilities for channel 0 will be set to 0.0.
        static void applyWeight(unsigned baseline, unsigned nrChannels,
                                float weight, LOFAR::Cobalt::CorrelatedData &output);
      private:
        template<typename T>
        static void applyNrValidSamples(
          unsigned nrChannels,
          LOFAR::Cobalt::CorrelatedData &output);

        template<typename T>
        static void
        calcNrValidSamples(
          unsigned nrStations,
          unsigned nrSamplesPerBlock,
          unsigned nrIntegrationsPerBlock,
          unsigned nrChannels,
          MultiDimArray<SparseSet<unsigned>, 1> const &flagsPerChannel,
          SubbandProcOutputData::CorrelatedData &output);

      };

    private:
      // Parameters
      KernelParameters::Observation obsParameters;
      KernelParameters::Preprocessor preParameters;
      KernelParameters::Correlator corParameters;

      const bool correlatorPPF;

      // Preallocated flags for input data, to prefix with FIR history -- used locally
      MultiDimArray<SparseSet<unsigned>, 1> flagsWithHistorySamples;

      // Preallocated flags for FFT-ed data -- used locally
      MultiDimArray<SparseSet<unsigned>, 1> flagsPerChannel;

      //Data members
      std::shared_ptr<gpu::DeviceMemory> devA;
      std::shared_ptr<gpu::DeviceMemory> devB;
      std::shared_ptr<gpu::DeviceMemory> devE;

      /*
       * Kernels
       */

      // FIR filter
      std::unique_ptr<FIR_FilterKernel> firFilterKernel;

      // FFT
      std::unique_ptr<FFT_Kernel> fftKernel;

      // IntToFloat (in case of no FFT)
      std::unique_ptr<IntToFloatKernel> intToFloatKernel;

      // Zeroing + Delay and Bandpass
      std::unique_ptr<DelayAndBandPassKernel> delayAndBandPassKernel;

      // Correlator
      std::unique_ptr<CorrelatorKernel> correlatorKernel;

      PerformanceCounter outputCounter;

      // Buffers for long-time integration; one buffer for each subband that
      // will be processed by this class instance. Each element of the vector
      // contains a counter that tracks the number of additions made to the data
      // buffer and the data buffer itself.
      std::vector<std::pair<size_t, std::unique_ptr<LOFAR::Cobalt::CorrelatedData>>>
      integratedData;

      bool integrate(SubbandProcOutputData &output);
    };
  }
}

#endif
