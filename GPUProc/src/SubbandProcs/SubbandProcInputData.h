//# SubbandProcInputData.h
//# Copyright (C) 2012-2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id: SubbandProcInputData.h 29848 2014-07-31 09:32:20Z mol $

#ifndef LOFAR_GPUPROC_CUDA_SUBBAND_PROC_INPUT_DATA_H
#define LOFAR_GPUPROC_CUDA_SUBBAND_PROC_INPUT_DATA_H

#include <vector>

#include <CoInterface/BlockID.h>
#include <CoInterface/Parset.h>
#include <CoInterface/SubbandMetaData.h>
#include <GPUProc/MultiDimArrayHostBuffer.h>
#include <GPUProc/gpu_wrapper.h>

// \file
// TODO: Update documentation

namespace LOFAR
{
  namespace Cobalt
  {
    //   Collect all inputData for the correlatorSubbandProc item:
    //    \arg inputsamples
    //    \arg delays
    //    \arg phaseOffSets
    //    \arg flags
    // It also contains a read function parsing all this data from an input stream.   
    class SubbandProcInputData
    {
    public:
      // Which block this InputData represents
      struct BlockID blockID;

      // Delays are computed and applied in double precision,
      // otherwise the to be computed phase shifts become too inprecise.

      //!< Whole sample delays at the start of the workitem      
      MultiDimArrayHostBuffer<double, 2> delaysAtBegin;

      //!< Whole sample delays at the end of the workitem      
      MultiDimArrayHostBuffer<double, 2> delaysAfterEnd;

      //!< Remainder of delays
      MultiDimArrayHostBuffer<double, 2> phase0s;

      //!< Delays for TABs (aka pencil beams) after station beam correction
      std::vector<std::shared_ptr<MultiDimArrayHostBuffer<double, 2>>> tabDelays;

      // inputdata with flagged data set to zero
      MultiDimArrayHostBuffer<char, 4> inputSamples;

      // The input flags
      MultiDimArray<SparseSet<unsigned>, 1> inputFlags;

      // CPU-side holder for the Meta Data
      std::vector<SubbandMetaData> metaData; // [station]

      // Create the inputData object we need shared host/device memory on the
      // supplied devicequeue
      SubbandProcInputData(const Parset &ps,
                           gpu::Context &context,
                           unsigned int hostBufferFlags = 0);

      // process the given meta data 
      void applyMetaData(const Parset &ps, unsigned station,
                         unsigned SAP, const SubbandMetaData &metaData);

      // set all flagged inputSamples to zero.
      void flagInputSamples(unsigned station, const SubbandMetaData& metaData);
    };
  }
}

#endif

