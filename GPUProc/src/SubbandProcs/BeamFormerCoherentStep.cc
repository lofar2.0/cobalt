//# BeamFormerCoherentStep.h
//# Copyright (C) 2012-2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$
#include <lofar_config.h>

#include <complex>

#include <Common/LofarLogger.h>

#include <boost/shared_ptr.hpp>
#include <boost/format.hpp>

#include <GPUProc/gpu_wrapper.h>
#include <GPUProc/global_defines.h>
#include <GPUProc/MultiDimArrayHostBuffer.h>
#include <CoInterface/BlockID.h>

#include "SubbandProc.h"
#include "BeamFormerCoherentStep.h"

#include <iomanip>

namespace LOFAR
{
  namespace Cobalt
  {

    BeamFormerCoherentStep::Factories::Factories(
      const KernelParameters::Observation& obsParameters,
      const KernelParameters::Preprocessor& preParameters,
      const KernelParameters::BeamFormer& bfParameters,
      const KernelParameters::Cobalt& cobParameters,
      size_t nrSubbandsPerSubbandProc) :
      beamFormer(BeamFormerKernel::Parameters(
        bfParameters.preStationIndices, // stationIndices
        bfParameters.obsStationIndices, // delayIndices
        obsParameters.nrStations, // nrDelays
        preParameters.nrDelayCompensationChannels,
        obsParameters.blockSize / preParameters.nrDelayCompensationChannels,
        bfParameters.maxNrCoherentTABsPerSAP,
        obsParameters.subbandWidth,
        bfParameters.doFlysEye,
        cobParameters.kernel.dumpBeamFormerKernel,
        str(boost::format("L%d_SB%%03d_BL%%03d_BFCoh_BeamFormerKernel.dat") % obsParameters.observationID)
      )),
      coherentTranspose(CoherentStokesTransposeKernel::Parameters(
        preParameters.nrDelayCompensationChannels,
        obsParameters.blockSize / preParameters.nrDelayCompensationChannels,
        bfParameters.maxNrCoherentTABsPerSAP,
        cobParameters.kernel.dumpCoherentStokesTransposeKernel,
        str(boost::format("L%d_SB%%03d_BL%%03d_BFCoh_CoherentStokesTransposeKernel.dat") % obsParameters.observationID)
      )),

      coherentInverseFFT(FFT_Kernel::Parameters(
        preParameters.nrDelayCompensationChannels,
        bfParameters.maxNrCoherentTABsPerSAP * NR_POLARIZATIONS * obsParameters.blockSize,
        false,
        "FFT (coherent, inverse)")),
      coherentInverseFFTShift(
        !preParameters.inputPPF ?
        new KernelFactory<FFTShiftKernel>(FFTShiftKernel::Parameters(
        bfParameters.maxNrCoherentTABsPerSAP,
        preParameters.nrDelayCompensationChannels,
        obsParameters.blockSize / preParameters.nrDelayCompensationChannels,
        "FFT-shift (coherent, inverse)",
        cobParameters.kernel.dumpFFTShiftKernel,
        str(boost::format("L%d_SB%%03d_BL%%03d_BFCoh_FFTShiftKernel.dat") % obsParameters.observationID)
      )) : NULL),

      coherentIFirFilter(
        preParameters.inputPPF
        ? new KernelFactory<FIR_FilterKernel>(FIR_FilterKernel::Parameters(
            bfParameters.maxNrCoherentTABsPerSAP,
            obsParameters.nrBitsPerSample,
            false, // inputIsStationData
            false, // DopplerCorrection
            nrSubbandsPerSubbandProc,
            preParameters.nrDelayCompensationChannels,
            obsParameters.blockSize / preParameters.nrDelayCompensationChannels,
            obsParameters.clockMHz, // not used 
            1.0f, //scale factor
            "IFIR (coherent, synthesis)",
            cobParameters.kernel.dumpFIR_FilterKernel,
            str(boost::format("L%d_SB%%03d_BL%%03d_BFCoh_IFIR_FilterKernel.dat") % obsParameters.observationID),
            preParameters.inputPPF
            ))
        : NULL),

      coherentFirFilter(
        bfParameters.coherentSettings.nrChannels > 1
        ? new KernelFactory<FIR_FilterKernel>(FIR_FilterKernel::Parameters(
            bfParameters.maxNrCoherentTABsPerSAP,
            obsParameters.nrBitsPerSample,
            false, // inputIsStationData
            false, // dopplerCorrection
            nrSubbandsPerSubbandProc,
            bfParameters.coherentSettings.nrChannels,
            obsParameters.blockSize / bfParameters.coherentSettings.nrChannels,
            obsParameters.clockMHz,
            static_cast<float>(bfParameters.coherentSettings.nrChannels),
            "FIR (coherent, final)",
            cobParameters.kernel.dumpFIR_FilterKernel,
            str(boost::format("L%d_SB%%03d_BL%%03d_BFCoh_FIR_FilterKernel.dat") % obsParameters.observationID)
            ))
        : NULL),
      coherentFinalFFT(
        bfParameters.coherentSettings.nrChannels > 1
        ? new KernelFactory<FFT_Kernel>(FFT_Kernel::Parameters(
            bfParameters.coherentSettings.nrChannels,
            bfParameters.maxNrCoherentTABsPerSAP * NR_POLARIZATIONS * obsParameters.blockSize,
            true, // forward
            "FFT (coherent, final)"))
        : NULL),

      coherentStokes(CoherentStokesKernel::Parameters(
        bfParameters.coherentSettings.nrChannels,
        obsParameters.blockSize / bfParameters.coherentSettings.nrChannels,
        bfParameters.maxNrCoherentTABsPerSAP,
        bfParameters.coherentSettings.nrStokes,
        bfParameters.coherentSettings.type == STOKES_XXYY,
        bfParameters.coherentSettings.timeIntegrationFactor,
        bfParameters.coherentSettings.quantizerSettings.enabled,
        // bandpass correction for input PPF if output channels 
        // are greater than delay comp. channels
        (preParameters.inputPPF && (bfParameters.coherentSettings.nrChannels > preParameters.nrDelayCompensationChannels) ? preParameters.nrDelayCompensationChannels:0),
        cobParameters.kernel.dumpCoherentStokesKernel,
        str(boost::format("L%d_SB%%03d_BL%%03d_BFCoh_CoherentStokesKernel.dat") % obsParameters.observationID)
      )),

      quantizeOutput(bfParameters.coherentSettings.quantizerSettings.enabled
        ? new KernelFactory<QuantizeOutputKernel>(QuantizeOutputKernel::Parameters(
        bfParameters.coherentSettings.nrChannels,
        obsParameters.blockSize /
         (bfParameters.coherentSettings.nrChannels
          * bfParameters.coherentSettings.timeIntegrationFactor),
        bfParameters.maxNrCoherentTABsPerSAP,
        bfParameters.coherentSettings.nrStokes,
        bfParameters.coherentSettings.type == STOKES_XXYY,
        bfParameters.coherentSettings.quantizerSettings.nrBits,
        bfParameters.coherentSettings.quantizerSettings.scaleMax,
        bfParameters.coherentSettings.quantizerSettings.scaleMin,
        bfParameters.coherentSettings.quantizerSettings.sIpositive,
        cobParameters.kernel.dumpQuantizeOutputKernel,
        str(boost::format("L%d_SB%%03d_BL%%03d_BFCoh_QuantizeOutputKernel.dat") % obsParameters.observationID)
        ))
        : NULL)
    {
    }

    BeamFormerCoherentStep::BeamFormerCoherentStep(
      const KernelParameters::Observation& obsParameters,
      std::shared_ptr<gpu::Stream> i_htod_stream,
      std::shared_ptr<gpu::Stream> i_dtoh_stream,
      std::shared_ptr<gpu::Stream> i_execute_stream,
      gpu::Context &context,
      Factories &factories,
      std::shared_ptr<gpu::DeviceMemory> i_devB,
      std::shared_ptr<gpu::DeviceMemory> i_devD,
      std::shared_ptr<gpu::DeviceMemory> i_devE)
      :
      ProcessStep(i_htod_stream, i_dtoh_stream, i_execute_stream, context),
      obsParameters(obsParameters),
      coherentStokesPPF(factories.coherentFirFilter != NULL),
      quantizeOutput(factories.quantizeOutput != NULL),
      devO(context, factories.coherentTranspose.bufferSize(CoherentStokesTransposeKernel::OUTPUT_DATA)),
      outputCounter(context, "output (coherent)")
    {
      devB = i_devB;
      devD = i_devD;
      devE = i_devE;

      marker_gpu.reset(new gpu::Marker("coherentStep", gpu::Marker::green));

     beamFormerKernel = std::unique_ptr<BeamFormerKernel>(
       factories.beamFormer.create(*executeStream, *devB, *devD));

     // transpose after beamforming: D -> O
     coherentTransposeKernel = std::unique_ptr<CoherentStokesTransposeKernel>(
       factories.coherentTranspose.create(
       *executeStream, *devD, devO));

     //make sure one signal path is available
     assert(factories.coherentInverseFFTShift || factories.coherentIFirFilter);

     if(factories.coherentInverseFFTShift) {
     // inverse FFT: O -> D
     inverseFFT = std::unique_ptr<FFT_Kernel>(
       factories.coherentInverseFFT.create(
         *executeStream, devO, *devD));

     // fftshift: D -> D (in place)
      inverseFFTShiftKernel = std::unique_ptr<FFTShiftKernel>(
       factories.coherentInverseFFTShift->create(
         *executeStream, *devD, *devD));
      ifirFilterKernel =NULL;
     } else {
     // inverse FFT: O -> O
     inverseFFT = std::unique_ptr<FFT_Kernel>(
       factories.coherentInverseFFT.create(
         *executeStream, devO, devO));
      inverseFFTShiftKernel=NULL;
       // IFIR filter: O -> D
       ifirFilterKernel = std::unique_ptr<FIR_FilterKernel>(
         factories.coherentIFirFilter->create(*executeStream, devO, *devD));
     }
     if (coherentStokesPPF) {
       // FIR filter: D -> O
       firFilterKernel = std::unique_ptr<FIR_FilterKernel>(
         factories.coherentFirFilter->create(*executeStream, *devD, devO));

       // final FFT: O -> D
       coherentFinalFFT = std::unique_ptr<FFT_Kernel>(
         factories.coherentFinalFFT->create(*executeStream, devO, *devD));
     }

     sizeof_data = factories.coherentStokes.bufferSize(CoherentStokesKernel::OUTPUT_DATA);

     if (quantizeOutput) {
       // coherentStokes: D -> E
       coherentStokesKernel = std::unique_ptr<CoherentStokesKernel>(
         factories.coherentStokes.create(*executeStream,
         *devD, *devE));

        // quantizeOutput: E -> O
       quantizeOutputKernel = std::unique_ptr<QuantizeOutputKernel>(
         factories.quantizeOutput->create(*executeStream,
         *devE, devO));

       sizeof_qdata = factories.quantizeOutput->bufferSize(QuantizeOutputKernel::OUTPUT_VALUES);
       sizeof_qmeta = factories.quantizeOutput->bufferSize(QuantizeOutputKernel::OUTPUT_METADATA);
     } else {
       // coherentStokes: D -> O
       coherentStokesKernel = std::unique_ptr<CoherentStokesKernel>(
         factories.coherentStokes.create(*executeStream,
         *devD, devO));
     }
    }

    gpu::DeviceMemory BeamFormerCoherentStep::outputBuffer() {
      return devO;
    }


    void BeamFormerCoherentStep::writeInput(const MultiDimArrayHostBuffer<double, 2>& tabDelays)
    {
      // Upload the new beamformerDelays (pointings) to the GPU
      htodStream->waitEvent(executeFinished);
      htodStream->writeBuffer(beamFormerKernel->beamFormerDelays, tabDelays, false);
      htodStream->recordEvent(inputFinished);
    }


    void BeamFormerCoherentStep::process(const SubbandProcInputData &input)
    {
      executeStream->waitEvent(inputFinished);
      executeStream->waitEvent(outputFinished);

      // The centralFrequency immediate kernel arg must outlive kernel runs.
      beamFormerKernel->enqueue(input.blockID,
        obsParameters.subbands[input.blockID.globalSubbandIdx].centralFrequency);

      coherentTransposeKernel->enqueue(input.blockID);

      inverseFFT->enqueue(input.blockID);

      if (inverseFFTShiftKernel) {
       inverseFFTShiftKernel->enqueue(input.blockID);
      } else {
        ifirFilterKernel->enqueue(input.blockID,input.blockID.subbandProcSubbandIdx);
      }

      if (coherentStokesPPF) {
        // The subbandIdx immediate kernel arg must outlive kernel runs.
        firFilterKernel->enqueue(input.blockID,
          input.blockID.subbandProcSubbandIdx);
        coherentFinalFFT->enqueue(input.blockID);
      }

      coherentStokesKernel->enqueue(input.blockID);

      if (quantizeOutput) {
        quantizeOutputKernel->enqueue(input.blockID);
      }

      executeStream->recordEvent(executeFinished);

      marker_mutex.unlock();
    }


    void BeamFormerCoherentStep::readOutput(
      SubbandProcOutputData::TABOutputData &output)
    {
      dtohStream->waitEvent(executeFinished);

      if (!quantizeOutput) {
        dtohStream->readBuffer(
          output.get_data(), outputBuffer(),
          0, 0,
          sizeof_data,
          outputCounter, false);
      } else {
        struct QuantizeOutputKernel::GPUBufferOffsets v = quantizeOutputKernel->bufferOffsets();
        dtohStream->readBuffer(
          output.get_qdata(), outputBuffer(),
          0, v.data,
          sizeof_qdata,
          outputCounter, false);
        dtohStream->readBuffer(
          output.get_qoffsets(), outputBuffer(),
          0, v.offset,
          sizeof_qmeta,
          outputCounter, false);
        dtohStream->readBuffer(
          output.get_qscales(), outputBuffer(),
          0, v.scale,
          sizeof_qmeta,
          outputCounter, false);
      }

      dtohStream->recordEvent(outputFinished);
    }
  }
}
