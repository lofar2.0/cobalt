//# BeamFormerPreprocessingStep.h
//# Copyright (C) 2012-2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id: BeamFormerCoherentStep.h 42125 2019-03-08 13:17:27Z mol $

#ifndef LOFAR_GPUPROC_CUDA_BEAM_FORMER_COHERENT_STEP_H
#define LOFAR_GPUPROC_CUDA_BEAM_FORMER_COHERENT_STEP_H


#include <complex>

#include <Common/LofarLogger.h>
#include <CoInterface/Parset.h>

#include <GPUProc/gpu_wrapper.h>

#include <GPUProc/MultiDimArrayHostBuffer.h>
#include <CoInterface/BlockID.h>

#include "SubbandProcInputData.h"
#include "SubbandProcOutputData.h"
#include "ProcessStep.h"

#include <GPUProc/Kernels/BeamFormerKernel.h>
#include <GPUProc/Kernels/CoherentStokesTransposeKernel.h>
#include <GPUProc/Kernels/DelayAndBandPassKernel.h>
#include <GPUProc/Kernels/FFTShiftKernel.h>
#include <GPUProc/Kernels/FFT_Kernel.h>
#include <GPUProc/Kernels/FIR_FilterKernel.h>
#include <GPUProc/Kernels/CoherentStokesKernel.h>
#include <GPUProc/Kernels/QuantizeOutputKernel.h>

namespace LOFAR
{
  namespace Cobalt
  {
    class BeamFormerCoherentStep: public ProcessStep
    {
      friend class SubbandProc;

    public:
      struct Factories
      {
        Factories(
          const KernelParameters::Observation& obsParameters,
          const KernelParameters::Preprocessor& preParameters,
          const KernelParameters::BeamFormer& bfParameters,
          const KernelParameters::Cobalt& cobParameters,
          size_t nrSubbandsPerSubbandProc);

        KernelFactory<BeamFormerKernel> beamFormer;
        KernelFactory<CoherentStokesTransposeKernel> coherentTranspose;
        KernelFactory<FFT_Kernel> coherentInverseFFT;
        std::unique_ptr<KernelFactory<FFTShiftKernel>> coherentInverseFFTShift;
        std::unique_ptr<KernelFactory<FIR_FilterKernel>> coherentIFirFilter;
        std::unique_ptr<KernelFactory<FIR_FilterKernel>> coherentFirFilter;
        std::unique_ptr<KernelFactory<FFT_Kernel>> coherentFinalFFT;
        KernelFactory<CoherentStokesKernel> coherentStokes;
        std::unique_ptr<KernelFactory<QuantizeOutputKernel>> quantizeOutput;
      };

      BeamFormerCoherentStep(
        const KernelParameters::Observation& obsParameters,
        std::shared_ptr<gpu::Stream> i_htod_stream,
        std::shared_ptr<gpu::Stream> i_dtoh_stream,
        std::shared_ptr<gpu::Stream> i_execute_stream,
        gpu::Context &context,
        Factories &factories,
        std::shared_ptr<gpu::DeviceMemory> i_devB,
        std::shared_ptr<gpu::DeviceMemory> i_devD,
        std::shared_ptr<gpu::DeviceMemory> i_devE);

      gpu::DeviceMemory outputBuffer();

      void writeInput(const MultiDimArrayHostBuffer<double, 2>& tabDelays);

      void process(const SubbandProcInputData &input);

      void readOutput(
        SubbandProcOutputData::TABOutputData &output);

    private:
      // Parameters
      KernelParameters::Observation obsParameters;

      const bool coherentStokesPPF;

      // flag to remember if quantization is enabled or not
      const bool quantizeOutput;

      // Data members
      std::shared_ptr<gpu::DeviceMemory> devB;
      std::shared_ptr<gpu::DeviceMemory> devD;
      std::shared_ptr<gpu::DeviceMemory> devE;
      gpu::DeviceMemory devO;

      // Kernel members
      std::unique_ptr<BeamFormerKernel> beamFormerKernel;

      // Transpose 
      std::unique_ptr<CoherentStokesTransposeKernel> coherentTransposeKernel;

      // inverse (4k points) FFT
      std::unique_ptr<FFT_Kernel> inverseFFT;

      // inverse FFT-shift
      std::unique_ptr<FFTShiftKernel> inverseFFTShiftKernel;
      // synthesis FIR filter
      std::unique_ptr<FIR_FilterKernel> ifirFilterKernel;

      // Poly-phase filter (FIR + FFT)
      std::unique_ptr<FIR_FilterKernel> firFilterKernel;
      std::unique_ptr<FFT_Kernel> coherentFinalFFT;

      // Coherent Stokes
      std::unique_ptr<CoherentStokesKernel> coherentStokesKernel;

      PerformanceCounter outputCounter;

      size_t sizeof_data;

      // Quantizer
      std::unique_ptr<QuantizeOutputKernel> quantizeOutputKernel;
      size_t sizeof_qdata;
      size_t sizeof_qmeta;
    };


  }
}

#endif


