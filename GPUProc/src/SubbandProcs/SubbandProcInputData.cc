//# SubbandProcInputData.cc
//# Copyright (C) 2012-2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

#include <lofar_config.h>

#include "SubbandProcInputData.h"

#include <CoInterface/Config.h>

namespace LOFAR
{
  namespace Cobalt
  {
    // Constructor pulling all relevant values from a Parset
    SubbandProcInputData::SubbandProcInputData(const Parset &ps,
                         gpu::Context &context,
                         unsigned int hostBufferFlags)
      :
      delaysAtBegin(boost::extents[ps.settings.antennaFields.size()][NR_POLARIZATIONS],
                     context, hostBufferFlags),
      delaysAfterEnd(boost::extents[ps.settings.antennaFields.size()][NR_POLARIZATIONS],
                     context, hostBufferFlags),
      phase0s(boost::extents[ps.settings.antennaFields.size()][NR_POLARIZATIONS],
                     context, hostBufferFlags),
      inputSamples(boost::extents[ps.settings.antennaFields.size()][ps.settings.blockSize][NR_POLARIZATIONS][ps.nrBytesPerComplexSample()],
                     context, hostBufferFlags), // TODO: The size of the buffer is NOT validated
      inputFlags(boost::extents[ps.settings.antennaFields.size()]),
      metaData(ps.settings.antennaFields.size())
    {
      auto& bfPipelines = ps.settings.beamFormer.pipelines;

      tabDelays.resize(bfPipelines.size());

      for (unsigned i = 0; i < bfPipelines.size(); i++)
      {
        auto& bfPipeline = bfPipelines[i];

        tabDelays[i].reset(
          new MultiDimArrayHostBuffer<double, 2>(
            boost::extents[ps.settings.antennaFields.size()][bfPipeline.maxNrCoherentTABsPerSAP()],
            context,
            hostBufferFlags)
        );
      }
    }

    void SubbandProcInputData::applyMetaData(const Parset &ps,
                                           unsigned station, unsigned SAP,
                                           const SubbandMetaData &metaData)
    {
      // extract and apply the flags
      inputFlags[station] = metaData.flags;

      // NOTE: We do not zero flagged samples here anymore,
      // as we are using the DelayAndBandPassKernel to do so.
      // extract and assign the delays for the station beams

      // X polarisation
      delaysAtBegin[station][0]  = ps.settings.antennaFields[station].delay.x + metaData.stationBeam.delayAtBegin;
      delaysAfterEnd[station][0] = ps.settings.antennaFields[station].delay.x + metaData.stationBeam.delayAfterEnd;
      phase0s[station][0]             = ps.settings.antennaFields[station].phase0.x;

      // Y polarisation
      delaysAtBegin[station][1]  = ps.settings.antennaFields[station].delay.y + metaData.stationBeam.delayAtBegin;
      delaysAfterEnd[station][1] = ps.settings.antennaFields[station].delay.y + metaData.stationBeam.delayAfterEnd;
      phase0s[station][1]             = ps.settings.antennaFields[station].phase0.y;

      if (ps.settings.beamFormer.enabled)
      {
        // we already compensated for the delay for the first beam
        double compensatedDelay = (metaData.stationBeam.delayAfterEnd +
                                   metaData.stationBeam.delayAtBegin) * 0.5;

        // Count the number of coherent TABs as a sanity check
        size_t nrTABs = 0;

        for (unsigned pipelineNr = 0; pipelineNr < ps.settings.beamFormer.pipelines.size(); pipelineNr++)
        {
          auto& pipeline = ps.settings.beamFormer.pipelines[pipelineNr];

          // The coherentIdx for this pipeline
          unsigned coherentIdxInSAP = 0;

          // Note: We only get delays for the coherent TABs
          for (unsigned tabNr = 0; tabNr < pipeline.SAPs[SAP].TABs.size(); tabNr++)
          {
            auto& tab = pipeline.SAPs[SAP].TABs[tabNr];

            if (tab.coherent)
            {
              // subtract the delay that was already compensated for
              (*tabDelays[pipelineNr])[station][coherentIdxInSAP] = (metaData.TABs[tab.coherentIdxInSAP].delayAtBegin +
                                                                     metaData.TABs[tab.coherentIdxInSAP].delayAfterEnd) * 0.5 -
                                                                     compensatedDelay;
              coherentIdxInSAP++;
              nrTABs++;
            }

            ASSERTSTR(pipelineNr == tab.pipelineNr,
              "Mismatch in pipeline index: " << pipelineNr << " != " << tab.pipelineNr);
          }

          // Zero padding entries that exist because we always produce maxNrCoherentTABsPerSAP for any subband
          for (unsigned tab = pipeline.SAPs[SAP].TABs.size(); tab < pipeline.maxNrCoherentTABsPerSAP(); tab++)
            (*tabDelays[pipelineNr])[station][tab] = 0.0;
        }

        ASSERTSTR(nrTABs == ps.settings.beamFormer.SAPs[SAP].nrCoherent,
          "Mismatch in coherent TAB count: " << nrTABs << " != " << ps.settings.beamFormer.SAPs[SAP].nrCoherent);
      }
    }
  }
}


