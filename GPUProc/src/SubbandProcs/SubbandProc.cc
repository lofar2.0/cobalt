//# SubbandProc.cc
//# Copyright (C) 2012-2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

#include <lofar_config.h>

#include "SubbandProc.h"

#include <GPUProc/global_defines.h>
#include <GPUProc/gpu_wrapper.h>

#include <CoInterface/Parset.h>
#include <CoInterface/Align.h>

#include <ApplCommon/PosixTime.h>
#include <Common/LofarLogger.h>

#include <iomanip>
#include <thread>

namespace LOFAR
{
  namespace Cobalt
  {
    SubbandProc::SubbandProc(
      const Parset &ps,
      gpu::Context &context,
      size_t nrSubbandsPerSubbandProc)
    :
      inputPool("SubbandProc::inputPool", ps.settings.realTime),
      processPool("SubbandProc::processPool", ps.settings.realTime, nrSubbandsPerSubbandProc),
      outputPool("SubbandProc::outputPool", ps.settings.realTime),

      ps(ps),
      context(context),
      nrSubbandsPerSubbandProc(nrSubbandsPerSubbandProc),
      htodStream(new gpu::Stream(context)),
      dtohStream(new gpu::Stream(context)),
      executeStream(new gpu::Stream(context)),
      prevSAP(-1),
      prevBlock(-1),
      totalCounter(context, "total"),
      inputCounter(context, "input"),
      processCPUTimer("processCPU", ps.settings.blockDuration() / nrSubbandsPerSubbandProc, true, true)
    {
      // See doc/bf-pipeline.txt
      size_t devA_size = 0;
      size_t devB_size = 0;
      size_t devD_size = 0;
      size_t devE_size = 0;

      bool correlatorEnabled = ps.settings.correlator.enabled;
      bool preprocessingEnabled = ps.settings.beamFormer.enabled;
      KernelParameters::Observation obsParameters(ps.settings);
      KernelParameters::Correlator corParameters = ps.settings.correlator;
      KernelParameters::Preprocessor preParameters(
        ps.settings.antennaFieldNames,
        ps.settings.beamFormer.antennaFieldNames,
        ps.settings.delayCompensation.enabled,
        ps.settings.corrections.bandPass,
        ps.settings.beamFormer.nrDelayCompensationChannels,
        ps.settings.beamFormer.inputPPF);
      KernelParameters::Cobalt cobParameters(ps.settings.cobalt);

      //################################################
      // Create objects containing the kernel and device buffers
      // for the correlator pipeline (if any)

      if (correlatorEnabled)
      {
        correlatorFactory.reset(
          new CorrelatorStep::Factories(
            obsParameters,
            corParameters,
            preParameters,
            cobParameters,
            nrSubbandsPerSubbandProc));

        devA_size = std::max(devA_size,
          correlatorFactory->firFilter ? correlatorFactory->firFilter->bufferSize(FIR_FilterKernel::INPUT_DATA)
                       : correlatorFactory->delayAndBandPass.bufferSize(DelayAndBandPassKernel::INPUT_DATA));
        devB_size = std::max(devB_size,
                      correlatorFactory->correlator.bufferSize(CorrelatorKernel::INPUT_DATA));
        devE_size = std::max(correlatorFactory->correlator.bufferSize(CorrelatorKernel::INPUT_DATA),
                             correlatorFactory->correlator.bufferSize(CorrelatorKernel::OUTPUT_DATA));
      }

      //################################################
      // Create objects containing the kernel and device buffers
      // for the beamformer preprocessing pipeline (if any)

      if (preprocessingEnabled)
      {
        preprocessingFactory.reset(
          new BeamFormerPreprocessingStep::Factories(
            obsParameters,
            preParameters,
            cobParameters, 
            nrSubbandsPerSubbandProc));

        devA_size = std::max(devA_size,
          (preprocessingFactory->intToFloat?
           preprocessingFactory->intToFloat->bufferSize(IntToFloatKernel::INPUT_DATA)
           : preprocessingFactory->inputFirFilter->bufferSize(FIR_FilterKernel::INPUT_DATA)));
        devA_size = std::max(devA_size,
          (preprocessingFactory->intToFloat?
           preprocessingFactory->intToFloat->bufferSize(IntToFloatKernel::OUTPUT_DATA)
           : preprocessingFactory->inputFirFilter->bufferSize(FIR_FilterKernel::OUTPUT_DATA)));
        devB_size = std::max(devB_size,
          (preprocessingFactory->intToFloat?
           preprocessingFactory->intToFloat->bufferSize(IntToFloatKernel::OUTPUT_DATA)
           : preprocessingFactory->inputFirFilter->bufferSize(FIR_FilterKernel::OUTPUT_DATA)));
      }


      //################################################
      // Create objects containing the kernel and device buffers
      // for the beamformer coherent stokes and incoherent stokes pipelines

      auto& bfPipelines = ps.settings.beamFormer.pipelines;

      coherentFactories.resize(bfPipelines.size());
      incoherentFactories.resize(bfPipelines.size());
      coherentSteps.resize(bfPipelines.size());
      incoherentSteps.resize(bfPipelines.size());

      std::vector<std::unique_ptr<KernelParameters::BeamFormer>> bfParameters(bfPipelines.size());

      for (unsigned i = 0; i < bfPipelines.size(); i++)
      {
        auto& bfPipeline = bfPipelines[i];

        bfParameters[i].reset(new KernelParameters::BeamFormer(
          ps.settings.antennaFieldNames,
          ps.settings.beamFormer.antennaFieldNames,
          bfPipeline));

        if (bfPipeline.maxNrCoherentTABsPerSAP() > 0) {
          coherentFactories[i].reset(
            new BeamFormerCoherentStep::Factories(
              obsParameters,
              preParameters,
              *bfParameters[i],
              cobParameters,
              nrSubbandsPerSubbandProc));

          devB_size = std::max(devB_size,
            coherentFactories[i]->beamFormer.bufferSize(BeamFormerKernel::INPUT_DATA));

          devD_size = std::max(devD_size,
            coherentFactories[i]->beamFormer.bufferSize(BeamFormerKernel::OUTPUT_DATA));

          devE_size = std::max(devE_size,
            coherentFactories[i]->coherentStokes.bufferSize(CoherentStokesKernel::OUTPUT_DATA));
        }

        if (bfPipeline.maxNrIncoherentTABsPerSAP() > 0) {
          incoherentFactories[i].reset(
            new BeamFormerIncoherentStep::Factories(
              obsParameters,
              preParameters,
              *bfParameters[i],
              cobParameters,
              nrSubbandsPerSubbandProc));

          /* incoherentStokes uses devA and devB, but the sizes provided by the preprocessing
             pipeline are already sufficient. */

          devE_size = std::max(devE_size,
            incoherentFactories[i]->incoherentStokes.bufferSize(IncoherentStokesKernel::OUTPUT_DATA));
        }
      }

      // NOTE: For an explanation of the different buffers being used, please refer
      // to the document pipeline-buffers.txt in the GPUProc/doc directory.
      devA.reset(new gpu::DeviceMemory(context, devA_size));
      devB.reset(new gpu::DeviceMemory(context, devB_size));
      devD.reset(new gpu::DeviceMemory(context, devD_size));
      devE.reset(new gpu::DeviceMemory(context, devE_size));

      //################################################
      // Now that the maximum sizes of the device buffers are known,
      // initialize the process steps.

      if (correlatorFactory.get())
      {
        correlatorStep.reset(
          new CorrelatorStep(
            obsParameters,
            preParameters,
            corParameters,
            htodStream, dtohStream, executeStream, context, *correlatorFactory,
          devA, devB, devE, nrSubbandsPerSubbandProc));
      }

      if (preprocessingFactory.get())
      {
        preprocessingStep.reset(
          new BeamFormerPreprocessingStep(
            obsParameters,
            preParameters,
            htodStream, dtohStream, executeStream, context, *preprocessingFactory,
          devA, devB));
      }

      for (unsigned i = 0; i < bfPipelines.size(); i++)
      {
        if (coherentFactories[i]) {
          coherentSteps[i].reset(
            new BeamFormerCoherentStep(
              obsParameters,
              htodStream, dtohStream, executeStream, context, *coherentFactories[i],
            devB, devD, devE));
        }

        if (incoherentFactories[i]) {
          incoherentSteps[i].reset(
            new BeamFormerIncoherentStep(
              htodStream, dtohStream, executeStream, context, *incoherentFactories[i],
            devA, devB, devE));
        }
      }

      LOG_INFO_STR("Pipeline configuration: ");
      if (correlatorStep.get())
      {
        LOG_INFO_STR("[correlator]");
      }
      if (preprocessingStep.get())
      {
        std::stringstream info;
        info << "[bf preproc]";
        info << " " << ps.settings.beamFormer.antennaFieldNames.size() << " stations: [ ";
        for (unsigned stationIdx : preParameters.obsStationIndices)
        {
          info << stationIdx << " ";
        }
        info << "]";
        LOG_INFO_STR(info.str());
      }
      for (unsigned i = 0; i < bfPipelines.size(); i++)
      {
        std::stringstream info;
        info << "[bf]";
        auto& bfp = bfPipelines[i];
        if (bfPipelines.size() > 1)
        {
          info << " " << i << ":";
        }
        if (coherentSteps[i].get())
        {
          info << " [coh stokes] ";
          info << bfp.maxNrCoherentTABsPerSAP() << " TABs";
        }
        if (incoherentSteps[i].get())
        {
          info << " [incoh stokes] ";
          info << bfp.maxNrIncoherentTABsPerSAP() << " TABs";
        }
        info << ", " << bfp.antennaFieldNames.size() << " stations: [ ";
        ASSERTSTR(bfp.antennaFieldNames.size() == bfParameters[i]->preStationIndices.size(), "Mismatch in station selection");
        for (unsigned stationIdx : bfParameters[i]->preStationIndices)
        {
          info << stationIdx << " ";
        }
        info << "]";
        LOG_INFO_STR(info.str());
      }

      // put enough objects in the inputPool to operate
      //
      // At least 3 items are needed for a smooth Pool operation.
      LOG_INFO_STR("Initializing input pool");
      size_t nrInputDatas = std::max(3UL, 2 * nrSubbandsPerSubbandProc);
      for (size_t i = 0; i < nrInputDatas; ++i) {
        std::shared_ptr<SubbandProcInputData> data(new SubbandProcInputData(ps, context));
        inputPool.free.append(data, false);
      }

      // put enough objects in the outputPool to operate
      LOG_INFO_STR("Initializing output pool");
      for (size_t i = 0; i < nrOutputElements(); ++i)
      {
        std::shared_ptr<SubbandProcOutputData> data(new SubbandProcOutputData(ps, context));
        outputPool.free.append(data, false);
      }
    }

    SubbandProc::~SubbandProc()
    {
      const double averageGPURunTime = totalCounter.getStats().mean() / 1000.0; /* counters are in ms */
      const double blockDuration =  ps.settings.blockDuration();

      // Report how our processing relates to real time
      LOG_INFO_STR("[GPU] Processing ran at " << (100.0 * (averageGPURunTime * nrSubbandsPerSubbandProc) / blockDuration) << "% of real time (GPU required " << averageGPURunTime << "s to process " << blockDuration << "s of data for one subband, and needs to process " << nrSubbandsPerSubbandProc << " subbands per GPU).");

      // Report how many subbands would yield up to 99% load
      LOG_INFO_STR("[GPU] I can process at most  " << static_cast<int>(floor(0.99 * blockDuration / averageGPURunTime)) << " subbands per GPU at real time.");

      // Report kernel statistics for correlator step
      LOG_INFO_STR("[GPU] Correlator step");
      correlatorStep.reset();

      // Report kernel statistics for preprocessing step
      LOG_INFO_STR("[GPU] Preprocessing step");
      preprocessingStep.reset();

      // Report kernel statistics for coherent steps
      for (unsigned int i = 0; i < coherentSteps.size(); i++)
      {
        LOG_INFO_STR("[GPU] Coherent step " << i);
        coherentSteps[i].reset();
      }

      // Report kernel statistics for incoherent steps
      for (unsigned int i = 0; i < incoherentSteps.size(); i++)
      {
        LOG_INFO_STR("[GPU] Incoherent step " << i);
        incoherentSteps[i].reset();
      }
    }


    size_t SubbandProc::nrOutputElements() const
    {
      /*
       * Output elements can get stuck in:
       *   process()                1 element
       *   Best-effort queue:       3 elements
       *   In flight to BE queue:   1 element
       *   In flight to outputProc: 1 element
       *
       * which means we'll need at least 7 elements
       * in the pool to get a smooth operation.
       */
      return 7 * nrSubbandsPerSubbandProc;
    }


    void SubbandProc::processSubband( SubbandProcInputData &input,
      SubbandProcOutputData &output)
    {
      //*******************************************************************
      // calculate some variables depending on the input subband
      size_t block = input.blockID.block;
      unsigned subbandNr = input.blockID.globalSubbandIdx;
      auto &subband = ps.settings.subbands[subbandNr];
      auto &obsSap = ps.settings.SAPs[subband.SAP];

      totalCounter.recordStart(*executeStream);

      //****************************************
      // Wait for input buffer to be free
      if (correlatorStep.get())
      {
        htodStream->waitEvent(correlatorStep->executeFinished);
      }
      if (preprocessingStep.get())
      {
        htodStream->waitEvent(preprocessingStep->executeFinished);
      }

      //****************************************
      // Send inputs to GPU
      htodStream->writeBuffer(*devA, input.inputSamples, inputCounter, true);

      // Some additional buffers
      // Only upload delays if they changed w.r.t. the previous subband.
      //
      // The delays are the same if and only if this subband differs
      // from the previous one only by frequency (subband index).
      // If we changed pointing (SAP) or time (block), we will have new delays.
      // 
      // So, upload delays if and only if either the SAP or the block
      // has changed with respect to the previous subband that was processed.
      //
      // The same goes for the TAB delays below. If they're the same SAP
      // and the same block, they're the same delays.
      bool uploadDelays = (int)subband.SAP != prevSAP || (ssize_t)block != prevBlock;
      if (uploadDelays) {
        if (obsSap.correlatorEnabled) {
          correlatorStep->writeInput(input);
        }

        if (preprocessingStep.get()) {
          preprocessingStep->writeInput(input);
        }

        prevSAP = subband.SAP;
        prevBlock = block;
      }

      // ************************************************
      // Start the correlator step

      if (obsSap.correlatorEnabled) {
        correlatorStep->process(input);
        correlatorStep->readOutput(output);
      }

      // ************************************************
      // Start the beamFormer step

      if (preprocessingStep.get()) {
        preprocessingStep->process(input);
      }


      for (unsigned pipelineNr = 0; pipelineNr < ps.settings.beamFormer.pipelines.size(); pipelineNr++)
      {
        // Skip this beamFormer pipeline when the current subband is not specified
        auto& bfSap = ps.settings.beamFormer.pipelines[pipelineNr].SAPs[subband.SAP];
        if (std::find(bfSap.subbandIndices.begin(), bfSap.subbandIndices.end(), subbandNr) == bfSap.subbandIndices.end())
        {
          continue;
        }

        // Start coherent step
        auto& csStep = coherentSteps[pipelineNr];
        if (csStep.get()) {
          if (uploadDelays) {
            csStep->writeInput(*input.tabDelays[pipelineNr]);
          }

          if (csStep->quantizeOutput && correlatorStep.get()) {
            executeStream->waitEvent(correlatorStep->outputFinished);
          }

          csStep->process(input);
          csStep->readOutput(output.coherentData(pipelineNr));
        }

        // Start incoherent step
        auto& isStep = incoherentSteps[pipelineNr];
        if (isStep.get()) {
          if (isStep->quantizeOutput && correlatorStep.get()) {
            executeStream->waitEvent(correlatorStep->outputFinished);
          }

          isStep->process(input);
          isStep->readOutput(output.incoherentData(pipelineNr));
        }
      }

      totalCounter.recordStop(*executeStream);

      // ************************************************
      // Do CPU computations while the GPU is working

      processCPUTimer.start();

      if (obsSap.correlatorEnabled) {
        correlatorStep->processCPU(input, output);
      }

      processCPUTimer.stop();

      // ************************************************
      // Wait for output to be copied from GPU to host

      dtohStream->synchronize();
    }


    void SubbandProc::postprocessSubband(SubbandProcOutputData &output)
    {
      if (correlatorStep.get()) {
        output.emit_correlatedData = correlatorStep->postprocessSubband(output);
      } else {
        output.emit_correlatedData = false;
      }
    }

    void SubbandProc::synchronize()
    {
      htodStream->synchronize();
      executeStream->synchronize();
      dtohStream->synchronize();
    }
  }
}


