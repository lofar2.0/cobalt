//# SubbandProcOutputData.h
//# Copyright (C) 2012-2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id: SubbandProcOutputData.h 30219 2014-10-15 12:15:41Z mol $

#ifndef LOFAR_GPUPROC_CUDA_SUBBAND_PROC_OUTPUT_DATA_H
#define LOFAR_GPUPROC_CUDA_SUBBAND_PROC_OUTPUT_DATA_H

#include <CoInterface/BlockID.h>
#include <CoInterface/Parset.h>
#include <CoInterface/CorrelatedData.h>
#include <GPUProc/gpu_wrapper.h>
#include <GPUProc/MultiDimArrayHostBuffer.h>

// \file
// TODO: Update documentation

namespace LOFAR
{
  namespace Cobalt
  {
    // Our output data type
    class SubbandProcOutputData
    {
    public:
      struct BlockID blockID;

      struct TABOutputData
      {
        TABOutputData(
          bool quantized,
          long unsigned nrTABs,
          unsigned nrStokes,
          long unsigned nrSamples,
          unsigned nrChannels,
          gpu::Context &context);

        float get_data(
          unsigned tab,
          unsigned stokes,
          unsigned smaple,
          unsigned channel) const;

        bool is_quantized() const { return quantized; };

        float* get_data_origin(
          unsigned tab = 0,
          unsigned stokes = 0);

        int8_t* get_qdata_origin(
          unsigned tab = 0,
          unsigned stokes = 0);

        float* get_qoffsets_origin(
          unsigned tab = 0,
          unsigned stokes = 0);

        float* get_qscales_origin(
          unsigned tab = 0,
          unsigned stokes = 0);

        size_t data_num_elements() const     { return data.num_elements(); };
        size_t qdata_num_elements() const    { return qdata.num_elements(); };
        size_t qoffsets_num_elements() const { return qoffsets.num_elements(); };
        size_t qscales_num_elements() const  { return qscales.num_elements(); };

        void resize(
          long unsigned nrTABs,
          long unsigned nrStokes);

        gpu::HostMemory& get_data()     { return data;     }
        gpu::HostMemory& get_qdata()    { return qdata;    }
        gpu::HostMemory& get_qoffsets() { return qoffsets; }
        gpu::HostMemory& get_qscales()  { return qscales;  }

        size_t data_offset(unsigned long tabStokes);
        size_t qdata_offset(unsigned long tabStokes);
        size_t qoffsets_offset(unsigned long tabStokes);
        size_t qscales_offset(unsigned long tabStokes);

      private:
        bool quantized;
        unsigned nrStokes;
        MultiDimArrayHostBuffer<float, 3> data;
        MultiDimArrayHostBuffer<int8_t, 3> qdata;
        MultiDimArrayHostBuffer<float, 2> qoffsets;
        MultiDimArrayHostBuffer<float, 2> qscales;
      };

      TABOutputData& coherentData(unsigned idx = 0) { return *coherentDatas[idx]; };
      TABOutputData& incoherentData(unsigned idx = 0) { return *incoherentDatas[idx]; };

      std::vector<std::shared_ptr<TABOutputData>> coherentDatas;
      std::vector<std::shared_ptr<TABOutputData>> incoherentDatas;

      struct CorrelatedData
      {
        CorrelatedData(unsigned nrIntegrations,
                       unsigned nrStations, 
                       unsigned nrChannels,
                       unsigned maxNrValidSamples,
                       gpu::Context &context);

        MultiDimArrayHostBuffer<fcomplex, 5> data;
        std::vector<std::unique_ptr<LOFAR::Cobalt::CorrelatedData>> subblocks;
      };

      CorrelatedData correlatedData;
      bool emit_correlatedData;

      SubbandProcOutputData(const Parset &ps, gpu::Context &context);
    };
  }
}

#endif

