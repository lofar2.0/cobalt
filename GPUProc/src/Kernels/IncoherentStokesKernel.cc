//# IncoherentStokesKernel.cc
//# Copyright (C) 2012-2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

#include <lofar_config.h>

#include "IncoherentStokesKernel.h"

#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

#include <Common/complex.h>
#include <CoInterface/Align.h>
#include <CoInterface/Config.h>
#include <GPUProc/FilterBank.h>

namespace LOFAR
{
  namespace Cobalt
  {
    using boost::lexical_cast;
    using boost::format;

    string IncoherentStokesKernel::theirSourceFile = "IncoherentStokes.cu";
    string IncoherentStokesKernel::theirFunction = "incoherentStokes";

    IncoherentStokesKernel::Parameters::Parameters() :
      Kernel::Parameters(theirFunction)
    {}

    IncoherentStokesKernel::Parameters::Parameters(
      unsigned nrStations_,
      unsigned nrChannels_,
      unsigned nrSamplesPerChannel_,
      unsigned nrStokes_,
      unsigned timeIntegrationFactor_,
      bool quantizeOutput_,
      unsigned nrChannelsDelayComp_,
      bool dumpBuffers_,
      std::string dumpFilePattern_
    ) :
      Kernel::Parameters(theirFunction),
      nrStations(nrStations_),
      nrChannels(nrChannels_),
      nrSamplesPerChannel(nrSamplesPerChannel_),
      nrStokes(nrStokes_),
      timeIntegrationFactor(timeIntegrationFactor_),
      outputOrder(quantizeOutput_),
      nrBandpassWeights(nrChannelsDelayComp_?nrChannels_/nrChannelsDelayComp_:0),
      nrChannelsDelayComp(nrChannelsDelayComp_)
    {
      dumpBuffers = dumpBuffers_;
      dumpFilePattern = dumpFilePattern_;
    }

    size_t IncoherentStokesKernel::Parameters::bufferSize(BufferType bufferType) const
    {
      switch (bufferType) {
      case IncoherentStokesKernel::INPUT_DATA:
        return 
          (size_t) nrStations * NR_POLARIZATIONS * 
          nrSamplesPerChannel * 
          nrChannels * sizeof(std::complex<float>);
      case IncoherentStokesKernel::OUTPUT_DATA:
        return 
          (size_t) nrStokes * nrSamplesPerChannel / 
          timeIntegrationFactor * 
          nrChannels * sizeof(float);

      case IncoherentStokesKernel::BAND_PASS_CORRECTION_WEIGHTS:
        return nrBandpassWeights?(size_t)nrBandpassWeights*sizeof(float):1UL;

      default:
        THROW(GPUProcException, "Invalid bufferType (" << bufferType << ")");
      }
    }

    IncoherentStokesKernel::IncoherentStokesKernel(const gpu::Stream& stream,
                                                   const gpu::Module& module,
                                                   const Buffers& buffers,
                                                   const Parameters& params) :
      CompiledKernel(stream, gpu::Function(module, theirFunction), buffers, params),
      bandPassCorrectionWeights(stream.getContext(),params.bufferSize(BAND_PASS_CORRECTION_WEIGHTS))
    {
      setArg(0, buffers.output);
      setArg(1, buffers.input);

      unsigned nrTimes = 
        params.nrSamplesPerChannel / params.timeIntegrationFactor;
      unsigned nrPasses = 
        ceilDiv(nrTimes, maxThreadsPerBlock);
      unsigned nrTimesPerPass = 
        ceilDiv(nrTimes, nrPasses);

      if (params.nrBandpassWeights) {
         ASSERT(1 < params.nrBandpassWeights &&
            params.nrBandpassWeights < params.nrChannels);
         ASSERT(params.nrChannels % params.nrBandpassWeights == 0);
      }


      LOG_DEBUG_STR("nrTimes = " << nrTimes);
      LOG_DEBUG_STR("nrPasses = " << nrPasses);
      LOG_DEBUG_STR("nrTimesPerPass = " << nrTimesPerPass);

      setArg(2,bandPassCorrectionWeights);
      if (params.nrBandpassWeights) {
        gpu::HostMemory bpWeights(stream.getContext(), bandPassCorrectionWeights.size());
        const unsigned nrTaps=16;
        FilterBank filterBank(true, nrTaps,
                            params.nrChannelsDelayComp, KAISER);
        filterBank.computeCorrectionFactors(bpWeights.get<float>(),params.nrBandpassWeights,params.nrChannels);
        stream.writeBuffer(bandPassCorrectionWeights, bpWeights, true);
      }

      setEnqueueWorkSizes(
        gpu::Grid(params.nrChannels, nrPasses),
        gpu::Block(1, nrTimesPerPass));

    }

    //--------  Template specializations for KernelFactory  --------//

    template<> CompileDefinitions
    KernelFactory<IncoherentStokesKernel>::compileDefinitions() const
    {
      CompileDefinitions defs =
        KernelFactoryBase::compileDefinitions(itsParameters);

      defs["NR_STATIONS"] = 
        lexical_cast<string>(itsParameters.nrStations);

      defs["NR_CHANNELS"] = 
        lexical_cast<string>(itsParameters.nrChannels);
      defs["NR_SAMPLES_PER_CHANNEL"] = 
        lexical_cast<string>(itsParameters.nrSamplesPerChannel);

      defs["NR_INCOHERENT_STOKES"] = 
        lexical_cast<string>(itsParameters.nrStokes);
      defs["TIME_INTEGRATION_FACTOR"] = 
        lexical_cast<string>(itsParameters.timeIntegrationFactor);

      if (!itsParameters.outputOrder)
      {
        defs["OUTPUT_ORDER_SAMPLES_CHANNELS"] = "1";
      } else
      {
        defs["OUTPUT_ORDER_CHANNELS_SAMPLES"] = "1";
      }

      defs["NR_BANDPASS_WEIGHTS"]=lexical_cast<string>(itsParameters.nrBandpassWeights);

      return defs;
    }

  }
}

