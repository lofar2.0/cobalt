//# IncoherentStokesTransposeKernel.cc
//# Copyright (C) 2012-2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

#include <lofar_config.h>

#include "IncoherentStokesTransposeKernel.h"

#include <CoInterface/Align.h>
#include <CoInterface/Config.h>
#include <Common/complex.h>

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

namespace LOFAR
{
  namespace Cobalt
  {
    using boost::format;
    using boost::lexical_cast;

    const string IncoherentStokesTransposeKernel::theirSourceFile = 
      "IncoherentStokesTranspose.cu";
    const string IncoherentStokesTransposeKernel::theirFunction = 
      "transpose";

    IncoherentStokesTransposeKernel::Parameters::Parameters(
      unsigned nrInputStations_,
      std::vector<unsigned> stationIndices_,
      unsigned nrChannels_,
      unsigned nrSamplesPerChannel_,
      bool beamFormerStationSubset,
      bool dumpBuffers_,
      std::string dumpFilePattern_
    ) :
      Kernel::Parameters("incoherentStokesTranspose"),
      nrInputStations(nrInputStations_),
      stationIndices(stationIndices_),
      doStationSubset(beamFormerStationSubset),
      nrChannels(nrChannels_),
      nrSamplesPerChannel(nrSamplesPerChannel_),
      tileSize(16)
    {
      dumpBuffers = dumpBuffers_;
      dumpFilePattern = dumpFilePattern_;
    }

    size_t IncoherentStokesTransposeKernel::Parameters::bufferSize(BufferType bufferType) const
    {
      switch (bufferType) {
      case IncoherentStokesTransposeKernel::INPUT_DATA:
        return
          (size_t) nrInputStations *
          nrChannels * nrSamplesPerChannel *
          NR_POLARIZATIONS * sizeof(std::complex<float>);
      case IncoherentStokesTransposeKernel::OUTPUT_DATA:
        return 
          (size_t) nrOutputStations() *
          nrChannels * nrSamplesPerChannel * 
          NR_POLARIZATIONS * sizeof(std::complex<float>);
      case IncoherentStokesTransposeKernel::STATION_INDICES:
        return
          (size_t) stationIndices.size() * sizeof stationIndices[0];
      default:
        THROW(GPUProcException, "Invalid bufferType (" << bufferType << ")");
      }
    }

    IncoherentStokesTransposeKernel::
    IncoherentStokesTransposeKernel(const gpu::Stream& stream,
                                    const gpu::Module& module,
                                    const Buffers& buffers,
                                    const Parameters& params) :
      CompiledKernel(stream, gpu::Function(module, theirFunction), buffers, params),
      stationIndices(stream.getContext(), params.bufferSize(STATION_INDICES))
    {
      setArg(0, buffers.output);
      setArg(1, buffers.input);
      setArg(2, stationIndices);

      // upload stationIndices, as they are static across the observation
      gpu::HostMemory stationIndicesHost(stream.getContext(), params.bufferSize(STATION_INDICES));
      std::memcpy(stationIndicesHost.get<void>(), &params.stationIndices.front(),
                  stationIndicesHost.size());
      stream.writeBuffer(stationIndices, stationIndicesHost, true);

      // LOG_DEBUG_STR("align(params.nrSamplesPerChannel, params.tileSize) = "
      //               << "align(" << params.nrSamplesPerChannel
      //               << ", " << params.tileSize << ") = " 
      //               << align(params.nrSamplesPerChannel, params.tileSize));
      // LOG_DEBUG_STR("align(params.nrChannels, params.tileSize)) = "
      //               << "align(" << params.nrChannels 
      //               << ", " << params.tileSize << ") = "
      //               << align(params.nrChannels, params.tileSize));

      const int block_x = params.tileSize;
      const int block_y = params.tileSize;
      setEnqueueWorkSizes(
        gpu::Grid(align(params.nrSamplesPerChannel, params.tileSize) / block_x,
                  align(params.nrChannels, params.tileSize) / block_y),
        gpu::Block(block_x, block_y));
    }

    //--------  Template specializations for KernelFactory  --------//

    template<> CompileDefinitions
    KernelFactory<IncoherentStokesTransposeKernel>::compileDefinitions() const
    {
      CompileDefinitions defs =
        KernelFactoryBase::compileDefinitions(itsParameters);

      defs["NR_INPUT_STATIONS"] =
        lexical_cast<string>(itsParameters.nrInputStations);
      defs["NR_OUTPUT_STATIONS"] =
        lexical_cast<string>(itsParameters.nrOutputStations());
      defs["NR_CHANNELS"] = 
        lexical_cast<string>(itsParameters.nrChannels);
      defs["NR_SAMPLES_PER_CHANNEL"] = 
        lexical_cast<string>(itsParameters.nrSamplesPerChannel);

      defs["TILE_SIZE"] = lexical_cast<string>(itsParameters.tileSize);

      if (itsParameters.doStationSubset)
        defs["DO_STATIONSUBSET"] = "1";

      return defs;
    }

  }
}

