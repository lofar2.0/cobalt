//# QuantizeOutputKernel.cc
//# Copyright (C) 2012-2014  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id: QuantizeOutputKernel.cc 43504 2019-07-04 12:07:45Z klazema $

#include <lofar_config.h>

#include "QuantizeOutputKernel.h"

#include <utility>
#include <fstream>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

#include <Common/complex.h>
#include <Common/LofarLogger.h>
#include <CoInterface/BlockID.h>
#include <CoInterface/Align.h>
#include <CoInterface/Config.h>
#include <CoInterface/fpequals.h>
#include <GPUProc/gpu_utils.h>

namespace LOFAR
{
  namespace Cobalt
  {
    using boost::lexical_cast;
    using boost::format;

    string QuantizeOutputKernel::theirSourceFile = "QuantizeOutput.cu";
    string QuantizeOutputKernel::theirFunction = "quantizeOutput";

    QuantizeOutputKernel::Parameters::Parameters() :
      Kernel::Parameters(theirFunction)
    {}

    QuantizeOutputKernel::Parameters::Parameters(
      unsigned nrChannels_,
      unsigned nrSamplesPerChannel_,
      unsigned nrTABs_,
      unsigned nrStokes_,
      bool outputComplexVoltages_,
      unsigned nrQuantizeBits_,
      float    quantizeScaleMax_,
      float    quantizeScaleMin_,
      bool     sIpositive_,
      bool dumpBuffers_,
      std::string dumpFilePattern_
    ) :
      Kernel::Parameters(theirFunction),
      nrChannels(nrChannels_),
      nrSamplesPerChannel(nrSamplesPerChannel_),
      nrTABs(nrTABs_),
      nrStokes(nrStokes_),
      outputComplexVoltages(outputComplexVoltages_),
      nrQuantizeBits(nrQuantizeBits_),
      quantizeScaleMax(quantizeScaleMax_),
      quantizeScaleMin(quantizeScaleMin_),
      sIpositive(sIpositive_)
    {
      nrThreadsPerBlock = 512;

      // The QuantizeOutputKernel reuses the buffers of CoherentStokesKernel in reverse order:
      // CoherentStokesKernel: C -> D,  QuantizeOutputKernel: D -> C'
      // While D < C due to integration of the data, there is no guarantee that
      // C' <= C, thus need to make sure that C is large enough.
      ASSERT(bufferSize(INPUT_DATA) > bufferSize(OUTPUT_DATA));

      dumpBuffers = dumpBuffers_;
      dumpFilePattern = dumpFilePattern_;
    }


    size_t QuantizeOutputKernel::Parameters::bufferSize(BufferType bufferType) const
    {
      switch (bufferType) {
      case QuantizeOutputKernel::INPUT_DATA:
        return
          (size_t) nrTABs * nrStokes * nrSamplesPerChannel * nrChannels *
          sizeof(float); // values
      case QuantizeOutputKernel::OUTPUT_VALUES:
        return
          (size_t) nrTABs * nrStokes * nrSamplesPerChannel * nrChannels *
          ((float) nrQuantizeBits / 8);
      case QuantizeOutputKernel::OUTPUT_METADATA:
        return
          (size_t) nrTABs * nrStokes * nrChannels * sizeof(float);
      case QuantizeOutputKernel::OUTPUT_DATA:
        return
          bufferSize(OUTPUT_VALUES) + 2 * bufferSize(OUTPUT_METADATA);

      default:
        THROW(GPUProcException, "Invalid bufferType (" << bufferType << ")");
      }
    }


    QuantizeOutputKernel::QuantizeOutputKernel(const gpu::Stream& stream,
                                       const gpu::Module& module,
                                       const Buffers& buffers,
                                       const Parameters& params) :
      CompiledKernel(stream, gpu::Function(module, theirFunction), buffers, params),
      parameters(params)
    {
      ASSERT(params.nrSamplesPerChannel > 0);
      ASSERT(params.nrStokes == 1 || params.nrStokes == 4);

      setArg(0, buffers.output);
      setArg(1, buffers.input);
      setArg(2, params.quantizeScaleMax);
      setArg(3, params.quantizeScaleMin);

      const gpu::Device device(_context.getDevice());
      const int nrMPs = device.getAttribute(CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT);
      gpu::Grid grid(params.nrStokes * nrMPs);
      gpu::Block block(params.nrThreadsPerBlock);

      setEnqueueWorkSizes(grid, block);
    }

    struct QuantizeOutputKernel::GPUBufferOffsets QuantizeOutputKernel::bufferOffsets() const
    {
      unsigned nrTABs     = parameters.nrTABs;
      unsigned nrStokes   = parameters.nrStokes;
      unsigned nrChannels = parameters.nrChannels;
      size_t size = nrTABs * nrStokes * nrChannels * sizeof(float);
      // use a struct for default buffer offset values
      // offset for scale, offset, data
      struct GPUBufferOffsets  defBoff = { 0, size, 2*size };
      return defBoff;
    }

    #define DEBUG_CONVERT_I2F 0
    template<typename INT, typename UINT>
    void convertI2F_(
      MultiDimArray<float, 4>& data,
      const MultiDimArray<INT, 4>&   qdata,
      const MultiDimArray<float, 3>& qscales,
      const MultiDimArray<float, 3>& qoffsets,
      bool outputComplexVoltages = false)
    {
      ASSERTSTR(sizeof(INT)==sizeof(UINT), "INT/UINT sizes differ.");

      // Get parameters
      unsigned nrTABs     = data.shape()[0];
      unsigned nrStokes   = data.shape()[1];
      unsigned nrSamples  = data.shape()[2];
      unsigned nrChannels = data.shape()[3];

      #if DEBUG_CONVERT_I2F
      unsigned i = 0;
      #endif

      // Convert quantized output to floating-point output
      for (size_t tab = 0; tab < nrTABs; tab++)
      {
        for (size_t stokes = 0; stokes < nrStokes; stokes++)
        {
          // Determine the type of quantization to apply
          bool stokes_quv = (stokes > 0);
          bool zero_mean = (outputComplexVoltages || stokes_quv);

          for (size_t sample = 0; sample < nrSamples; sample++)
          {
            for (size_t channel = 0; channel < nrChannels; channel++)
            {
              // Reconstruct sample
              auto x_ptr    = &(qdata[tab][stokes][sample][channel]);
              auto x_int    = zero_mean ? *((INT *) x_ptr) : *((UINT *) x_ptr);
              auto x_scale  = qscales[tab][stokes][channel];
              auto x_offset = qoffsets[tab][stokes][channel];
              auto x_float  = x_int * x_scale + x_offset;
              data[tab][stokes][sample][channel] = x_float;

              #if DEBUG_CONVERT_I2F
              std::cout << "[" << i++ << "] ";
              std::cout << "xint: " << x_int << ", ";
              std::cout << "xscale: " << x_scale << ", ";
              std::cout << "xoffset: " << x_offset << ", ";
              std::cout << "xfloat: " << x_float << std::endl;
              #endif
            }
          }
        }
      }
    }

    void QuantizeOutputKernel::convertI2F(
        MultiDimArrayHostBuffer<float, 4>& dst,
        MultiDimArrayHostBuffer<fcomplex, 4>& src)
    {
      auto nrQuantizeBits = parameters.nrQuantizeBits;
      auto outputComplexVoltages = parameters.outputComplexVoltages;

      // Get multi-dimensional array references
      MultiDimArray<float, 3> qscales  = getQuantizeScales(src);
      MultiDimArray<float, 3> qoffsets = getQuantizeOffsets(src);

      if (nrQuantizeBits == 8)
      {
        MultiDimArray<int8_t, 4> qdata = getQuantizedData<int8_t>(src);
        convertI2F_<int8_t, uint8_t>(dst, qdata, qscales, qoffsets, outputComplexVoltages);
      } else if (nrQuantizeBits == 16)
      {
        MultiDimArray<int16_t, 4> qdata = getQuantizedData<int16_t>(src);
        convertI2F_<int16_t, uint16_t>(dst, qdata, qscales, qoffsets, outputComplexVoltages);
      } else {
        throw std::runtime_error("Invalid value for nrQuantizeBits");
      }
    };
    //--------  Template specializations for KernelFactory  --------//

    template<> CompileDefinitions
    KernelFactory<QuantizeOutputKernel>::compileDefinitions() const
    {
      CompileDefinitions defs =
        KernelFactoryBase::compileDefinitions(itsParameters);

      defs["NR_CHANNELS"] = lexical_cast<string>(itsParameters.nrChannels);
      defs["NR_SAMPLES_PER_CHANNEL"] =
        lexical_cast<string>(itsParameters.nrSamplesPerChannel);

      defs["NR_TABS"] =
        lexical_cast<string>(itsParameters.nrTABs);
      defs["COMPLEX_VOLTAGES"] =
        itsParameters.outputComplexVoltages ? "1" : "0";
      defs["STOKES_I_POSITIVE"] =
        itsParameters.sIpositive ? "1" : "0";
      defs["NR_COHERENT_STOKES"] =
        lexical_cast<string>(itsParameters.nrStokes);

      defs["NR_QUANTIZE_BITS"] =
        lexical_cast<string>(itsParameters.nrQuantizeBits);

      defs["NR_THREADS_PER_BLOCK"] =
        lexical_cast<string>(itsParameters.nrThreadsPerBlock);

      return defs;
    }

  }
}

