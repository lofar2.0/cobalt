//# QuantizeOutputKernel.h
//# Copyright (C) 2012-2014  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id: QuantizeOutputKernel.h 43504 2019-07-04 12:07:45Z klazema $

#ifndef LOFAR_GPUPROC_CUDA_QUANTIZE_OUTPUT_KERNEL_H
#define LOFAR_GPUPROC_CUDA_QUANTIZE_OUTPUT_KERNEL_H

#include <Common/complex.h>
#include <CoInterface/Parset.h>

#include <GPUProc/Kernels/Kernel.h>
#include <GPUProc/KernelFactory.h>
#include <GPUProc/gpu_wrapper.h>
#include <GPUProc/MultiDimArrayHostBuffer.h>

namespace LOFAR
{
  namespace Cobalt
  {

    class QuantizeOutputKernel : public CompiledKernel
    {
    public:
      static std::string theirSourceFile;
      static std::string theirFunction;

      enum BufferType
      {
        INPUT_DATA,
        OUTPUT_DATA, // total size of output, quantized values, scales and offsets
        OUTPUT_VALUES, // size of quantized values
        OUTPUT_METADATA // size of metadata (same for scales and offsets)
      };

      // Parameters that must be passed to the constructor of the
      struct Parameters : Kernel::Parameters
      {
        Parameters();
        Parameters(
          unsigned nrChannels,
          unsigned nrSamplesPerChannel,
          unsigned nrTABs,
          unsigned nrStokes,
          bool outputComplexVoltages,
          unsigned nrQuantizeBits,
          float    quantizeScaleMax,
          float    quantizeScaleMin,
          bool     sIpositive,
          bool dumpBuffers = false,
          std::string dumpFilePattern = "");

        unsigned nrChannels;
        unsigned nrSamplesPerChannel;

        unsigned nrTABs;
        unsigned nrStokes;
        bool     outputComplexVoltages;
        unsigned nrQuantizeBits;
        float    quantizeScaleMax;
        float    quantizeScaleMin;
        bool     sIpositive;

        unsigned int nrThreadsPerBlock;

        size_t bufferSize(BufferType bufferType) const;
      };

      // offsets in GPU buffer during quantization
      struct GPUBufferOffsets { size_t scale,offset,data; };

      QuantizeOutputKernel(const gpu::Stream &stream,
                           const gpu::Module &module,
                           const Buffers &buffers,
                           const Parameters &param);

      MultiDimArray<float, 3> getQuantizeScales(
        MultiDimArrayHostBuffer<fcomplex, 4>& src)
      {
        size_t ptr = (size_t) src.data() + bufferOffsets().scale;
        boost::multi_array_types::extent_gen extent_gen;
        auto& p = parameters;
        auto extents(extent_gen[p.nrTABs][p.nrStokes][p.nrChannels]);
        return MultiDimArray<float, 3>(extents, (float *) ptr, false);
      }

      MultiDimArray<float, 3> getQuantizeOffsets(
        MultiDimArrayHostBuffer<fcomplex, 4>& src)
      {
        size_t ptr = (size_t) src.data() + bufferOffsets().offset;
        boost::multi_array_types::extent_gen extent_gen;
        auto& p = parameters;
        auto extents(extent_gen[p.nrTABs][p.nrStokes][p.nrChannels]);
        return MultiDimArray<float, 3>(extents, (float *) ptr, false);
      }

      template<typename INT>
      MultiDimArray<INT, 4> getQuantizedData(
        MultiDimArrayHostBuffer<fcomplex, 4>& src)
      {
        size_t ptr = (size_t) src.data() + bufferOffsets().data;
        boost::multi_array_types::extent_gen extent_gen;
        auto& p = parameters;
        auto extents(extent_gen[p.nrTABs][p.nrStokes][p.nrSamplesPerChannel][p.nrChannels]);
        MultiDimArray<INT, 4> qdata(extents, (INT *) ptr, false);
        return qdata;
      }

      // Reconstuct floating point data from quantized (integer) data
      void convertI2F(
        MultiDimArrayHostBuffer<float, 4>& dst,
        MultiDimArrayHostBuffer<fcomplex, 4>& src);

    struct  GPUBufferOffsets bufferOffsets() const;

    struct Parameters parameters;

    };
    //# --------  Template specializations for KernelFactory  -------- #//
    template<> CompileDefinitions
    KernelFactory<QuantizeOutputKernel>::compileDefinitions() const;
  }
}

#endif

