//# DelayAndBandPassKernel.cc
//# Copyright (C) 2012-2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

#include <lofar_config.h>

#include "DelayAndBandPassKernel.h"

#include <GPUProc/gpu_utils.h>
#include <GPUProc/BandPass.h>
#include <CoInterface/BlockID.h>
#include <CoInterface/Config.h>
#include <Common/complex.h>
#include <Common/LofarLogger.h>

#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

#include <fstream>

using boost::lexical_cast;
using boost::format;

namespace LOFAR
{
  namespace Cobalt
  {
    string DelayAndBandPassKernel::theirSourceFile = "DelayAndBandPass.cu";
    string DelayAndBandPassKernel::theirFunction = "applyDelaysAndCorrectBandPass";

    DelayAndBandPassKernel::Parameters::Parameters(
      unsigned nrStations_,
      std::vector<unsigned> delayIndices_,
      unsigned nrDelays_,
      unsigned nrBitsPerSample_,
      unsigned nrChannels_,
      unsigned nrSamplesPerChannel_,
      unsigned clockMHz_,
      double subbandBandwidth_,
      bool correlator_,
      bool zeroing_,
      bool delayCompensation_,
      bool correctBandPass_,
      double bandPassScale_,
      bool transpose_,
      bool dopplerCorrection_,
      bool dumpBuffers_,
      std::string dumpFilePattern_) :
      Kernel::Parameters(correlator_ ? "delayAndBandPass" : "delayCompensation"),
      nrStations(nrStations_),
      delayIndices(nrStations_),
      nrDelays(nrDelays_),
      nrBitsPerSample(nrBitsPerSample_),
      nrChannels(nrChannels_),
      nrSamplesPerChannel(nrSamplesPerChannel_),
      clockMHz(clockMHz_),
      subbandBandwidth(subbandBandwidth_),
      zeroing(zeroing_),
      delayCompensation(delayCompensation_),
      correctBandPass(correctBandPass_),
      bandPassScale(bandPassScale_),
      transpose(transpose_),
      dopplerCorrection(dopplerCorrection_)
    {
      if (correlator_) {
        // Use identity mappnig for station indices
        for (unsigned i = 0; i < nrStations; i++)
          delayIndices[i] = i;
      } else {
        delayIndices = delayIndices_;
      }

      dumpBuffers = dumpBuffers_;
      dumpFilePattern = dumpFilePattern_;
    }


    unsigned DelayAndBandPassKernel::Parameters::nrSamplesPerSubband() const {
      return nrChannels * nrSamplesPerChannel;
    }


    unsigned DelayAndBandPassKernel::Parameters::nrBytesPerComplexSample() const {
      return sizeof(std::complex<float>);
    }


    size_t DelayAndBandPassKernel::Parameters::bufferSize(BufferType bufferType) const {
      switch (bufferType) {
      case DelayAndBandPassKernel::INPUT_DATA: 
        return 
          (size_t) nrStations * NR_POLARIZATIONS * 
            nrSamplesPerSubband() * nrBytesPerComplexSample();
      case DelayAndBandPassKernel::OUTPUT_DATA:
        return
          (size_t) nrStations * NR_POLARIZATIONS * 
            nrSamplesPerSubband() * sizeof(std::complex<float>);
      case DelayAndBandPassKernel::DELAY_INDICES:
        return
          delayIndices.size() * sizeof delayIndices[0];
      case DelayAndBandPassKernel::DELAYS:
        return 
          (size_t) nrDelays *  
            NR_POLARIZATIONS * sizeof(double);
      case DelayAndBandPassKernel::PHASE_ZEROS:
        return
          (size_t) nrDelays * NR_POLARIZATIONS * sizeof(double);
      case DelayAndBandPassKernel::BAND_PASS_CORRECTION_WEIGHTS:
        return
          correctBandPass ? (size_t) nrChannels * sizeof(float) : 1UL;
      case DelayAndBandPassKernel::MASK:
        return (size_t) nrStations * nrSamplesPerChannel;
      default:
        THROW(GPUProcException, "Invalid bufferType (" << bufferType << ")");
      }
    }


    DelayAndBandPassKernel::DelayAndBandPassKernel(const gpu::Stream& stream,
                                       const gpu::Module& module,
                                       const Buffers& buffers,
                                       const Parameters& params) :
      CompiledKernel(stream, gpu::Function(module, theirFunction), buffers, params),
      delayIndices(stream.getContext(), params.bufferSize(DELAY_INDICES)),
      delaysAtBegin(stream.getContext(), params.bufferSize(DELAYS)),
      delaysAfterEnd(stream.getContext(), params.bufferSize(DELAYS)),
      phase0s(stream.getContext(), params.bufferSize(PHASE_ZEROS)),
      zeroing(params.zeroing),
      nrStations(params.nrStations),
      nrSamplesPerChannel(params.nrSamplesPerChannel),
      bandPassCorrectionWeights(stream.getContext(), params.bufferSize(BAND_PASS_CORRECTION_WEIGHTS)),
      gpuMask(stream.getContext(), params.bufferSize(MASK)),
      hostMask(stream.getContext(), params.bufferSize(MASK))
    {
      LOG_DEBUG_STR("DelayAndBandPassKernel:" <<
                    " delayCompensation=" <<
                    (params.delayCompensation ? "true" : "false") <<
                    " #channels/sb=" << params.nrChannels <<
                    " correctBandPass=" << 
                    (params.correctBandPass ? "true" : "false") <<
                    " transpose=" << (params.transpose ? "true" : "false"));

      ASSERT(params.nrChannels % 16 == 0 || params.nrChannels == 1);
      ASSERT(params.nrSamplesPerChannel % 16 == 0);

      setArg(0, buffers.output);
      setArg(1, buffers.input);
      setArg(2, delayIndices);
      setArg(4, delaysAtBegin);
      setArg(5, delaysAfterEnd);
      setArg(6, phase0s);
      setArg(7, bandPassCorrectionWeights);

      if (params.transpose) {
        const int block_x = 256;
        const int grid_y = params.nrChannels == 1 ?  1 : params.nrChannels / 16;
        setEnqueueWorkSizes( gpu::Grid(1,
                                       grid_y,
                                       params.nrStations),
                             gpu::Block(block_x, 1, 1) );
      } else {
        const int block_x = 16;
        const int block_y = params.nrChannels == 1 ? 1 : 16;
        setEnqueueWorkSizes( gpu::Grid(params.nrSamplesPerChannel / block_x,
                                       params.nrChannels / block_y,
                                       params.nrStations),
                             gpu::Block(block_x, block_y) );
      }
      
      size_t nrSamples = (size_t)params.nrStations * params.nrChannels * params.nrSamplesPerChannel * NR_POLARIZATIONS;
      nrOperations = nrSamples * 12;
      nrBytesRead = nrBytesWritten = nrSamples * params.nrBytesPerComplexSample();

      // Initialise bandpass correction weights
      if (params.correctBandPass)
      {
        gpu::HostMemory bpWeights(stream.getContext(), bandPassCorrectionWeights.size());
        BandPass::computeCorrectionFactors(bpWeights.get<float>(),
                                           params.nrChannels, params.bandPassScale);
        stream.writeBuffer(bandPassCorrectionWeights, bpWeights, true);
      }

      // upload delayIndices, as they are static across the observation
      gpu::HostMemory delayIndicesHost(stream.getContext(), params.bufferSize(DELAY_INDICES));
      std::memcpy(delayIndicesHost.get<void>(), &params.delayIndices.front(),
                  delayIndicesHost.size());
      stream.writeBuffer(delayIndices, delayIndicesHost, true);
    }


    void DelayAndBandPassKernel::writeChannelFlags(const MultiDimArray<SparseSet<unsigned>, 1> &channelFlags)
    {
      if (!zeroing) {
        throw std::runtime_error("Called DelayAndBandPassKernel::writeChannelFlags with zeroing disabled.");
      }

      // marshall flags to GPU host buffer
      computeMaskTimer.start();
      for(unsigned station = 0; station < nrStations; ++station) {
        //LOG_DEBUG_STR("Flags for block " << blockId << ", station " << station << ": " << channelFlags[station]);
        channelFlags[station].toByteset(hostMask.get<char>() + station * nrSamplesPerChannel, nrSamplesPerChannel);
      }
      computeMaskTimer.stop();

      // Copy host buffer to GPU
      itsStream.writeBuffer(gpuMask, hostMask, false);

      setArg(8, gpuMask);
    }

    void DelayAndBandPassKernel::enqueue(const BlockID &blockId,
                                         double subbandFrequency)
    {
      setArg(3, subbandFrequency);
      Kernel::enqueue(blockId);
    }

    //--------  Template specializations for KernelFactory  --------//

    template<> CompileDefinitions
    KernelFactory<DelayAndBandPassKernel>::compileDefinitions() const
    {
      CompileDefinitions defs =
        KernelFactoryBase::compileDefinitions(itsParameters);

      defs["NR_STATIONS"] = lexical_cast<string>(itsParameters.nrStations);
      defs["NR_DELAYS"] = lexical_cast<string>(itsParameters.nrDelays);
      defs["NR_BITS_PER_SAMPLE"] =
        lexical_cast<string>(itsParameters.nrBitsPerSample);

      defs["NR_CHANNELS"] = lexical_cast<string>(itsParameters.nrChannels);
      defs["NR_SAMPLES_PER_CHANNEL"] = 
        lexical_cast<string>(itsParameters.nrSamplesPerChannel);
      defs["NR_SAMPLES_PER_SUBBAND"] = 
        lexical_cast<string>(itsParameters.nrSamplesPerSubband());
      defs["SUBBAND_BANDWIDTH"] =
        str(format("%.7f") % itsParameters.subbandBandwidth);

      if (itsParameters.delayCompensation)
        defs["DELAY_COMPENSATION"] = "1";

      if (itsParameters.correctBandPass)
        defs["BANDPASS_CORRECTION"] = "1";

      if (itsParameters.transpose)
        defs["DO_TRANSPOSE"] = "1";

      if (itsParameters.dopplerCorrection) {
        defs["DOPPLER_CORRECTION"] = "1";
        defs["CLOCK_MHZ"] = lexical_cast<string>(itsParameters.clockMHz);
      }

      if (itsParameters.zeroing) {
        defs["DO_ZEROING"] = "1";
      }

      return defs;
    }
  }
}
