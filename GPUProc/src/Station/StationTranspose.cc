
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#include <lofar_config.h>

#include "StationTranspose.h"

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <map>
#include <vector>
#include <string>
#include <boost/format.hpp>

#include <mpi.h>
#include <InputProc/Transpose/MPISendStation.h>
#include <InputProc/Transpose/MapUtil.h>
#include <InputProc/Transpose/MPIUtil.h>

#include <Common/LofarLogger.h>
#include <Common/Timer.h>
#include <CoInterface/OMPThread.h>
#include <CoInterface/BudgetTimer.h>
#include <InputProc/SampleType.h>

using namespace LOFAR;
using namespace LOFAR::Cobalt;
using namespace std;
using boost::format;

namespace LOFAR {
    namespace Cobalt {
        template<typename SampleT>
        MPIData<SampleT>::MPIData(TimeStamp obsStartTime, size_t nrSubbands, size_t nrSamples):
                mpi_samples(boost::extents[nrSubbands][nrSamples], 1, mpiAllocator),
                mpi_metaData(boost::extents[nrSubbands], 1, mpiAllocator),
                metaData(nrSubbands),
                read_offsets(nrSubbands, 0),
                obsStartTime(obsStartTime),
                nrSamples(nrSamples),
                nrSubbands(nrSubbands) {
            reset(0);
            //memset(mpi_samples.origin(), 0, mpi_samples.num_elements() * sizeof *mpi_samples.origin());
        }


        template<typename SampleT>
        void MPIData<SampleT>::reset(ssize_t block) {
            this->block = block;

            // update time span for this block
            from = obsStartTime + block * nrSamples;
            to = from + nrSamples;

            // clear flags
            for (size_t sb = 0; sb < nrSubbands; ++sb) {
                metaData[sb].flags.reset();
            }
        }

        template<typename SampleT>
        bool MPIData<SampleT>::write(const std::shared_ptr<StationPacket> &packet, const ssize_t *beamletIndices,
                                     size_t nrBeamletIndices) {
            /* Prevent accesses beyond beamletIndices */
            ASSERTSTR(packet->beamletCount() <= nrBeamletIndices,
                      "packet has " << packet->beamletCount() << " beamlets, expected at most " << nrBeamletIndices
                                    << ".");

            const size_t nrSamples = packet->blockCount();

            const uint64_t packetBegin = packet->packetBegin();
            const uint64_t packetEnd = packetBegin + nrSamples;

            bool considerNext = false;

            const SampleT *srcPtr = reinterpret_cast<const SampleT *>(&packet->payload().data[0]);

            for (size_t b = 0; b < packet->beamletCount(); ++b) {
                const ssize_t absBeamlet = beamletIndices[b];

                /* Discard beamlets that are not used in the observation */
                if (absBeamlet == -1) {
                    continue;
                }

                /* Reading with offset X is the same as writing that data with offset -X */
                const ssize_t offset = read_offsets[absBeamlet];
                const uint64_t beamletBegin = packetBegin - offset;
                const uint64_t beamletEnd = packetEnd - offset;

                /* XXXX    = packet data
                 * [.....] = this->data
                 */

                /* XXXX [......] -> discard */
                if (beamletEnd < from) {
                    continue;
                }

                /* [......] XXXX -> discard */
                if (beamletBegin >= to) {
                    considerNext = true;
                    continue;
                }

                // number of samples to transfer
                size_t nrSamplesToCopy = nrSamples;

                // first sample to write
                size_t firstSample = 0;

                /* XX [XX....] -> cut off start */
                if (beamletBegin < from) {
                    firstSample = from - beamletBegin;
                    nrSamplesToCopy -= firstSample;
                }

                /* [...XX] XX -> cut off end */
                if (beamletEnd > to) {
                    nrSamplesToCopy -= beamletEnd - to;
                    considerNext = true;
                }

                if (beamletEnd == to) {
                    // The next packet has at most 1 sample overlap with this packet,
                    // due to the speed of light and the earth's rotation speed.
                    considerNext = true;
                }

                /* Absolute offset within our block */
                const size_t absSample = beamletBegin - from + firstSample;

                /* Write the remaining data */
                // LOFAR packets are [beamlet][block]
                memcpy(&mpi_samples[absBeamlet][absSample],
                       srcPtr + b * nrSamples + firstSample,
                       nrSamplesToCopy * sizeof(SampleT));

                /* Update the flags */
                metaData[absBeamlet].flags.include(absSample, absSample + nrSamplesToCopy);
            }

            return considerNext;
        }

        template<typename SampleT>
        void MPIData<SampleT>::serialiseMetaData() {
            // Convert the metaData -> mpi_metaData for transfer over MPI
            for (size_t sb = 0; sb < nrSubbands; ++sb) {
                SubbandMetaData md = metaData[sb];

                // MPIData::write adds flags for what IS present, but the receiver
                // needs flags for what IS NOT present. So invert the flags here.
                md.flags = md.flags.invert(0, nrSamples);

                // Write the meta data into the fixed buffer.
                mpi_metaData[sb] = md;
                mpi_metaData[sb].EOS = false;
            }
        }

        template<typename SampleT>
        void MPIData<SampleT>::setEOS() {
            // Make sure all data is flagged
            reset(block);
            serialiseMetaData();

            // Set EOS bit.
            for (size_t sb = 0; sb < mpi_metaData.size(); ++sb)
                mpi_metaData[sb].EOS = true;
        }

        template
        struct MPIData<SampleType<i16complex> >;
        template
        struct MPIData<SampleType<i8complex> >;
        template
        struct MPIData<SampleType<i4complex> >;

        MPISender::MPISender(const std::string &logPrefix, size_t stationIdx,
                             const SubbandDistribution &subbandDistribution, float blockDuration)
                :
                logPrefix(logPrefix),
                stationIdx(stationIdx),
                subbandDistribution(subbandDistribution),
                targetRanks(keys(subbandDistribution)),
                subbandOffsets(targetRanks.size(), 0),
                nrSubbands(values(subbandDistribution).size()),
                blockDuration(blockDuration) {
            // Determine the offset of the set of subbands for each rank within
            // the members in MPIData<SampleT>.
            for (size_t rank = 0; rank < targetRanks.size(); ++rank)
                for (size_t i = 0; i < rank; ++i)
                    subbandOffsets[rank] += subbandDistribution.at(i).size();
        }


        template<typename SampleT>
        void MPISender::sendBlock(MPIData<SampleT> &mpiData) {
            std::vector<MPI_Request> requests;

            {
                ScopedLock sl(MPIMutex);

                for (size_t i = 0; i < targetRanks.size(); ++i) {
                    const int rank = targetRanks.at(i);

                    if (subbandDistribution.at(rank).empty())
                        continue;

                    // TODO: Pull this out of loop into a class member, as this object is constant per rank
                    MPISendStation sender(stationIdx, rank, subbandDistribution.at(rank), mpiData.nrSamples);

                    const size_t offset = subbandOffsets[rank];

                    requests.push_back(sender.sendData<SampleT>(&mpiData.mpi_samples[offset][0]));
                    requests.push_back(sender.sendMetaData(&mpiData.mpi_metaData[offset]));
                }
            }

            RequestSet rs(requests, true, str(format("station %d block %d") % stationIdx % mpiData.block));
            rs.waitAll();
        }


        template<typename SampleT>
        void MPISender::sendBlocks(Queue<std::shared_ptr<MPIData<SampleT>>> &inputQueue,
                                   Queue<std::shared_ptr<MPIData<SampleT>>> &outputQueue,
                                   bool skipDataTransfers) {
            std::shared_ptr<MPIData<SampleT>> mpiData;

            NSTimer mpiSendTimer(str(format("%s MPI send data") % logPrefix), true, true);
            BudgetTimer copyTimer(
                    str(format("%s MPI send data") % logPrefix),
                    blockDuration,
                    true, true);

            size_t nrProcessedSamples = 0;
            size_t nrFlaggedSamples = 0;

            while ((mpiData = inputQueue.remove()) != NULL) {
                const ssize_t block = mpiData->block;
                const size_t nrSamples = mpiData->nrSamples;

                (void) block; //prevent unused var warning in release mode
                LOG_DEBUG_STR(logPrefix << str(format("[block %d] Finalising metaData") % block));

                mpiData->serialiseMetaData();

                // Update statistics
                nrProcessedSamples += nrSamples * nrSubbands;

                for (size_t sb = 0; sb < mpiData->metaData.size(); ++sb) {
                    nrFlaggedSamples += nrSamples - mpiData->metaData[sb].flags.count();
                }

                // Actual block sending can be skipped for RSP raw output only observations or some tests.
                if (!skipDataTransfers) {
                    LOG_DEBUG_STR(logPrefix << str(format("[block %d] Sending data") % block));

                    mpiSendTimer.start();
                    sendBlock(*mpiData);
                    mpiSendTimer.stop();

                    LOG_DEBUG_STR(logPrefix << str(format("[block %d] Data sent") % block));
                }

                outputQueue.append(mpiData);
                mpiData.reset();
                ASSERT(!mpiData);
            }

            // Repop from outputQueue to signal EOS
            mpiData = outputQueue.remove();
            ASSERT(mpiData.get());

            mpiData->block++; // lets be nice and retain an increasing block number
            mpiData->setEOS();

            LOG_DEBUG_STR(logPrefix << "Sending EOS");
            sendBlock(*mpiData);
            LOG_DEBUG_STR(logPrefix << "EOS sent");

            // report average loss
            const double avgloss = nrProcessedSamples == 0 ? 0.0 : 100.0 * nrFlaggedSamples / nrProcessedSamples;

            LOG_INFO_STR(logPrefix << str(format("Average data loss/flagged: %.4f%%") % avgloss));
        }

        template void MPISender::sendBlocks(Queue<std::shared_ptr<MPIData<SampleType<i16complex> > > > &inputQueue,
                                            Queue<std::shared_ptr<MPIData<SampleType<i16complex> > > > &outputQueue,
                                            bool skipDataTransfers);

        template void MPISender::sendBlocks(Queue<std::shared_ptr<MPIData<SampleType<i8complex> > > > &inputQueue,
                                            Queue<std::shared_ptr<MPIData<SampleType<i8complex> > > > &outputQueue,
                                            bool skipDataTransfers);

        template void MPISender::sendBlocks(Queue<std::shared_ptr<MPIData<SampleType<i4complex> > > > &inputQueue,
                                            Queue<std::shared_ptr<MPIData<SampleType<i4complex> > > > &outputQueue,
                                            bool skipDataTransfers);
    }
}

