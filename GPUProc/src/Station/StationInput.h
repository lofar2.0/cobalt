
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#ifndef LOFAR_GPUPROC_STATIONINPUT_H
#define LOFAR_GPUPROC_STATIONINPUT_H

#include <cstring>
#include <map>
#include <vector>

#include <CoInterface/BestEffortQueue.h>
#include <CoInterface/Parset.h>
#include <CoInterface/Pool.h>
#include <CoInterface/Queue.h>
#include <CoInterface/RSPTimeStamp.h>
#include <CoInterface/SubbandMetaData.h>
#include <Common/Thread/Trigger.h>
#include <InputProc/Buffer/BoardMode.h>
#include <InputProc/Buffer/StationID.h>
#include <Stream/Stream.h>

#include "StationTranspose.h"
#include "RSPRawSender.h"

namespace LOFAR {
    namespace Cobalt {
        /*
         * Generates MPIData<Sample> blocks, and computes its meta data (delays, etc).
         */
        template<typename SampleT>
        class StationMetaData {
        public:
            StationMetaData(const Parset &ps, size_t stationIdx, const SubbandDistribution &subbandDistribution);

            void computeMetaData(const Trigger *stopSwitch = nullptr);

        private:
            const Parset &ps;
            const size_t stationIdx;
            const struct StationID stationID;
            const std::string logPrefix;

            const TimeStamp startTime;
            const TimeStamp stopTime;

            const size_t nrSamples;
        public:
            const size_t nrBlocks;

            Pool<MPIData<SampleT> > metaDataPool;
        private:

            const std::vector<size_t> subbands;
        };


        class StationInput {
        public:
            StationInput(const Parset &ps, size_t stationIdx,
                         const SubbandDistribution &subbandDistribution,
                         unsigned hostID = 0);

            template<typename SampleT>
            void processInput(Queue<std::shared_ptr<MPIData<SampleT>>> &inputQueue,
                              Queue<std::shared_ptr<MPIData<SampleT>>> &outputQueue);

        private:
            // Each packet is expected to have 16 samples per subband, i.e. ~80 us worth of data @ 200 MHz.
            // So 512 packets is ~40 ms of data.
            static const unsigned RT_PACKET_BATCH_SIZE = 512;  // RSP raw sendmmsg(2) needs this to be <= UIO_MAXIOV (1024)

            static const unsigned NONRT_PACKET_BATCH_SIZE = 1;

            const Parset &ps;

            const size_t stationIdx;
            const struct StationID stationID;

            const std::string logPrefix;

            const BoardMode mode;
            const unsigned nrBoards;


            std::vector<std::unique_ptr<Pool<StationPackets> > > rspDataPool; // [nrBoards]
            std::vector<RSPRawSender> rspRawSenders; // [nrBoards] if RSP raw enabled and the antenna field is selected, else []

            // Whether we emitted certain errors (to prevent log spam)
            TimeStamp loggedSeenFutureData;
            TimeStamp loggedNonRealTime;

            const std::vector<size_t> targetSubbands;

            // Mapping of
            // [board][slot] -> offset of this beamlet in the MPIData struct
            //                  or: -1 = discard this beamlet (not used in obs)
            const MultiDimArray<ssize_t, 2> beamletIndices;

            MultiDimArray<ssize_t, 2> generateBeamletIndices();

            std::unique_ptr<Stream> inputStream(unsigned board) const;

            void initRspRaw(unsigned hostID);

            /*
             * Reads data from all the station input streams, and puts their packets in rspDataPool.
             *
             * Real-time mode:
             *   - Packets are collected in batches per board
             *   - Batches are interleaved as they arrive
             *
             * Non-real-time mode:
             *   - Packets are interleaved between the streams,
             *     staying as close to in-order as possible.
             *   - An "+inf" TimeStamp is added at the end to signal
             *     end-of-data
             *
             * Reads:  rspDataPool.free
             * Writes: rspDataPool.filled
             *
             * Read data from one board in real-time mode.
             */
            void readRSPRealTime(unsigned board);

            /*
             * Read data from all boards in non-real-time mode.
             */
            void readRSPNonRealTime();

            /*
             * Fills 'current' with RSP data. Potentially spills into 'next'.
             */
            template<typename SampleT>
            void writeRSPRealTime(std::shared_ptr<MPIData<SampleT>> &current, std::shared_ptr<MPIData<SampleT>> &next);

            template<typename SampleT>
            void
            writeRSPNonRealTime(std::shared_ptr<MPIData<SampleT>> &current, std::shared_ptr<MPIData<SampleT>> &next);
        };


        void sendInputToPipeline(const Parset &ps, size_t stationIdx,
                                 const SubbandDistribution &subbandDistribution,
                                 unsigned hostID, // mpi rank for this node
                                 Trigger *stopSwitch = NULL);
    }
}

#endif

