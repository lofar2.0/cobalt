
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#ifndef LOFAR_GPUPROC_RSPRAWSENDER_H
#define LOFAR_GPUPROC_RSPRAWSENDER_H

// \file
// Sender for RSP raw output. Corresponding receiver: OutputProc/src/InputThread.h
// Sender can also be used for COBALT RSP data piggy-backing.

#include <CoInterface/StationPacket.h>
#include <Stream/SocketStream.h>
#include <ctime> // time_t
#include <string>

namespace LOFAR
{
  namespace Cobalt
  {

class RSPRawSender {
public:
  RSPRawSender();

  // deadline is an absolute timestamp or 0 for no connection timeout (blocking).
  RSPRawSender(unsigned maxNrPacketsPerTrySend, unsigned maxNrBeamletsToSend,
               const std::string& streamDesc, time_t deadline = 0);

  ~RSPRawSender();

  bool initialized() const;

  void swap(RSPRawSender& other) /*noexcept*/;

  // Ensure nrPackets <= maxNrPacketsPerSend (passed upon object construction).
  // If deadline (passed upon object construction) was non-zero, trySend() may drop packets to avoid blocking.
  void trySend(StationPackets &packets, unsigned nrPackets);

  unsigned getNrDroppedPackets() const;

private:
  void trySendUdp(SocketStream *sockStream, StationPackets &packets,
                  unsigned packetSize, unsigned nrPackets);
  void trySendByteStream(StationPackets &packets, unsigned packetSize, unsigned nrPackets);
  void trySendPending();

  std::shared_ptr<Stream> itsStream;
  std::vector<unsigned> itsSentMsgSizes;
  unsigned itsMaxNrBeamletsToSend;
  unsigned itsNrDroppedPackets;
  std::vector<unsigned char> itsPendingData; // w/ non-UDP to retry sending the remainder of a partially sent RSP packet
};

  } // namespace Cobalt
} // namespace LOFAR

#endif

