
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#include <lofar_config.h>

#include "StationInput.h"

#include <algorithm>
#include <boost/format.hpp>
#include <cstring>
#include <ctime>
#include <iostream>
#include <memory>
#include <string>
#include <sys/types.h>
#include <vector>

#include <CoInterface/OMPThread.h>
#include <CoInterface/Parset.h>
#include <CoInterface/PrintVector.h>
#include <CoInterface/RspStationPacket.h>
#include <CoInterface/SdpStationPacket.h>
#include <CoInterface/Stream.h>
#include <Common/LofarLogger.h>
#include <Stream/FileStream.h>
#include <Stream/StreamFactory.h>

#include <InputProc/Buffer/BoardMode.h>
#include <InputProc/Delays/Delays.h>
#include <InputProc/SampleType.h>
#include <InputProc/Station/PacketFactory.h>
#include <InputProc/Station/PacketReader.h>
#include <InputProc/Station/PacketStream.h>
#include <InputProc/Transpose/MapUtil.h>

using namespace LOFAR;
using namespace LOFAR::Cobalt;
using namespace std;
using boost::format;

namespace LOFAR {
    namespace Cobalt {

        template<typename SampleT>
        StationMetaData<SampleT>::StationMetaData(const Parset &ps, size_t stationIdx,
                                                  const SubbandDistribution &subbandDistribution)
                :
                ps(ps),
                stationIdx(stationIdx),
                stationID(StationID::parseFullFieldName(ps.settings.antennaFields.at(stationIdx).name)),
                logPrefix(stationID.name() + " "),

                startTime(ps.settings.startTime * ps.settings.subbandWidth(), ps.settings.clockHz()),
                stopTime(ps.settings.stopTime * ps.settings.subbandWidth(), ps.settings.clockHz()),

                nrSamples(ps.settings.blockSize),
                nrBlocks(ps.settings.nrBlocks()),

                // Do not warn on bad performance. StationMetaData uses the free queue as its INPUT, so it tends to
                // consume all elements in the free queue. This is contrary to the other pools (they wait for input
                // on a filled queue), resulting in the wrong analysis.
                metaDataPool(str(format("StationMetaData::metaDataPool [station %s]") % stationID.name()), false),

                subbands(values(subbandDistribution)) {
        }

        /*
         * Initialises blocks from metaDataPool, and adds meta data.
         *
         * Input: metaDataPool.free
         * Output: metaDataPool.filled
         */
        template<typename SampleT>
        void StationMetaData<SampleT>::computeMetaData(const Trigger *stopSwitch) {
            /*
             * Allocate buffer elements.
             */

            // Each element represents 1 block of buffer.
            for (size_t i = 0; i < 10; ++i) {
                metaDataPool.free.append(
                        make_shared<MPIData<SampleT>>(startTime, ps.settings.subbands.size(), nrSamples), false);
            }

            // Set up delay compensation.
            Delays delays(ps, stationIdx, startTime - nrSamples, nrSamples);

            // We keep track of the delays at the beginning and end of each block.
            // After each block, we'll swap the afterEnd delays into atBegin.
            Delays::AllDelays delaySet1(ps), delaySet2(ps);
            Delays::AllDelays *delaysAtBegin = &delaySet1;
            Delays::AllDelays *delaysAfterEnd = &delaySet2;

            /*
             * Generate all the blocks.
             * NOTE: We start at block -1 to initalise the FIR's HistorySamples!
             */
            for (ssize_t block = -1; block < (ssize_t) nrBlocks; ++block) {
                if (stopSwitch && stopSwitch->test()) {
                    LOG_WARN_STR(logPrefix << "Requested to stop");
                    break;
                }

                LOG_DEBUG_STR(logPrefix << str(format("[block %d] Retrieving delays") % block));

                // Fetch start and end delays
                delays.getNextDelays(*delaysAtBegin, *delaysAfterEnd, block);

                // INPUT
                std::shared_ptr<MPIData<SampleT>> mpiData = metaDataPool.free.remove();

                // Annotate
                mpiData->reset(block);

                LOG_DEBUG_STR(logPrefix << str(format("[block %d] Applying delays") % block));

                // Compute the next set of metaData and read_offsets from the new
                // delays pair.
                delays.generateMetaData(*delaysAtBegin, *delaysAfterEnd, subbands, mpiData->metaData,
                                        mpiData->read_offsets);

                // OUTPUT
                metaDataPool.filled.append(mpiData);
                mpiData.reset();
                ASSERT(!mpiData);
            }

            // Signal EOD
            metaDataPool.filled.append(NULL, false);
        }


        StationInput::StationInput(const Parset &ps, size_t stationIdx,
                                   const SubbandDistribution &subbandDistribution,
                                   unsigned hostID)
                :
                ps(ps),
                stationIdx(stationIdx),
                stationID(StationID::parseFullFieldName(ps.settings.antennaFields.at(stationIdx).name)),

                logPrefix(stationID.name() + " "),

                mode(ps.settings.nrBitsPerSample, ps.settings.clockMHz, ps.settings.antennaFields.at(stationIdx).nrBeamletsPerBoard),
                nrBoards(ps.settings.antennaFields.at(stationIdx).inputStreams.size()),

                loggedSeenFutureData(0),
                loggedNonRealTime(0),

                targetSubbands(values(subbandDistribution)),
                beamletIndices(generateBeamletIndices()) {
            ASSERTSTR(nrBoards > 0, logPrefix << "No input streams");

            // Log all input descriptions
            LOG_INFO_STR(logPrefix << "Input streams: " << ps.settings.antennaFields.at(stationIdx).inputStreams);

            rspDataPool.reserve(nrBoards);
            for (unsigned i = 0; i < nrBoards; ++i) {
                std::unique_ptr<Pool<StationPackets>> pool(
                        new Pool<StationPackets>(
                                str(format("StationInput::rspDataPool[%u] [station %s]") % i % stationID.name()),
                                ps.settings.realTime));
                rspDataPool.push_back(std::move(pool));
            }

            if (ps.settings.rspRaw.enabled &&
                std::find(ps.settings.rspRaw.antennaFieldNames.begin(), ps.settings.rspRaw.antennaFieldNames.end(),
                          stationID.name()) != ps.settings.rspRaw.antennaFieldNames.end()) {
                initRspRaw(hostID);
            }
        }


        MultiDimArray<ssize_t, 2> StationInput::generateBeamletIndices() {
            /*
             * We need to create a mapping from
             *
             * [board][slot]          = the dimensions of the RSP data
             *
             * to
             *
             * [subband]              = the dimensions of the data sent over MPI,
             *                          which is ordered by `targetSubbands'.
             */

            MultiDimArray<ssize_t, 2> result(boost::extents[nrBoards][mode.nrBeamletsPerBoard]);

            // Any untouched [board][slot] means we'll discard that input
            for (size_t n = 0; n < result.num_elements(); ++n) {
                result.origin()[n] = -1;
            }

            for (size_t i = 0; i < targetSubbands.size(); ++i) {
                // The subband stored at position i
                const size_t sb = targetSubbands.at(i);

                // The corresponding (board,slot) combination for that subband,
                // for this station.
                const unsigned board = ps.settings.antennaFields[stationIdx].rspBoardMap[sb];
                const unsigned slot = ps.settings.antennaFields[stationIdx].rspSlotMap[sb];

                ASSERTSTR(board < nrBoards,
                          logPrefix << "RSP Board Map refers to board " << board << " but we have only " << nrBoards
                                    << " inputs");
                ASSERTSTR(slot < mode.nrBeamletsPerBoard,
                          logPrefix << "RSP Slot Map refers to slot " << slot << " but each board only has "
                                    << mode.nrBeamletsPerBoard);

                // The specified (board,slot) is stored at position i
                ASSERTSTR(result[board][slot] == -1,
                          "Station " << stationID.name() << ": board " << board << " slot " << slot
                                     << " is used multiple times!");
                result[board][slot] = i;
            }

            return result;
        }


        std::unique_ptr<Stream> StationInput::inputStream(unsigned board) const {
            std::unique_ptr<Stream> stream;

            // Connect to specified input stream
            const string &desc = ps.settings.antennaFields.at(stationIdx).inputStreams.at(board);

            LOG_DEBUG_STR(logPrefix << "Connecting input stream for board " << board << ": " << desc);

            // Sanity checks
            if (ps.settings.realTime) {
                ASSERTSTR(desc.find("udp") == 0,
                          logPrefix << "Real-time observations should read input from UDP, not " << desc);
            } else {
                ASSERTSTR(desc.find("udp") != 0,
                          logPrefix << "Non-real-time observations should NOT read input from UDP, got " << desc);
            }

            if (desc == "factory:" || desc == "factory+sdp:") {
                const TimeStamp from(ps.settings.startTime * ps.settings.subbandWidth(), ps.settings.clockHz());
                const TimeStamp to(ps.settings.stopTime * ps.settings.subbandWidth(), ps.settings.clockHz());

                const struct BoardMode boardMode(
                    ps.settings.nrBitsPerSample,
                    ps.settings.clockMHz,
                    desc.find("+sdp:") != string::npos ? 488 : 0); // LOFAR2 stations have 488 beamlets/packet.
                PacketFactory factory(boardMode);

                if (desc == "factory:") {
                    stream.reset(new PacketStream<RspStationPacket>(factory, from, to, board));
                } else {
                    stream.reset(new PacketStream<SdpStationPacket>(factory, from, to, board));
                }
            } else {
                if (ps.settings.realTime) {
                    try {
                        stream.reset(
                                createStream(desc, true, 0, str(format("from %s-RSP%u") % stationID.name() % board)));
                    } catch (Exception &ex) {
                        LOG_ERROR_STR(logPrefix << "Caught exception creating stream (continuing on /dev/null): "
                                                << ex.what());
                        stream.reset(new FileStream(
                                "/dev/null")); /* will read end-of-stream: avoid spamming illegal packets */
                    }
                } else { // non real time: un-tried call, so no rethrow (changes exc backtrace)
                    stream.reset(createStream(desc, true));
                }
            }

            return stream;
        }


        void StationInput::initRspRaw(unsigned hostID) {
            unsigned maxNrPacketsToSend = NONRT_PACKET_BATCH_SIZE;
            time_t connTimeout = 0;
            if (ps.settings.realTime) {
                maxNrPacketsToSend = RT_PACKET_BATCH_SIZE;
                connTimeout = std::time(NULL) + 3; // a few secs from now
            }

            rspRawSenders.resize(nrBoards);
#pragma omp parallel for num_threads(nrBoards)  // easy way to apply a single connTimeout to all
            for (unsigned i = 0; i < nrBoards; ++i) {
                if (ps.settings.rspRaw.nrBeamletsPerBoardList.at(i) == 0) {
                    continue;
                }

                unsigned fileIdx = ps.getRSPRawOutputStreamIdx(stationID.name(), i);
                // TODO: produce UDP stream descriptor for piggy backing instead of TCP for RSP raw storage
                string desc = getStreamDescriptorBetweenIONandStorage(ps, RSP_RAW_DATA, fileIdx,
                                                                      hostID < ps.settings.nodes.size()
                                                                      ? ps.settings.nodes[hostID].out_nic : "");
                LOG_INFO_STR(logPrefix << "Opening RSP raw output stream for data from antenna field " <<
                                       stationID.name() << " board " << i << " with stream descriptor " << desc);

                try {
                    RSPRawSender rspRawSender(maxNrPacketsToSend, ps.settings.rspRaw.nrBeamletsPerBoardList[i],
                                              desc, connTimeout);
                    rspRawSenders[i].swap(rspRawSender); // swap into place (no need for vector of smart ptrs pre-C++11)
                } catch (Exception &exc) { // TimeOutException, SystemCallException
                    LOG_ERROR_STR(logPrefix << "RSPRawSender creation failure for stream " << desc << ": " << exc);
                }
            }
        }


        void StationInput::readRSPRealTime(unsigned board) {
            /*
             * In real-time mode, we can't get ahead of the `current'
             * block of the reader due to clock synchronisation.
             */

            try {
                std::unique_ptr<Stream> stream(inputStream(board));
                PacketReader reader(str(format("%s RSP%s ") % logPrefix % board),
                                    *stream, mode);

                auto &inputQueue = rspDataPool[board]->free;
                auto &outputQueue = rspDataPool[board]->filled;

                for (uint8_t i = 1 /* avoid printing statistics immediately */; true; i++) {
                    // Fill rspDataPool elements with RSP packets
                    std::shared_ptr<StationPackets> rspData = inputQueue.remove();

                    // Abort condition needed to avoid getting stuck in free.remove()
                    if (!rspData) {
                        break;
                    }

                    reader.readPackets(rspData);

                    // Periodically LOG() and log() (for monitoring (PVSS)) progress
                    if (i == 0) { // Each block is ~40ms, so log every ~10s worth of data.
                        reader.logStatistics(board);
                    }

                    outputQueue.append(rspData);
                }
            } catch (EndOfStreamException &ex) {
                // Ran out of data
                LOG_INFO_STR(logPrefix << "End of stream");

            } catch (SystemCallException &ex) {
                if (ex.error == EINTR || ex.error == 512 /* ERESTARTSYS, should not be propagated to user space. */) {
                    LOG_INFO_STR(logPrefix << "Stopped");
                } else {
                    LOG_ERROR_STR(logPrefix << "Caught Exception: " << ex);
                }
            } catch (Exception &ex) {
                LOG_ERROR_STR(logPrefix << "Caught Exception: " << ex);
            }
        }


        template<typename SampleT>
        void StationInput::writeRSPRealTime(std::shared_ptr<MPIData<SampleT>> &current,
                                            std::shared_ptr<MPIData<SampleT>> &next) {
            /* real-time */

            /*
             * 1. We can receive data out-of-order.
             * 2. We can't receive data from beyond `next', because
             *    we sync on the wall clock.
             * 3. We can receive old data from before `current'.
             */

            const TimeStamp deadline = TimeStamp(current->to + mode.secondsToSamples(0.33), mode.clockHz());

            LOG_INFO_STR(logPrefix << "[block " << current->block << "] Waiting until " << deadline);

            const TimeStamp now = TimeStamp::now(mode.clockHz());

            if (deadline < now) {
                // Only emit log lines every minute seconds to prevent spam
                if (loggedNonRealTime + mode.secondsToSamples(60) < now) {
                    // We're too late! Don't process data, or we'll get even further behind!
                    LOG_ERROR_STR(
                            logPrefix << "[block " << current->block << "] Not running at real time! Deadline was " <<
                                      TimeStamp(now - deadline, deadline.getClock()).getSeconds() << " seconds ago");

                    loggedNonRealTime = now;
                }
                return;
            }

            // One core can't handle the load, so use multiple
#pragma omp parallel for num_threads(nrBoards)
            for (unsigned board = 0; board < nrBoards; board++) {
                //NSTimer copyRSPTimer(str(format("%s [board %i] copy RSP -> block") % logPrefix % board), true, true);
                OMPThread::ScopedName sn(
                        str(format("%s wr %u") % ps.settings.antennaFields.at(stationIdx).name % board));

                auto &inputQueue = rspDataPool[board]->filled;
                auto &outputQueue = rspDataPool[board]->free;

                const ssize_t *beamletIndicesPtr = &this->beamletIndices[board][0];
                const size_t nrBeamletIndices = mode.nrBeamletsPerBoard;

                std::shared_ptr<StationPackets> rspData;

                while ((rspData = inputQueue.remove(deadline, NULL)) != nullptr) {
                    // Write valid packets to the current and/or next packet
                    //copyRSPTimer.start();

                    if (ps.settings.correlator.enabled || ps.settings.beamFormer.enabled) {
                        for (unsigned p = 0; p < RT_PACKET_BATCH_SIZE; ++p) {
                            auto &packet = rspData->packets[p];

                            if (packet->payloadError()) {
                                continue;
                            }

                            if (current->write(packet, beamletIndicesPtr, nrBeamletIndices) && next) {
                                // We have data (potentially) spilling into `next'.

                                if (next->write(packet, beamletIndicesPtr, nrBeamletIndices)) {
                                    // Only emit log lines every minute seconds to prevent spam
                                    if (loggedSeenFutureData + mode.secondsToSamples(60) < now) {
                                        LOG_ERROR_STR(logPrefix
                                                              << "Received data for several blocks into the future -- discarding.");
                                        loggedSeenFutureData = now;
                                    }
                                }
                            }
                        }
                    }

                    if (!rspRawSenders.empty() && rspRawSenders[board].initialized()) {
                        // Quick check on timestamp
                        // Don't cater for out-of-order input: the extra logic is not worth the benefit.
                        // We may not always get all RT_PACKET_BATCH_SIZE. PacketReader sets payloadError and
                        // timestamp as invalid in remaining entries. Such timestamps are filtered here too.
                        unsigned nrPackets = RT_PACKET_BATCH_SIZE;
                        for (unsigned p = 0; p < nrPackets; p++) {
                            if (rspData->packets[p]->seqId() >= ps.settings.rspRaw.stopTime ||
                                rspData->packets[p]->seqId() < ps.settings.rspRaw.startTime) {
                                nrPackets = p;
                                break;
                            }
                        }
                        if (nrPackets > 0) {
                            rspRawSenders[board].trySend(*rspData, nrPackets);
                        }
                    }
                    //copyRSPTimer.stop();

                    outputQueue.append(rspData);
                    rspData.reset();
                    ASSERT(!rspData);
                }
            }
        }


        void StationInput::readRSPNonRealTime() {
            vector<std::unique_ptr<Stream>> streams(nrBoards);
            vector<std::unique_ptr<PacketReader>> readers(nrBoards);

            for (size_t i = 0; i < nrBoards; ++i) {
                streams[i] = inputStream(i);
                readers[i].reset(new PacketReader(logPrefix, *streams[i], mode));
            }

            /* Since the boards will be read at different speeds, we need to
             * manually keep them in sync. We read a packet from each board,
             * and let the board providing the youngest packet read again.
             *
             * We will multiplex all packets using rspDataPool[0].
             */

            // Cache for last packets read from each board
            vector<std::shared_ptr<StationPackets>> lastPackets(nrBoards);
            for (size_t i = 0; i < nrBoards; i++) {
                const string &desc =
                        ps.settings.antennaFields.at(stationIdx).inputStreams.at(i);

                LOG_INFO_STR(logPrefix << desc);
                const auto crtFun = desc.find("+sdp:") != string::npos ? StationPackets::create<SdpStationPacket>
                                                                      : StationPackets::create<RspStationPacket>;
                lastPackets[i] = crtFun(NONRT_PACKET_BATCH_SIZE);
            }

            // Whether we should refresh last_packets[board]
            vector<bool> readNextPacket(nrBoards, true);

            for (;;) {
                for (unsigned board = 0; board < nrBoards; board++) {
                    if (!readNextPacket[board] || !readers[board]) {
                        continue;
                    }

                    try {
                        // Retry until we have a valid packet
                        while (!readers[board]->readPacket(lastPackets[board])) {}
                    } catch (EndOfStreamException &ex) {
                        // Ran out of data
                        LOG_INFO_STR(logPrefix << "End of stream");

                        readers[board]->logStatistics(board);
                        readers[board] = NULL;
                    }
                }

                // Determine which board provided the youngest packet
                unsigned youngest = nrBoards; // init to special (invalid) value

                for (unsigned board = 0; board < nrBoards; board++) {
                    if (!readers[board]) {
                        continue;
                    }

                    if (youngest == nrBoards || lastPackets[youngest]->packets[0]->timeStamp() >
                                                lastPackets[board]->packets[0]->timeStamp()) {
                        youngest = board;
                    }
                }

                // Break if all streams turned out to be inactive
                if (youngest == nrBoards) {
                    break;
                }

                // Emit youngest packet
                auto data = rspDataPool[0]->free.remove();

                // Abort of writer does not desire any more data
                if (!data) {
                    LOG_INFO_STR(logPrefix << "readRSPNonRealTime: received EOS");
                    return;
                }

                StationPackets::convert(data, lastPackets[youngest]);

                rspDataPool[0]->filled.append(data);

                // Next packet should only be read from the stream we
                // emitted from
                for (unsigned board = 0; board < nrBoards; board++) {
                    readNextPacket[board] = (board == youngest);
                }
            }

            // Signal EOD by inserting a packet beyond obs end
            auto data = rspDataPool[0]->free.remove();

            // Abort if writer does not desire any more data
            if (!data) {
                LOG_INFO_STR(logPrefix << "readRSPNonRealTime: received EOS");
                return;
            }

            LOG_INFO_STR(logPrefix << "readRSPNonRealTime: sending EOS");
            rspDataPool[0]->filled.append(NULL, false);
        }


        template<typename SampleT>
        void StationInput::writeRSPNonRealTime(std::shared_ptr<MPIData<SampleT>> &current,
                                               std::shared_ptr<MPIData<SampleT>> &next) {
            LOG_INFO_STR("[block " << current->block << "] Waiting for data");

            /*
             * 1. We'll receive one packet at a time, in-order.
             * 2. We might receive data beyond what we're processing now.
             *    In that case, we break and keep rspData around.
             */

            const size_t nrBeamletIndices = mode.nrBeamletsPerBoard;

            for (;;) {
                auto data = rspDataPool[0]->filled.remove();

                if (!data) {
                    LOG_DEBUG_STR(logPrefix << "writeRSPNonRealTime: received EOS");

                    // reinsert EOS for next call to writeRSPNonRealTime
                    rspDataPool[0]->filled.prepend(NULL);
                    return;
                }

                // LOFAR2 has all data in the same packet, so board == 0. But it
                // does not fill the 'source' field with boards offsetting from 0,
                // so we have to improvise here.
                const unsigned board = data->packets[0]->version() >= 5 ? 0 : data->packets[0]->source();

                const ssize_t *beamletIndicesPtr = &this->beamletIndices[board][0];

                // Only packet 0 is used in non-rt mode

                if (ps.settings.correlator.enabled || ps.settings.beamFormer.enabled) {
                    if (current->write(data->packets[0], beamletIndicesPtr, nrBeamletIndices)) {
                        // We have data (potentially) spilling into `next'.
                        if (!next || next->write(data->packets[0], beamletIndicesPtr, nrBeamletIndices)) {
                            // Data is even later than next? Put this data back for a future block.
                            rspDataPool[0]->filled.prepend(data);
                            data.reset();
                            ASSERT(!data);
                            return;
                        }
                    }
                }

                if (!rspRawSenders.empty() && rspRawSenders[data->packets[0]->source()].initialized()) {
                    if (data->packets[0]->seqId() < ps.settings.rspRaw.stopTime &&
                        data->packets[0]->seqId() >= ps.settings.rspRaw.startTime) {
                        rspRawSenders[data->packets[0]->source()].trySend(*data, NONRT_PACKET_BATCH_SIZE);
                    }
                }

                rspDataPool[0]->free.append(data);
                data.reset();
                ASSERT(!data);
            }
        }


        template<typename SampleT>
        void StationInput::processInput(Queue<std::shared_ptr<MPIData<SampleT>>> &inputQueue,
                                        Queue<std::shared_ptr<MPIData<SampleT>>> &outputQueue) {
            OMPThreadSet packetReaderThreads("packetReaders");

            if (ps.settings.realTime) {
                // Each board has its own pool to reduce lock contention
                for (size_t board = 0; board < nrBoards; ++board) {
                    const string &desc =
                            ps.settings.antennaFields.at(stationIdx).inputStreams.at(board);
                    const auto crtFun = desc.find("udp+sdp:") == 0 ? StationPackets::create<SdpStationPacket>
                                                                  : StationPackets::create<RspStationPacket>;
                    for (size_t i = 0; i < 48; ++i) {
                        rspDataPool[board]->free.append(crtFun(RT_PACKET_BATCH_SIZE), false);
                    }
                }
            } else {
                // We just process one packet at a time, merging all the streams into rspDataPool[0].
                for (size_t i = 0; i < 16; ++i) {
                    // We will always use the new sdp packet format but convert it during read anyway.
                    std::shared_ptr<StationPackets> data(
                            StationPackets::create<SdpStationPacket>(NONRT_PACKET_BATCH_SIZE));
                    rspDataPool[0]->free.append(data, false);
                }
            }

            // Make sure we only read RSP packets when we're ready to actually process them. Otherwise,
            // the rspDataPool[*]->free queues will starve, causing both WARNings and blocking the
            // reading of the RSP packets we do need.
            Trigger startSwitch;

#pragma omp parallel sections num_threads(2)
            {
                /*
                 * PACKET READERS: Read input data from station
                 *
                 *
                 * Reads:  rspDataPool.free
                 * Writes: rspDataPool.filled
                 *
                 * StationPackets written will have roughly equal time stamps (that is,
                 * they won't differ by a block), or contain some lingering data
                 * from before. The latter happens if packet loss occurs and no
                 * full set of packets could be read.
                 */
#pragma omp section
                {
                    OMPThread::ScopedName sn(str(format("%s rd") % ps.settings.antennaFields.at(stationIdx).name));

                    LOG_INFO_STR(logPrefix << "Processing packets");

                    // Wait until RSP packets can actually be processed
                    startSwitch.wait();

                    if (ps.settings.realTime) {
                        #pragma omp parallel for num_threads(nrBoards)
                        for (unsigned board = 0; board < nrBoards; board++) {
                            try {
                                OMPThreadSet::ScopedRun sr(packetReaderThreads);
                                OMPThread::ScopedName fsn(
                                        str(format("%s rd %u") % ps.settings.antennaFields.at(stationIdx).name %
                                            board));

                                Thread::ScopedPriority sp(SCHED_FIFO, 10);

                                readRSPRealTime(board);
                            } catch (OMPThreadSet::CannotStartException &ex) {
                                LOG_INFO_STR(logPrefix << "Stopped");
                            }
                        }
                    } else {
                        readRSPNonRealTime();
                    }

                }

                /*
                 * inputQueue -> outputQueue
                 */
#pragma omp section
                {
                    OMPThread::ScopedName sn(str(format("%s wr") % ps.settings.antennaFields.at(stationIdx).name));
                    //Thread::ScopedPriority sp(SCHED_FIFO, 10);

                    /*
                     * We maintain a `current' and a `next' block,
                     * because the offsets between subbands can require
                     * us to write to two blocks in edge cases.
                     *
                     * Also, data can arrive slightly out-of-order.
                     */

                    std::shared_ptr<MPIData<SampleT>> current, next;
                    next = inputQueue.remove();

                    while ((current = next) != NULL) {
                        next = inputQueue.remove();

                        // We can now process RSP packets
                        startSwitch.trigger();

                        if (ps.settings.realTime) {
                            writeRSPRealTime<SampleT>(current, next);
                        } else {
                            writeRSPNonRealTime<SampleT>(current, next);
                        }


                        outputQueue.append(current);
                        current.reset();
                        ASSERT(!current);
                    }

                    // Signal EOD to output
                    outputQueue.append(NULL, false);

                    // Signal EOD to input. It's a free queue, so prepend to avoid
                    // having the reader flush the whole queue first.
                    for (size_t i = 0; i < nrBoards; ++i) {
                        rspDataPool[i]->free.prepend(NULL);
                    }

                    // Make sure we don't get stuck in startup
                    startSwitch.trigger();

                    if (ps.settings.realTime) {
                        // kill reader threads
                        LOG_INFO_STR(logPrefix << "Stopping all boards");
                        packetReaderThreads.killAll();
                    }
                }
            }
        }


        template<typename SampleT>
        void sendInputToPipeline(const Parset &ps,
                                 size_t stationIdx, const SubbandDistribution &subbandDistribution, unsigned hostID,
                                 Trigger *stopSwitch) {
            // sanity check: Find out if we should actual start working here.
            StationMetaData<SampleT> sm(ps, stationIdx, subbandDistribution);

            StationInput si(ps, stationIdx, subbandDistribution, hostID);

            const struct StationID stationID(StationID::parseFullFieldName(
                    ps.settings.antennaFields.at(stationIdx).name));

            const std::string logPrefix = str(format("[station %s] ") % stationID.name());

            LOG_INFO_STR(logPrefix << "Processing station data");

            Queue<std::shared_ptr<MPIData<SampleT>>> mpiQueue(str(format(
                    "sendInputToPipeline::mpiQueue [station %s]") % stationID.name()));

            MPISender sender(logPrefix, stationIdx, subbandDistribution, ps.settings.blockDuration());

            /*
             * Stream the data.
             */
#pragma omp parallel sections num_threads(3)
            {
                /*
                 * Generate blocks and add delays
                 *
                 * sm.metaDataPool.free -> sm.metaDataPool.filled
                 */
#pragma omp section
                {
                    OMPThread::ScopedName sn(str(format("%s meta") % ps.settings.antennaFields.at(stationIdx).name));

                    LOG_INFO_STR(logPrefix << "StationMetaData: start");
                    sm.computeMetaData(stopSwitch);
                    LOG_INFO_STR(logPrefix << "StationMetaData: done");
                }

                /*
                 * Adds samples from the input streams to the blocks
                 *
                 * sm.metaDataPool.filled -> mpiQueue
                 */
#pragma omp section
                {
                    OMPThread::ScopedName sn(str(format("%s proc") % ps.settings.antennaFields.at(stationIdx).name));

                    LOG_INFO_STR(logPrefix << "StationInput: start");
                    si.processInput<SampleT>(sm.metaDataPool.filled, mpiQueue);
                    LOG_INFO_STR(logPrefix << "StationInput: done");
                }

                /*
                 * MPI POOL: Send block to receivers
                 *
                 * mpiQueue -> sm.metaDataPool.free
                 */
#pragma omp section
                {
                    OMPThread::ScopedName sn(str(format("%s send") % ps.settings.antennaFields.at(stationIdx).name));

                    LOG_INFO_STR(logPrefix << "MPISender: start");
                    bool skipDataTransfers = !ps.settings.correlator.enabled && !ps.settings.beamFormer.enabled;
                    sender.sendBlocks<SampleT>(mpiQueue, sm.metaDataPool.free, skipDataTransfers);
                    LOG_INFO_STR(logPrefix << "MPISender: done");
                }
            }

            LOG_INFO_STR(logPrefix << "Done processing station data");
        }

        void sendInputToPipeline(const Parset &ps, size_t stationIdx,
                                 const SubbandDistribution &subbandDistribution, unsigned hostID,
                                 Trigger *stopSwitch) {
            switch (ps.nrBitsPerSample()) {
                default:
                case 16:
                    sendInputToPipeline<SampleType<i16complex>>(ps, stationIdx,
                                                                subbandDistribution, hostID,
                                                                stopSwitch);
                    break;

                case 8:
                    sendInputToPipeline<SampleType<i8complex>>(ps, stationIdx,
                                                               subbandDistribution, hostID,
                                                               stopSwitch);
                    break;

                case 4:
                    sendInputToPipeline<SampleType<i4complex>>(ps, stationIdx,
                                                               subbandDistribution, hostID,
                                                               stopSwitch);
                    break;
            }
        }
    } // namespace Cobalt
} // namespace LOFAR
