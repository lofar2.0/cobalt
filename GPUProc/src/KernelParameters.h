//# KernelParameters.h
//#
//# Copyright (C) 2020  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

#ifndef LOFAR_GPUPROC_KERNEL_PARAMETERS
#define LOFAR_GPUPROC_KERNEL_PARAMETERS

#include <vector>

#include <CoInterface/Parset.h>

namespace LOFAR
{
  namespace Cobalt
  {
    struct KernelParameters
    {
    public:
      // Components
      bool correlatorEnabled;
      bool preprocessingEnabled;
      bool beamFormerCoherentStokesEnabled;
      bool beamFormerIncoherentStokesEnabled;

      // Observation settings
      struct Observation
      {
        // Constructor
        Observation(
          const ObservationSettings& obsSettings);

        // Settings
        unsigned observationID;
        unsigned nrSAPs;
        unsigned nrStations;
        unsigned nrBitsPerSample;
        unsigned blockSize;
        double subbandWidth;
        std::vector<ObservationSettings::Subband> subbands;
        unsigned clockMHz;
      };

      Observation observation;

      // Correlator settings
      using Correlator = ObservationSettings::Correlator;
      Correlator correlator;

      // Preprocessing settings
      struct Preprocessor
      {
        // Constructor
        Preprocessor(
          const std::vector<ObservationSettings::AntennaFieldName>& obsAntennaFieldNames,
          const std::vector<ObservationSettings::AntennaFieldName>& bfAntennaFieldNames,
          bool delayCompensationEnabled,
          bool bandPassCorrectionEnabled,
          unsigned nrDelayCompensationSettings,
          bool inputPPF);

        // Settings
        std::vector<unsigned> obsStationIndices;
        bool delayCompensationEnabled;
        bool bandPassCorrectionEnabled;
        unsigned nrDelayCompensationChannels;
        bool inputPPF;
      };

      Preprocessor preprocessing;

      // BeamFormer settings
      struct BeamFormer
      {
        // Constructor
        BeamFormer(
          const std::vector<ObservationSettings::AntennaFieldName>& obsAntennaFieldNames,
          const std::vector<ObservationSettings::AntennaFieldName>& preAntennaFieldNames,
          const ObservationSettings::BeamFormer::Pipeline& bfSettings);

        // The observation has a set of stations specified, ordered by name and numbered
        // from 0 to ps.settings.obsAntennaFieldNames.size()
        // For the beamFormer, there are two sets of indices for the (subset of) stations being processed:
        // obsStationIndices: the indices of the stations in the global list of stations
        //                    use these indices when indexing the tabDelays
        // preStationIndices: the indices of the stations in the list of stations in the preprocessor
        //                    use these indices when indexing the preprocssed input data
        // Note that stationIndices must be a subset of delayIndices
        std::vector<unsigned> preStationIndices;
        std::vector<unsigned> obsStationIndices;

        // Settings
        bool doFlysEye;
        unsigned int nrSAPs;
        unsigned int maxNrCoherentTABsPerSAP;
        unsigned int maxNrIncoherentTABsPerSAP;
        ObservationSettings::BeamFormer::StokesSettings coherentSettings;
        ObservationSettings::BeamFormer::StokesSettings incoherentSettings;
      };

      BeamFormer beamFormer;

      // Cobalt settings
      using Cobalt = ObservationSettings::Cobalt;
      Cobalt cobalt;
    };
  } // end namespace Cobalt
} // end namespace LOFAR

#endif
