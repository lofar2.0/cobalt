//# KernelParameters.cc
//#
//# Copyright (C) 2020  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

#include "KernelParameters.h"

namespace LOFAR
{
  namespace Cobalt
  {

    KernelParameters::Observation::Observation(
      const ObservationSettings& obsSettings) :
      observationID(obsSettings.observationID),
      nrSAPs(obsSettings.SAPs.size()),
      nrStations(obsSettings.antennaFieldNames.size()),
      nrBitsPerSample(obsSettings.nrBitsPerSample),
      blockSize(obsSettings.blockSize),
      subbandWidth(obsSettings.subbandWidth()),
      subbands(obsSettings.subbands),
      clockMHz(obsSettings.clockMHz)
    {
      ASSERT(obsSettings.antennaFieldNames.size() == obsSettings.antennaFields.size());
    }

    KernelParameters::Preprocessor::Preprocessor(
      const std::vector<ObservationSettings::AntennaFieldName>& obsAntennaFieldNames,
      const std::vector<ObservationSettings::AntennaFieldName>& bfAntennaFieldNames,
      bool delayCompensationEnabled,
      bool bandPassCorrectionEnabled,
      unsigned nrDelayCompensationChannels,
      bool inputPPF) :
      obsStationIndices(ObservationSettings::AntennaFieldName::indices(bfAntennaFieldNames, obsAntennaFieldNames)),
      delayCompensationEnabled(delayCompensationEnabled),
      bandPassCorrectionEnabled(bandPassCorrectionEnabled),
      nrDelayCompensationChannels(nrDelayCompensationChannels),
      inputPPF(inputPPF)
    {
    }

    KernelParameters::BeamFormer::BeamFormer(
      const std::vector<ObservationSettings::AntennaFieldName>& obsAntennaFieldNames,
      const std::vector<ObservationSettings::AntennaFieldName>& preAntennaFieldNames,
      const ObservationSettings::BeamFormer::Pipeline& bfSettings) :
      preStationIndices(ObservationSettings::AntennaFieldName::indices(bfSettings.antennaFieldNames, preAntennaFieldNames)),
      obsStationIndices(ObservationSettings::AntennaFieldName::indices(bfSettings.antennaFieldNames, obsAntennaFieldNames)),
      doFlysEye(bfSettings.doFlysEye),
      nrSAPs(bfSettings.SAPs.size()),
      maxNrCoherentTABsPerSAP(bfSettings.maxNrCoherentTABsPerSAP()),
      maxNrIncoherentTABsPerSAP(bfSettings.maxNrIncoherentTABsPerSAP()),
      coherentSettings(bfSettings.coherentSettings),
      incoherentSettings(bfSettings.incoherentSettings)
    {
      ASSERTSTR(preStationIndices.size() <= obsStationIndices.size(), "preStationIndices.size() <= ObsStationIndices.size()");
    }

  } // end namespace Cobalt
} // end namespace LOFAR
