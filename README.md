# COrrelator and BeAmformer for the Lofar Telescope

## Description

The cobalt repository contains the correlator and beamformer software packages to operate
the LOFAR radio telescope and process its measurement output.

LOFAR is operated by ASTRON, the Netherlands Institute for Radio Astronomy.
For more information, see http://www.astron.nl/ and http://www.lofar.org/

Source code is available at https://git.astron.nl/lofar2.0/cobalt.git

Repository web interface: https://git.astron.nl/lofar2.0/cobalt

## Contributing

To contribute, please create a feature branch and a "Draft" merge request. Upon
completion, the merge request should be marked as ready and a reviewer should be
assigned.

## Build instructions
### Required dependencies 
For Debian/Ubuntu (likely incomplete list, but goes a long way):
```shell 
apt-get install cmake make g++ python3-dev python-numpy libpython3-dev libboost-dev libfftw3-dev 
  libboost-mpi-dev libboost-mpi-python-dev libunittest++-dev mpi-dev libboost-python-dev libopenmpi-dev libhdf5-dev 
  autogen swig libpqxx-dev libboost-regex-dev libboost-date-time-dev libreadline-dev
```
### DAL2 dependency
```shell
git clone git@git.astron.nl:ro/dal2.git
cd dal2
cmake -DCMAKE_INSTALL_PREFIX=/opt/DAL -DPYTHON_BINDINGS=OFF .
make 
make install
```

### Build
```shell
cmake  -DDAL_ROOT_DIR=/opt/DAL -Bbuild/gnucxx11_2018_optarch .
cd build/gnucxx11_2018_optarch
make -j
```
## License
This project is licensed under the Apache License 2.0.
