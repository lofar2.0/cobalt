.. Lofar documentation master file, created by
   sphinx-quickstart on Fri Jun 19 17:54:42 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the LOFAR documentation!
====================================
The LOFAR repository contains some of the software packages to operate
the LOFAR radio telescope and process its measurement output.
LOFAR is operated by ASTRON, the Netherlands Institute for Radio Astronomy.
For more information, see http://www.astron.nl/ and http://www.lofar.org/

Source code is available at https://git.astron.nl/ro/lofar.git

Python documentation:

.. toctree::
   :maxdepth: 4

   lofar


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

