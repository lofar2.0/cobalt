//# SumbitFeedback.h: Store LTA feedback on disk and submit to TMSS
//# Copyright (C) 2025  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

//# Always #include <lofar_config.h> first!
#include <lofar_config.h>

#include <string>
#include <cstdlib>    // for getenv()
#include <iostream>
#include <sys/stat.h> //mkdir
#include <boost/format.hpp>
#include <chrono>
#include <thread>

#include <Common/LofarLogger.h>
#include <Common/HTTPClient.h>
#include <CoInterface/Parset.h>

using namespace LOFAR;
using namespace LOFAR::Cobalt;
using namespace std;

using boost::format;

inline void submitFeedback(const ParameterSet &feedbackParset, unsigned observationID, const string &postfix = "")
{
    // Write feedback to disk
    try {
      if(getenv("LOFARROOT")) {
          // assume the root directory is present, obs specific subdir is/canbe created
          string fb_root_dir = str(format("%s/nfs/feedback/L%d") % string(getenv("LOFARROOT")) % observationID);
          mkdir(fb_root_dir.c_str(), 0777);
          string path = fb_root_dir + string("/") + str(format("Observation%d_feedback%s.parset") % observationID % postfix);
          LOG_INFO_STR("Writing feedback to disk: " << path);
          feedbackParset.writeFile(path, false);
      } else {
          LOG_WARN_STR("Missing environment variable(s) LOFARROOT. Cannot save feedback on disk.");
      }
    } catch (Exception &ex) {
      LOG_ERROR_STR("Exception while writing feedback: " << ex);
    }

    // submit feedback to TMSS
    try {
        if(getenv("TMSS_HOST") && getenv("TMSS_USER") && getenv("TMSS_PASSWORD") &&
           string(getenv("TMSS_HOST")).length() > 0 && string(getenv("TMSS_USER")).length() > 0  && string(getenv("TMSS_PASSWORD")).length() > 0) {
              LOG_INFO_STR("Submitting feedback to TMSS " << string(getenv("TMSS_HOST")));

              string fb_string;
              feedbackParset.writeBuffer(fb_string);

              HTTPClient httpClient(getenv("TMSS_HOST"), 443, getenv("TMSS_USER"), getenv("TMSS_PASSWORD"));
              string url_path = str(format("/api/subtask/%d/process_feedback_and_set_to_finished_if_complete") % observationID);
              string result;

              // try to POST the data to TMSS, try a few times with an incrementing wait in case it fails due to unavailability
              for (int tryCntr = 0; tryCntr < 10; tryCntr++) {
                  if(httpClient.httpQuery(url_path, result, "POST", fb_string, true)) {
                      // succeeded, exit (re)try loop
                      break;
                  }
                  std::this_thread::sleep_for(std::chrono::seconds(tryCntr));
              }
        } else {
          LOG_WARN_STR("Missing environment variable(s) TMSS_HOST/TMSS_USER/TMSS_PASSWORD. Cannot submit feedback to TMSS");
        }
    } catch (Exception &ex) {
      LOG_ERROR_STR("Exception while posting feedback to TMSS: " << ex);
    }
}

