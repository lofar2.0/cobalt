//# LTAFeedback.cc
//# Copyright (C) 2008-2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

//# Always #include <lofar_config.h> first!
#include <lofar_config.h>

#include <CoInterface/LTAFeedback.h>
#include <CoInterface/TimeFuncs.h>
#include <CoInterface/CommonLofarAttributes.h>
#include <CoInterface/PrintVector.h>
#include <CoInterface/CommonLofarAttributes.h>

#include <string>
#include <boost/format.hpp>

using namespace std;
using boost::format;

namespace LOFAR
{
  namespace Cobalt
  {
    // LOFAR-RELEASE 3.1.0
    // See also CEP/pipeline/framework/lofarpipe/support/feedback.py
    int LTAFeedback::major_version(3);
    int LTAFeedback::minor_version(2);
    int LTAFeedback::patch_number(0);
    std::string LTAFeedback::feedback_version(boost::str(boost::format("%02d.%02d.%02d") %
                                        major_version % minor_version % patch_number));
      
    LTAFeedback::LTAFeedback(const ObservationSettings &settings):
      settings(settings)
    {
    }

    std::string LTAFeedback::correlatedPrefix(size_t fileno)
    {
      return str(format("Observation.DataProducts.Output_Correlated_[%u].") % fileno);
    }

    std::string LTAFeedback::beamFormedPrefix(size_t fileno)
    {
      return str(format("Observation.DataProducts.Output_Beamformed_[%u].") % fileno);
    }

    ParameterSet LTAFeedback::correlatedFeedback(size_t fileno) const
    {
      ParameterSet ps;

      const ObservationSettings::Correlator::File &f = settings.correlator.files[fileno];

      const string prefix = correlatedPrefix(fileno);
      const string locationHost = f.location.cluster != "" ? f.location.cluster : f.location.host;

      ps.add(prefix + "fileFormat",           "AIPS++/CASA");
      ps.add(prefix + "filename",             f.location.filename);
      ps.add(prefix + "size",                 "0");
      ps.add(prefix + "location",             locationHost + ":" + f.location.directory);
      ps.add(prefix + "storageWriter",        "LOFAR");
      ps.add(prefix + "storageWriterVersion", str(format("%d") % LofarStManVersion));

      ps.add(prefix + "percentageWritten",    "0");
      ps.add(prefix + "startTime",            TimeDouble::toString(settings.startTime, false));
      ps.add(prefix + "duration",             "0");
      ps.add(prefix + "integrationInterval",  str(format("%f") % settings.correlator.integrationTime()));
      ps.add(prefix + "centralFrequency",     str(format("%f") % settings.subbands[fileno].centralFrequency));
      ps.add(prefix + "channelWidth",         str(format("%f") % settings.correlator.channelWidth));
      ps.add(prefix + "channelsPerSubband",   str(format("%u") % settings.correlator.nrChannels));
      ps.add(prefix + "stationSubband",       str(format("%u") % settings.subbands[fileno].stationIdx));
      ps.add(prefix + "subband",              str(format("%u") % fileno));
      ps.add(prefix + "SAP",                  str(format("%u") % settings.subbands[fileno].SAP));
      ps.add(prefix + "antennaSet",           settings.antennaSet);
      ps.add(prefix + "dopplerCorrection",    settings.correlator.dopplerCorrection ? "true" : "false");

      ostringstream antennaFieldNamesStr;
      antennaFieldNamesStr << settings.antennaFieldNames;
      ps.add(prefix + "antennaFields",        antennaFieldNamesStr.str());

      const ObservationSettings::SAP &sap = settings.SAPs.at(settings.subbands.at(fileno).SAP);

      ps.add(prefix + "Pointing.directionType", sap.direction.type);
      ps.add(prefix + "Pointing.angle1",      str(format("%f") % sap.direction.angle1));
      ps.add(prefix + "Pointing.angle2",      str(format("%f") % sap.direction.angle2));
      ps.add(prefix + "Pointing.target",      sap.target);

      return ps;
    }


    ParameterSet LTAFeedback::beamFormedFeedback(size_t fileno) const
    {
      ParameterSet ps;

      // construct feedback for LTA -- Implements Output_Beamformed_.comp
      const struct ObservationSettings::BeamFormer::File &f = settings.beamFormer.files.at(fileno);

      const string prefix = beamFormedPrefix(fileno);
      const string locationHost = f.location.cluster != "" ? f.location.cluster : f.location.host;

      const struct ObservationSettings::SAP &sap =
        settings.SAPs.at(f.sapNr);
      const struct ObservationSettings::BeamFormer::TAB &tab =
        settings.beamFormer.SAPs.at(f.sapNr).TABs.at(f.tabNr);

      const struct ObservationSettings::BeamFormer::StokesSettings &stokesSet =
        f.coherent ? settings.beamFormer.pipelines[tab.pipelineNr].coherentSettings
                   : settings.beamFormer.pipelines[tab.pipelineNr].incoherentSettings;

      vector<string> stokesVars;

      switch (stokesSet.type) {
      case STOKES_I:
        stokesVars.push_back("I");
        break;

      case STOKES_IQUV:
        stokesVars.push_back("I");
        stokesVars.push_back("Q");
        stokesVars.push_back("U");
        stokesVars.push_back("V");
        break;

      case STOKES_XXYY:
        stokesVars.push_back("Xre");
        stokesVars.push_back("Xim");
        stokesVars.push_back("Yre");
        stokesVars.push_back("Yim");
        break;

      case INVALID_STOKES:
        THROW(CoInterfaceException, "MSWriterDAL asked to write INVALID_STOKES");
      }

      ps.add(prefix + "fileFormat",                "HDF5");
      ps.add(prefix + "filename",                  f.location.filename);
      ps.add(prefix + "size",                      "0");
      ps.add(prefix + "location",                  locationHost + ":" + f.location.directory);
      ps.add(prefix + "percentageWritten",         "0");
      ps.add(prefix + "startTime",                 TimeDouble::toString(settings.getBlockStartTime(f.blockStart), false));
      ps.add(prefix + "duration",                  "0");
      ps.add(prefix + "beamTypes",                 "[]");
      ps.add(prefix + "storageWriter",             "HDF5DEFAULT");
      ps.add(prefix + "storageWriterVersion",      "UNKNOWN");
      ps.add(prefix + "beamformingSpectralLeakage",settings.beamFormer.inputPPF ? "false" : "true");

      const string type =
        settings.beamFormer.pipelines[tab.pipelineNr].doFlysEye ? "FlysEyeBeam" :
        (f.coherent ? "CoherentStokesBeam" : "IncoherentStokesBeam");

      ps.add(prefix + "nrOfCoherentStokesBeams",   "0");
      ps.add(prefix + "nrOfIncoherentStokesBeams", "0");
      ps.add(prefix + "nrOfFlysEyeBeams",          "0");
      ps.replace(prefix + str(format("nrOf%ss") % type), "1");

      // add start and duration times similar to correlatedfeedback

      const string beamPrefix = str(format("%s%s[0].") % prefix % type);
      ps.add(prefix + "beamPrefix",            str(format("%s[0].") % type));

      ps.add(beamPrefix + "SAP",               str(format("%u") % f.sapNr));
      ps.add(beamPrefix + "TAB",               str(format("%u") % f.tabNr));
      ps.add(beamPrefix + "repointing",        str(format("%d") % f.pointingNr));
      ps.add(beamPrefix + "pipeline",          str(format("%u") % tab.pipelineNr));
      ps.add(beamPrefix + "pipelineTAB",       str(format("%u") % tab.localTabNr));
      ps.add(beamPrefix + "samplingTime",      str(format("%f") % (settings.sampleDuration() * stokesSet.nrChannels * stokesSet.timeIntegrationFactor)));
      ps.add(beamPrefix + "sampleType",        stokesSet.quantizerSettings.enabled ? "integer" : "float");
      ps.add(beamPrefix + "sampleNrBits",      str(format("%u") % (stokesSet.quantizerSettings.enabled ? stokesSet.quantizerSettings.nrBits : 32)));
      ps.add(beamPrefix + "dispersionMeasure", str(format("%f") % tab.dispersionMeasure));
      ps.add(beamPrefix + "nrSubbands",        str(format("%u") % (f.lastSubbandIdx - f.firstSubbandIdx)));

      ostringstream centralFreqsStr;
      centralFreqsStr << "[";
      for (size_t i = f.firstSubbandIdx; i < f.lastSubbandIdx; ++i) {
        if (i > f.firstSubbandIdx)
          centralFreqsStr << ", ";
        centralFreqsStr << str(format("%.4lf") % settings.subbands[i].centralFrequency);
      }
      centralFreqsStr << "]";

      ps.add(beamPrefix + "centralFrequencies", centralFreqsStr.str());

      ostringstream stationSubbandsStr;
      stationSubbandsStr << "[";
      for (size_t i = f.firstSubbandIdx; i < f.lastSubbandIdx; ++i) {
        if (i > f.firstSubbandIdx)
          stationSubbandsStr << ", ";
        stationSubbandsStr << str(format("%u") % settings.subbands[i].stationIdx);
      }
      stationSubbandsStr << "]";

      ps.add(beamPrefix + "stationSubbands",  stationSubbandsStr.str());
      ps.add(beamPrefix + "antennaSet",       settings.antennaSet);

      ps.add(beamPrefix + "channelWidth",      str(format("%f") % (settings.subbandWidth() / stokesSet.nrChannels)));
      ps.add(beamPrefix + "channelsPerSubband",str(format("%u") % stokesSet.nrChannels));
      ps.add(beamPrefix + "stokes",            str(format("[%s]") % stokesVars[f.stokesNr]));

      const struct ObservationSettings::Direction &tabDir = 
        f.pointingNr >= 0 ? tab.pointings[f.pointingNr].direction : tab.direction;
      const string &tabTarget = 
        f.pointingNr >= 0 ? tab.pointings[f.pointingNr].target : tab.target;

      ps.add(beamPrefix + "Pointing.equinox",   "J2000");
      ps.add(beamPrefix + "Pointing.coordType", "RA-DEC");
      ps.add(beamPrefix + "Pointing.directionType", tabDir.type);
      ps.add(beamPrefix + "Pointing.angle1",    str(format("%f") % tabDir.angle1));
      ps.add(beamPrefix + "Pointing.angle2",    str(format("%f") % tabDir.angle2));
      ps.add(beamPrefix + "Pointing.target",    tabTarget);

      if (type == "CoherentStokesBeam") {
        ps.add(beamPrefix + "Offset.equinox",     "J2000");
        ps.add(beamPrefix + "Offset.coordType",   "RA-DEC");
        ps.add(beamPrefix + "Offset.directionType", tabDir.type);
        ps.add(beamPrefix + "Offset.angle1",      str(format("%f") % (tabDir.angle1 - sap.direction.angle1)));
        ps.add(beamPrefix + "Offset.angle2",      str(format("%f") % (tabDir.angle2 - sap.direction.angle2)));

        ostringstream antennaFieldNamesStr;
        antennaFieldNamesStr << settings.beamFormer.pipelines[tab.pipelineNr].antennaFieldNames;
        ps.add(beamPrefix + "antennaFields",      antennaFieldNamesStr.str());
      } else if (type == "IncoherentStokesBeam") {
        ostringstream antennaFieldNamesStr;
        antennaFieldNamesStr << settings.beamFormer.pipelines[tab.pipelineNr].antennaFieldNames;
        ps.add(beamPrefix + "antennaFields",      antennaFieldNamesStr.str());
      } else if (type == "FlysEyeBeam") {
        auto& antennaFieldName = settings.beamFormer.pipelines.at(tab.pipelineNr).antennaFieldNames.at(tab.localTabNr);
        ps.add(beamPrefix + "stationName", antennaFieldName.station);
        ps.add(beamPrefix + "antennaFieldName", antennaFieldName.antennaField);

        ostringstream antennaFieldNamesStr;
        antennaFieldNamesStr << "[" << antennaFieldName << "]";
        ps.add(beamPrefix + "antennaFields", antennaFieldNamesStr.str());
      }

      return ps;
    }


    ParameterSet LTAFeedback::processingFeedback(size_t nr_blocks) const
    {
      ParameterSet ps;

      // for MoM, to discriminate between older and newer feedback
      ps.add("feedback_version", feedback_version); // e.g. 03.01.00

      // for MoM, to discriminate between Cobalt and BG/P observations
      ps.add("_isCobalt", "T");

      // actual start/end times that we used
      ps.add("Observation.startTime", toUTC(settings.startTime));
      ps.add("Observation.stopTime", toUTC(settings.getBlockStopTime((ssize_t)nr_blocks - 1)));

      ps.add("Observation.DataProducts.nrOfOutput_Correlated_", 
             str(format("%u") % (settings.correlator.enabled ? settings.correlator.files.size() : 0)));

      if (settings.correlator.enabled) {
        ps.add("Observation.Correlator.integrationInterval",
               str(format("%.16g") % settings.correlator.integrationTime()));
        ps.add("Observation.Correlator.channelsPerSubband",
               str(format("%u") % settings.correlator.nrChannels));
        ps.add("Observation.Correlator.channelWidth",
               str(format("%.16g") % (settings.subbandWidth() / settings.correlator.nrChannels)));
      }

      ps.add("Observation.DataProducts.nrOfOutput_Beamformed_", 
             str(format("%u") % (settings.beamFormer.enabled ? settings.beamFormer.files.size() : 0)));

      if (settings.beamFormer.enabled && settings.beamFormer.anyCoherentTABs()) {
        /* Coherent Stokes, or Fly's Eye.
         *
         * The specifications have to enable Coherent Stokes even in Fly's Eye mode, which means
         * that the specification tools (MoM) expect feedback about Coherent Stokes, even if there
         * are no coherent non-Fly's Eye TABs (related to #7109 and SIP 2.5.0).
         */
        const ObservationSettings::BeamFormer::StokesSettings&
          coherentStokes = settings.beamFormer.coherentSettings;

        // The 'rawSamplingTime' is the duration of a sample right after the PPF
        ps.add("Observation.CoherentStokes.rawSamplingTime",
               str(format("%.16g") % 
                   (settings.sampleDuration() * coherentStokes.nrChannels)));

        // The 'samplingTime' is the duration of a sample in the output
        ps.add("Observation.CoherentStokes.samplingTime",
               str(format("%.16g") % 
                   (settings.sampleDuration() * coherentStokes.nrChannels * coherentStokes.timeIntegrationFactor)));

        ps.add("Observation.CoherentStokes.timeDownsamplingFactor",
               str(format("%.16g") % coherentStokes.timeIntegrationFactor));

        ps.add("Observation.CoherentStokes.channelsPerSubband",
               str(format("%u") % coherentStokes.nrChannels));
        ps.add("Observation.CoherentStokes.channelWidth",
               str(format("%.16g") % (settings.subbandWidth() / coherentStokes.nrChannels)));

        ps.add("Observation.CoherentStokes.stokes",
               stokesType(coherentStokes.type));

        ps.add("Observation.CoherentStokes.antennaSet",
               settings.antennaSet);
        ps.add("Observation.CoherentStokes.stationList",
               settings.rawStationList);
      }

      for (auto& pipeline : settings.beamFormer.pipelines)
      {
        if (settings.beamFormer.enabled && pipeline.anyCoherentTABs() && pipeline.doFlysEye) {
          /* Fly's Eye */
          const ObservationSettings::BeamFormer::StokesSettings&
            coherentStokes = pipeline.coherentSettings;

          // The 'rawSamplingTime' is the duration of a sample right after the PPF
          ps.add("Observation.FlysEye.rawSamplingTime",
                 str(format("%.16g") %
                     (settings.sampleDuration() * coherentStokes.nrChannels)));

          // The 'samplingTime' is the duration of a sample in the output
          ps.add("Observation.FlysEye.samplingTime",
                 str(format("%.16g") %
                     (settings.sampleDuration() * coherentStokes.nrChannels * coherentStokes.timeIntegrationFactor)));

          ps.add("Observation.FlysEye.timeDownsamplingFactor",
                 str(format("%.16g") % coherentStokes.timeIntegrationFactor));

          ps.add("Observation.FlysEye.channelsPerSubband",
                 str(format("%u") % coherentStokes.nrChannels));
          ps.add("Observation.FlysEye.channelWidth",
                 str(format("%.16g") % (settings.subbandWidth() / coherentStokes.nrChannels)));

          ps.add("Observation.FlysEye.stokes",
                 stokesType(coherentStokes.type));

          if (settings.beamFormer.pipelines.size() > 1)
          {
            LOG_INFO_STR("The Observation.FlysEye.* settings are written only once, for the first BF pipeline in FE mode.");
            break;
          }
        }
      }

      if (settings.beamFormer.enabled && settings.beamFormer.anyIncoherentTABs()) {
        /* Incoherent Stokes */
        const ObservationSettings::BeamFormer::StokesSettings&
          incoherentStokes = settings.beamFormer.incoherentSettings;

        // The 'rawSamplingTime' is the duration of a sample right after the PPF
        ps.add("Observation.IncoherentStokes.rawSamplingTime",
               str(format("%.16g") % 
                   (settings.sampleDuration() * incoherentStokes.nrChannels)));

        // The 'samplingTime' is the duration of a sample in the output
        ps.add("Observation.IncoherentStokes.samplingTime",
               str(format("%.16g") % 
                   (settings.sampleDuration() * incoherentStokes.nrChannels * incoherentStokes.timeIntegrationFactor)));

        ps.add("Observation.IncoherentStokes.timeDownsamplingFactor",
               str(format("%.16g") % incoherentStokes.timeIntegrationFactor));

        ps.add("Observation.IncoherentStokes.channelsPerSubband",
               str(format("%u") % incoherentStokes.nrChannels));
        ps.add("Observation.IncoherentStokes.channelWidth",
               str(format("%.16g") % (settings.subbandWidth() / incoherentStokes.nrChannels)));

        ps.add("Observation.IncoherentStokes.stokes",
               stokesType(incoherentStokes.type));

        ps.add("Observation.IncoherentStokes.antennaSet",
               settings.antennaSet);
        ps.add("Observation.IncoherentStokes.stationList",
               settings.rawStationList);
      }

      return ps;
    }


    ParameterSet LTAFeedback::allFeedback(size_t nr_blocks) const
    {
      ParameterSet ps;

      ps.adoptCollection(processingFeedback(nr_blocks));

      // add the feedback for the individual files
      if (settings.correlator.enabled) {
        for (size_t i = 0; i < settings.correlator.files.size(); ++i) {
          ps.adoptCollection(correlatedFeedback(i));
        }
      }
      
      if (settings.beamFormer.enabled) {
        for (size_t i = 0; i < settings.beamFormer.files.size(); ++i) {
          ps.adoptCollection(beamFormedFeedback(i));
        }
      }

      return ps;
    }
  } // namespace Cobalt
} // namespace LOFAR
