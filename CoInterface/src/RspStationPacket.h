
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#ifndef COBALT_RSPSTATIONPACKET_H
#define COBALT_RSPSTATIONPACKET_H

#include "RSPTimeStamp.h"
#include "StationPacket.h"
#include <complex>
#include <cstdint>
#include <cstdlib>
#include <memory>
#include <queue>
#include <string>

namespace LOFAR {
    namespace Cobalt {

        class RspStationPacket : public StationPacket {
        private:
            struct __attribute__((__packed__))  RSP {
                // ----------------------------------------------------------------------
                // Header and payload, in little endian!
                // ----------------------------------------------------------------------

                struct __attribute__((__packed__))  Header {
                    // 2: Beamlet Data CoInterface 5.0: no longer supported
                    // 3: Beamlet Data CoInterface 6.0 (add 8- and 4-bit mode support apart from 16-bit mode)
                    uint8 version;

                    // bit (0=LSB)
                    //
                    // 4:0    RSP board number
                    // 5      (reserved, set to 0)
                    // 6      0: payload ok, 1: payload has data errors
                    // 7      0: 160 MHz     1: 200 MHz
                    uint8 sourceInfo1;

                    // bit (0=LSB)
                    //
                    // 1:0    0: 16-bit      1: 8-bit       2: 4-bit
                    // 7:2    (reserved, set to 0)
                    uint8 sourceInfo2;

                    // identifiers
                    uint8 configuration;
                    uint16 station;

                    // number of beamlets, typically at maximum:
                    //   16-bit: 61
                    //    8-bit: 122
                    //    4-bit: 244
                    uint8 nrBeamlets;

                    // number of Xr+Xi+Yr+Yi samples per beamlet, typically 16
                    uint8 nrBlocks;

                    // UNIX timestamp in UTC (= # seconds since 1970-01-01 00:00:00)
                    // 0xFFFFFFFF = clock not initialised
                    uint32 timestamp;

                    // Sample offset within the timestamp.
                    //
                    // 160 MHz: 160M/1024 = 156250 samples/second.
                    //
                    // 200 MHz: 200M/1024 = 195212.5 samples/second.
                    //                      Even seconds have 195213 samples,
                    //                      odd seconds have 195212 samples.
                    uint32 blockSequenceNumber;
                } header;
                Payload payload;
            } *packet;

        public:
            static const size_t PACKET_SIZE = sizeof(RSP);
            static const size_t PACKET_HEADER_SIZE = sizeof(RSP::Header);

            RspStationPacket(uint8_t *data) : StationPacket(data), packet((RSP *) data) {}

            RSP::Header &header();

            virtual void initHeader();

            virtual unsigned version() const;

            virtual unsigned station() const;

            virtual void station(uint16_t stationId);

            virtual size_t blockCount() const;

            virtual void blockCount(uint8_t blockCount);

            virtual uint64_t packetBegin() const;

            virtual void packetBegin(uint64_t packetBegin);

            virtual TimeStamp timeStamp() const;

            virtual time_t seqId() const;

            virtual size_t beamletCount() const;

            virtual void beamletCount(uint16_t beamletCount);

            virtual size_t packetSize() const;

            virtual size_t headerSize() const;

            virtual unsigned bitMode() const;

            virtual void bitMode(unsigned bitMode);

            virtual unsigned source() const;

            virtual void source(unsigned src) const;

            virtual unsigned clockMHz() const;

            virtual void clockMHz(unsigned clockMHz);

            virtual Payload &payload() const;

            virtual bool payloadError() const;

            virtual void payloadError(bool error);

            virtual void timeStampError();

            virtual StationPacketValidationResult validate(unsigned recvdSize) const;
        };


    } // namespace Cobalt
} // namespace LOFAR
#endif // COBALT_RSPSTATIONPACKET_H
