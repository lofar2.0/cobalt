
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#ifndef LOFAR_COINTERFACE_POOL_H
#define LOFAR_COINTERFACE_POOL_H

#include <CoInterface/Queue.h>

namespace LOFAR
{
  namespace Cobalt
  {
    // The pool operates using a free and a filled queue to cycle through buffers. Producers
    // move elements free->filled, and consumers move elements filled->free. By
    // wrapping the elements in an shared_ptr, memory leaks are prevented.
    //
    // If warn_on_bad_performance == True, the queues will log warnings if they are not filled/freed
    // fast enough (see Queue.h for more details).
    //
    // batch_size is the number of elements appended to "filled" in short repetition, too fast for the consumer
    // to reasonably consume between appends.
    template <typename T>
    struct Pool
    {
      typedef T element_type;

      Queue<std::shared_ptr<element_type>> free;
      Queue<std::shared_ptr<element_type>> filled;

      Pool(const std::string &name, bool warn_on_bad_performance, int batch_size = 1)
      :
        free(name + " [.free]", warn_on_bad_performance ? true : false, -1),
        filled(name + " [.filled]", false, warn_on_bad_performance ? batch_size : -1)
      {
      }
    };
  }
}

#endif
