// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#ifndef COBALT_STATION_PACKET_H
#define COBALT_STATION_PACKET_H

#include "RSPTimeStamp.h"
#include <complex>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <memory>
#include <queue>
#include <string>

namespace LOFAR {
    namespace Cobalt {
        enum StationPacketValidationResult {
            SP_PACKET_OK,
            SP_PACKET_ERROR_BAD_TIME = 1,
            SP_PACKET_ERROR_BAD_DATA,
            SP_PACKET_ERROR_BAD_VERSION,
            SP_PACKET_ERROR_BAD_SIZE
        };
        // Payload, allocated for maximum size.
        // Actual size depends on the header (nrBeamlets, nrBlocks). It changed in the past (61 vs 60) and
        // may be less for tests, old pre-recorded data, in 4 bit mode for 1 of the boards, and
        // RSP raw output for offline reprocessing or piggy-backing (selectable beamlet subset).
        union Payload {
            char data[7808];

            // samples are structured as samples[nrBeamlets][nrBlocks],
            // so first all blocks of the first beamlet, then all blocks of the second
            // beamlet, etc.
            //
            // for 4-bit mode:
            //  low octet: real      (2's complement)
            // high octet: imaginary (2's complement)

            struct __attribute__((__packed__)) Samples16bit {
                short Xr, Xi, Yr, Yi;
            } samples16bit[61 * 16];
            struct __attribute__((__packed__)) Samples8bit {
                signed char Xr, Xi, Yr, Yi;
            } samples8bit[122 * 16];
            struct __attribute__((__packed__)) Samples4bit {
                signed char X, Y;
            } samples4bit[244 * 16];
        };

        class StationPacket {
        protected:
            static std::complex<int> decode4bit(int8 sample) {
                // intermediate after << will be int, not int8,
                // so cast to get a signed int8 value.
                int8 re = (int8) (sample << 4) >> 4; // preserve sign
                int8 im = (sample) >> 4; // preserve sign

                // balance range to [-7..7], subject to change!
                if (re == -8) {
                    re = -7;
                }
                if (im == -8) {
                    im = -7;
                }

                return std::complex<int>(re, im);
            }
        public:
            uint8_t *data;

            virtual void initHeader() = 0;

            virtual unsigned version() const = 0;

            virtual unsigned station() const = 0;

            virtual void station(uint16_t stationId) = 0;

            virtual size_t blockCount() const = 0;

            virtual void blockCount(uint8_t blockCount) = 0;

            // Returns the timestamp of the packet in samples as required in station transpose.
            virtual uint64_t packetBegin() const = 0;

            virtual void packetBegin(uint64_t packetBegin) = 0;

            virtual TimeStamp timeStamp() const = 0;

            virtual time_t seqId() const = 0;

            virtual size_t beamletCount() const = 0;

            virtual void beamletCount(uint16_t beamletCount) = 0;

            virtual size_t packetSize() const = 0;

            virtual size_t headerSize() const = 0;

            virtual unsigned bitMode() const = 0;

            virtual void bitMode(unsigned bitMode) = 0;

            virtual unsigned clockMHz() const = 0;

            virtual void clockMHz(unsigned clockMHz) = 0;

            virtual Payload &payload() const = 0;

            virtual bool payloadError() const = 0;

            virtual void payloadError(bool error) = 0;

            virtual void timeStampError() = 0;

            virtual unsigned source() const = 0;

            virtual void source(unsigned src) const = 0;

            virtual StationPacketValidationResult validate(unsigned recvdSize) const = 0;

            std::complex<int>
            sample(unsigned beamlet, unsigned block,
                   char polarisation /* 'X' or 'Y' */) const;

            StationPacket(uint8_t *data) : data(data) {}

            virtual ~StationPacket() {}


        };



        inline std::complex<int>
        StationPacket::sample(unsigned beamlet, unsigned block,
                                 char polarisation /* 'X' or 'Y' */) const {

            const unsigned offset = beamlet * blockCount() + block;
            const auto &packagePayload = payload();

            switch (bitMode()) {
                default:
                case 16:
                    return polarisation == 'X'
                           ? std::complex<int>(packagePayload.samples16bit[offset].Xr,
                                               packagePayload.samples16bit[offset].Xi)
                           : std::complex<int>(packagePayload.samples16bit[offset].Yr,
                                               packagePayload.samples16bit[offset].Yi);

                case 8:
                    return polarisation == 'X'
                           ? std::complex<int>(packagePayload.samples8bit[offset].Xr,
                                               packagePayload.samples8bit[offset].Xi)
                           : std::complex<int>(packagePayload.samples8bit[offset].Yr,
                                               packagePayload.samples8bit[offset].Yi);

                case 4:
                    return polarisation == 'X'
                           ? decode4bit(packagePayload.samples4bit[offset].X)
                           : decode4bit(packagePayload.samples4bit[offset].Y);
            }
        }

        class StationPackets {
        public:
            std::vector<uint8_t> memory;
            const unsigned long size;
            const unsigned long count;
            std::vector<std::shared_ptr<StationPacket>> packets;

            template<typename T>
            static std::shared_ptr<StationPackets> create(const unsigned long packetCount) {
                static_assert(std::is_base_of<StationPacket, T>::value,
                              "T not derived from StationPacket");

                std::shared_ptr<StationPackets> packets(
                        new StationPackets(T::PACKET_SIZE, packetCount));

                for (unsigned long i = 0; i < packetCount; i++) {
                    packets->packets[i] = std::make_shared<T>(
                            (uint8_t *) (packets->memory.data() + i * T::PACKET_SIZE));
                }
                return packets;
            }

            static void convert(const std::shared_ptr<StationPackets> &dst, std::shared_ptr<StationPackets> &src) {
                for (size_t i = 0; i < src->packets.size(); i ++) {
                    auto &srcPacket = src->packets[i];
                    auto &dstPacket = dst->packets[i];
                    dstPacket->initHeader();
                    dstPacket->station(srcPacket->station());
                    dstPacket->bitMode(srcPacket->bitMode());
                    dstPacket->payloadError(srcPacket->payloadError());
                    dstPacket->clockMHz(srcPacket->clockMHz());
                    dstPacket->source(srcPacket->source());
                    dstPacket->blockCount(srcPacket->blockCount());
                    dstPacket->beamletCount(srcPacket->beamletCount());
                    dstPacket->packetBegin(srcPacket->packetBegin());
                    memcpy(dstPacket->payload().data, srcPacket->payload().data, sizeof(Payload));
                }
            }

        protected:
            StationPackets(const unsigned long packetSize, const unsigned long packetCount);
        };

        inline StationPackets::StationPackets(const unsigned long packetSize, const unsigned long packetCount)
                : memory(packetSize * packetCount), size(packetSize),
                  count(packetCount), packets(packetCount) {

        }
    } // namespace Cobalt
} // namespace LOFAR

#endif //COBALT_STATION_PACKET_H
