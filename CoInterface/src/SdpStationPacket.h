// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#ifndef COBALT_SDPSTATIONPACKET_H
#define COBALT_SDPSTATIONPACKET_H

#include "StationPacket.h"
#include "RSPTimeStamp.h"
#include <complex>
#include <cstdint>
#include <cstdlib>
#include <memory>
#include <queue>
#include <string>

namespace LOFAR {
    namespace Cobalt {

        class SdpStationPacket : public StationPacket {
        private:
            struct __attribute__((__packed__))  Sdp {
                struct __attribute__((__packed__)) Header {
                    uint8_t marker;
                    uint8_t versionId;
                    uint32_t observationId;
                    // The packet writes these bytes in big-endian format,
                    // but we will run on little-endian machines. So,
                    // we define the bytes in order they come from the network,
                    // so from high to low, but the bits within a byte
                    // from low to high.
                    struct __attribute__((__packed__)) StationInfo {
                        // byte 0 (MSB)
                        uint8_t stationId_H: 7;
                        uint8_t antennaFieldIndex: 1;

                        // byte 1 (LSB)
                        uint8_t stationId_L: 8;
                    } stationInfo;
                    struct __attribute__((__packed__)) SourceInfo {
                        // byte 0 (MSB)
                        uint8_t nyquistZoneIndex: 2;
                        uint8_t antennaBandIndex: 1;
                        uint8_t reserved: 5;

                        // byte 1
                        uint8_t beamletWidth: 4;
                        uint8_t repositioningFlag: 1;
                        uint8_t payloadError: 1;
                        uint8_t fsubType: 1;
                        uint8_t fAdc: 1;

                        // byte 2 (LSB)
                        uint8_t gnIndex: 8;
                    } sourceInfo;
                    uint8_t reserved[4];
                    uint16_t beamletScale;
                    uint16_t beamletIndex;
                    uint8_t nofBlocksPerPacket;
                    uint16_t nofBeamletsPerBlock;
                    uint16_t blockPeriod;
                    uint64_t blockSequenceNumber;
                } header;
                LOFAR::Cobalt::Payload payload;
            } *packet;
        protected:
            inline uint64_t exactTimestamp() const {
                return __builtin_bswap64(packet->header.blockSequenceNumber) *
                                        __builtin_bswap16(packet->header.blockPeriod) / 1000000000;
            }
        public:
            static const size_t PACKET_SIZE = sizeof(Sdp);
            static const size_t PACKET_HEADER_SIZE = sizeof(Sdp::Header);

            SdpStationPacket(uint8_t *data) : StationPacket(data), packet((Sdp *) data)  {}

            Sdp::Header &header();

            virtual void initHeader();

            virtual unsigned version() const;

            virtual unsigned station() const;

            virtual void station(uint16_t stationId);

            virtual size_t blockCount() const;

            virtual void blockCount(uint8_t blockCount);

            virtual uint64_t packetBegin() const;

            virtual void packetBegin(uint64_t packetBegin);

            virtual TimeStamp timeStamp() const;

            virtual time_t seqId() const;

            virtual size_t beamletCount() const;

            virtual void beamletCount(uint16_t beamletCount);

            virtual size_t packetSize() const;

            virtual size_t headerSize() const;

            virtual uint bitMode() const;

            virtual void bitMode(unsigned bitMode);

            virtual unsigned source() const;

            virtual void source(unsigned src) const;

            virtual uint clockMHz() const;

            virtual void clockMHz(unsigned clockMHz);

            virtual LOFAR::Cobalt::Payload &payload() const;

            virtual bool payloadError() const;

            virtual void payloadError(bool error);

            virtual void timeStampError();

            virtual LOFAR::Cobalt::StationPacketValidationResult validate(unsigned recvdSize) const;
        };

    } // namespace Cobalt
} // namespace LOFAR

#endif //COBALT_SDPSTATIONPACKET_H
