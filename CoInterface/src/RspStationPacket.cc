
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#include "RspStationPacket.h"
#include "StationPacket.h"
#include "SdpStationPacket.h"

namespace LOFAR {
    namespace Cobalt {
        size_t RspStationPacket::blockCount() const {
            return packet->header.nrBlocks;
        }

        void RspStationPacket::blockCount(uint8_t blockCount) {
            packet->header.nrBlocks = blockCount;
        }

        bool RspStationPacket::payloadError() const {
            return packet->header.sourceInfo1 & 0x40;
        }

        void RspStationPacket::payloadError(bool error) {
            if (error) {
                packet->header.sourceInfo1 |= 0x40;
            } else {
                packet->header.sourceInfo1 &= ~0x40;
            }
        }

        unsigned RspStationPacket::clockMHz() const {
            return packet->header.sourceInfo1 & 0x80 ? 200 : 160;
        }

        void RspStationPacket::clockMHz(unsigned int clockMHz) {
            packet->header.sourceInfo1 |= (clockMHz == 200 ? 1 << 7 : 0);
        }

        unsigned RspStationPacket::bitMode() const {
            // if (header.version < 3)  // disabled: Beamlet Data CoInterface 5.0 is too
            // old to care and in a hot path
            //   return 16;
            return 16 >> packet->header.sourceInfo2; // 0x0: 16, 0x1: 8, 0x2: 4
        }

        void RspStationPacket::bitMode(unsigned int bitMode) {
            packet->header.sourceInfo2 &= ~0x3;
            switch (bitMode) {
                default:
                case 16:
                    packet->header.sourceInfo2 |= 0x0;
                    break;
                case 8 :
                    packet->header.sourceInfo2 |= 0x1;
                    break;
                case 4 :
                    packet->header.sourceInfo2 |= 0x2;
                    break;
            }
        }

        size_t RspStationPacket::beamletCount() const {
            return packet->header.nrBeamlets;
        }

        void RspStationPacket::beamletCount(uint16_t beamletCount) {
            packet->header.nrBeamlets = beamletCount;
        }

        size_t RspStationPacket::packetSize() const {
            return sizeof(RSP::Header) + packet->header.nrBlocks *
                                         packet->header.nrBeamlets * 2 * 2 *
                                         bitMode() / 8;
        }

        Payload &RspStationPacket::payload() const {
            return *reinterpret_cast<Payload *>(data + headerSize());
        }

        uint64_t RspStationPacket::packetBegin() const {
            return timeStamp();
        }

        void RspStationPacket::packetBegin(uint64_t packetBegin) {
            const TimeStamp timeStamp(packetBegin, clockMHz()*1000000);
            packet->header.timestamp = timeStamp.getSeqId();
            packet->header.blockSequenceNumber = timeStamp.getBlockId();
        }

        TimeStamp RspStationPacket::timeStamp() const {
            auto t = TimeStamp(packet->header.timestamp,
                               packet->header.blockSequenceNumber, clockMHz() * 1000000);
            return t;
        }


        void RspStationPacket::timeStampError() {
            packet->header.timestamp = 0xFFFFFFFF; // clock not initialised
        }

        StationPacketValidationResult RspStationPacket::validate(unsigned int numbytes) const {
            // illegal size means illegal packet; don't touch
            if (numbytes != packetSize()) {
                return SP_PACKET_ERROR_BAD_SIZE;
            }

            // illegal version means illegal packet
            if (packet->header.version < 3) {
                // This mainly catches packets that are all zero (f.e. /dev/zero or
                // null: streams).
                return SP_PACKET_ERROR_BAD_VERSION;
            }

            // illegal timestamp means illegal packet
            if (packet->header.timestamp == ~0U) {
                return SP_PACKET_ERROR_BAD_TIME;
            }

            // discard packets with errors
            if (payloadError()) {
                return SP_PACKET_ERROR_BAD_DATA;
            }

            // everything is ok
            return SP_PACKET_OK;
        }

        size_t RspStationPacket::headerSize() const {
            return RspStationPacket::PACKET_HEADER_SIZE;
        }

        time_t RspStationPacket::seqId() const {
            return packet->header.timestamp;
        }

        RspStationPacket::RSP::Header &RspStationPacket::header() {
            return packet->header;
        }


        unsigned RspStationPacket::source() const {
            return packet->header.sourceInfo1 & 0x1F;
        }

        void RspStationPacket::source(unsigned int src) const {
            packet->header.sourceInfo1 |= src;
        }

        void RspStationPacket::initHeader() {
            packet->header.version = 3;
        }

        unsigned RspStationPacket::version() const {
            return packet->header.version;
        }

        unsigned RspStationPacket::station() const {
            return packet->header.station;
        }

        void RspStationPacket::station(uint16_t stationId) {
            packet->header.station = stationId;
        }

    } // namespace Cobalt
} // namespace LOFAR
