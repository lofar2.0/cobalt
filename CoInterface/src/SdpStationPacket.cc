// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

#include "SdpStationPacket.h"

namespace LOFAR {
    namespace Cobalt {

        size_t SdpStationPacket::blockCount() const {
            return packet->header.nofBlocksPerPacket;
        }

        void SdpStationPacket::blockCount(uint8_t blockCount) {
            packet->header.nofBlocksPerPacket = blockCount;
        }

        bool SdpStationPacket::payloadError() const {
            return packet->header.sourceInfo.payloadError;
        }

        void SdpStationPacket::payloadError(bool error) {
            packet->header.sourceInfo.payloadError = error;
        }

        unsigned SdpStationPacket::clockMHz() const {
            return packet->header.sourceInfo.fAdc ? 200 : 160;
        }

        void SdpStationPacket::clockMHz(unsigned int clockMHz) {
            packet->header.sourceInfo.fAdc = clockMHz == 200;
            packet->header.blockPeriod = clockMHz == 200 ? 0x14 : 0x19;
        }

        unsigned SdpStationPacket::bitMode() const {
            const auto bitMode = packet->header.sourceInfo.beamletWidth;
            if (bitMode == 0) {
                return 16;
            }
            return bitMode;
        }

        void SdpStationPacket::bitMode(unsigned int bitMode) {
            packet->header.sourceInfo.beamletWidth = bitMode;
        }

        size_t SdpStationPacket::beamletCount() const {
            return __builtin_bswap16(packet->header.nofBeamletsPerBlock);
        }

        void SdpStationPacket::beamletCount(uint16_t beamletCount) {
            packet->header.nofBeamletsPerBlock = __builtin_bswap16(beamletCount);
        }

        size_t SdpStationPacket::packetSize() const {
            return sizeof(Sdp::Header) + packet->header.nofBlocksPerPacket *
                                         beamletCount() * 2 * 2 *
                                         bitMode() / 8;
        }

        LOFAR::Cobalt::Payload &SdpStationPacket::payload() const {
            return packet->payload;
        }

        LOFAR::Cobalt::TimeStamp SdpStationPacket::timeStamp() const {
            return TimeStamp(__builtin_bswap64(packet->header.blockSequenceNumber), clockMHz() * 1000000);
        }

        void SdpStationPacket::timeStampError() {
            packet->header.blockPeriod = UINT16_MAX;
        }

        LOFAR::Cobalt::StationPacketValidationResult SdpStationPacket::validate(unsigned int numbytes) const {
            // illegal size means illegal packet; don't touch
            if (numbytes != packetSize()) {
                return LOFAR::Cobalt::SP_PACKET_ERROR_BAD_SIZE;
            }

            // illegal version means illegal packet
            if (packet->header.marker != 'b' || packet->header.versionId < 5) {
                // This mainly catches packets that are all zero (f.e. /dev/zero or
                // null: streams).
                return LOFAR::Cobalt::SP_PACKET_ERROR_BAD_VERSION;
            }

            // illegal timestamp means illegal packet
            if (packet->header.blockPeriod == UINT16_MAX) {
                return LOFAR::Cobalt::SP_PACKET_ERROR_BAD_TIME;
            }

            // discard packets with errors
            if (payloadError()) {
                return LOFAR::Cobalt::SP_PACKET_ERROR_BAD_DATA;
            }

            // everything is ok
            return LOFAR::Cobalt::SP_PACKET_OK;
        }

        size_t SdpStationPacket::headerSize() const {
            return SdpStationPacket::PACKET_HEADER_SIZE;
        }

        time_t SdpStationPacket::seqId() const {
            return timeStamp().getSeqId();
        }

        void SdpStationPacket::source(unsigned int src) const {
            packet->header.sourceInfo.gnIndex = src;
        }

        unsigned SdpStationPacket::source() const {
            return packet->header.sourceInfo.gnIndex;
        }

        SdpStationPacket::Sdp::Header &SdpStationPacket::header() {
            return packet->header;
        }

        void SdpStationPacket::initHeader() {
            packet->header.marker = 'b';
            packet->header.versionId = 5;
            packet->header.blockPeriod = 0x14; // 0x19
        }

        uint64_t SdpStationPacket::packetBegin() const {
            return __builtin_bswap64(packet->header.blockSequenceNumber);
        }

        void SdpStationPacket::packetBegin(uint64_t packetBegin) {
            packet->header.blockSequenceNumber = __builtin_bswap64(packetBegin);
        }

        unsigned SdpStationPacket::version() const {
            return packet->header.versionId;
        }

        unsigned SdpStationPacket::station() const {
            return (packet->header.stationInfo.stationId_H << 8) | packet->header.stationInfo.stationId_L;
        }

        void SdpStationPacket::station(uint16_t stationId) {
            packet->header.stationInfo.stationId_L = stationId & 0xFF;
            packet->header.stationInfo.stationId_H = stationId >> 8;
        }
    } // namespace Cobalt
} // namespace LOFAR
