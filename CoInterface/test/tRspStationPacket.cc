
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

//# Always #include <lofar_config.h> first!
#include <lofar_config.h>

#include <string>
#include <iostream>

#include <Common/LofarLogger.h>
#include <Stream/FileStream.h>

#include <CoInterface/RspStationPacket.h>


using namespace LOFAR;
using namespace LOFAR::Cobalt;
using namespace std;

void report( const string &filename )
{
  cout << "---- Checking " << filename << endl;
  FileStream f(filename);

  std::vector<uint8_t> dataFrame(RspStationPacket::PACKET_SIZE);
  RspStationPacket packet(dataFrame.data());

  // read header
  f.read( &packet.header(), RspStationPacket::PACKET_HEADER_SIZE);

  cout << "RSP version:  " << (int)packet.header().version << endl;
  cout << "RSP board nr: " << packet.source() << endl;
  cout << "Payload OK:   " << (packet.payloadError() ? "NO" : "YES") << endl;
  cout << "Clock:        " << packet.clockMHz() << " MHz" << endl;
  cout << "Bit mode:     " << packet.bitMode() << " bit" << endl;
  cout << "Blocks:       " << (int)packet.blockCount() << endl;
  cout << "Beamlets:     " << (int)packet.beamletCount() << endl;

  // read payload
  f.read( &packet.payload(), packet.packetSize() - packet.headerSize() );

#ifdef WORDS_BIGENDIAN
  if (packet.bitMode() == 16)
    dataConvert(LittleEndian, (int16*)&packet.payload, packet.blockCount() * packet.beamletCount() * 2 * 2);
#endif

  cout << "Sample 4 of beamlet 2:  X = " << packet.sample(2, 4, 'X') << endl;
  cout << "Sample 4 of beamlet 2:  Y = " << packet.sample(2, 4, 'Y') << endl;
}

int main()
{
  INIT_LOGGER("tRspStationPacket");

  try {
    report( "tRspStationPacket.in_16bit" );
    report( "tRspStationPacket.in_8bit" );
  } catch (Exception &ex) {
    LOG_FATAL_STR("Caught exception: " << ex);
    return 1;
  }

  return 0;
}

