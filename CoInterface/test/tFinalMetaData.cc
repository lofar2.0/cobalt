//# tOMPThread.cc
//# Copyright (C) 2012-2013  ASTRON (Netherlands Institute for Radio Astronomy)
//# P.O. Box 2, 7990 AA Dwingeloo, The Netherlands
//#
//# This file is part of the LOFAR software suite.
//# The LOFAR software suite is free software: you can redistribute it and/or
//# modify it under the terms of the GNU General Public License as published
//# by the Free Software Foundation, either version 3 of the License, or
//# (at your option) any later version.
//#
//# The LOFAR software suite is distributed in the hope that it will be useful,
//# but WITHOUT ANY WARRANTY; without even the implied warranty of
//# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//# GNU General Public License for more details.
//#
//# You should have received a copy of the GNU General Public License along
//# with the LOFAR software suite. If not, see <http://www.gnu.org/licenses/>.
//#
//# $Id$

#include <lofar_config.h>

#include <Common/LofarLogger.h>
#include <Stream/StringStream.h>
#include <CoInterface/FinalMetaData.h>

#include <UnitTest++.h>

using namespace LOFAR;
using namespace LOFAR::Cobalt;
using namespace std;

SUITE(FinalMetaData) {
  TEST(Basic) {
    FinalMetaData f;
  }
}

SUITE(ReadWrite) {
  TEST(NoRCUs) {
    FinalMetaData f_in, f_out;

    f_in.nrBlocksInObservation = 42;

    StringStream ss;
    f_in.write(ss);
    f_out.read(ss);
    CHECK_EQUAL(0,  ss.bufferSize());

    CHECK_EQUAL(42, f_out.nrBlocksInObservation);
    CHECK_EQUAL(0,  f_out.brokenRCUsAtBegin.size());
    CHECK_EQUAL(0,  f_out.brokenRCUsDuring.size());
  }

  TEST(RCUsAtBegin) {
    FinalMetaData f_in, f_out;
    FinalMetaData::BrokenRCU brokenRCU( "CS001",  "LBA", 3, "now" );

    f_in.nrBlocksInObservation = 42;
    f_in.brokenRCUsAtBegin.push_back(brokenRCU);

    StringStream ss;
    f_in.write(ss);
    f_out.read(ss);
    CHECK_EQUAL(0,  ss.bufferSize());

    CHECK_EQUAL(42, f_out.nrBlocksInObservation);
    CHECK_EQUAL(1,  f_out.brokenRCUsAtBegin.size());
    CHECK_EQUAL(0,  f_out.brokenRCUsDuring.size());
    CHECK_EQUAL(brokenRCU,  f_out.brokenRCUsAtBegin[0]);
  }

  TEST(RCUsDuring) {
    FinalMetaData f_in, f_out;
    FinalMetaData::BrokenRCU brokenRCU( "CS001",  "LBA", 3, "now" );

    f_in.nrBlocksInObservation = 42;
    f_in.brokenRCUsDuring.push_back(brokenRCU);

    StringStream ss;
    f_in.write(ss);
    f_out.read(ss);
    CHECK_EQUAL(0,  ss.bufferSize());

    CHECK_EQUAL(42, f_out.nrBlocksInObservation);
    CHECK_EQUAL(0,  f_out.brokenRCUsAtBegin.size());
    CHECK_EQUAL(1,  f_out.brokenRCUsDuring.size());
    CHECK_EQUAL(brokenRCU,  f_out.brokenRCUsDuring[0]);
  }
}

int main(void)
{
  INIT_LOGGER("tFinalMetaData");

  return UnitTest::RunAllTests() > 0;
}

