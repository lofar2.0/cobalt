
// Copyright (C) 2023 ASTRON (Netherlands Institute for Radio Astronomy)
// SPDX-License-Identifier: Apache-2.0

//# Always #include <lofar_config.h> first!
#include <lofar_config.h>

#include <iostream>
#include <string>

#include <Common/LofarLogger.h>
#include <Stream/FileStream.h>

#include <CoInterface/SdpStationPacket.h>


using namespace LOFAR;
using namespace LOFAR::Cobalt;
using namespace std;

void report( const string &filename )
{
  cout << "---- Checking " << filename << endl;
  FileStream f(filename);

  std::vector<::uint8_t> dataFrame(SdpStationPacket::PACKET_SIZE);
  SdpStationPacket packet(dataFrame.data());

  // read header
  f.read( &packet.header(), SdpStationPacket::PACKET_HEADER_SIZE);

  cout << "SDP version:  " << (int)packet.header().versionId << endl;
  cout << "gn idx:       " << packet.source() << endl;
  cout << "Payload OK:   " << (packet.payloadError() ? "NO" : "YES") << endl;
  cout << "Clock:        " << packet.clockMHz() << " MHz" << endl;
  cout << "Bit mode:     " << packet.bitMode() << " bit" << endl;
  cout << "Blocks:       " << (int)packet.blockCount() << endl;
  cout << "Beamlets:     " << (int)packet.beamletCount() << endl;

  ASSERTSTR(packet.packetSize() <= SdpStationPacket::PACKET_SIZE, "Packet too big: " << packet.packetSize() << " bytes, while we support up to " << SdpStationPacket::PACKET_SIZE << " bytes.");

  // read payload
  f.read( &packet.payload(), packet.packetSize() - packet.headerSize() );

  cout << "Sample 4 of beamlet 2:  X = " << packet.sample(2, 4, 'X') << endl;
  cout << "Sample 4 of beamlet 2:  Y = " << packet.sample(2, 4, 'Y') << endl;
}

int main()
{
  INIT_LOGGER("tSdpStationPacket");

  try {
    report( "tSdpStationPacket.in_16bit" );
    report( "tSdpStationPacket.in_8bit" );
  } catch (Exception &ex) {
    LOG_FATAL_STR("Caught exception: " << ex);
    return 1;
  }

  return 0;
}

