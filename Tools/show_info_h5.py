#!/usr/bin/env python
import numpy
import dal


def read_hdf5_file(filename):
  f=dal.BF_File(filename)

  # times
  startT=f.getNode('OBSERVATION_START_UTC').value
  endT=f.getNode('OBSERVATION_END_UTC').value
  print("Observation duration")
  print(startT)
  print(endT)
  nrSAP=f.getNode('NOF_SUB_ARRAY_POINTINGS').value
  # iterate over SAP
  for SAP in range(nrSAP):
    sap=f.subArrayPointing(SAP)
    pRA=sap.pointRA().value
    pDec=sap.pointDEC().value
    print(pRA)
    print(pDec)
    print("SAP duration")
    print(sap.expTimeStartUTC().value)
    print(sap.expTimeEndUTC().value)
    nrBeams=sap.observationNofBeams().value
    print("Total Beams=%d"%nrBeams)
    for ibeam in range(nrBeams):
      beam1=sap.beam(ibeam)
      if beam1.exists():
        print("Beam %d pointing:"%ibeam)
        print(beam1.pointRA().value)
        print(beam1.pointDEC().value)
        quantized=beam1.quantized().value[0]
        for STOKES in [0,1,2,3]:
          if not quantized:
           stokes=beam1.stokes(STOKES)
           if stokes.exists():
             print("STOKES=%d"%STOKES)
             print("Dims %d x %d"%(stokes.dims()[0],stokes.dims()[1]))
             # try reading the full dataset 
             xx=numpy.zeros(stokes.dims(),dtype=stokes.dtype())
             stokes.get2D([0,0],xx)
             print(xx)
          else:
           stokes=beam1.qstokes(STOKES)
           if stokes.exists():
             print("QSTOKES=%d"%STOKES)
             scale=stokes.scale()
             offset=stokes.offset()
             print("Dims %d x %d"%(offset.dims()[0],offset.dims()[1]))
             datatype=stokes.dataType().value
             # check if data are unsigned
             dataUnsigned=datatype=='unsigned char'
             if dataUnsigned:
              data2=stokes.dataUInt8()
             else:
              data2=stokes.dataInt8()
             xx=numpy.zeros(data2.dims(),dtype=data2.dtype())
             xscale=numpy.zeros(scale.dims(),dtype=scale.dtype())
             xoffset=numpy.zeros(offset.dims(),dtype=offset.dtype())
             data2.get2D([0,0],xx)
             scale.get2D([0,0],xscale)
             offset.get2D([0,0],xoffset)
             print(xscale)



if __name__ == '__main__':
  # args thisscript filename.h5
  import sys
  argc=len(sys.argv)
  if argc==2:
   read_hdf5_file(sys.argv[1])
  else:
   print("Usage %s filename.h5"%(sys.argv[0]))
   print("This will print some useful info about the 'filename.h5'")
   print("that is produced by the beamformer pipeline.")

  exit()
