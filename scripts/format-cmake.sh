#!/bin/bash

set -e

if [ ! $(which cmake-format) ]; then
	echo "Please install cmake-format"
	exit 1
fi

ROOT="$(git rev-parse --show-toplevel)"

find "$ROOT" -type d -name build -prune -false -o -type f \( -name 'CMakeLists.txt' -o -name '*.cmake' \) -exec cmake-format -i \{} \;
