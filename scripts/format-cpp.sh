#!/bin/bash

set -e

if [ ! $(which clang-format) ]; then
	echo "Please install clang-format"
	exit 1
fi

ROOT="$(git rev-parse --show-toplevel)"

find "$ROOT" -type d -name build -prune -false -o -type f -name '*.[ch]pp' -exec clang-format -i \{} \;
